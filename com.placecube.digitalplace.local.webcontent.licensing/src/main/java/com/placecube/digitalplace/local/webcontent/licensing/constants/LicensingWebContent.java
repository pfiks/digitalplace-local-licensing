package com.placecube.digitalplace.local.webcontent.licensing.constants;

public enum LicensingWebContent {

	CHECK_YOUR_DETAILS("CHECKYOURDETAILS", "Check Your Details"),
	CHECKS_DUE_CONTACT_THE_LICENSING_TEAM("CHECKSDUECONTACTTHELICENSINGTEAM", "Checks Due Contact Licensing Team"),
	CHECKS_DUE_FIND_OUT_MORE("CHECKSDUEFINDOUTMORE", "Checks Due Find Out More"),
	CONFIRM_AND_PAY_MULTIYEAR("CONFIRMANDPAYMULTIYEAR", "Confirm and Pay Multi Year"),
	CONFIRM_AND_PAYONEYEAR("CONFIRMANDPAYONEYEAR", "Confirm and Pay One Year"),
	CONFIRM_VEHICLE_DETAILS_INTERESTED_PARTIES("CONFIRMVEHICLEDETAILSINTERESTEDPARTIES", "Confirm Vehicle Details"),
	CONFIRM_YOUR_DETAILS_BOTTOM("CONFIRMYOURDETAILSBOTTOM", "Confirm Your Details Bottom"),
	CONFIRM_YOUR_DETAILS_TOP("CONFIRMYOURDETAILSTOP", "Confirm Your Details Top"),
	CONFIRMATION("CONFIRMATION", "Confirmation"),
	CONTACT_THE_LICENSING_TEAM("CONTACTTHELICENSINGTEAM", "Contact Licensing Team"),
	DECLARATION_WHAT_THE_LAW_SAYS("DECLARATIONWHATTHELAWSAYS", "Decleration What The Law Says"),
	LICENSING_FEEDBACK_SURVEY("LICENSINGFEEDBACKSURVEY", "Licensing Feedback Survey"),
	RENEWAL_LEAD_IN_BOTTOM("RENEWALLEADINBOTTOM", "Renewal Leadin Bottom"),
	RENEWAL_LEAD_IN_TOP("RENEWALLEADINTOP", "Renewal Leadin Top"),
	THE_LAW("THELAW", "The Law"),
	THE_LAW_TITLE("THELAWTITLE", "The Law Title"),
	UNIFORM_ERROR("UNIFORMERROR", "Uniform Error"),
	UPLOAD_DOCUMENTS_FAILED_PANEL("UPLOADDOCUMENTSFAILEDPANEL", "Upload Documents Failed Panel"),
	VEHICLE_CONFIRMATION("VEHICLECONFIRMATION", "Vehicle Information"),
	VEHICLE_DECLARATION_IAG_HACKNEY_CARRIAGE("VEHICLEDECLARATIONIAGHACKNEYCARRIAGE", "Vehicle Declaration Hackney Carriage"),
	VEHICLE_DECLARATION_IAG_PRIVATE_HIRE("VEHICLEDECLARATIONIAGPRIVATEHIRE", "Vehicle Declaration Private Hire"),
	VEHICLE_EMAIL_CONFIRMATION_BODY("VEHICLEEMAILCONFIRMATIONBODY", "Vehicle Email Confirmation Body"),
	VEHICLE_EMAIL_CONFIRMATION_HEAD("VEHICLEEMAILCONFIRMATIONHEAD", "Vehicle Email Confirmation Head"),
	VEHICLE_RENEWAL_LEAD_IN_BOTTOM("VEHICLERENEWALLEADINBOTTOM", "Vehicle Renewal Leadin Bottom"),
	VEHICLE_RENEWAL_LEAD_IN_TOP("VEHICLERENEWALLEADINTOP", "Vehicle Renewal Leadin Top"),
	VEHICLE_YOUR_DOCUMENTS_PROBLEMS_PANEL("VEHICLEYOURDOCUMENTSPROBLEMSPANEL", "Vehicle Your Documents Problems Panel"),
	VEHICLE_YOUR_DOCUMENTS_YOU_CAN("VEHICLEYOURDOCUMENTSYOUCAN", "Vehicle Your Documents You Can"),
	YOUR_DETAILS_TOP("YOURDETAILSTOP", "Your Details Top"),
	YOUR_LICENCE_HAS_EXPIRED("YOURLICENCEHASEXPIRED", "Your Licence Has Expired"),
	YOUR_RENEWAL_IS_TOO_LATE("YOURRENEWALISTOOLATE", "Your Renewal Is Too Late"),
	YOUR_RENEWAL_IS_TOO_SOON("YOURRENEWALISTOOSOON", "Your Renewal Is Too Soon"),
	YOUR_VEHICLE_RENEWAL_IS_TOO_SOON("YOURVEHICLERENEWALISTOOSOON", "Your Vehicle Renewal Is Too Soon");

	private final String articleId;
	private final String title;

	LicensingWebContent(String articleId, String title) {
		this.articleId = articleId;
		this.title = title;
	}

	public String getArticleId() {
		return articleId;
	}

	public String getTitle() {
		return title;
	}

}