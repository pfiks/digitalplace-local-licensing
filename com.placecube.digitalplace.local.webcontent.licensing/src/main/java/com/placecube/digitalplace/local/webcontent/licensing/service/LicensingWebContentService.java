package com.placecube.digitalplace.local.webcontent.licensing.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.webcontent.licensing.constants.LicensingEmailWebContent;
import com.placecube.digitalplace.local.webcontent.licensing.constants.LicensingWebContent;
import com.placecube.digitalplace.local.webcontent.licensing.service.helper.WebContentCreationService;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;

@Component(immediate = true, service = LicensingWebContentService.class)
public class LicensingWebContentService {

	@Reference
	private EmailWebContentService emailWebContentService;

	@Reference
	private WebContentCreationService webContentCreationService;

	public void initializeLicensingWebContent(ServiceContext serviceContext) throws PortalException {

		JournalFolder journalFolder = webContentCreationService.getFolder(serviceContext);

		for (LicensingWebContent licensingWebContent : LicensingWebContent.values()) {
			webContentCreationService.addBasicWebContent(licensingWebContent, serviceContext, journalFolder);
		}

		DDMStructure emailStructure = emailWebContentService.getOrCreateDDMStructure(serviceContext);
		for (LicensingEmailWebContent licensingWebContent : LicensingEmailWebContent.values()) {
			webContentCreationService.addEmailWebContent(licensingWebContent, serviceContext, journalFolder, emailStructure);
		}
	}

}