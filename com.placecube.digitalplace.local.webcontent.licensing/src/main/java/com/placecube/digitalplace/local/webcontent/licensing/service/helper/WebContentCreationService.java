package com.placecube.digitalplace.local.webcontent.licensing.service.helper;

import java.io.IOException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.webcontent.licensing.constants.LicensingEmailWebContent;
import com.placecube.digitalplace.local.webcontent.licensing.constants.LicensingWebContent;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;

@Component(immediate = true, service = WebContentCreationService.class)
public class WebContentCreationService {

	@Reference
	private JournalArticleCreationService journalArticleCreationService;

	public void addBasicWebContent(LicensingWebContent licensingWebContent, ServiceContext serviceContext, JournalFolder journalFolder) throws PortalException {
		try {
			String articleContent = StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/webcontent/licensing/" + licensingWebContent.getArticleId() + ".xml");
			journalArticleCreationService.getOrCreateBasicWebContentArticle(licensingWebContent.getArticleId(), licensingWebContent.getTitle(), articleContent, journalFolder, serviceContext);
		} catch (IOException e) {
			throw new PortalException(e);
		}
	}

	public void addEmailWebContent(LicensingEmailWebContent licensingWebContent, ServiceContext serviceContext, JournalFolder journalFolder, DDMStructure emailStructure) throws PortalException {
		try {
			String articleContent = StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/webcontent/licensing/email/" + licensingWebContent.getArticleId() + ".xml");

			JournalArticleContext context = JournalArticleContext.init(licensingWebContent.getArticleId(), licensingWebContent.getTitle(), articleContent);
			context.setJournalFolder(journalFolder);
			context.setDDMStructure(emailStructure);
			context.setIndexable(false);

			journalArticleCreationService.getOrCreateArticle(context, serviceContext);

		} catch (IOException e) {
			throw new PortalException(e);

		}
	}

	public JournalFolder getFolder(ServiceContext serviceContext) throws PortalException {
		return journalArticleCreationService.getOrCreateJournalFolder("Licensing", serviceContext);
	}
}
