package com.placecube.digitalplace.local.webcontent.licensing.constants;

public enum LicensingEmailWebContent {

    DRIVING_LICENCE_EMAIL("DRIVINGLICENCEEMAIL", "Driving Licence Confirmation Email Template");

    private final String articleId;

    private final String title;

    LicensingEmailWebContent(String articleId, String title) {
        this.articleId = articleId;
        this.title = title;
    }

    public String getArticleId() {
        return articleId;
    }

    public String getTitle() {
        return title;
    }

}