package com.placecube.digitalplace.local.licensing.vehicle.service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.placecube.digitalplace.local.licensing.vehicle.constants.DvlaRestKeys;

@Component(immediate = true, service = DvlaRestService.class)
public class DvlaRestService {

	@Reference
	private DvlaVehicleConfigurationService dvlaVehicleConfigurationService;

	public Invocation.Builder getDlvaRestBuilder(long companyId) throws Exception {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target(dvlaVehicleConfigurationService.getDvlaEndpointUrl(dvlaVehicleConfigurationService.getConfiguration(companyId)));

		MultivaluedMap<String, Object> params = new MultivaluedHashMap<>();
		params.add(DvlaRestKeys.API_KEY, dvlaVehicleConfigurationService.getDvlaApikey(dvlaVehicleConfigurationService.getConfiguration(companyId)));

		return webTarget.request(MediaType.APPLICATION_JSON).headers(params);

	}

}