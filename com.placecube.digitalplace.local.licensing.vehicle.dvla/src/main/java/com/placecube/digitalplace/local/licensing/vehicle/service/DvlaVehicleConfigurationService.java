package com.placecube.digitalplace.local.licensing.vehicle.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.vehicle.configuration.DvlaVehicleConfiguration;

@Component(immediate = true, service = DvlaVehicleConfigurationService.class)
public class DvlaVehicleConfigurationService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public DvlaVehicleConfiguration getConfiguration(Long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(DvlaVehicleConfiguration.class, companyId);
	}

	public String getDvlaApikey(DvlaVehicleConfiguration configuration) throws ConfigurationException {
		String dlvaApikey = configuration.dvlaApikey();

		if (Validator.isNull(dlvaApikey)) {
			throw new ConfigurationException("Dvla Api key configuration cannot be empty");
		}

		return dlvaApikey;
	}

	public String getDvlaEndpointUrl(DvlaVehicleConfiguration configuration) throws ConfigurationException {
		String dlvaEndpointUrl = configuration.dvlaEndpointUrl();

		if (Validator.isNull(dlvaEndpointUrl)) {
			throw new ConfigurationException("Dvla End point Url configuration cannot be empty");
		}

		return dlvaEndpointUrl;
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		DvlaVehicleConfiguration configuration = configurationProvider.getCompanyConfiguration(DvlaVehicleConfiguration.class, companyId);
		return configuration.enabled();
	}

}
