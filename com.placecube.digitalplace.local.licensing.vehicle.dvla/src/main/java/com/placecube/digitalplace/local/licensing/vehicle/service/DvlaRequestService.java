package com.placecube.digitalplace.local.licensing.vehicle.service;

import java.util.Optional;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.local.licensing.vehicle.api.model.VehicleDetails;
import com.placecube.digitalplace.local.licensing.vehicle.api.service.VehicleService;
import com.placecube.digitalplace.local.licensing.vehicle.constants.DvlaRestKeys;

@Component(immediate = true, service = DvlaRequestService.class)
public class DvlaRequestService implements VehicleService {

	@Reference
	private DvlaRestService dlvaRestService;

	@Reference
	private JSONFactory json;

	@Override
	public Optional<VehicleDetails> getVehicleDetails(long companyId, String registrationNumber) throws Exception {

		Invocation.Builder builder = getDvlaRestBuilder(companyId);

		JSONObject payload = JSONFactoryUtil.createJSONObject();
		payload.put(DvlaRestKeys.REGISTRATION_NUMBER, registrationNumber);

		Response response = builder.post(Entity.json(payload.toString()));

		if (response.getStatus() == 200) {
			return Optional.of(json.looseDeserialize(response.readEntity(String.class), VehicleDetails.class));
		} else {
			return Optional.empty();
		}

	}

	private Invocation.Builder getDvlaRestBuilder(long companyId) throws Exception {
		return dlvaRestService.getDlvaRestBuilder(companyId);
	}

}
