package com.placecube.digitalplace.local.licensing.vehicle.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.local.licensing.vehicle.api.model.VehicleDetails;
import com.placecube.digitalplace.local.licensing.vehicle.api.service.VehicleConnector;

@Component(immediate = true, service = VehicleConnector.class)
public class DvlaVehicleConnector implements VehicleConnector {

	private static final Log LOG = LogFactoryUtil.getLog(DvlaVehicleConnector.class);

	@Reference
	private DvlaRequestService dvlaRequestService;

	@Reference
	private DvlaVehicleConfigurationService dvlaVehicleConfigurationService;

	@Override
	public boolean enabled(long companyId) {
		try {
			return dvlaVehicleConfigurationService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public Optional<VehicleDetails> getVehicleDetails(long companyId, String registrationNumber) throws Exception {
		return dvlaRequestService.getVehicleDetails(companyId, registrationNumber);

	}

}
