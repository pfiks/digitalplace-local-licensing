package com.placecube.digitalplace.local.licensing.vehicle.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.licensing.vehicle.configuration.DvlaVehicleConfiguration", localization = "content/Language", name = "dvla-vehicle")
public interface DvlaVehicleConfiguration {

	@Meta.AD(required = false, deflt = "", name = "dlva-api-key")
	String dvlaApikey();

	@Meta.AD(required = false, deflt = "https://driver-vehicle-licensing.api.gov.uk/vehicle-enquiry/v1/vehicles", name = "dlva-endpoint-url")
	String dvlaEndpointUrl();

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

}
