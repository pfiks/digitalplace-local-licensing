package com.placecube.digitalplace.local.licensing.vehicle.constants;

public final class DvlaRestKeys {

	public static final String API_KEY = "x-api-key";

	public static final String REGISTRATION_NUMBER = "registrationNumber";

	private DvlaRestKeys() {
	}

}