package com.placecube.digitalplace.local.licensing.vehicle.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.local.licensing.vehicle.api.model.VehicleDetails;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class DvlaVehicleConnectorTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	private static final String REGISTRATION_NUMBER = "FV11 BWU";

	@InjectMocks
	private DvlaVehicleConnector dvlaVehicleConnector;

	@Mock
	private DvlaRequestService mockDvlaRequestService;

	@Mock
	private DvlaVehicleConfigurationService mockDvlaVehicleConfigurationService;

	@Mock
	private VehicleDetails mockVehicleDetails;

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockDvlaVehicleConfigurationService.isEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = dvlaVehicleConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockDvlaVehicleConfigurationService.isEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = dvlaVehicleConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getVehicleDetails_WhenNoErrors_ThenReturnResponse() throws Exception {

		when(mockDvlaRequestService.getVehicleDetails(COMPANY_ID, REGISTRATION_NUMBER)).thenReturn(Optional.of(mockVehicleDetails));

		Optional<VehicleDetails> vehicleDetails = dvlaVehicleConnector.getVehicleDetails(COMPANY_ID, REGISTRATION_NUMBER);

		assertEquals(mockVehicleDetails, vehicleDetails.get());
	}
}
