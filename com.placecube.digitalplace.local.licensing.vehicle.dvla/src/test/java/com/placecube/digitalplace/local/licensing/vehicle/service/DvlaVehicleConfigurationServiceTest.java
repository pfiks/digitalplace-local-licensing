package com.placecube.digitalplace.local.licensing.vehicle.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.licensing.vehicle.configuration.DvlaVehicleConfiguration;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class DvlaVehicleConfigurationServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final String DVAL_API_KEY = "ik65Cbyc4q6axL8KqGYWD5CbGkIQUujz5Lt2Bpuv";
	private static final String DVLA_ENDPOINT_URL = "https://driver-vehicle-licensing.api.gov.uk/vehicle-enquiry/v1/vehicles";

	@InjectMocks
	private DvlaVehicleConfigurationService dvlaVehicleConfigurationService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private DvlaVehicleConfiguration mockDvlaVehicleConfiguration;

	@Test(expected = ConfigurationException.class)
	public void getConfiguration_WhenErrorGettingConfiguration_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(DvlaVehicleConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		dvlaVehicleConfigurationService.getConfiguration(COMPANY_ID);
	}

	@Test
	public void getConfiguration_WhenNoError_ThenReturnsConfiguration() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(DvlaVehicleConfiguration.class, COMPANY_ID)).thenReturn(mockDvlaVehicleConfiguration);

		dvlaVehicleConfigurationService.getConfiguration(COMPANY_ID);
	}

	@Test(expected = ConfigurationException.class)
	public void getDlvaApikey_WhenDlvaApikeyIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(mockDvlaVehicleConfiguration.dvlaApikey()).thenReturn(StringPool.BLANK);

		dvlaVehicleConfigurationService.getDvlaApikey(mockDvlaVehicleConfiguration);
	}

	@Test
	public void getDlvaApikey_WhenDlvaApikeyIsNotBlank_ThenReturnsDlvaApikey() throws Exception {
		when(mockDvlaVehicleConfiguration.dvlaApikey()).thenReturn(DVAL_API_KEY);

		String result = dvlaVehicleConfigurationService.getDvlaApikey(mockDvlaVehicleConfiguration);

		assertEquals(DVAL_API_KEY, result);
	}

	@Test(expected = ConfigurationException.class)
	public void getDvlaEndpointUrl_WhenDvlaEndpointUrlIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(mockDvlaVehicleConfiguration.dvlaEndpointUrl()).thenReturn(StringPool.BLANK);

		dvlaVehicleConfigurationService.getDvlaEndpointUrl(mockDvlaVehicleConfiguration);
	}

	@Test
	public void getDvlaEndpointUrl_WhenDvlaEndpointUrlIsNotBlank_ThenReturnsDvlaEndpointUrl() throws Exception {
		when(mockDvlaVehicleConfiguration.dvlaEndpointUrl()).thenReturn(DVLA_ENDPOINT_URL);

		String result = dvlaVehicleConfigurationService.getDvlaEndpointUrl(mockDvlaVehicleConfiguration);

		assertEquals(DVLA_ENDPOINT_URL, result);
	}

}
