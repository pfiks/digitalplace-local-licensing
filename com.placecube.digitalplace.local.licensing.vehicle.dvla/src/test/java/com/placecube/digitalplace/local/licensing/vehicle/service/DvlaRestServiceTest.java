package com.placecube.digitalplace.local.licensing.vehicle.service;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.ws.rs.client.Invocation;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.local.licensing.vehicle.configuration.DvlaVehicleConfiguration;

public class DvlaRestServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final String DVAL_API_KEY = "ik65Cbyc4q6axL8KqGYWD5CbGkIQUujz5Lt2Bpuv";
	private static final String DVLA_ENDPOINT_URL = "https://driver-vehicle-licensing.api.gov.uk/vehicle-enquiry/v1/vehicles";

	@InjectMocks
	private DvlaRestService dvlaRestService;

	@Mock
	private DvlaVehicleConfiguration mockDvlaVehicleConfiguration;

	@Mock
	private DvlaVehicleConfigurationService mockDvlaVehicleConfigurationService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getDlvaRestBuilder_WhenNoErrors_ThenReturnsInvocationBuilder() throws Exception {

		when(mockDvlaVehicleConfigurationService.getConfiguration(COMPANY_ID)).thenReturn(mockDvlaVehicleConfiguration);

		when(mockDvlaVehicleConfigurationService.getDvlaEndpointUrl(mockDvlaVehicleConfiguration)).thenReturn(DVLA_ENDPOINT_URL);
		when(mockDvlaVehicleConfigurationService.getDvlaApikey(mockDvlaVehicleConfiguration)).thenReturn(DVAL_API_KEY);

		Invocation.Builder builder = dvlaRestService.getDlvaRestBuilder(COMPANY_ID);

		assertThat(builder, instanceOf(Invocation.Builder.class));
	}

}