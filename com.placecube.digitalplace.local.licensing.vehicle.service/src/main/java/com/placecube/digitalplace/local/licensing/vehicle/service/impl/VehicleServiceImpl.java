package com.placecube.digitalplace.local.licensing.vehicle.service.impl;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.placecube.digitalplace.local.licensing.vehicle.api.exception.VehicleRetrievalException;
import com.placecube.digitalplace.local.licensing.vehicle.api.model.VehicleDetails;
import com.placecube.digitalplace.local.licensing.vehicle.api.service.VehicleConnector;
import com.placecube.digitalplace.local.licensing.vehicle.api.service.VehicleService;

@Component(immediate = true, service = VehicleService.class)
public class VehicleServiceImpl implements VehicleService {

	private Set<VehicleConnector> vehicleConnectors = new LinkedHashSet<>();

	public VehicleConnector getVehicleConnector(long companyId) throws VehicleRetrievalException {
		Optional<VehicleConnector> vehicleConnector = vehicleConnectors.stream().filter(entry -> entry.enabled(companyId)).findFirst();
		if (vehicleConnector.isPresent()) {
			return vehicleConnector.get();
		} else {
			throw new VehicleRetrievalException("No vehicle connector configured.");
		}
	}

	@Override
	public Optional<VehicleDetails> getVehicleDetails(long companyId, String registrationNumber) throws Exception {
		return getVehicleConnector(companyId).getVehicleDetails(companyId, registrationNumber);
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setVehicleConnector(VehicleConnector vehicleConnector) {
		vehicleConnectors.add(vehicleConnector);
	}

	protected void unsetVehicleConnector(VehicleConnector vehicleConnector) {
		vehicleConnectors.remove(vehicleConnector);
	}

}
