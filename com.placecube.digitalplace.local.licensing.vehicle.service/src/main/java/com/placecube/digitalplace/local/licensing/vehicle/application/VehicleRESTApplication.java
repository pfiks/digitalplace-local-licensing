package com.placecube.digitalplace.local.licensing.vehicle.application;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.liferay.portal.kernel.json.JSONFactory;
import com.placecube.digitalplace.local.licensing.vehicle.api.model.VehicleDetails;
import com.placecube.digitalplace.local.licensing.vehicle.api.service.VehicleService;

@Component(immediate = true, property = { JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/licensing/vehicle", JaxrsWhiteboardConstants.JAX_RS_NAME + "=Vehicle.Rest",
		"oauth2.scopechecker.type=none", "auth.verifier.guest.allowed=false", "liferay.access.control.disable=true" }, service = Application.class)
public class VehicleRESTApplication extends Application {

	@Reference
	private VehicleService vehicleService;

	@Reference
	private JSONFactory json;

	@GET
	@Path("/get-vehicle-details/{registrationNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVehicleDetails(@PathParam("registrationNumber") String registrationNumber, @Context HttpServletRequest request) throws Exception {

		Optional<VehicleDetails> vehicleDetails = vehicleService.getVehicleDetails(getCompanyId(request), registrationNumber);
		if (vehicleDetails.isPresent()) {
			return Response.ok(json.serialize(vehicleDetails.get()), MediaType.APPLICATION_JSON).build();
		} else {
			String jsonResponse = json.createJSONObject("{'message': 'Vehicle not found', 'success': false}").toJSONString();
			return Response.status(Response.Status.OK).entity(jsonResponse).build();
		}

	}

	@Override
	public Set<Object> getSingletons() {
		return Collections.<Object>singleton(this);
	}

	private long getCompanyId(HttpServletRequest request) {
		return (Long) request.getAttribute("COMPANY_ID");
	}

}