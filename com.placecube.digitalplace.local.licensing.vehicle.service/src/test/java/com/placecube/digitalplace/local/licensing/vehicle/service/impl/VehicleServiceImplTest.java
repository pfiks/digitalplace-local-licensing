package com.placecube.digitalplace.local.licensing.vehicle.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.local.licensing.vehicle.api.exception.VehicleRetrievalException;
import com.placecube.digitalplace.local.licensing.vehicle.api.model.VehicleDetails;
import com.placecube.digitalplace.local.licensing.vehicle.api.service.VehicleConnector;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class VehicleServiceImplTest extends PowerMockito {

	private static final Long COMPANY_ID = 11l;

	private static final String REGISTRATION_NUMBER = "FV11 BWU";

	@Mock
	private VehicleConnector mockVehicleConnector1;

	@Mock
	private VehicleConnector mockVehicleConnector2;

	@Mock
	private VehicleDetails mockVehicleDetails;

	@InjectMocks
	private VehicleServiceImpl vehicleServiceImpl;

	@Test
	public void getVehicleConnector_WhenTwoConnectorsAvailable_ThenReturnEnabledOne() throws Exception {
		addVehicleConnector(COMPANY_ID, mockVehicleConnector1, false);
		addVehicleConnector(COMPANY_ID, mockVehicleConnector2, true);

		VehicleConnector result = vehicleServiceImpl.getVehicleConnector(COMPANY_ID);

		assertEquals(mockVehicleConnector2, result);
	}

	@Test
	public void getVehicleConnector_WhenTwoEnabledConnectors_ThenReturnFirstOne() throws Exception {
		addVehicleConnector(COMPANY_ID, mockVehicleConnector1, true);
		addVehicleConnector(COMPANY_ID, mockVehicleConnector2, true);

		VehicleConnector result = vehicleServiceImpl.getVehicleConnector(COMPANY_ID);

		assertEquals(mockVehicleConnector1, result);
	}

	@Test
	public void getVehicleDetails_WhenRegistrationNumberIsNotEmptyAndEnabledVehicleConnectorFound_ThenReturnsResponse() throws Exception {
		addVehicleConnector(COMPANY_ID, mockVehicleConnector1, false);
		addVehicleConnector(COMPANY_ID, mockVehicleConnector2, true);
		when(mockVehicleConnector2.getVehicleDetails(COMPANY_ID, REGISTRATION_NUMBER)).thenReturn(Optional.of(mockVehicleDetails));

		Optional<VehicleDetails> result = vehicleServiceImpl.getVehicleDetails(COMPANY_ID, REGISTRATION_NUMBER);

		assertThat(result.get(), sameInstance(result.get()));
	}

	@Test(expected = Exception.class)
	public void getVehicleDetails_WhenRegistrationNumberIsNotEmptyAndNoEnabledVehicleConnectorFound_ThenThrowsException() throws Exception {
		addVehicleConnector(COMPANY_ID, mockVehicleConnector1, false);

		vehicleServiceImpl.getVehicleDetails(COMPANY_ID, REGISTRATION_NUMBER);
	}

	@Test(expected = Exception.class)
	public void getVehicleDetails_WhenVehicleConnectorIsUnset_ThenThrowsException() throws Exception {
		addVehicleConnector(COMPANY_ID, mockVehicleConnector1, true);

		unsetVehicleConnector(COMPANY_ID, mockVehicleConnector1);

		vehicleServiceImpl.getVehicleDetails(COMPANY_ID, REGISTRATION_NUMBER);
	}

	@Test(expected = VehicleRetrievalException.class)
	public void getWasteConnectorConnector_WhenNoEnabledConnectorsForCompany_ThenThrowsException() throws Exception {
		addVehicleConnector(COMPANY_ID, mockVehicleConnector1, false);
		addVehicleConnector(1l, mockVehicleConnector2, true);

		vehicleServiceImpl.getVehicleConnector(COMPANY_ID);
	}

	@Before
	public void setUp() {
		initMocks(this);
		vehicleServiceImpl = new VehicleServiceImpl();
	}

	private void addVehicleConnector(long companyId, VehicleConnector vehicleConnector, boolean enabled) {
		when(vehicleConnector.enabled(companyId)).thenReturn(enabled);
		vehicleServiceImpl.setVehicleConnector(vehicleConnector);
	}

	private void unsetVehicleConnector(long companyId, VehicleConnector vehicleConnector) {

		vehicleServiceImpl.unsetVehicleConnector(vehicleConnector);
	}
}
