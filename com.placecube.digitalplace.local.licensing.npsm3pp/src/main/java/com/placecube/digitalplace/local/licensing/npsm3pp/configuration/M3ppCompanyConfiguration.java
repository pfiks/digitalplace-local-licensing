package com.placecube.digitalplace.local.licensing.npsm3pp.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.licensing.npsm3pp.configuration.M3ppCompanyConfiguration", localization = "content/Language", name = "licensing-m3pp")
public interface M3ppCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "http://assure-web/Northgate/SIGWebServices/M3PPServ/M3PPServ.asmx", name = "m3pp-endpoint-url")
	String m3ppEndpointUrl();

	@Meta.AD(required = false, deflt = "", name = "password", type = Meta.Type.Password)
	String password();

	@Meta.AD(required = false, deflt = "", name = "username")
	String username();

}
