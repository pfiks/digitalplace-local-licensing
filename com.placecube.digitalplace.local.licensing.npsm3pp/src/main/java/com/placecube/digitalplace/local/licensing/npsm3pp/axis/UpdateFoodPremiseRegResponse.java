/**
 * UpdateFoodPremiseRegResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class UpdateFoodPremiseRegResponse  implements java.io.Serializable {
    private UpdateFoodPremiseRegResponseUpdateFoodPremiseRegResult updateFoodPremiseRegResult;

    public UpdateFoodPremiseRegResponse() {
    }

    public UpdateFoodPremiseRegResponse(
           UpdateFoodPremiseRegResponseUpdateFoodPremiseRegResult updateFoodPremiseRegResult) {
           this.updateFoodPremiseRegResult = updateFoodPremiseRegResult;
    }


    /**
     * Gets the updateFoodPremiseRegResult value for this UpdateFoodPremiseRegResponse.
     * 
     * @return updateFoodPremiseRegResult
     */
    public UpdateFoodPremiseRegResponseUpdateFoodPremiseRegResult getUpdateFoodPremiseRegResult() {
        return updateFoodPremiseRegResult;
    }


    /**
     * Sets the updateFoodPremiseRegResult value for this UpdateFoodPremiseRegResponse.
     * 
     * @param updateFoodPremiseRegResult
     */
    public void setUpdateFoodPremiseRegResult(UpdateFoodPremiseRegResponseUpdateFoodPremiseRegResult updateFoodPremiseRegResult) {
        this.updateFoodPremiseRegResult = updateFoodPremiseRegResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateFoodPremiseRegResponse)) return false;
        UpdateFoodPremiseRegResponse other = (UpdateFoodPremiseRegResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.updateFoodPremiseRegResult==null && other.getUpdateFoodPremiseRegResult()==null) || 
             (this.updateFoodPremiseRegResult!=null &&
              this.updateFoodPremiseRegResult.equals(other.getUpdateFoodPremiseRegResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUpdateFoodPremiseRegResult() != null) {
            _hashCode += getUpdateFoodPremiseRegResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateFoodPremiseRegResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateFoodPremiseRegResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateFoodPremiseRegResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateFoodPremiseRegResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateFoodPremiseRegResponse>UpdateFoodPremiseRegResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
