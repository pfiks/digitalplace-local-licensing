/**
 * UpdateRelatedAddr.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class UpdateRelatedAddr  implements java.io.Serializable {
    private UpdateRelatedAddrXmlRelAddr xmlRelAddr;

    public UpdateRelatedAddr() {
    }

    public UpdateRelatedAddr(
           UpdateRelatedAddrXmlRelAddr xmlRelAddr) {
           this.xmlRelAddr = xmlRelAddr;
    }


    /**
     * Gets the xmlRelAddr value for this UpdateRelatedAddr.
     * 
     * @return xmlRelAddr
     */
    public UpdateRelatedAddrXmlRelAddr getXmlRelAddr() {
        return xmlRelAddr;
    }


    /**
     * Sets the xmlRelAddr value for this UpdateRelatedAddr.
     * 
     * @param xmlRelAddr
     */
    public void setXmlRelAddr(UpdateRelatedAddrXmlRelAddr xmlRelAddr) {
        this.xmlRelAddr = xmlRelAddr;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateRelatedAddr)) return false;
        UpdateRelatedAddr other = (UpdateRelatedAddr) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.xmlRelAddr==null && other.getXmlRelAddr()==null) || 
             (this.xmlRelAddr!=null &&
              this.xmlRelAddr.equals(other.getXmlRelAddr())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getXmlRelAddr() != null) {
            _hashCode += getXmlRelAddr().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateRelatedAddr.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateRelatedAddr"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xmlRelAddr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlRelAddr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateRelatedAddr>xmlRelAddr"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
