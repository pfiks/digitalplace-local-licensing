package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class M3PPServiceSoapProxy implements M3PPServiceSoap {
  private String _endpoint = null;
  private M3PPServiceSoap m3PPServiceSoap = null;
  
  public M3PPServiceSoapProxy() {
    _initM3PPServiceSoapProxy();
  }
  
  public M3PPServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initM3PPServiceSoapProxy();
  }
  
  private void _initM3PPServiceSoapProxy() {
    try {
      m3PPServiceSoap = (new M3PPServiceLocator()).getM3PPServiceSoap();
      if (m3PPServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)m3PPServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)m3PPServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (m3PPServiceSoap != null)
      ((javax.xml.rpc.Stub)m3PPServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public M3PPServiceSoap getM3PPServiceSoap() {
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap;
  }
  
  public java.lang.String getVersion(java.lang.String verType) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getVersion(verType);
  }
  
  public UpdateWorksheetResponseUpdateWorksheetResult updateWorksheet(UpdateWorksheetXmlWork xmlWork) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateWorksheet(xmlWork);
  }
  
  public GetWorksheetDetsResponseGetWorksheetDetsResult getWorksheetDets(java.lang.String strWorkId, java.lang.String strWorkSearch, java.lang.String strSearchFld) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getWorksheetDets(strWorkId, strWorkSearch, strSearchFld);
  }
  
  public UpdateActionResponseUpdateActionResult updateAction(UpdateActionXmlAction xmlAction) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateAction(xmlAction);
  }
  
  public GetActionsResponseGetActionsResult getActions(java.lang.String strWorkId) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getActions(strWorkId);
  }
  
  public GetSitesResponseGetSitesResult getSites(java.lang.String strUPRN) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getSites(strUPRN);
  }
  
  public GetWorksheetsResponseGetWorksheetsResult getWorksheets(java.lang.String strUPRN, java.lang.String strRepUPRN, java.lang.String strDateFrom, java.lang.String strDateTo, java.lang.String strApplication, java.lang.String strTask, java.lang.String strOfficer, java.lang.String strOS, java.lang.String strSortColumn, java.lang.String strSortOrder) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getWorksheets(strUPRN, strRepUPRN, strDateFrom, strDateTo, strApplication, strTask, strOfficer, strOS, strSortColumn, strSortOrder);
  }
  
  public GetCodeListResponseGetCodeListResult getCodeList(java.lang.String strCodeType, java.lang.String strCodeFrom, java.lang.String strCodeTo, java.lang.String strOrder, java.lang.String strParameters) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getCodeList(strCodeType, strCodeFrom, strCodeTo, strOrder, strParameters);
  }
  
  public GetSubComponentsResponseGetSubComponentsResult getSubComponents(java.lang.String strSiteKey) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getSubComponents(strSiteKey);
  }
  
  public GetPropertiesResponseGetPropertiesResult getProperties(java.lang.String strSearch, java.lang.String strBuilding_Number) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getProperties(strSearch, strBuilding_Number);
  }
  
  public UpdateRelatedAddrResponseUpdateRelatedAddrResult updateRelatedAddr(UpdateRelatedAddrXmlRelAddr xmlRelAddr) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateRelatedAddr(xmlRelAddr);
  }
  
  public UpdateFoodPremiseRegResponseUpdateFoodPremiseRegResult updateFoodPremiseReg(UpdateFoodPremiseRegXmlA xmlA) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateFoodPremiseReg(xmlA);
  }
  
  public UpdateEPAResponseUpdateEPAResult updateEPA(UpdateEPAXmlA xmlA) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateEPA(xmlA);
  }
  
  public UpdateLicenceResponseUpdateLicenceResult updateLicence(UpdateLicenceXmlA xmlA) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateLicence(xmlA);
  }
  
  public UpdateBulkyResponseUpdateBulkyResult updateBulky(UpdateBulkyXmlA xmlA) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateBulky(xmlA);
  }
  
  public GetBulkyDetailsResponseGetBulkyDetailsResult getBulkyDetails(java.lang.String strWorkId) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getBulkyDetails(strWorkId);
  }
  
  public UpdateAVResponseUpdateAVResult updateAV(UpdateAVXmlA xmlA) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateAV(xmlA);
  }
  
  public GetAVDetailsResponseGetAVDetailsResult getAVDetails(java.lang.String strWorkId) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getAVDetails(strWorkId);
  }
  
  public UpdateAdWorksOrderResponseUpdateAdWorksOrderResult updateAdWorksOrder(UpdateAdWorksOrderXmlA xmlA) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateAdWorksOrder(xmlA);
  }
  
  public GetAdWorksOrderDetsResponseGetAdWorksOrderDetsResult getAdWorksOrderDets(java.lang.String strWorkId) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getAdWorksOrderDets(strWorkId);
  }
  
  public GetAvailableSlotsResponseGetAvailableSlotsResult getAvailableSlots(java.lang.String strOfficer, java.lang.String strWorkId, java.lang.String strDaysAhead) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getAvailableSlots(strOfficer, strWorkId, strDaysAhead);
  }
  
  public BookAppointmentResponseBookAppointmentResult bookAppointment(BookAppointmentXmlA xmlA) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.bookAppointment(xmlA);
  }
  
  public GetBulkyItemSlotsResponseGetBulkyItemSlotsResult getBulkyItemSlots(java.lang.String strWorkId) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getBulkyItemSlots(strWorkId);
  }
  
  public BookBulkyItemAppointmentResponseBookBulkyItemAppointmentResult bookBulkyItemAppointment(BookBulkyItemAppointmentXmlA xmlA) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.bookBulkyItemAppointment(xmlA);
  }
  
  public GetWorksheetAppointmentsResponseGetWorksheetAppointmentsResult getWorksheetAppointments(java.lang.String strWorkId) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getWorksheetAppointments(strWorkId);
  }
  
  public GetRoundDetailsResponseGetRoundDetailsResult getRoundDetails(java.lang.String strUKEY, java.lang.String strSITEKEY) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getRoundDetails(strUKEY, strSITEKEY);
  }
  
  public OOAPropertyResponseOOAPropertyResult OOAProperty(OOAPropertyXmlA xmlA) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.OOAProperty(xmlA);
  }
  
  public GetLinkedWorksheetsResponseGetLinkedWorksheetsResult getLinkedWorksheets(java.lang.String strWorkId) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getLinkedWorksheets(strWorkId);
  }
  
  public UpdateMonitorNoticesResponseUpdateMonitorNoticesResult updateMonitorNotices(UpdateMonitorNoticesXmlMN xmlMN) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateMonitorNotices(xmlMN);
  }
  
  public UpdateSourcesResponseUpdateSourcesResult updateSources(UpdateSourcesXmlS xmlS) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateSources(xmlS);
  }
  
  public GetAssetsResponseGetAssetsResult getAssets(java.lang.String strSiteKey, java.lang.String strUPRN, java.lang.String strAssetKey) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getAssets(strSiteKey, strUPRN, strAssetKey);
  }
  
  public UpdateAssetResponseUpdateAssetResult updateAsset(UpdateAssetXmlAsset xmlAsset) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateAsset(xmlAsset);
  }
  
  public DeleteAssetResponseDeleteAssetResult deleteAsset(java.lang.String strAssetKey) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.deleteAsset(strAssetKey);
  }
  
  public GetAssetInspectionsResponseGetAssetInspectionsResult getAssetInspections(java.lang.String strAssetKey) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.getAssetInspections(strAssetKey);
  }
  
  public UpdateAssetInspectionResponseUpdateAssetInspectionResult updateAssetInspection(UpdateAssetInspectionXmlAssetInsp xmlAssetInsp) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateAssetInspection(xmlAssetInsp);
  }
  
  public UpdatePropertyContactResponseUpdatePropertyContactResult updatePropertyContact(UpdatePropertyContactXmlPC xmlPC) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updatePropertyContact(xmlPC);
  }
  
  public UpdateHMODetailsResponseUpdateHMODetailsResult updateHMODetails(UpdateHMODetailsXmlHMO xmlHMO) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.updateHMODetails(xmlHMO);
  }
  
  public IssueLicenceResponseIssueLicenceResult issueLicence(IssueLicenceXmlLicence xmlLicence) throws java.rmi.RemoteException{
    if (m3PPServiceSoap == null)
      _initM3PPServiceSoapProxy();
    return m3PPServiceSoap.issueLicence(xmlLicence);
  }
  
  
}