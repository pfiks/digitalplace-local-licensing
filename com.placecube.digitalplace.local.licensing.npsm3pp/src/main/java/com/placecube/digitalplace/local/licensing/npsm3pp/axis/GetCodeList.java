/**
 * GetCodeList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class GetCodeList  implements java.io.Serializable {
    private java.lang.String strCodeType;

    private java.lang.String strCodeFrom;

    private java.lang.String strCodeTo;

    private java.lang.String strOrder;

    private java.lang.String strParameters;

    public GetCodeList() {
    }

    public GetCodeList(
           java.lang.String strCodeType,
           java.lang.String strCodeFrom,
           java.lang.String strCodeTo,
           java.lang.String strOrder,
           java.lang.String strParameters) {
           this.strCodeType = strCodeType;
           this.strCodeFrom = strCodeFrom;
           this.strCodeTo = strCodeTo;
           this.strOrder = strOrder;
           this.strParameters = strParameters;
    }


    /**
     * Gets the strCodeType value for this GetCodeList.
     * 
     * @return strCodeType
     */
    public java.lang.String getStrCodeType() {
        return strCodeType;
    }


    /**
     * Sets the strCodeType value for this GetCodeList.
     * 
     * @param strCodeType
     */
    public void setStrCodeType(java.lang.String strCodeType) {
        this.strCodeType = strCodeType;
    }


    /**
     * Gets the strCodeFrom value for this GetCodeList.
     * 
     * @return strCodeFrom
     */
    public java.lang.String getStrCodeFrom() {
        return strCodeFrom;
    }


    /**
     * Sets the strCodeFrom value for this GetCodeList.
     * 
     * @param strCodeFrom
     */
    public void setStrCodeFrom(java.lang.String strCodeFrom) {
        this.strCodeFrom = strCodeFrom;
    }


    /**
     * Gets the strCodeTo value for this GetCodeList.
     * 
     * @return strCodeTo
     */
    public java.lang.String getStrCodeTo() {
        return strCodeTo;
    }


    /**
     * Sets the strCodeTo value for this GetCodeList.
     * 
     * @param strCodeTo
     */
    public void setStrCodeTo(java.lang.String strCodeTo) {
        this.strCodeTo = strCodeTo;
    }


    /**
     * Gets the strOrder value for this GetCodeList.
     * 
     * @return strOrder
     */
    public java.lang.String getStrOrder() {
        return strOrder;
    }


    /**
     * Sets the strOrder value for this GetCodeList.
     * 
     * @param strOrder
     */
    public void setStrOrder(java.lang.String strOrder) {
        this.strOrder = strOrder;
    }


    /**
     * Gets the strParameters value for this GetCodeList.
     * 
     * @return strParameters
     */
    public java.lang.String getStrParameters() {
        return strParameters;
    }


    /**
     * Sets the strParameters value for this GetCodeList.
     * 
     * @param strParameters
     */
    public void setStrParameters(java.lang.String strParameters) {
        this.strParameters = strParameters;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetCodeList)) return false;
        GetCodeList other = (GetCodeList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.strCodeType==null && other.getStrCodeType()==null) || 
             (this.strCodeType!=null &&
              this.strCodeType.equals(other.getStrCodeType()))) &&
            ((this.strCodeFrom==null && other.getStrCodeFrom()==null) || 
             (this.strCodeFrom!=null &&
              this.strCodeFrom.equals(other.getStrCodeFrom()))) &&
            ((this.strCodeTo==null && other.getStrCodeTo()==null) || 
             (this.strCodeTo!=null &&
              this.strCodeTo.equals(other.getStrCodeTo()))) &&
            ((this.strOrder==null && other.getStrOrder()==null) || 
             (this.strOrder!=null &&
              this.strOrder.equals(other.getStrOrder()))) &&
            ((this.strParameters==null && other.getStrParameters()==null) || 
             (this.strParameters!=null &&
              this.strParameters.equals(other.getStrParameters())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStrCodeType() != null) {
            _hashCode += getStrCodeType().hashCode();
        }
        if (getStrCodeFrom() != null) {
            _hashCode += getStrCodeFrom().hashCode();
        }
        if (getStrCodeTo() != null) {
            _hashCode += getStrCodeTo().hashCode();
        }
        if (getStrOrder() != null) {
            _hashCode += getStrOrder().hashCode();
        }
        if (getStrParameters() != null) {
            _hashCode += getStrParameters().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetCodeList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetCodeList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strCodeType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strCodeType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strCodeFrom");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strCodeFrom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strCodeTo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strCodeTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strOrder");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strOrder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strParameters");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strParameters"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
