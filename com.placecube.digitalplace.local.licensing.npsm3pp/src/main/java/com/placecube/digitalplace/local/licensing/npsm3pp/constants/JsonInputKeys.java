package com.placecube.digitalplace.local.licensing.npsm3pp.constants;

public final class JsonInputKeys {

	public static final String DRIVER_TYPE = "driverType";

	public static final String EMAIL = "email";

	public static final String EXPIRY_PERIOD = "expiryPeriod";

	public static final String EXPIRY_PERIOD_MONTH_OR_DATE = "expiryPeriodMonthOrDate";

	public static final String FAMILY_NAME = "familyName";

	public static final String FEE_AMOUNT = "feeAmount";

	public static final String FEE_PAID = "feePaid";

	public static final String FEE_PAID_DATE = "feePaidDate";

	public static final String FEE_RECITE_NUMBER = "feeReciteNumber";

	public static final String FIRST_NAME = "firstName";

	public static final String HAS_FIRE_EXTINGUISHER = "hasFireExtinguisher";

	public static final String HAS_WHEEL_CHAIR = "hasWheelChair";

	public static final String MESSAGE = "message";

	public static final String MOBILE = "mobile";

	public static final String REMARKS = "remarks";

	public static final String REP_U_KEY = "repukey";

	public static final String REP_UPRN = "repuprn";

	public static final String TELEPHONE = "telephone";

	public static final String UPRN = "uprn";

	public static final String VEHICLE_NOTES = "vehicleNotes";

	public static final String VEHICLE_TYPE = "vehicleType";

	private JsonInputKeys() {
	}

}