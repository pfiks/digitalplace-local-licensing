package com.placecube.digitalplace.local.licensing.npsm3pp.service;

import java.math.BigDecimal;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.model.LicenceApplicationContext;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.GetPropertiesResponseGetPropertiesResult;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.OOAPropertyResponseOOAPropertyResult;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.UpdateLicenceResponseUpdateLicenceResult;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.UpdateWorksheetResponseUpdateWorksheetResult;
import com.placecube.digitalplace.local.licensing.npsm3pp.constants.M3ppSoapKeys;
import com.placecube.digitalplace.local.licensing.npsm3pp.util.MessageElementUtil;
import com.placecube.digitalplace.local.licensing.service.LicensingConnector;

@Component(immediate = true, service = LicensingConnector.class)
public class M3ppConnector implements LicensingConnector {

	private static final Log LOG = LogFactoryUtil.getLog(M3ppConnector.class);

	@Reference
	private M3ppConfigurationService m3ppConfigurationService;

	@Reference
	private M3ppRequestService m3ppRequestService;

	@Override
	public String applyForNewLicence(long companyId, LicenceApplicationContext licenceApplicationContext) throws LicenceRenewalException {

		try {

			GetPropertiesResponseGetPropertiesResult getPropertiesResult = m3ppRequestService.getProperties(companyId, licenceApplicationContext);

			if (!MessageElementUtil.checkForSuccessfulResponse(getPropertiesResult.get_any())) {
				LOG.debug(String.format("Property does not exist for uprn:%s.  Treating as out of area.", licenceApplicationContext.getLicenceAddressContext().getUprn()));
				OOAPropertyResponseOOAPropertyResult responseOOAPropertyResult = m3ppRequestService.getOOAProperty(companyId, licenceApplicationContext);
				if (!MessageElementUtil.checkForSuccessfulResponse(responseOOAPropertyResult.get_any())) {
					LOG.debug("Exception while creating OOAProperties from given address.");
				} else {
					String ukey = MessageElementUtil.getUkeyFromOOAPropertyResponse(responseOOAPropertyResult.get_any());
					LOG.debug("Ukey Retrieved from of out of Borough propert - OOAPropertyResult:" + ukey);
					licenceApplicationContext.getParameters().put(M3ppSoapKeys.REP_UPRN, ukey);
					licenceApplicationContext.getParameters().put(M3ppSoapKeys.REP_UKEY, ukey);
				}
			}

			UpdateWorksheetResponseUpdateWorksheetResult worksheetResult = m3ppRequestService.updateWorksheet(companyId, licenceApplicationContext);

			String newWorkId = MessageElementUtil.getWorkIdFromResponse(worksheetResult.get_any(), M3ppSoapKeys.M3PP_WORKSHEET);

			if (Validator.isNotNull(newWorkId)) {

				UpdateLicenceResponseUpdateLicenceResult updateLicenceResult = m3ppRequestService.updateLicence(companyId, licenceApplicationContext, newWorkId);

				String workIdFromLicence = MessageElementUtil.getWorkIdFromResponse(updateLicenceResult.get_any(), M3ppSoapKeys.M3PP_LICENCE);

				if (!newWorkId.equalsIgnoreCase(workIdFromLicence)) {
					LOG.error("Exception while applying for new licence.");
					throw new LicenceRenewalException(
							String.format("Exception while applying for new licence. Work ids don't match updateWorksheet:%s updateLicence:%s", newWorkId, workIdFromLicence));
				}

				return workIdFromLicence;

			} else {
				LOG.error("Exception while applying for new licence as failed to retrieve new worksheetId.");
				throw new LicenceRenewalException("Exception while applying for new licence as fail to retrieve new worksheetId.");
			}

		} catch (LicenceRenewalException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("Unexpected exception while applying for new licence.", e);
			throw new LicenceRenewalException("Unexpected exception while applying for new licence.", e);
		}

	}

	@Override
	public void changeAddress(String licenceId, AddressContext address) {
		// Not implemented

	}

	@Override
	public boolean enabled(long companyId) {
		try {
			return m3ppConfigurationService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public String getFirstRegisteredDate(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return null;
	}

	@Override
	public LicenceDetails getLicenceDetailsByLicenceId(String licenceId) {
		// Not implemented
		return null;
	}

	@Override
	public LicenceDetails getLicenceDetailsByReference(String renewalReference) {
		// Not implemented
		return null;
	}

	@Override
	public String getVehicleRenewalDateTime(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return null;
	}

	@Override
	public String getVehicleRenewalLicenseLength(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return null;
	}

	@Override
	public int getVehicleRenewalLicenseLengthIntMonths(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return 0;
	}

	@Override
	public BigDecimal getVehicleRenewalPrice(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return null;
	}

	@Override
	public boolean hasLicenceExpired(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return false;
	}

	@Override
	public boolean isRenewalTooLate(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return false;
	}

	@Override
	public boolean isRenewalTooSoon(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return false;
	}

	@Override
	public void renewLicence(String licenceId, int monthsToAdd, String paymentReference) throws LicenceRenewalException {
		// Not implemented
	}

}
