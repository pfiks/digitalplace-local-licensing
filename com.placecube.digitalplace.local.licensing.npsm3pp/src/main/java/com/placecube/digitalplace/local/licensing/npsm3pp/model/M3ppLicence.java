package com.placecube.digitalplace.local.licensing.npsm3pp.model;

public class M3ppLicence {

	private String apDate;
	private String feeDatePaid;
	private String feeFeceipt;
	private String feePaid;
	private String lGroup;
	private String licDayMon;
	private String licPer;
	private String stdFee;
	private String vehColour;
	private String veheSize;
	private String vehFext;
	private String vehFuel;
	private String vehMake;
	private String vehModel;
	private String vehNotes;
	private String vehPlate;
	private String vehRegDate;
	private String vehRegno;
	private String vehSeatsNo;
	private String vehWc;
	private String workId;

	public M3ppLicence() {
	}

	public String getApDate() {
		return apDate;
	}

	public String getFeeDatePaid() {
		return feeDatePaid;
	}

	public String getFeeFeceipt() {
		return feeFeceipt;
	}

	public String getFeePaid() {
		return feePaid;
	}

	public String getlGroup() {
		return lGroup;
	}

	public String getLicDayMon() {
		return licDayMon;
	}

	public String getLicPer() {
		return licPer;
	}

	public String getStdFee() {
		return stdFee;
	}

	public String getVehColour() {
		return vehColour;
	}

	public String getVeheSize() {
		return veheSize;
	}

	public String getVehFext() {
		return vehFext;
	}

	public String getVehFuel() {
		return vehFuel;
	}

	public String getVehMake() {
		return vehMake;
	}

	public String getVehModel() {
		return vehModel;
	}

	public String getVehNotes() {
		return vehNotes;
	}

	public String getVehPlate() {
		return vehPlate;
	}

	public String getVehRegDate() {
		return vehRegDate;
	}

	public String getVehRegno() {
		return vehRegno;
	}

	public String getVehSeatsNo() {
		return vehSeatsNo;
	}

	public String getVehWc() {
		return vehWc;
	}

	public String getWorkId() {
		return workId;
	}

	public void setApDate(String apDate) {
		this.apDate = apDate;
	}

	public void setFeeDatePaid(String feeDatePaid) {
		this.feeDatePaid = feeDatePaid;
	}

	public void setFeeFeceipt(String feeFeceipt) {
		this.feeFeceipt = feeFeceipt;
	}

	public void setFeePaid(String feePaid) {
		this.feePaid = feePaid;
	}

	public void setlGroup(String lGroup) {
		this.lGroup = lGroup;
	}

	public void setLicDayMon(String licDayMon) {
		this.licDayMon = licDayMon;
	}

	public void setLicPer(String licPer) {
		this.licPer = licPer;
	}

	public void setStdFee(String stdFee) {
		this.stdFee = stdFee;
	}

	public void setVehColour(String vehColour) {
		this.vehColour = vehColour;
	}

	public void setVeheSize(String veheSize) {
		this.veheSize = veheSize;
	}

	public void setVehFext(String vehFext) {
		this.vehFext = vehFext;
	}

	public void setVehFuel(String vehFuel) {
		this.vehFuel = vehFuel;
	}

	public void setVehMake(String vehMake) {
		this.vehMake = vehMake;
	}

	public void setVehModel(String vehModel) {
		this.vehModel = vehModel;
	}

	public void setVehNotes(String vehNotes) {
		this.vehNotes = vehNotes;
	}

	public void setVehPlate(String vehPlate) {
		this.vehPlate = vehPlate;
	}

	public void setVehRegDate(String vehRegDate) {
		this.vehRegDate = vehRegDate;
	}

	public void setVehRegno(String vehRegno) {
		this.vehRegno = vehRegno;
	}

	public void setVehSeatsNo(String vehSeatsNo) {
		this.vehSeatsNo = vehSeatsNo;
	}

	public void setVehWc(String vehWc) {
		this.vehWc = vehWc;
	}

	public void setWorkId(String workId) {
		this.workId = workId;
	}
}
