/**
 * GetRoundDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class GetRoundDetails  implements java.io.Serializable {
    private java.lang.String strUKEY;

    private java.lang.String strSITEKEY;

    public GetRoundDetails() {
    }

    public GetRoundDetails(
           java.lang.String strUKEY,
           java.lang.String strSITEKEY) {
           this.strUKEY = strUKEY;
           this.strSITEKEY = strSITEKEY;
    }


    /**
     * Gets the strUKEY value for this GetRoundDetails.
     * 
     * @return strUKEY
     */
    public java.lang.String getStrUKEY() {
        return strUKEY;
    }


    /**
     * Sets the strUKEY value for this GetRoundDetails.
     * 
     * @param strUKEY
     */
    public void setStrUKEY(java.lang.String strUKEY) {
        this.strUKEY = strUKEY;
    }


    /**
     * Gets the strSITEKEY value for this GetRoundDetails.
     * 
     * @return strSITEKEY
     */
    public java.lang.String getStrSITEKEY() {
        return strSITEKEY;
    }


    /**
     * Sets the strSITEKEY value for this GetRoundDetails.
     * 
     * @param strSITEKEY
     */
    public void setStrSITEKEY(java.lang.String strSITEKEY) {
        this.strSITEKEY = strSITEKEY;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetRoundDetails)) return false;
        GetRoundDetails other = (GetRoundDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.strUKEY==null && other.getStrUKEY()==null) || 
             (this.strUKEY!=null &&
              this.strUKEY.equals(other.getStrUKEY()))) &&
            ((this.strSITEKEY==null && other.getStrSITEKEY()==null) || 
             (this.strSITEKEY!=null &&
              this.strSITEKEY.equals(other.getStrSITEKEY())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStrUKEY() != null) {
            _hashCode += getStrUKEY().hashCode();
        }
        if (getStrSITEKEY() != null) {
            _hashCode += getStrSITEKEY().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetRoundDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetRoundDetails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strUKEY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strUKEY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strSITEKEY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strSITEKEY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
