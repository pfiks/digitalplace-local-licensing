package com.placecube.digitalplace.local.licensing.npsm3pp.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.npsm3pp.configuration.M3ppCompanyConfiguration;

@Component(immediate = true, service = M3ppConfigurationService.class)
public class M3ppConfigurationService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public M3ppCompanyConfiguration getConfiguration(Long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(M3ppCompanyConfiguration.class, companyId);
	}

	public String getM3ppEndpointUrl(M3ppCompanyConfiguration configuration) throws ConfigurationException {
		String dlvaEndpointUrl = configuration.m3ppEndpointUrl();

		if (Validator.isNull(dlvaEndpointUrl)) {
			throw new ConfigurationException("M3pp End point Url configuration cannot be empty");
		}

		return dlvaEndpointUrl;
	}

	public String getPassword(M3ppCompanyConfiguration configuration) throws ConfigurationException {
		String password = configuration.password();

		if (Validator.isNull(password)) {
			throw new ConfigurationException("Password configuration cannot be empty");
		}

		return password;
	}

	public String getUsername(M3ppCompanyConfiguration configuration) throws ConfigurationException {
		String username = configuration.username();

		if (Validator.isNull(username)) {
			throw new ConfigurationException("Username configuration cannot be empty");
		}

		return username;
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		M3ppCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(M3ppCompanyConfiguration.class, companyId);
		return configuration.enabled();
	}

}
