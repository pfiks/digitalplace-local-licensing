package com.placecube.digitalplace.local.licensing.npsm3pp.model;

public class M3ppOOAProperty {

	private String addr1;
	private String addr2;
	private String addr3;
	private String addr4;
	private String addr5;
	private String addr6;
	private String indexType;
	private String postcde;
	private String tradName;

	public M3ppOOAProperty() {
	}

	public String getAddr1() {
		return addr1;
	}

	public String getAddr2() {
		return addr2;
	}

	public String getAddr3() {
		return addr3;
	}

	public String getAddr4() {
		return addr4;
	}

	public String getAddr5() {
		return addr5;
	}

	public String getAddr6() {
		return addr6;
	}

	public String getIndexType() {
		return indexType;
	}

	public String getPostcde() {
		return postcde;
	}

	public String getTradName() {
		return tradName;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public void setAddr3(String addr3) {
		this.addr3 = addr3;
	}

	public void setAddr4(String addr4) {
		this.addr4 = addr4;
	}

	public void setAddr5(String addr5) {
		this.addr5 = addr5;
	}

	public void setAddr6(String addr6) {
		this.addr6 = addr6;
	}

	public void setIndexType(String indexType) {
		this.indexType = indexType;
	}

	public void setPostcde(String postcde) {
		this.postcde = postcde;
	}

	public void setTradName(String tradName) {
		this.tradName = tradName;
	}
}
