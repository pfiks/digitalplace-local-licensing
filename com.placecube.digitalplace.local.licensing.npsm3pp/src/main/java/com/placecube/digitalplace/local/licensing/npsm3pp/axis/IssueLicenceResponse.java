/**
 * IssueLicenceResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class IssueLicenceResponse  implements java.io.Serializable {
    private IssueLicenceResponseIssueLicenceResult issueLicenceResult;

    public IssueLicenceResponse() {
    }

    public IssueLicenceResponse(
           IssueLicenceResponseIssueLicenceResult issueLicenceResult) {
           this.issueLicenceResult = issueLicenceResult;
    }


    /**
     * Gets the issueLicenceResult value for this IssueLicenceResponse.
     * 
     * @return issueLicenceResult
     */
    public IssueLicenceResponseIssueLicenceResult getIssueLicenceResult() {
        return issueLicenceResult;
    }


    /**
     * Sets the issueLicenceResult value for this IssueLicenceResponse.
     * 
     * @param issueLicenceResult
     */
    public void setIssueLicenceResult(IssueLicenceResponseIssueLicenceResult issueLicenceResult) {
        this.issueLicenceResult = issueLicenceResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IssueLicenceResponse)) return false;
        IssueLicenceResponse other = (IssueLicenceResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.issueLicenceResult==null && other.getIssueLicenceResult()==null) || 
             (this.issueLicenceResult!=null &&
              this.issueLicenceResult.equals(other.getIssueLicenceResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIssueLicenceResult() != null) {
            _hashCode += getIssueLicenceResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IssueLicenceResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">IssueLicenceResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issueLicenceResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "IssueLicenceResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>IssueLicenceResponse>IssueLicenceResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
