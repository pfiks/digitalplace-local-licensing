/**
 * M3PPService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public interface M3PPService extends javax.xml.rpc.Service {

/**
 * SIG 2.9.3.7
 */
    public java.lang.String getM3PPServiceSoapAddress();

    public M3PPServiceSoap getM3PPServiceSoap() throws javax.xml.rpc.ServiceException;

    public M3PPServiceSoap getM3PPServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
