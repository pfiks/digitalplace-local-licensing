package com.placecube.digitalplace.local.licensing.npsm3pp.util;

import java.util.Iterator;

import javax.xml.soap.Name;
import javax.xml.soap.SOAPException;

import org.apache.axis.message.MessageElement;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.npsm3pp.constants.LicenseApplicationContextKeys;
import com.placecube.digitalplace.local.licensing.npsm3pp.constants.M3ppSoapKeys;
import com.placecube.digitalplace.local.licensing.npsm3pp.model.M3ppLicence;
import com.placecube.digitalplace.local.licensing.npsm3pp.model.M3ppOOAProperty;
import com.placecube.digitalplace.local.licensing.npsm3pp.model.M3ppWorksheet;

public class MessageElementUtil {

	private static final Log LOG = LogFactoryUtil.getLog(MessageElementUtil.class);

	public static boolean checkForSuccessfulResponse(MessageElement[] messageElements) {
		boolean success = false;
		if (Validator.isNotNull(messageElements) && messageElements.length > 0) {

			Iterator<MessageElement> mvmChildElements = messageElements[0].getRealElement().getChildElements();
			success = true;
			while (mvmChildElements.hasNext()) {
				MessageElement mvmElement = mvmChildElements.next();

				success &= isElementSuccess(mvmElement);

			}
		}

		return success;
	}

	public static boolean isElementSuccess(MessageElement messageElement) {
		boolean success = true;
		Name mvmElementName = messageElement.getElementName();
		if (mvmElementName.getLocalName().equals(M3ppSoapKeys.RESULTS)) {
			Iterator<MessageElement> resultElements = messageElement.getChildElements();
			while (resultElements.hasNext()) {
				MessageElement resultElement = resultElements.next();
				if (resultElement.getElementName().getLocalName().equals(M3ppSoapKeys.RESULT)) {

					MessageElement resultChildElement = processChildElements(resultElement.getChildElements(), M3ppSoapKeys.RESULT_CODE);
					if (Validator.isNotNull(resultChildElement)) {
						if (!resultChildElement.getValue().equalsIgnoreCase(LicenseApplicationContextKeys.ELEMENT_SUCCESS_CODE)) {
							success = false;
							MessageElement errorDescriptionElement = processChildElements(resultElement.getChildElements(), M3ppSoapKeys.RESULT_DESC);
							LOG.error("M3 Error:" + (Validator.isNotNull(errorDescriptionElement) ? errorDescriptionElement.getValue() : "No description."));
						}
					}
				}
			}
		}

		return success;
	}

	public static MessageElement[] getOOAPropertyMessageElements(M3ppOOAProperty m3ppOOAProperty) throws SOAPException {

		MessageElement m3ppOOAPropertyElement = new MessageElement();
		m3ppOOAPropertyElement.setName(M3ppSoapKeys.M3PP_OOAPROPERTY);

		setKeyAndValuesToElement(m3ppOOAPropertyElement, M3ppSoapKeys.TRADNAME, m3ppOOAProperty.getTradName());
		setKeyAndValuesToElement(m3ppOOAPropertyElement, M3ppSoapKeys.ADDR1, m3ppOOAProperty.getAddr1());
		setKeyAndValuesToElement(m3ppOOAPropertyElement, M3ppSoapKeys.ADDR2, m3ppOOAProperty.getAddr2());
		setKeyAndValuesToElement(m3ppOOAPropertyElement, M3ppSoapKeys.ADDR3, m3ppOOAProperty.getAddr3());
		setKeyAndValuesToElement(m3ppOOAPropertyElement, M3ppSoapKeys.ADDR4, m3ppOOAProperty.getAddr4());
		setKeyAndValuesToElement(m3ppOOAPropertyElement, M3ppSoapKeys.ADDR5, m3ppOOAProperty.getAddr5());
		setKeyAndValuesToElement(m3ppOOAPropertyElement, M3ppSoapKeys.ADDR6, m3ppOOAProperty.getAddr6());
		setKeyAndValuesToElement(m3ppOOAPropertyElement, M3ppSoapKeys.POSTCDE, m3ppOOAProperty.getPostcde());
		setKeyAndValuesToElement(m3ppOOAPropertyElement, M3ppSoapKeys.INDEXTYPE, m3ppOOAProperty.getIndexType());

		return getMessageElements(m3ppOOAPropertyElement);
	}

	public static String getUkeyFromOOAPropertyResponse(MessageElement[] messageElements) {

		String ukey = null;
		if (Validator.isNotNull(messageElements) && messageElements.length > 0) {

			Iterator<MessageElement> mvmChildElements = messageElements[0].getRealElement().getChildElements();

			while (mvmChildElements.hasNext()) {
				MessageElement mvmElement = mvmChildElements.next();
				if (!isElementSuccess(mvmElement)) {
					continue;
				}

				if (mvmElement.getElementName().getLocalName().equals(M3ppSoapKeys.PUBLIC_PROTECTION)) {
					MessageElement childElement = processChildElements(mvmElement.getChildElements(), M3ppSoapKeys.M3PP_OOAPROPERTY);
					if (Validator.isNotNull(childElement)) {
						MessageElement ukeyElement = processChildElements(childElement.getChildElements(), M3ppSoapKeys.UKEY);
						if (Validator.isNotNull(ukeyElement)) {
							return ukeyElement.getValue();
						}
					}
				}
			}
		}

		return ukey;
	}

	public static MessageElement[] getUpdateLicenceMessageElements(M3ppLicence m3ppLicence) throws SOAPException {

		MessageElement m3ppLicenceElement = new MessageElement();
		m3ppLicenceElement.setName(M3ppSoapKeys.M3PP_LICENCE);

		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.WORK_ID, m3ppLicence.getWorkId());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.LGROUP, m3ppLicence.getlGroup());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.AP_DATE, m3ppLicence.getApDate());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.LICPER, m3ppLicence.getLicPer());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.LIC_DAY_MON, m3ppLicence.getLicDayMon());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.STDFEE, m3ppLicence.getStdFee());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.FEE_PAID, m3ppLicence.getFeePaid());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.FEE_RECEIPT, m3ppLicence.getFeeFeceipt());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.FEE_DTE_PAID, m3ppLicence.getFeeDatePaid());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.VEH_PLATE, m3ppLicence.getVehPlate());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.VEH_MAKE, m3ppLicence.getVehMake());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.VEH_FUEL, m3ppLicence.getVehFuel());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.VEH_REGNO, m3ppLicence.getVehRegno());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.VEH_MODEL, m3ppLicence.getVehModel());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.VEHE_SIZE, m3ppLicence.getVeheSize());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.VEH_REG_DATE, m3ppLicence.getVehRegDate());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.VEH_COLOUR, m3ppLicence.getVehColour());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.VEH_FEXT, m3ppLicence.getVehFext());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.VEH_SEATS_NO, m3ppLicence.getVehSeatsNo());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.VEH_WC, m3ppLicence.getVehWc());
		setKeyAndValuesToElement(m3ppLicenceElement, M3ppSoapKeys.VEH_NOTES, m3ppLicence.getVehNotes());

		return getMessageElements(m3ppLicenceElement);
	}

	public static MessageElement[] getUpdateWorksheetMessageElements(M3ppWorksheet m3ppWorksheet) throws SOAPException {

		MessageElement m3ppWorksheetElement = new MessageElement();
		m3ppWorksheetElement.setName(M3ppSoapKeys.M3PP_WORKSHEET);

		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.WORK_ID, m3ppWorksheet.getWorkid());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.SUB_UPRN, m3ppWorksheet.getSubUprn());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.SUB_UKEY, m3ppWorksheet.getSubUkey());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.REP_UPRN, m3ppWorksheet.getRepUprn());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.REP_UKEY, m3ppWorksheet.getRepUkey());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.REP_TITLE, m3ppWorksheet.getRepTitle());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.REP_FIRST_NAME, m3ppWorksheet.getRepFirstName());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.REP_FAMILY_NAME, m3ppWorksheet.getRepFamilyName());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.REP_TEL_HOME, m3ppWorksheet.getRepTelHome());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.REP_TEL_MOB, m3ppWorksheet.getRepTelMob());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.REP_EMAIL, m3ppWorksheet.getRepeMail());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.DEPARTMENT, m3ppWorksheet.getDepartment());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.APPLICATION, m3ppWorksheet.getApplication());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.TASK_GROUP, m3ppWorksheet.getTaskGroup());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.TASK, m3ppWorksheet.getTask());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.DT_RECD, m3ppWorksheet.getDtRecd());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.TM_RECD, m3ppWorksheet.getTmRecd());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.REC_DBY, m3ppWorksheet.getRecDby());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.METHOD, m3ppWorksheet.getMethod());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.SOURCE, m3ppWorksheet.getSource());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.DETAILS, m3ppWorksheet.getDetails());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.MESSAGE, m3ppWorksheet.getMessage());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.AREA, m3ppWorksheet.getArea());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.OFFICER, m3ppWorksheet.getOfficer());
		setKeyAndValuesToElement(m3ppWorksheetElement, M3ppSoapKeys.REMARKS, m3ppWorksheet.getRemarks());

		return getMessageElements(m3ppWorksheetElement);
	}

	public static String getWorkIdFromResponse(MessageElement[] messageElements, String lookUpElement) {

		String workId = null;

		if (Validator.isNotNull(messageElements) && messageElements.length > 0) {
			Iterator<MessageElement> mvmChildElements = messageElements[0].getRealElement().getChildElements();

			while (mvmChildElements.hasNext()) {
				MessageElement mvmElement = mvmChildElements.next();
				if (!isElementSuccess(mvmElement)) {
					continue;
				}

				if (mvmElement.getElementName().getLocalName().equals(M3ppSoapKeys.PUBLIC_PROTECTION)) {
					MessageElement childElement = processChildElements(mvmElement.getChildElements(), lookUpElement);
					if (Validator.isNotNull(childElement)) {

						MessageElement workIdElement = processChildElements(childElement.getChildElements(), M3ppSoapKeys.WORK_ID);
						if (Validator.isNotNull(workIdElement)) {
							LOG.debug("Found work id:" + workIdElement.getValue());
							return workIdElement.getValue();
						}
					}
				}

			}
		}

		return workId;
	}

	private static MessageElement[] getMessageElements(MessageElement messageElement) throws SOAPException {
		MessageElement publicProtectionElement = new MessageElement();
		publicProtectionElement.setName(M3ppSoapKeys.PUBLIC_PROTECTION);
		publicProtectionElement.addChild(messageElement);

		MessageElement mvmElement = new MessageElement();
		mvmElement.setName(M3ppSoapKeys.MVM);
		mvmElement.setAttributeNS("", "xmlns", M3ppSoapKeys.XMLNS_URL);
		mvmElement.addChild(publicProtectionElement);

		MessageElement[] messageElements = new MessageElement[1];
		messageElements[0] = mvmElement;

		return messageElements;
	}

	private static MessageElement processChildElements(Iterator<MessageElement> iterator, String elementLocalName) {

		if (Validator.isNotNull(iterator)) {
			while (iterator.hasNext()) {
				MessageElement ppElement = iterator.next();
				if (ppElement.getElementName().getLocalName().equals(elementLocalName)) {
					return ppElement;
				}
			}
		}

		return null;
	}

	private static void setKeyAndValuesToElement(MessageElement messageElement, String key, String value) throws SOAPException {
		if (Validator.isNotNull(value)) {
			messageElement.addChildElement(key).addTextNode(value);
		} else {
			messageElement.addChildElement(key);
		}
	}

	private MessageElementUtil() {
		throw new IllegalStateException("MessageElementUtil class");
	}
}
