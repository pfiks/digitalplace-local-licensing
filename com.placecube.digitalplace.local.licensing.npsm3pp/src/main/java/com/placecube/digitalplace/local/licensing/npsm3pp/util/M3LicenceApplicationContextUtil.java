package com.placecube.digitalplace.local.licensing.npsm3pp.util;

import java.time.LocalDateTime;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.model.LicenceApplicationContext;
import com.placecube.digitalplace.local.licensing.npsm3pp.constants.JsonInputKeys;
import com.placecube.digitalplace.local.licensing.npsm3pp.constants.LicenseApplicationContextKeys;
import com.placecube.digitalplace.local.licensing.npsm3pp.constants.M3ppSoapKeys;

public class M3LicenceApplicationContextUtil {

	public static LicenceApplicationContext getLicenceApplicationContext(String jsonRawInput) throws JSONException {

		JSONObject jsonInput = JSONFactoryUtil.createJSONObject(jsonRawInput);
		LicenceApplicationContext licenceApplicationContext = JSONFactoryUtil.looseDeserialize(jsonRawInput, LicenceApplicationContext.class);

		// M3 Specific fix worksheet parameters needs to be sent to call
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.WORK_ID, LicenseApplicationContextKeys.NEW_LICENSE);
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.APPLICATION, LicenseApplicationContextKeys.LN_CODE);
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.DEPARTMENT, LicenseApplicationContextKeys.LN_CODE);
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.DT_RECD, LocalDateTime.now().toString());
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.REP_TITLE, "");
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.REC_DBY, LicenseApplicationContextKeys.RECDBY_WEB_CODE);
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.METHOD, LicenseApplicationContextKeys.METHOD_WEB_CODE);
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.SOURCE, LicenseApplicationContextKeys.SOURCE_CODE);
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.AREA, LicenseApplicationContextKeys.AREA_CODE);
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.OFFICER, LicenseApplicationContextKeys.OFFICER_CODE);
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.TM_RECD, LicenseApplicationContextKeys.TM_RECD_CODE);
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.SUB_UKEY, LicenseApplicationContextKeys.SUB_UKEY_CODE);

		//// M3 Specific fix Licence parameters needs to be sent to call

		licenceApplicationContext.getParameters().put(M3ppSoapKeys.LGROUP, LicenseApplicationContextKeys.LGROUP_CODE);
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.AP_DATE, LocalDateTime.now().toString());

		// Form selective worksheet parameters
		if (Validator.isNotNull(licenceApplicationContext.getTypeOfLicense())) {

			if (licenceApplicationContext.getTypeOfLicense().equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_DRIVER_TASK_GROUP)) {

				licenceApplicationContext.getParameters().put(M3ppSoapKeys.TASK_GROUP, LicenseApplicationContextKeys.TASK_GROUP_CODE_L4);

			} else if (licenceApplicationContext.getTypeOfLicense().equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_VEHICLE_TASK_GROUP)) {

				licenceApplicationContext.getParameters().put(M3ppSoapKeys.TASK_GROUP, LicenseApplicationContextKeys.TASK_GROUP_CODE_L3);

			} else if (licenceApplicationContext.getTypeOfLicense().equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_OPERATOR_TASK_GROUP)) {

				licenceApplicationContext.getParameters().put(M3ppSoapKeys.TASK_GROUP, LicenseApplicationContextKeys.TASK_GROUP_CODE_L5);

				licenceApplicationContext.getParameters().put(M3ppSoapKeys.TASK, LicenseApplicationContextKeys.TASK_CODE_TAXI_OPERATOR);

			}

			String driverType = jsonInput.getString(JsonInputKeys.DRIVER_TYPE);
			String vehicleType = jsonInput.getString(JsonInputKeys.VEHICLE_TYPE);

			if (driverType.equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_HACKNEY_CARRIAGE_DRIVER)) {

				licenceApplicationContext.getParameters().put(M3ppSoapKeys.TASK, LicenseApplicationContextKeys.TASK_CODE_HACKNEY_CARRIAGE_DRIVER);

			} else if (driverType.equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_PRIVATE_DRIVER_HIRE)) {

				licenceApplicationContext.getParameters().put(M3ppSoapKeys.TASK, LicenseApplicationContextKeys.TASK_CODE_PRIVATE_DRIVER_HIRE);

			} else if (driverType.equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_PRIVATE_DRIVER_HIRE_RESTRICTED)) {

				licenceApplicationContext.getParameters().put(M3ppSoapKeys.TASK, LicenseApplicationContextKeys.TASK_CODE_PRIVATE_DRIVER_HIRE_RESTRICTED);

			} else if (driverType.equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_DUAL_HC_PH_DRIVER)) {

				licenceApplicationContext.getParameters().put(M3ppSoapKeys.TASK, LicenseApplicationContextKeys.TASK_CODE_DUAL_HC_PH_DRIVER);

			}

			if (vehicleType.equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_PRIVATE_HIRE_VEHICLE)) {

				licenceApplicationContext.getParameters().put(M3ppSoapKeys.TASK, LicenseApplicationContextKeys.TASK_CODE_PRIVATE_HIRE_VEHICLE);

			} else if (vehicleType.equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_EXECUTIVE_PRIVATE_HIRE_VEHICLE)) {

				licenceApplicationContext.getParameters().put(M3ppSoapKeys.TASK, LicenseApplicationContextKeys.TASK_CODE_EXECUTIVE_PRIVATE_HIRE_VEHICLE);
			} else if (vehicleType.equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_HACKNEY_CARRIAGE_VEHICLE)) {

				licenceApplicationContext.getParameters().put(M3ppSoapKeys.TASK, LicenseApplicationContextKeys.TASK_CODE_HACKNEY_CARRIAGE_VEHICLE);

			}
		}

		// For worksheet call
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.REP_UKEY, jsonInput.getString(JsonInputKeys.REP_U_KEY));
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.REMARKS, jsonInput.getString(JsonInputKeys.REMARKS));

		licenceApplicationContext.getParameters().put(M3ppSoapKeys.REP_FAMILY_NAME, jsonInput.getString(JsonInputKeys.FAMILY_NAME));
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.REP_FIRST_NAME, jsonInput.getString(JsonInputKeys.FIRST_NAME));
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.REP_TEL_HOME, jsonInput.getString(JsonInputKeys.TELEPHONE));
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.REP_TEL_MOB, jsonInput.getString(JsonInputKeys.MOBILE));

		// for licences call
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.LICPER, jsonInput.getString(JsonInputKeys.EXPIRY_PERIOD));
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.LIC_DAY_MON, jsonInput.getString(JsonInputKeys.EXPIRY_PERIOD_MONTH_OR_DATE));
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.STDFEE, jsonInput.getString(JsonInputKeys.FEE_AMOUNT));
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.FEE_PAID, jsonInput.getString(JsonInputKeys.FEE_PAID));
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.FEE_RECEIPT, jsonInput.getString(JsonInputKeys.FEE_RECITE_NUMBER));
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.FEE_DTE_PAID, jsonInput.getString(JsonInputKeys.FEE_PAID_DATE));

		licenceApplicationContext.getParameters().put(M3ppSoapKeys.VEH_FEXT, jsonInput.getString(JsonInputKeys.HAS_FIRE_EXTINGUISHER));
		licenceApplicationContext.getParameters().put(M3ppSoapKeys.VEH_WC, jsonInput.getString(JsonInputKeys.HAS_WHEEL_CHAIR));

		licenceApplicationContext.getParameters().put(M3ppSoapKeys.SUB_UPRN, jsonInput.getString(JsonInputKeys.UPRN));

		licenceApplicationContext.getParameters().put(M3ppSoapKeys.MESSAGE, jsonInput.getString(JsonInputKeys.MESSAGE));

		licenceApplicationContext.getParameters().put(M3ppSoapKeys.REP_EMAIL, jsonInput.getString(JsonInputKeys.EMAIL));

		licenceApplicationContext.getParameters().put(M3ppSoapKeys.REP_UPRN, jsonInput.getString(JsonInputKeys.REP_UPRN));

		licenceApplicationContext.getParameters().put(M3ppSoapKeys.VEH_NOTES, jsonInput.getString(JsonInputKeys.VEHICLE_NOTES));

		return licenceApplicationContext;

	}

}
