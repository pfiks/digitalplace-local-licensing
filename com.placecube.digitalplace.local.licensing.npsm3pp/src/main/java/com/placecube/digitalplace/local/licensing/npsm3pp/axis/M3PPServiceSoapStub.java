/**
 * M3PPServiceSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class M3PPServiceSoapStub extends org.apache.axis.client.Stub implements M3PPServiceSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[38];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
        _initOperationDesc4();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetVersion");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "verType"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetVersionResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateWorksheet");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlWork"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateWorksheet>xmlWork"), UpdateWorksheetXmlWork.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateWorksheetResponse>UpdateWorksheetResult"));
        oper.setReturnClass(UpdateWorksheetResponseUpdateWorksheetResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateWorksheetResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetWorksheetDets");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkSearch"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strSearchFld"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetWorksheetDetsResponse>GetWorksheetDetsResult"));
        oper.setReturnClass(GetWorksheetDetsResponseGetWorksheetDetsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetWorksheetDetsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateAction");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlAction"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAction>xmlAction"), UpdateActionXmlAction.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateActionResponse>UpdateActionResult"));
        oper.setReturnClass(UpdateActionResponseUpdateActionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateActionResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetActions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetActionsResponse>GetActionsResult"));
        oper.setReturnClass(GetActionsResponseGetActionsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetActionsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetSites");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strUPRN"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetSitesResponse>GetSitesResult"));
        oper.setReturnClass(GetSitesResponseGetSitesResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetSitesResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetWorksheets");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strUPRN"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strRepUPRN"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strDateFrom"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strDateTo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strApplication"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strTask"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strOfficer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strOS"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strSortColumn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strSortOrder"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetWorksheetsResponse>GetWorksheetsResult"));
        oper.setReturnClass(GetWorksheetsResponseGetWorksheetsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetWorksheetsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetCodeList");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strCodeType"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strCodeFrom"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strCodeTo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strOrder"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strParameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetCodeListResponse>GetCodeListResult"));
        oper.setReturnClass(GetCodeListResponseGetCodeListResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetCodeListResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetSubComponents");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strSiteKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetSubComponentsResponse>GetSubComponentsResult"));
        oper.setReturnClass(GetSubComponentsResponseGetSubComponentsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetSubComponentsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetProperties");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strSearch"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strBuilding_Number"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetPropertiesResponse>GetPropertiesResult"));
        oper.setReturnClass(GetPropertiesResponseGetPropertiesResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetPropertiesResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateRelatedAddr");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlRelAddr"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateRelatedAddr>xmlRelAddr"), UpdateRelatedAddrXmlRelAddr.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateRelatedAddrResponse>UpdateRelatedAddrResult"));
        oper.setReturnClass(UpdateRelatedAddrResponseUpdateRelatedAddrResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateRelatedAddrResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateFoodPremiseReg");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlA"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateFoodPremiseReg>xmlA"), UpdateFoodPremiseRegXmlA.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateFoodPremiseRegResponse>UpdateFoodPremiseRegResult"));
        oper.setReturnClass(UpdateFoodPremiseRegResponseUpdateFoodPremiseRegResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateFoodPremiseRegResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateEPA");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlA"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateEPA>xmlA"), UpdateEPAXmlA.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateEPAResponse>UpdateEPAResult"));
        oper.setReturnClass(UpdateEPAResponseUpdateEPAResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateEPAResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateLicence");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlA"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateLicence>xmlA"), UpdateLicenceXmlA.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateLicenceResponse>UpdateLicenceResult"));
        oper.setReturnClass(UpdateLicenceResponseUpdateLicenceResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateLicenceResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateBulky");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlA"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateBulky>xmlA"), UpdateBulkyXmlA.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateBulkyResponse>UpdateBulkyResult"));
        oper.setReturnClass(UpdateBulkyResponseUpdateBulkyResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateBulkyResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBulkyDetails");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetBulkyDetailsResponse>GetBulkyDetailsResult"));
        oper.setReturnClass(GetBulkyDetailsResponseGetBulkyDetailsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetBulkyDetailsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateAV");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlA"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAV>xmlA"), UpdateAVXmlA.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAVResponse>UpdateAVResult"));
        oper.setReturnClass(UpdateAVResponseUpdateAVResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateAVResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAVDetails");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetAVDetailsResponse>GetAVDetailsResult"));
        oper.setReturnClass(GetAVDetailsResponseGetAVDetailsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetAVDetailsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateAdWorksOrder");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlA"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAdWorksOrder>xmlA"), UpdateAdWorksOrderXmlA.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAdWorksOrderResponse>UpdateAdWorksOrderResult"));
        oper.setReturnClass(UpdateAdWorksOrderResponseUpdateAdWorksOrderResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateAdWorksOrderResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAdWorksOrderDets");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetAdWorksOrderDetsResponse>GetAdWorksOrderDetsResult"));
        oper.setReturnClass(GetAdWorksOrderDetsResponseGetAdWorksOrderDetsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetAdWorksOrderDetsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAvailableSlots");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strOfficer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strDaysAhead"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetAvailableSlotsResponse>GetAvailableSlotsResult"));
        oper.setReturnClass(GetAvailableSlotsResponseGetAvailableSlotsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetAvailableSlotsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BookAppointment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlA"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>BookAppointment>xmlA"), BookAppointmentXmlA.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>BookAppointmentResponse>BookAppointmentResult"));
        oper.setReturnClass(BookAppointmentResponseBookAppointmentResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "BookAppointmentResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBulkyItemSlots");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetBulkyItemSlotsResponse>GetBulkyItemSlotsResult"));
        oper.setReturnClass(GetBulkyItemSlotsResponseGetBulkyItemSlotsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetBulkyItemSlotsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BookBulkyItemAppointment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlA"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>BookBulkyItemAppointment>xmlA"), BookBulkyItemAppointmentXmlA.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>BookBulkyItemAppointmentResponse>BookBulkyItemAppointmentResult"));
        oper.setReturnClass(BookBulkyItemAppointmentResponseBookBulkyItemAppointmentResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "BookBulkyItemAppointmentResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetWorksheetAppointments");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetWorksheetAppointmentsResponse>GetWorksheetAppointmentsResult"));
        oper.setReturnClass(GetWorksheetAppointmentsResponseGetWorksheetAppointmentsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetWorksheetAppointmentsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetRoundDetails");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strUKEY"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strSITEKEY"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetRoundDetailsResponse>GetRoundDetailsResult"));
        oper.setReturnClass(GetRoundDetailsResponseGetRoundDetailsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetRoundDetailsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("OOAProperty");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlA"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>OOAProperty>xmlA"), OOAPropertyXmlA.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>OOAPropertyResponse>OOAPropertyResult"));
        oper.setReturnClass(OOAPropertyResponseOOAPropertyResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "OOAPropertyResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLinkedWorksheets");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetLinkedWorksheetsResponse>GetLinkedWorksheetsResult"));
        oper.setReturnClass(GetLinkedWorksheetsResponseGetLinkedWorksheetsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetLinkedWorksheetsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateMonitorNotices");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlMN"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateMonitorNotices>xmlMN"), UpdateMonitorNoticesXmlMN.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateMonitorNoticesResponse>UpdateMonitorNoticesResult"));
        oper.setReturnClass(UpdateMonitorNoticesResponseUpdateMonitorNoticesResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateMonitorNoticesResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateSources");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlS"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateSources>xmlS"), UpdateSourcesXmlS.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateSourcesResponse>UpdateSourcesResult"));
        oper.setReturnClass(UpdateSourcesResponseUpdateSourcesResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateSourcesResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[29] = oper;

    }

    private static void _initOperationDesc4(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAssets");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strSiteKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strUPRN"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strAssetKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetAssetsResponse>GetAssetsResult"));
        oper.setReturnClass(GetAssetsResponseGetAssetsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetAssetsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[30] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateAsset");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlAsset"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAsset>xmlAsset"), UpdateAssetXmlAsset.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAssetResponse>UpdateAssetResult"));
        oper.setReturnClass(UpdateAssetResponseUpdateAssetResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateAssetResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[31] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteAsset");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strAssetKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>DeleteAssetResponse>DeleteAssetResult"));
        oper.setReturnClass(DeleteAssetResponseDeleteAssetResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "DeleteAssetResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[32] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAssetInspections");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strAssetKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetAssetInspectionsResponse>GetAssetInspectionsResult"));
        oper.setReturnClass(GetAssetInspectionsResponseGetAssetInspectionsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetAssetInspectionsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[33] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateAssetInspection");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlAssetInsp"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAssetInspection>xmlAssetInsp"), UpdateAssetInspectionXmlAssetInsp.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAssetInspectionResponse>UpdateAssetInspectionResult"));
        oper.setReturnClass(UpdateAssetInspectionResponseUpdateAssetInspectionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateAssetInspectionResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[34] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdatePropertyContact");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlPC"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdatePropertyContact>xmlPC"), UpdatePropertyContactXmlPC.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdatePropertyContactResponse>UpdatePropertyContactResult"));
        oper.setReturnClass(UpdatePropertyContactResponseUpdatePropertyContactResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdatePropertyContactResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[35] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateHMODetails");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlHMO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateHMODetails>xmlHMO"), UpdateHMODetailsXmlHMO.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateHMODetailsResponse>UpdateHMODetailsResult"));
        oper.setReturnClass(UpdateHMODetailsResponseUpdateHMODetailsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateHMODetailsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[36] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("IssueLicence");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "xmlLicence"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>IssueLicence>xmlLicence"), IssueLicenceXmlLicence.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>IssueLicenceResponse>IssueLicenceResult"));
        oper.setReturnClass(IssueLicenceResponseIssueLicenceResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "IssueLicenceResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[37] = oper;

    }

    public M3PPServiceSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public M3PPServiceSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public M3PPServiceSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        addBindings0();
        addBindings1();
    }

    private void addBindings0() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>BookAppointment>xmlA");
            cachedSerQNames.add(qName);
            cls = BookAppointmentXmlA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>BookAppointmentResponse>BookAppointmentResult");
            cachedSerQNames.add(qName);
            cls = BookAppointmentResponseBookAppointmentResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>BookBulkyItemAppointment>xmlA");
            cachedSerQNames.add(qName);
            cls = BookBulkyItemAppointmentXmlA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>BookBulkyItemAppointmentResponse>BookBulkyItemAppointmentResult");
            cachedSerQNames.add(qName);
            cls = BookBulkyItemAppointmentResponseBookBulkyItemAppointmentResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>DeleteAssetResponse>DeleteAssetResult");
            cachedSerQNames.add(qName);
            cls = DeleteAssetResponseDeleteAssetResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetActionsResponse>GetActionsResult");
            cachedSerQNames.add(qName);
            cls = GetActionsResponseGetActionsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetAdWorksOrderDetsResponse>GetAdWorksOrderDetsResult");
            cachedSerQNames.add(qName);
            cls = GetAdWorksOrderDetsResponseGetAdWorksOrderDetsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetAssetInspectionsResponse>GetAssetInspectionsResult");
            cachedSerQNames.add(qName);
            cls = GetAssetInspectionsResponseGetAssetInspectionsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetAssetsResponse>GetAssetsResult");
            cachedSerQNames.add(qName);
            cls = GetAssetsResponseGetAssetsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetAvailableSlotsResponse>GetAvailableSlotsResult");
            cachedSerQNames.add(qName);
            cls = GetAvailableSlotsResponseGetAvailableSlotsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetAVDetailsResponse>GetAVDetailsResult");
            cachedSerQNames.add(qName);
            cls = GetAVDetailsResponseGetAVDetailsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetBulkyDetailsResponse>GetBulkyDetailsResult");
            cachedSerQNames.add(qName);
            cls = GetBulkyDetailsResponseGetBulkyDetailsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetBulkyItemSlotsResponse>GetBulkyItemSlotsResult");
            cachedSerQNames.add(qName);
            cls = GetBulkyItemSlotsResponseGetBulkyItemSlotsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetCodeListResponse>GetCodeListResult");
            cachedSerQNames.add(qName);
            cls = GetCodeListResponseGetCodeListResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetLinkedWorksheetsResponse>GetLinkedWorksheetsResult");
            cachedSerQNames.add(qName);
            cls = GetLinkedWorksheetsResponseGetLinkedWorksheetsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetPropertiesResponse>GetPropertiesResult");
            cachedSerQNames.add(qName);
            cls = GetPropertiesResponseGetPropertiesResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetRoundDetailsResponse>GetRoundDetailsResult");
            cachedSerQNames.add(qName);
            cls = GetRoundDetailsResponseGetRoundDetailsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetSitesResponse>GetSitesResult");
            cachedSerQNames.add(qName);
            cls = GetSitesResponseGetSitesResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetSubComponentsResponse>GetSubComponentsResult");
            cachedSerQNames.add(qName);
            cls = GetSubComponentsResponseGetSubComponentsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetWorksheetAppointmentsResponse>GetWorksheetAppointmentsResult");
            cachedSerQNames.add(qName);
            cls = GetWorksheetAppointmentsResponseGetWorksheetAppointmentsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetWorksheetDetsResponse>GetWorksheetDetsResult");
            cachedSerQNames.add(qName);
            cls = GetWorksheetDetsResponseGetWorksheetDetsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetWorksheetsResponse>GetWorksheetsResult");
            cachedSerQNames.add(qName);
            cls = GetWorksheetsResponseGetWorksheetsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>IssueLicence>xmlLicence");
            cachedSerQNames.add(qName);
            cls = IssueLicenceXmlLicence.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>IssueLicenceResponse>IssueLicenceResult");
            cachedSerQNames.add(qName);
            cls = IssueLicenceResponseIssueLicenceResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>OOAProperty>xmlA");
            cachedSerQNames.add(qName);
            cls = OOAPropertyXmlA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>OOAPropertyResponse>OOAPropertyResult");
            cachedSerQNames.add(qName);
            cls = OOAPropertyResponseOOAPropertyResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAction>xmlAction");
            cachedSerQNames.add(qName);
            cls = UpdateActionXmlAction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateActionResponse>UpdateActionResult");
            cachedSerQNames.add(qName);
            cls = UpdateActionResponseUpdateActionResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAdWorksOrder>xmlA");
            cachedSerQNames.add(qName);
            cls = UpdateAdWorksOrderXmlA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAdWorksOrderResponse>UpdateAdWorksOrderResult");
            cachedSerQNames.add(qName);
            cls = UpdateAdWorksOrderResponseUpdateAdWorksOrderResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAsset>xmlAsset");
            cachedSerQNames.add(qName);
            cls = UpdateAssetXmlAsset.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAssetInspection>xmlAssetInsp");
            cachedSerQNames.add(qName);
            cls = UpdateAssetInspectionXmlAssetInsp.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAssetInspectionResponse>UpdateAssetInspectionResult");
            cachedSerQNames.add(qName);
            cls = UpdateAssetInspectionResponseUpdateAssetInspectionResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAssetResponse>UpdateAssetResult");
            cachedSerQNames.add(qName);
            cls = UpdateAssetResponseUpdateAssetResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAV>xmlA");
            cachedSerQNames.add(qName);
            cls = UpdateAVXmlA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAVResponse>UpdateAVResult");
            cachedSerQNames.add(qName);
            cls = UpdateAVResponseUpdateAVResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateBulky>xmlA");
            cachedSerQNames.add(qName);
            cls = UpdateBulkyXmlA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateBulkyResponse>UpdateBulkyResult");
            cachedSerQNames.add(qName);
            cls = UpdateBulkyResponseUpdateBulkyResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateEPA>xmlA");
            cachedSerQNames.add(qName);
            cls = UpdateEPAXmlA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateEPAResponse>UpdateEPAResult");
            cachedSerQNames.add(qName);
            cls = UpdateEPAResponseUpdateEPAResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateFoodPremiseReg>xmlA");
            cachedSerQNames.add(qName);
            cls = UpdateFoodPremiseRegXmlA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateFoodPremiseRegResponse>UpdateFoodPremiseRegResult");
            cachedSerQNames.add(qName);
            cls = UpdateFoodPremiseRegResponseUpdateFoodPremiseRegResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateHMODetails>xmlHMO");
            cachedSerQNames.add(qName);
            cls = UpdateHMODetailsXmlHMO.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateHMODetailsResponse>UpdateHMODetailsResult");
            cachedSerQNames.add(qName);
            cls = UpdateHMODetailsResponseUpdateHMODetailsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateLicence>xmlA");
            cachedSerQNames.add(qName);
            cls = UpdateLicenceXmlA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateLicenceResponse>UpdateLicenceResult");
            cachedSerQNames.add(qName);
            cls = UpdateLicenceResponseUpdateLicenceResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateMonitorNotices>xmlMN");
            cachedSerQNames.add(qName);
            cls = UpdateMonitorNoticesXmlMN.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateMonitorNoticesResponse>UpdateMonitorNoticesResult");
            cachedSerQNames.add(qName);
            cls = UpdateMonitorNoticesResponseUpdateMonitorNoticesResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdatePropertyContact>xmlPC");
            cachedSerQNames.add(qName);
            cls = UpdatePropertyContactXmlPC.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdatePropertyContactResponse>UpdatePropertyContactResult");
            cachedSerQNames.add(qName);
            cls = UpdatePropertyContactResponseUpdatePropertyContactResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateRelatedAddr>xmlRelAddr");
            cachedSerQNames.add(qName);
            cls = UpdateRelatedAddrXmlRelAddr.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateRelatedAddrResponse>UpdateRelatedAddrResult");
            cachedSerQNames.add(qName);
            cls = UpdateRelatedAddrResponseUpdateRelatedAddrResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateSources>xmlS");
            cachedSerQNames.add(qName);
            cls = UpdateSourcesXmlS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateSourcesResponse>UpdateSourcesResult");
            cachedSerQNames.add(qName);
            cls = UpdateSourcesResponseUpdateSourcesResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateWorksheet>xmlWork");
            cachedSerQNames.add(qName);
            cls = UpdateWorksheetXmlWork.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateWorksheetResponse>UpdateWorksheetResult");
            cachedSerQNames.add(qName);
            cls = UpdateWorksheetResponseUpdateWorksheetResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">BookAppointment");
            cachedSerQNames.add(qName);
            cls = BookAppointment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">BookAppointmentResponse");
            cachedSerQNames.add(qName);
            cls = BookAppointmentResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">BookBulkyItemAppointment");
            cachedSerQNames.add(qName);
            cls = BookBulkyItemAppointment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">BookBulkyItemAppointmentResponse");
            cachedSerQNames.add(qName);
            cls = BookBulkyItemAppointmentResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">DeleteAsset");
            cachedSerQNames.add(qName);
            cls = DeleteAsset.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">DeleteAssetResponse");
            cachedSerQNames.add(qName);
            cls = DeleteAssetResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetActions");
            cachedSerQNames.add(qName);
            cls = GetActions.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetActionsResponse");
            cachedSerQNames.add(qName);
            cls = GetActionsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetAdWorksOrderDets");
            cachedSerQNames.add(qName);
            cls = GetAdWorksOrderDets.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetAdWorksOrderDetsResponse");
            cachedSerQNames.add(qName);
            cls = GetAdWorksOrderDetsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetAssetInspections");
            cachedSerQNames.add(qName);
            cls = GetAssetInspections.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetAssetInspectionsResponse");
            cachedSerQNames.add(qName);
            cls = GetAssetInspectionsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetAssets");
            cachedSerQNames.add(qName);
            cls = GetAssets.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetAssetsResponse");
            cachedSerQNames.add(qName);
            cls = GetAssetsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetAvailableSlots");
            cachedSerQNames.add(qName);
            cls = GetAvailableSlots.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetAvailableSlotsResponse");
            cachedSerQNames.add(qName);
            cls = GetAvailableSlotsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetAVDetails");
            cachedSerQNames.add(qName);
            cls = GetAVDetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetAVDetailsResponse");
            cachedSerQNames.add(qName);
            cls = GetAVDetailsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetBulkyDetails");
            cachedSerQNames.add(qName);
            cls = GetBulkyDetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetBulkyDetailsResponse");
            cachedSerQNames.add(qName);
            cls = GetBulkyDetailsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetBulkyItemSlots");
            cachedSerQNames.add(qName);
            cls = GetBulkyItemSlots.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetBulkyItemSlotsResponse");
            cachedSerQNames.add(qName);
            cls = GetBulkyItemSlotsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetCodeList");
            cachedSerQNames.add(qName);
            cls = GetCodeList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetCodeListResponse");
            cachedSerQNames.add(qName);
            cls = GetCodeListResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetLinkedWorksheets");
            cachedSerQNames.add(qName);
            cls = GetLinkedWorksheets.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetLinkedWorksheetsResponse");
            cachedSerQNames.add(qName);
            cls = GetLinkedWorksheetsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetProperties");
            cachedSerQNames.add(qName);
            cls = GetProperties.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetPropertiesResponse");
            cachedSerQNames.add(qName);
            cls = GetPropertiesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetRoundDetails");
            cachedSerQNames.add(qName);
            cls = GetRoundDetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetRoundDetailsResponse");
            cachedSerQNames.add(qName);
            cls = GetRoundDetailsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetSites");
            cachedSerQNames.add(qName);
            cls = GetSites.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetSitesResponse");
            cachedSerQNames.add(qName);
            cls = GetSitesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetSubComponents");
            cachedSerQNames.add(qName);
            cls = GetSubComponents.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetSubComponentsResponse");
            cachedSerQNames.add(qName);
            cls = GetSubComponentsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetWorksheetAppointments");
            cachedSerQNames.add(qName);
            cls = GetWorksheetAppointments.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetWorksheetAppointmentsResponse");
            cachedSerQNames.add(qName);
            cls = GetWorksheetAppointmentsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetWorksheetDets");
            cachedSerQNames.add(qName);
            cls = GetWorksheetDets.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetWorksheetDetsResponse");
            cachedSerQNames.add(qName);
            cls = GetWorksheetDetsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetWorksheets");
            cachedSerQNames.add(qName);
            cls = GetWorksheets.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetWorksheetsResponse");
            cachedSerQNames.add(qName);
            cls = GetWorksheetsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">IssueLicence");
            cachedSerQNames.add(qName);
            cls = IssueLicence.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">IssueLicenceResponse");
            cachedSerQNames.add(qName);
            cls = IssueLicenceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">OOAProperty");
            cachedSerQNames.add(qName);
            cls = OOAProperty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">OOAPropertyResponse");
            cachedSerQNames.add(qName);
            cls = OOAPropertyResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings1() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateAction");
            cachedSerQNames.add(qName);
            cls = UpdateAction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateActionResponse");
            cachedSerQNames.add(qName);
            cls = UpdateActionResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateAdWorksOrder");
            cachedSerQNames.add(qName);
            cls = UpdateAdWorksOrder.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateAdWorksOrderResponse");
            cachedSerQNames.add(qName);
            cls = UpdateAdWorksOrderResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateAsset");
            cachedSerQNames.add(qName);
            cls = UpdateAsset.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateAssetInspection");
            cachedSerQNames.add(qName);
            cls = UpdateAssetInspection.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateAssetInspectionResponse");
            cachedSerQNames.add(qName);
            cls = UpdateAssetInspectionResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateAssetResponse");
            cachedSerQNames.add(qName);
            cls = UpdateAssetResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateAV");
            cachedSerQNames.add(qName);
            cls = UpdateAV.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateAVResponse");
            cachedSerQNames.add(qName);
            cls = UpdateAVResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateBulky");
            cachedSerQNames.add(qName);
            cls = UpdateBulky.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateBulkyResponse");
            cachedSerQNames.add(qName);
            cls = UpdateBulkyResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateEPA");
            cachedSerQNames.add(qName);
            cls = UpdateEPA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateEPAResponse");
            cachedSerQNames.add(qName);
            cls = UpdateEPAResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateFoodPremiseReg");
            cachedSerQNames.add(qName);
            cls = UpdateFoodPremiseReg.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateFoodPremiseRegResponse");
            cachedSerQNames.add(qName);
            cls = UpdateFoodPremiseRegResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateHMODetails");
            cachedSerQNames.add(qName);
            cls = UpdateHMODetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateHMODetailsResponse");
            cachedSerQNames.add(qName);
            cls = UpdateHMODetailsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateLicence");
            cachedSerQNames.add(qName);
            cls = UpdateLicence.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateLicenceResponse");
            cachedSerQNames.add(qName);
            cls = UpdateLicenceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateMonitorNotices");
            cachedSerQNames.add(qName);
            cls = UpdateMonitorNotices.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateMonitorNoticesResponse");
            cachedSerQNames.add(qName);
            cls = UpdateMonitorNoticesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdatePropertyContact");
            cachedSerQNames.add(qName);
            cls = UpdatePropertyContact.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdatePropertyContactResponse");
            cachedSerQNames.add(qName);
            cls = UpdatePropertyContactResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateRelatedAddr");
            cachedSerQNames.add(qName);
            cls = UpdateRelatedAddr.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateRelatedAddrResponse");
            cachedSerQNames.add(qName);
            cls = UpdateRelatedAddrResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateSources");
            cachedSerQNames.add(qName);
            cls = UpdateSources.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateSourcesResponse");
            cachedSerQNames.add(qName);
            cls = UpdateSourcesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateWorksheetResponse");
            cachedSerQNames.add(qName);
            cls = UpdateWorksheetResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public java.lang.String getVersion(java.lang.String verType) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetVersion");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetVersion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {verType});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateWorksheetResponseUpdateWorksheetResult updateWorksheet(UpdateWorksheetXmlWork xmlWork) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateWorksheet");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateWorksheet"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlWork});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateWorksheetResponseUpdateWorksheetResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateWorksheetResponseUpdateWorksheetResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateWorksheetResponseUpdateWorksheetResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetWorksheetDetsResponseGetWorksheetDetsResult getWorksheetDets(java.lang.String strWorkId, java.lang.String strWorkSearch, java.lang.String strSearchFld) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetWorksheetDets");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetWorksheetDets"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strWorkId, strWorkSearch, strSearchFld});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetWorksheetDetsResponseGetWorksheetDetsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetWorksheetDetsResponseGetWorksheetDetsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetWorksheetDetsResponseGetWorksheetDetsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateActionResponseUpdateActionResult updateAction(UpdateActionXmlAction xmlAction) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateAction");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateAction"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlAction});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateActionResponseUpdateActionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateActionResponseUpdateActionResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateActionResponseUpdateActionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetActionsResponseGetActionsResult getActions(java.lang.String strWorkId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetActions");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetActions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strWorkId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetActionsResponseGetActionsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetActionsResponseGetActionsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetActionsResponseGetActionsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetSitesResponseGetSitesResult getSites(java.lang.String strUPRN) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetSites");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetSites"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strUPRN});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetSitesResponseGetSitesResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetSitesResponseGetSitesResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetSitesResponseGetSitesResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetWorksheetsResponseGetWorksheetsResult getWorksheets(java.lang.String strUPRN, java.lang.String strRepUPRN, java.lang.String strDateFrom, java.lang.String strDateTo, java.lang.String strApplication, java.lang.String strTask, java.lang.String strOfficer, java.lang.String strOS, java.lang.String strSortColumn, java.lang.String strSortOrder) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetWorksheets");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetWorksheets"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strUPRN, strRepUPRN, strDateFrom, strDateTo, strApplication, strTask, strOfficer, strOS, strSortColumn, strSortOrder});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetWorksheetsResponseGetWorksheetsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetWorksheetsResponseGetWorksheetsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetWorksheetsResponseGetWorksheetsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetCodeListResponseGetCodeListResult getCodeList(java.lang.String strCodeType, java.lang.String strCodeFrom, java.lang.String strCodeTo, java.lang.String strOrder, java.lang.String strParameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetCodeList");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetCodeList"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strCodeType, strCodeFrom, strCodeTo, strOrder, strParameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetCodeListResponseGetCodeListResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetCodeListResponseGetCodeListResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetCodeListResponseGetCodeListResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetSubComponentsResponseGetSubComponentsResult getSubComponents(java.lang.String strSiteKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetSubComponents");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetSubComponents"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strSiteKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetSubComponentsResponseGetSubComponentsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetSubComponentsResponseGetSubComponentsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetSubComponentsResponseGetSubComponentsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetPropertiesResponseGetPropertiesResult getProperties(java.lang.String strSearch, java.lang.String strBuilding_Number) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetProperties");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetProperties"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strSearch, strBuilding_Number});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetPropertiesResponseGetPropertiesResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetPropertiesResponseGetPropertiesResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetPropertiesResponseGetPropertiesResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateRelatedAddrResponseUpdateRelatedAddrResult updateRelatedAddr(UpdateRelatedAddrXmlRelAddr xmlRelAddr) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateRelatedAddr");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateRelatedAddr"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlRelAddr});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateRelatedAddrResponseUpdateRelatedAddrResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateRelatedAddrResponseUpdateRelatedAddrResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateRelatedAddrResponseUpdateRelatedAddrResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateFoodPremiseRegResponseUpdateFoodPremiseRegResult updateFoodPremiseReg(UpdateFoodPremiseRegXmlA xmlA) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateFoodPremiseReg");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateFoodPremiseReg"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlA});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateFoodPremiseRegResponseUpdateFoodPremiseRegResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateFoodPremiseRegResponseUpdateFoodPremiseRegResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateFoodPremiseRegResponseUpdateFoodPremiseRegResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateEPAResponseUpdateEPAResult updateEPA(UpdateEPAXmlA xmlA) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateEPA");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateEPA"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlA});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateEPAResponseUpdateEPAResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateEPAResponseUpdateEPAResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateEPAResponseUpdateEPAResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateLicenceResponseUpdateLicenceResult updateLicence(UpdateLicenceXmlA xmlA) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateLicence");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateLicence"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlA});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateLicenceResponseUpdateLicenceResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateLicenceResponseUpdateLicenceResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateLicenceResponseUpdateLicenceResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateBulkyResponseUpdateBulkyResult updateBulky(UpdateBulkyXmlA xmlA) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateBulky");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateBulky"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlA});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateBulkyResponseUpdateBulkyResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateBulkyResponseUpdateBulkyResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateBulkyResponseUpdateBulkyResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetBulkyDetailsResponseGetBulkyDetailsResult getBulkyDetails(java.lang.String strWorkId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetBulkyDetails");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetBulkyDetails"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strWorkId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetBulkyDetailsResponseGetBulkyDetailsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetBulkyDetailsResponseGetBulkyDetailsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetBulkyDetailsResponseGetBulkyDetailsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateAVResponseUpdateAVResult updateAV(UpdateAVXmlA xmlA) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateAV");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateAV"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlA});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateAVResponseUpdateAVResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateAVResponseUpdateAVResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateAVResponseUpdateAVResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetAVDetailsResponseGetAVDetailsResult getAVDetails(java.lang.String strWorkId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetAVDetails");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetAVDetails"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strWorkId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetAVDetailsResponseGetAVDetailsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetAVDetailsResponseGetAVDetailsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetAVDetailsResponseGetAVDetailsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateAdWorksOrderResponseUpdateAdWorksOrderResult updateAdWorksOrder(UpdateAdWorksOrderXmlA xmlA) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateAdWorksOrder");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateAdWorksOrder"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlA});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateAdWorksOrderResponseUpdateAdWorksOrderResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateAdWorksOrderResponseUpdateAdWorksOrderResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateAdWorksOrderResponseUpdateAdWorksOrderResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetAdWorksOrderDetsResponseGetAdWorksOrderDetsResult getAdWorksOrderDets(java.lang.String strWorkId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetAdWorksOrderDets");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetAdWorksOrderDets"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strWorkId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetAdWorksOrderDetsResponseGetAdWorksOrderDetsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetAdWorksOrderDetsResponseGetAdWorksOrderDetsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetAdWorksOrderDetsResponseGetAdWorksOrderDetsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetAvailableSlotsResponseGetAvailableSlotsResult getAvailableSlots(java.lang.String strOfficer, java.lang.String strWorkId, java.lang.String strDaysAhead) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetAvailableSlots");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetAvailableSlots"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strOfficer, strWorkId, strDaysAhead});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetAvailableSlotsResponseGetAvailableSlotsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetAvailableSlotsResponseGetAvailableSlotsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetAvailableSlotsResponseGetAvailableSlotsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public BookAppointmentResponseBookAppointmentResult bookAppointment(BookAppointmentXmlA xmlA) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/BookAppointment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "BookAppointment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlA});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (BookAppointmentResponseBookAppointmentResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (BookAppointmentResponseBookAppointmentResult) org.apache.axis.utils.JavaUtils.convert(_resp, BookAppointmentResponseBookAppointmentResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetBulkyItemSlotsResponseGetBulkyItemSlotsResult getBulkyItemSlots(java.lang.String strWorkId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetBulkyItemSlots");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetBulkyItemSlots"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strWorkId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetBulkyItemSlotsResponseGetBulkyItemSlotsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetBulkyItemSlotsResponseGetBulkyItemSlotsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetBulkyItemSlotsResponseGetBulkyItemSlotsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public BookBulkyItemAppointmentResponseBookBulkyItemAppointmentResult bookBulkyItemAppointment(BookBulkyItemAppointmentXmlA xmlA) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/BookBulkyItemAppointment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "BookBulkyItemAppointment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlA});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (BookBulkyItemAppointmentResponseBookBulkyItemAppointmentResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (BookBulkyItemAppointmentResponseBookBulkyItemAppointmentResult) org.apache.axis.utils.JavaUtils.convert(_resp, BookBulkyItemAppointmentResponseBookBulkyItemAppointmentResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetWorksheetAppointmentsResponseGetWorksheetAppointmentsResult getWorksheetAppointments(java.lang.String strWorkId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetWorksheetAppointments");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetWorksheetAppointments"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strWorkId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetWorksheetAppointmentsResponseGetWorksheetAppointmentsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetWorksheetAppointmentsResponseGetWorksheetAppointmentsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetWorksheetAppointmentsResponseGetWorksheetAppointmentsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetRoundDetailsResponseGetRoundDetailsResult getRoundDetails(java.lang.String strUKEY, java.lang.String strSITEKEY) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetRoundDetails");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetRoundDetails"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strUKEY, strSITEKEY});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetRoundDetailsResponseGetRoundDetailsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetRoundDetailsResponseGetRoundDetailsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetRoundDetailsResponseGetRoundDetailsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public OOAPropertyResponseOOAPropertyResult OOAProperty(OOAPropertyXmlA xmlA) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/OOAProperty");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "OOAProperty"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlA});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (OOAPropertyResponseOOAPropertyResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (OOAPropertyResponseOOAPropertyResult) org.apache.axis.utils.JavaUtils.convert(_resp, OOAPropertyResponseOOAPropertyResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetLinkedWorksheetsResponseGetLinkedWorksheetsResult getLinkedWorksheets(java.lang.String strWorkId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetLinkedWorksheets");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetLinkedWorksheets"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strWorkId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetLinkedWorksheetsResponseGetLinkedWorksheetsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetLinkedWorksheetsResponseGetLinkedWorksheetsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetLinkedWorksheetsResponseGetLinkedWorksheetsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateMonitorNoticesResponseUpdateMonitorNoticesResult updateMonitorNotices(UpdateMonitorNoticesXmlMN xmlMN) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateMonitorNotices");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateMonitorNotices"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlMN});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateMonitorNoticesResponseUpdateMonitorNoticesResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateMonitorNoticesResponseUpdateMonitorNoticesResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateMonitorNoticesResponseUpdateMonitorNoticesResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateSourcesResponseUpdateSourcesResult updateSources(UpdateSourcesXmlS xmlS) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateSources");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateSources"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlS});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateSourcesResponseUpdateSourcesResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateSourcesResponseUpdateSourcesResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateSourcesResponseUpdateSourcesResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetAssetsResponseGetAssetsResult getAssets(java.lang.String strSiteKey, java.lang.String strUPRN, java.lang.String strAssetKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[30]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetAssets");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetAssets"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strSiteKey, strUPRN, strAssetKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetAssetsResponseGetAssetsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetAssetsResponseGetAssetsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetAssetsResponseGetAssetsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateAssetResponseUpdateAssetResult updateAsset(UpdateAssetXmlAsset xmlAsset) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[31]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateAsset");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateAsset"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlAsset});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateAssetResponseUpdateAssetResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateAssetResponseUpdateAssetResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateAssetResponseUpdateAssetResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public DeleteAssetResponseDeleteAssetResult deleteAsset(java.lang.String strAssetKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[32]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/DeleteAsset");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "DeleteAsset"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strAssetKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (DeleteAssetResponseDeleteAssetResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (DeleteAssetResponseDeleteAssetResult) org.apache.axis.utils.JavaUtils.convert(_resp, DeleteAssetResponseDeleteAssetResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GetAssetInspectionsResponseGetAssetInspectionsResult getAssetInspections(java.lang.String strAssetKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[33]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/GetAssetInspections");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetAssetInspections"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {strAssetKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GetAssetInspectionsResponseGetAssetInspectionsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (GetAssetInspectionsResponseGetAssetInspectionsResult) org.apache.axis.utils.JavaUtils.convert(_resp, GetAssetInspectionsResponseGetAssetInspectionsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateAssetInspectionResponseUpdateAssetInspectionResult updateAssetInspection(UpdateAssetInspectionXmlAssetInsp xmlAssetInsp) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[34]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateAssetInspection");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateAssetInspection"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlAssetInsp});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateAssetInspectionResponseUpdateAssetInspectionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateAssetInspectionResponseUpdateAssetInspectionResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateAssetInspectionResponseUpdateAssetInspectionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdatePropertyContactResponseUpdatePropertyContactResult updatePropertyContact(UpdatePropertyContactXmlPC xmlPC) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[35]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdatePropertyContact");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdatePropertyContact"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlPC});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdatePropertyContactResponseUpdatePropertyContactResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdatePropertyContactResponseUpdatePropertyContactResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdatePropertyContactResponseUpdatePropertyContactResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public UpdateHMODetailsResponseUpdateHMODetailsResult updateHMODetails(UpdateHMODetailsXmlHMO xmlHMO) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[36]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/UpdateHMODetails");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateHMODetails"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlHMO});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (UpdateHMODetailsResponseUpdateHMODetailsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (UpdateHMODetailsResponseUpdateHMODetailsResult) org.apache.axis.utils.JavaUtils.convert(_resp, UpdateHMODetailsResponseUpdateHMODetailsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public IssueLicenceResponseIssueLicenceResult issueLicence(IssueLicenceXmlLicence xmlLicence) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[37]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.mvm.co.uk/webservices/M3PP/IssueLicence");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "IssueLicence"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xmlLicence});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (IssueLicenceResponseIssueLicenceResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (IssueLicenceResponseIssueLicenceResult) org.apache.axis.utils.JavaUtils.convert(_resp, IssueLicenceResponseIssueLicenceResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
