package com.placecube.digitalplace.local.licensing.npsm3pp.service;

import java.io.Serializable;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.model.LicenceAddressContext;
import com.placecube.digitalplace.local.licensing.model.LicenceApplicationContext;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.GetPropertiesResponseGetPropertiesResult;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.M3PPServiceSoap;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.OOAPropertyResponseOOAPropertyResult;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.OOAPropertyXmlA;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.UpdateLicenceResponseUpdateLicenceResult;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.UpdateLicenceXmlA;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.UpdateWorksheetResponseUpdateWorksheetResult;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.UpdateWorksheetXmlWork;
import com.placecube.digitalplace.local.licensing.npsm3pp.constants.LicenseApplicationContextKeys;
import com.placecube.digitalplace.local.licensing.npsm3pp.constants.M3ppSoapKeys;
import com.placecube.digitalplace.local.licensing.npsm3pp.model.M3ppLicence;
import com.placecube.digitalplace.local.licensing.npsm3pp.model.M3ppOOAProperty;
import com.placecube.digitalplace.local.licensing.npsm3pp.model.M3ppWorksheet;
import com.placecube.digitalplace.local.licensing.npsm3pp.util.MessageElementUtil;
import com.placecube.digitalplace.local.licensing.util.ObjectUtil;

@Component(immediate = true, service = M3ppRequestService.class)
public class M3ppRequestService {

	private static final Log LOG = LogFactoryUtil.getLog(M3ppRequestService.class);

	@Reference
	private M3ppAxisService m3ppAxisService;

	@Reference
	private ObjectFactoryService objectFactoryService;

	public String applyForNewLicence(long companyId, LicenceApplicationContext licenceApplicationContext) throws LicenceRenewalException {

		try {

			if (Validator.isNull(licenceApplicationContext.getLicenceAddressContext())) {
				licenceApplicationContext.setLicenceAddressContext(ObjectUtil.getNewLicenceAddressContext());
			}

			GetPropertiesResponseGetPropertiesResult getPropertiesResult = getProperties(companyId, licenceApplicationContext);

			if (!MessageElementUtil.checkForSuccessfulResponse(getPropertiesResult.get_any())) {
				LOG.debug("Exception while getting Properties from given UPRN.");
				OOAPropertyResponseOOAPropertyResult responseOOAPropertyResult = getOOAProperty(companyId, licenceApplicationContext);
				if (!MessageElementUtil.checkForSuccessfulResponse(responseOOAPropertyResult.get_any())) {
					LOG.debug("Exception while creating OOAProperties from given address.");
				} else {
					String ukey = MessageElementUtil.getUkeyFromOOAPropertyResponse(responseOOAPropertyResult.get_any());
					LOG.debug("Ukey Retrieved from OOAPropertyResult:" + ukey);
					licenceApplicationContext.getParameters().put(M3ppSoapKeys.REP_UPRN, ukey);
					licenceApplicationContext.getParameters().put(M3ppSoapKeys.REP_UKEY, ukey);
				}
			}

			UpdateWorksheetResponseUpdateWorksheetResult worksheetResult = updateWorksheet(companyId, licenceApplicationContext);

			String newWorkId = MessageElementUtil.getWorkIdFromResponse(worksheetResult.get_any(), M3ppSoapKeys.M3PP_WORKSHEET);

			if (Validator.isNotNull(newWorkId)) {

				UpdateLicenceResponseUpdateLicenceResult updateLicenceResult = updateLicence(companyId, licenceApplicationContext, newWorkId);

				return MessageElementUtil.getWorkIdFromResponse(updateLicenceResult.get_any(), M3ppSoapKeys.M3PP_LICENCE);

			} else {
				LOG.error("Exception while applying for new licence as fail to retrieve new worksheetId.");
				throw new LicenceRenewalException("Exception while applying for new licence as fail to retrieve new worksheetId.");
			}

		} catch (LicenceRenewalException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("Exception while applying for new licence.", e);
			throw new LicenceRenewalException("Exception while applying for new licence.", e);
		}

	}

	public OOAPropertyResponseOOAPropertyResult getOOAProperty(long companyId, LicenceApplicationContext licenceApplicationContext) throws Exception {

		OOAPropertyXmlA input = objectFactoryService.getOOAPropertyXmlA();

		LicenceAddressContext licenceAddressContext = licenceApplicationContext.getLicenceAddressContext();

		M3ppOOAProperty m3ppOOAProperty = new M3ppOOAProperty();

		m3ppOOAProperty.setAddr1(licenceAddressContext.getAddressLine1());
		m3ppOOAProperty.setAddr2(licenceAddressContext.getAddressLine2());
		m3ppOOAProperty.setAddr3(licenceAddressContext.getAddressLine3());
		m3ppOOAProperty.setAddr4(StringPool.BLANK);
		m3ppOOAProperty.setAddr5(StringPool.BLANK);
		m3ppOOAProperty.setAddr6(StringPool.BLANK);
		m3ppOOAProperty.setPostcde(licenceAddressContext.getPostcode());

		if (licenceApplicationContext.getTypeOfLicense().equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_DRIVER_TASK_GROUP)) {
			m3ppOOAProperty.setIndexType("P");
		} else if (licenceApplicationContext.getTypeOfLicense().equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_VEHICLE_TASK_GROUP)) {
			m3ppOOAProperty.setIndexType("V");
		} else if (licenceApplicationContext.getTypeOfLicense().equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_OPERATOR_TASK_GROUP)) {
			m3ppOOAProperty.setIndexType("C");
		}

		input.set_any(MessageElementUtil.getOOAPropertyMessageElements(m3ppOOAProperty));

		return getM3ppService(companyId).OOAProperty(input);
	}

	public GetPropertiesResponseGetPropertiesResult getProperties(long companyId, LicenceApplicationContext licenceApplicationContext) throws Exception {

		Map<String, Serializable> licenceParametersMap = licenceApplicationContext.getParameters();

		String uprn = GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.REP_UPRN));

		return getM3ppService(companyId).getProperties(uprn, StringPool.BLANK);
	}

	public UpdateLicenceResponseUpdateLicenceResult updateLicence(long companyId, LicenceApplicationContext licenceApplicationContext, String newWorkId) throws Exception {

		UpdateLicenceXmlA input = objectFactoryService.getUpdateLicenceXmlA();

		Map<String, Serializable> licenceParametersMap = licenceApplicationContext.getParameters();

		M3ppLicence m3ppLicence = new M3ppLicence();
		m3ppLicence.setApDate(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.AP_DATE)));
		m3ppLicence.setFeeDatePaid(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.FEE_DTE_PAID)));
		m3ppLicence.setFeeFeceipt(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.FEE_RECEIPT)));
		m3ppLicence.setFeePaid(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.FEE_PAID)));
		m3ppLicence.setlGroup(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.LGROUP)));
		m3ppLicence.setLicDayMon(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.LIC_DAY_MON)));
		m3ppLicence.setLicPer(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.LICPER)));
		m3ppLicence.setStdFee(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.STDFEE)));
		m3ppLicence.setVehColour(licenceApplicationContext.getVehicleColour());
		m3ppLicence.setVeheSize(licenceApplicationContext.getVehicleSize());
		m3ppLicence.setVehFext(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.VEH_FEXT)));
		m3ppLicence.setVehFuel(licenceApplicationContext.getVehicleFuel());
		m3ppLicence.setVehMake(licenceApplicationContext.getVehicleMake());
		m3ppLicence.setVehModel(licenceApplicationContext.getVehicleModel());
		m3ppLicence.setVehNotes(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.NOTES)));
		m3ppLicence.setVehPlate(licenceApplicationContext.getRegistrationNumber());
		m3ppLicence.setVehRegDate(licenceApplicationContext.getVehicleRegistrationDate());
		m3ppLicence.setVehRegno(licenceApplicationContext.getRegistrationNumber());
		m3ppLicence.setVehSeatsNo(licenceApplicationContext.getVehicleNumberOfSeats());
		m3ppLicence.setVehWc(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.VEH_WC)));
		m3ppLicence.setWorkId(newWorkId);

		input.set_any(MessageElementUtil.getUpdateLicenceMessageElements(m3ppLicence));

		return getM3ppService(companyId).updateLicence(input);
	}

	public UpdateWorksheetResponseUpdateWorksheetResult updateWorksheet(long companyId, LicenceApplicationContext licenceApplicationContext) throws Exception {

		UpdateWorksheetXmlWork input = objectFactoryService.getUpdateWorksheetXmlWork();

		Map<String, Serializable> licenceParametersMap = licenceApplicationContext.getParameters();

		LicenceAddressContext addressContext = licenceApplicationContext.getLicenceAddressContext();

		String subUprn;
		if (Validator.isNotNull(addressContext) && Validator.isNotNull(addressContext.getUprn())) {
			subUprn = addressContext.getUprn();
		} else {
			subUprn = GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.SUB_UPRN));
		}

		M3ppWorksheet m3ppWorksheet = new M3ppWorksheet();
		m3ppWorksheet.setApplication(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.APPLICATION)));
		m3ppWorksheet.setArea(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.AREA)));
		m3ppWorksheet.setDepartment(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.DEPARTMENT)));
		m3ppWorksheet.setDetails(licenceApplicationContext.getDetails());
		m3ppWorksheet.setDtRecd(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.DT_RECD)));
		m3ppWorksheet.setMessage(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.MESSAGE)));
		m3ppWorksheet.setMethod(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.METHOD)));
		m3ppWorksheet.setOfficer(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.OFFICER)));
		m3ppWorksheet.setRecDby(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.REC_DBY)));
		m3ppWorksheet.setRemarks(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.REMARKS)));
		m3ppWorksheet.setRepeMail(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.REP_EMAIL)));
		m3ppWorksheet.setRepFamilyName(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.REP_FAMILY_NAME)));
		m3ppWorksheet.setRepFirstName(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.REP_FIRST_NAME)));
		m3ppWorksheet.setRepTelHome(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.REP_TEL_HOME)));
		m3ppWorksheet.setRepTelMob(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.REP_TEL_MOB)));
		m3ppWorksheet.setRepTitle(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.REP_TITLE)));
		m3ppWorksheet.setRepUkey(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.REP_UKEY)));
		m3ppWorksheet.setRepUprn(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.REP_UPRN)));
		m3ppWorksheet.setSource(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.SOURCE)));
		m3ppWorksheet.setSubUkey(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.SUB_UKEY)));
		m3ppWorksheet.setSubUprn(subUprn);
		m3ppWorksheet.setTask(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.TASK)));
		m3ppWorksheet.setTaskGroup(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.TASK_GROUP)));
		m3ppWorksheet.setTmRecd(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.TM_RECD)));
		m3ppWorksheet.setWorkid(GetterUtil.getString(licenceParametersMap.get(M3ppSoapKeys.WORK_ID)));

		input.set_any(MessageElementUtil.getUpdateWorksheetMessageElements(m3ppWorksheet));

		return getM3ppService(companyId).updateWorksheet(input);
	}

	private M3PPServiceSoap getM3ppService(long companyId) throws Exception {
		return m3ppAxisService.getM3PPAxisService(companyId);
	}

}
