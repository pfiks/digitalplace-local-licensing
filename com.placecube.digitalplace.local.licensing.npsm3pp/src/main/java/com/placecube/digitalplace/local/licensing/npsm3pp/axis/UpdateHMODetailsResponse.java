/**
 * UpdateHMODetailsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class UpdateHMODetailsResponse  implements java.io.Serializable {
    private UpdateHMODetailsResponseUpdateHMODetailsResult updateHMODetailsResult;

    public UpdateHMODetailsResponse() {
    }

    public UpdateHMODetailsResponse(
           UpdateHMODetailsResponseUpdateHMODetailsResult updateHMODetailsResult) {
           this.updateHMODetailsResult = updateHMODetailsResult;
    }


    /**
     * Gets the updateHMODetailsResult value for this UpdateHMODetailsResponse.
     * 
     * @return updateHMODetailsResult
     */
    public UpdateHMODetailsResponseUpdateHMODetailsResult getUpdateHMODetailsResult() {
        return updateHMODetailsResult;
    }


    /**
     * Sets the updateHMODetailsResult value for this UpdateHMODetailsResponse.
     * 
     * @param updateHMODetailsResult
     */
    public void setUpdateHMODetailsResult(UpdateHMODetailsResponseUpdateHMODetailsResult updateHMODetailsResult) {
        this.updateHMODetailsResult = updateHMODetailsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateHMODetailsResponse)) return false;
        UpdateHMODetailsResponse other = (UpdateHMODetailsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.updateHMODetailsResult==null && other.getUpdateHMODetailsResult()==null) || 
             (this.updateHMODetailsResult!=null &&
              this.updateHMODetailsResult.equals(other.getUpdateHMODetailsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUpdateHMODetailsResult() != null) {
            _hashCode += getUpdateHMODetailsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateHMODetailsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateHMODetailsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateHMODetailsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateHMODetailsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateHMODetailsResponse>UpdateHMODetailsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
