/**
 * GetBulkyItemSlotsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class GetBulkyItemSlotsResponse  implements java.io.Serializable {
    private GetBulkyItemSlotsResponseGetBulkyItemSlotsResult getBulkyItemSlotsResult;

    public GetBulkyItemSlotsResponse() {
    }

    public GetBulkyItemSlotsResponse(
           GetBulkyItemSlotsResponseGetBulkyItemSlotsResult getBulkyItemSlotsResult) {
           this.getBulkyItemSlotsResult = getBulkyItemSlotsResult;
    }


    /**
     * Gets the getBulkyItemSlotsResult value for this GetBulkyItemSlotsResponse.
     * 
     * @return getBulkyItemSlotsResult
     */
    public GetBulkyItemSlotsResponseGetBulkyItemSlotsResult getGetBulkyItemSlotsResult() {
        return getBulkyItemSlotsResult;
    }


    /**
     * Sets the getBulkyItemSlotsResult value for this GetBulkyItemSlotsResponse.
     * 
     * @param getBulkyItemSlotsResult
     */
    public void setGetBulkyItemSlotsResult(GetBulkyItemSlotsResponseGetBulkyItemSlotsResult getBulkyItemSlotsResult) {
        this.getBulkyItemSlotsResult = getBulkyItemSlotsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetBulkyItemSlotsResponse)) return false;
        GetBulkyItemSlotsResponse other = (GetBulkyItemSlotsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getBulkyItemSlotsResult==null && other.getGetBulkyItemSlotsResult()==null) || 
             (this.getBulkyItemSlotsResult!=null &&
              this.getBulkyItemSlotsResult.equals(other.getGetBulkyItemSlotsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetBulkyItemSlotsResult() != null) {
            _hashCode += getGetBulkyItemSlotsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetBulkyItemSlotsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetBulkyItemSlotsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getBulkyItemSlotsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetBulkyItemSlotsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetBulkyItemSlotsResponse>GetBulkyItemSlotsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
