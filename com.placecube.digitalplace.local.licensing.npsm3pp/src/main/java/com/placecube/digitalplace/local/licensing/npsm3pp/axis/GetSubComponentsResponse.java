/**
 * GetSubComponentsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class GetSubComponentsResponse  implements java.io.Serializable {
    private GetSubComponentsResponseGetSubComponentsResult getSubComponentsResult;

    public GetSubComponentsResponse() {
    }

    public GetSubComponentsResponse(
           GetSubComponentsResponseGetSubComponentsResult getSubComponentsResult) {
           this.getSubComponentsResult = getSubComponentsResult;
    }


    /**
     * Gets the getSubComponentsResult value for this GetSubComponentsResponse.
     * 
     * @return getSubComponentsResult
     */
    public GetSubComponentsResponseGetSubComponentsResult getGetSubComponentsResult() {
        return getSubComponentsResult;
    }


    /**
     * Sets the getSubComponentsResult value for this GetSubComponentsResponse.
     * 
     * @param getSubComponentsResult
     */
    public void setGetSubComponentsResult(GetSubComponentsResponseGetSubComponentsResult getSubComponentsResult) {
        this.getSubComponentsResult = getSubComponentsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSubComponentsResponse)) return false;
        GetSubComponentsResponse other = (GetSubComponentsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getSubComponentsResult==null && other.getGetSubComponentsResult()==null) || 
             (this.getSubComponentsResult!=null &&
              this.getSubComponentsResult.equals(other.getGetSubComponentsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetSubComponentsResult() != null) {
            _hashCode += getGetSubComponentsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSubComponentsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetSubComponentsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getSubComponentsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetSubComponentsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetSubComponentsResponse>GetSubComponentsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
