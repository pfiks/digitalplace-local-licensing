/**
 * GetAvailableSlotsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class GetAvailableSlotsResponse  implements java.io.Serializable {
    private GetAvailableSlotsResponseGetAvailableSlotsResult getAvailableSlotsResult;

    public GetAvailableSlotsResponse() {
    }

    public GetAvailableSlotsResponse(
           GetAvailableSlotsResponseGetAvailableSlotsResult getAvailableSlotsResult) {
           this.getAvailableSlotsResult = getAvailableSlotsResult;
    }


    /**
     * Gets the getAvailableSlotsResult value for this GetAvailableSlotsResponse.
     * 
     * @return getAvailableSlotsResult
     */
    public GetAvailableSlotsResponseGetAvailableSlotsResult getGetAvailableSlotsResult() {
        return getAvailableSlotsResult;
    }


    /**
     * Sets the getAvailableSlotsResult value for this GetAvailableSlotsResponse.
     * 
     * @param getAvailableSlotsResult
     */
    public void setGetAvailableSlotsResult(GetAvailableSlotsResponseGetAvailableSlotsResult getAvailableSlotsResult) {
        this.getAvailableSlotsResult = getAvailableSlotsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAvailableSlotsResponse)) return false;
        GetAvailableSlotsResponse other = (GetAvailableSlotsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getAvailableSlotsResult==null && other.getGetAvailableSlotsResult()==null) || 
             (this.getAvailableSlotsResult!=null &&
              this.getAvailableSlotsResult.equals(other.getGetAvailableSlotsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetAvailableSlotsResult() != null) {
            _hashCode += getGetAvailableSlotsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAvailableSlotsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetAvailableSlotsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getAvailableSlotsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetAvailableSlotsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetAvailableSlotsResponse>GetAvailableSlotsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
