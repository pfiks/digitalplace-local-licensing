/**
 * UpdateLicenceResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class UpdateLicenceResponse  implements java.io.Serializable {
    private UpdateLicenceResponseUpdateLicenceResult updateLicenceResult;

    public UpdateLicenceResponse() {
    }

    public UpdateLicenceResponse(
           UpdateLicenceResponseUpdateLicenceResult updateLicenceResult) {
           this.updateLicenceResult = updateLicenceResult;
    }


    /**
     * Gets the updateLicenceResult value for this UpdateLicenceResponse.
     * 
     * @return updateLicenceResult
     */
    public UpdateLicenceResponseUpdateLicenceResult getUpdateLicenceResult() {
        return updateLicenceResult;
    }


    /**
     * Sets the updateLicenceResult value for this UpdateLicenceResponse.
     * 
     * @param updateLicenceResult
     */
    public void setUpdateLicenceResult(UpdateLicenceResponseUpdateLicenceResult updateLicenceResult) {
        this.updateLicenceResult = updateLicenceResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateLicenceResponse)) return false;
        UpdateLicenceResponse other = (UpdateLicenceResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.updateLicenceResult==null && other.getUpdateLicenceResult()==null) || 
             (this.updateLicenceResult!=null &&
              this.updateLicenceResult.equals(other.getUpdateLicenceResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUpdateLicenceResult() != null) {
            _hashCode += getUpdateLicenceResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateLicenceResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateLicenceResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateLicenceResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateLicenceResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateLicenceResponse>UpdateLicenceResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
