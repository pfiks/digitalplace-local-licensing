/**
 * GetSubComponents.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class GetSubComponents  implements java.io.Serializable {
    private java.lang.String strSiteKey;

    public GetSubComponents() {
    }

    public GetSubComponents(
           java.lang.String strSiteKey) {
           this.strSiteKey = strSiteKey;
    }


    /**
     * Gets the strSiteKey value for this GetSubComponents.
     * 
     * @return strSiteKey
     */
    public java.lang.String getStrSiteKey() {
        return strSiteKey;
    }


    /**
     * Sets the strSiteKey value for this GetSubComponents.
     * 
     * @param strSiteKey
     */
    public void setStrSiteKey(java.lang.String strSiteKey) {
        this.strSiteKey = strSiteKey;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSubComponents)) return false;
        GetSubComponents other = (GetSubComponents) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.strSiteKey==null && other.getStrSiteKey()==null) || 
             (this.strSiteKey!=null &&
              this.strSiteKey.equals(other.getStrSiteKey())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStrSiteKey() != null) {
            _hashCode += getStrSiteKey().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSubComponents.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetSubComponents"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strSiteKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strSiteKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
