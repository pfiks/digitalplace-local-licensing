package com.placecube.digitalplace.local.licensing.npsm3pp.constants;

public final class M3ppSoapKeys {

	public static final String ADDR1 = "ADDR1";

	public static final String ADDR2 = "ADDR2";

	public static final String ADDR3 = "ADDR3";

	public static final String ADDR4 = "ADDR4";

	public static final String ADDR5 = "ADDR5";

	public static final String ADDR6 = "ADDR6";

	public static final String AP_DATE = "APDATE";

	public static final String APPLICATION = "APPLICATION";

	public static final String AREA = "AREA";

	public static final String DEPARTMENT = "DEPARTMENT";

	public static final String DETAILS = "DETAILS";

	public static final String DT_RECD = "DTRECD";

	public static final String FEE_DTE_PAID = "FEEDTEPAID";

	public static final String FEE_PAID = "FEEPAID";

	public static final String FEE_RECEIPT = "FEERECEIPT";

	public static final String INDEXTYPE = "INDEXTYPE";

	public static final String LGROUP = "LGROUP";

	public static final String LIC_DAY_MON = "LICDAYMON";

	public static final String LICPER = "LICPER";

	public static final String M3PP_LICENCE = "M3PPLICENCE";

	public static final String M3PP_OOAPROPERTY = "M3PPOOAPROPERTY";

	public static final String M3PP_WORKSHEET = "M3PPWORKSHEET";

	public static final String MESSAGE = "MESSAGE";

	public static final String METHOD = "METHOD";

	public static final String MVM = "MVM";

	public static final String NOTES = "NOTES";

	public static final String OFFICER = "OFFICER";

	public static final String POSTCDE = "POSTCDE";

	public static final String PUBLIC_PROTECTION = "PUBLICPROTECTION";

	public static final String REC_DBY = "RECDBY";

	public static final String REMARKS = "REMARKS";

	public static final String REP_EMAIL = "REPEMAIL";

	public static final String REP_FAMILY_NAME = "REPFAMILYNAME";

	public static final String REP_FIRST_NAME = "REPFIRSTNAME";

	public static final String REP_TEL_HOME = "REPTELHOME";

	public static final String REP_TEL_MOB = "REPTELMOB";

	public static final String REP_TITLE = "REPTITLE";

	public static final String REP_UKEY = "REPUKEY";

	public static final String REP_UPRN = "REPUPRN";

	public static final String RESULT = "RESULT";

	public static final String RESULT_CODE = "RESULTCODE";

	public static final String RESULT_DESC = "RESULTDESC";

	public static final String RESULTS = "RESULTS";

	public static final String SOURCE = "SOURCE";

	public static final String STDFEE = "STDFEE";

	public static final String SUB_UKEY = "SUBUKEY";

	public static final String SUB_UPRN = "SUBUPRN";

	public static final String TASK = "TASK";

	public static final String TASK_GROUP = "TASKGROUP";

	public static final String TM_RECD = "TMRECD";

	public static final String TRADNAME = "TRADNAME";

	public static final String UKEY = "UKEY";

	public static final String VEH_COLOUR = "VEHCOLOUR";

	public static final String VEH_FEXT = "VEHFEXT";

	public static final String VEH_FUEL = "VEHFUEL";

	public static final String VEH_MAKE = "VEHMAKE";

	public static final String VEH_MODEL = "VEHMODEL";

	public static final String VEH_NOTES = "VEHNOTES";

	public static final String VEH_PLATE = "VEHPLATE";

	public static final String VEH_REG_DATE = "VEHREGDATE";

	public static final String VEH_REGNO = "VEHREGNO";

	public static final String VEH_SEATS_NO = "VEHSEATSNO";

	public static final String VEH_WC = "VEHWC";

	public static final String VEHE_SIZE = "VEHESIZE";

	public static final String WORK_ID = "WORKID";

	public static final String XMLNS_URL = "http://www.mvm.co.uk";

	private M3ppSoapKeys() {
	}

}
