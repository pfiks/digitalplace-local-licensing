/**
 * UpdateAdWorksOrderResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class UpdateAdWorksOrderResponse  implements java.io.Serializable {
    private UpdateAdWorksOrderResponseUpdateAdWorksOrderResult updateAdWorksOrderResult;

    public UpdateAdWorksOrderResponse() {
    }

    public UpdateAdWorksOrderResponse(
           UpdateAdWorksOrderResponseUpdateAdWorksOrderResult updateAdWorksOrderResult) {
           this.updateAdWorksOrderResult = updateAdWorksOrderResult;
    }


    /**
     * Gets the updateAdWorksOrderResult value for this UpdateAdWorksOrderResponse.
     * 
     * @return updateAdWorksOrderResult
     */
    public UpdateAdWorksOrderResponseUpdateAdWorksOrderResult getUpdateAdWorksOrderResult() {
        return updateAdWorksOrderResult;
    }


    /**
     * Sets the updateAdWorksOrderResult value for this UpdateAdWorksOrderResponse.
     * 
     * @param updateAdWorksOrderResult
     */
    public void setUpdateAdWorksOrderResult(UpdateAdWorksOrderResponseUpdateAdWorksOrderResult updateAdWorksOrderResult) {
        this.updateAdWorksOrderResult = updateAdWorksOrderResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateAdWorksOrderResponse)) return false;
        UpdateAdWorksOrderResponse other = (UpdateAdWorksOrderResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.updateAdWorksOrderResult==null && other.getUpdateAdWorksOrderResult()==null) || 
             (this.updateAdWorksOrderResult!=null &&
              this.updateAdWorksOrderResult.equals(other.getUpdateAdWorksOrderResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUpdateAdWorksOrderResult() != null) {
            _hashCode += getUpdateAdWorksOrderResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateAdWorksOrderResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">UpdateAdWorksOrderResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateAdWorksOrderResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "UpdateAdWorksOrderResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>UpdateAdWorksOrderResponse>UpdateAdWorksOrderResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
