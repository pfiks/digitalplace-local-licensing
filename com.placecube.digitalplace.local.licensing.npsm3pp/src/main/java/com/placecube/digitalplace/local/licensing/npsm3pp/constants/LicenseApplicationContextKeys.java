package com.placecube.digitalplace.local.licensing.npsm3pp.constants;

public final class LicenseApplicationContextKeys {

	public static final String AREA_CODE = "9020";

	public static final String ELEMENT_SUCCESS_CODE = "0";

	public static final String LGROUP_CODE = "1";

	public static final String LICENSE_TYPE_DRIVER_TASK_GROUP = "DRIVER";

	public static final String LICENSE_TYPE_DUAL_HC_PH_DRIVER = "DualHackneyPrivateHIreDriver";

	public static final String LICENSE_TYPE_EXECUTIVE_PRIVATE_HIRE_VEHICLE = "EXECUTIVE PRIVATE HIRE VEHICLE";

	public static final String LICENSE_TYPE_HACKNEY_CARRIAGE_DRIVER = "HACKNEY CARRIAGE DRIVER";

	public static final String LICENSE_TYPE_HACKNEY_CARRIAGE_VEHICLE = "HackneyCarriage";

	public static final String LICENSE_TYPE_OPERATOR_TASK_GROUP = "OPERATOR";

	public static final String LICENSE_TYPE_PRIVATE_DRIVER_HIRE = "PRIVATE DRIVER HIRE";

	public static final String LICENSE_TYPE_PRIVATE_DRIVER_HIRE_RESTRICTED = "RestrictedPrivateHire";

	public static final String LICENSE_TYPE_PRIVATE_HIRE_VEHICLE = "PrivateHire";

	public static final String LICENSE_TYPE_VEHICLE_TASK_GROUP = "VEHICLE";

	public static final String LN_CODE = "LN";

	public static final String METHOD_WEB_CODE = "WEB";

	public static final String NEW_LICENSE = "NEW";

	public static final String OFFICER_CODE = "LA";

	public static final String RECDBY_WEB_CODE = "WEB";

	public static final String SOURCE_CODE = "001";

	public static final String SUB_UKEY_CODE = "PI/500031199";

	public static final String TASK_CODE_DUAL_HC_PH_DRIVER = "L56";

	public static final String TASK_CODE_EXECUTIVE_PRIVATE_HIRE_VEHICLE = "L60";

	public static final String TASK_CODE_HACKNEY_CARRIAGE_DRIVER = "L51";

	public static final String TASK_CODE_HACKNEY_CARRIAGE_VEHICLE = "L50";

	public static final String TASK_CODE_PRIVATE_DRIVER_HIRE = "L52";

	public static final String TASK_CODE_PRIVATE_DRIVER_HIRE_RESTRICTED = "L55";

	public static final String TASK_CODE_PRIVATE_HIRE_VEHICLE = "L54";

	public static final String TASK_CODE_TAXI_OPERATOR = "L53";

	public static final String TASK_GROUP_CODE_L3 = "L3";

	public static final String TASK_GROUP_CODE_L4 = "L4";

	public static final String TASK_GROUP_CODE_L5 = "L5";

	public static final String TM_RECD_CODE = "HH:MM";

	private LicenseApplicationContextKeys() {
	}

}