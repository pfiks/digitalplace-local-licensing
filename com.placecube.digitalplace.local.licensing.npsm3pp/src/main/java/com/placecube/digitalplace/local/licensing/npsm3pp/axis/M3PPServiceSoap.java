/**
 * M3PPServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public interface M3PPServiceSoap extends java.rmi.Remote {

    /**
     * Get Version Number
     */
    public java.lang.String getVersion(java.lang.String verType) throws java.rmi.RemoteException;

    /**
     * Insert/Update Worksheet
     */
    public UpdateWorksheetResponseUpdateWorksheetResult updateWorksheet(UpdateWorksheetXmlWork xmlWork) throws java.rmi.RemoteException;

    /**
     * Get details of a Worksheet
     */
    public GetWorksheetDetsResponseGetWorksheetDetsResult getWorksheetDets(java.lang.String strWorkId, java.lang.String strWorkSearch, java.lang.String strSearchFld) throws java.rmi.RemoteException;

    /**
     * Insert/Update Action
     */
    public UpdateActionResponseUpdateActionResult updateAction(UpdateActionXmlAction xmlAction) throws java.rmi.RemoteException;

    /**
     * Get all actions for a Worksheet
     */
    public GetActionsResponseGetActionsResult getActions(java.lang.String strWorkId) throws java.rmi.RemoteException;

    /**
     * Get all sites for a UPRN
     */
    public GetSitesResponseGetSitesResult getSites(java.lang.String strUPRN) throws java.rmi.RemoteException;

    /**
     * Get worksheets based on criteria specified
     */
    public GetWorksheetsResponseGetWorksheetsResult getWorksheets(java.lang.String strUPRN, java.lang.String strRepUPRN, java.lang.String strDateFrom, java.lang.String strDateTo, java.lang.String strApplication, java.lang.String strTask, java.lang.String strOfficer, java.lang.String strOS, java.lang.String strSortColumn, java.lang.String strSortOrder) throws java.rmi.RemoteException;

    /**
     * Get list of codes for code type specified
     */
    public GetCodeListResponseGetCodeListResult getCodeList(java.lang.String strCodeType, java.lang.String strCodeFrom, java.lang.String strCodeTo, java.lang.String strOrder, java.lang.String strParameters) throws java.rmi.RemoteException;

    /**
     * Get all sub components for a Site
     */
    public GetSubComponentsResponseGetSubComponentsResult getSubComponents(java.lang.String strSiteKey) throws java.rmi.RemoteException;

    /**
     * Get all properties based on search criteria
     */
    public GetPropertiesResponseGetPropertiesResult getProperties(java.lang.String strSearch, java.lang.String strBuilding_Number) throws java.rmi.RemoteException;

    /**
     * Insert/Update Related Addresses for a worksheet
     */
    public UpdateRelatedAddrResponseUpdateRelatedAddrResult updateRelatedAddr(UpdateRelatedAddrXmlRelAddr xmlRelAddr) throws java.rmi.RemoteException;

    /**
     * Insert/Update Food Premise Registration data for a worksheet
     */
    public UpdateFoodPremiseRegResponseUpdateFoodPremiseRegResult updateFoodPremiseReg(UpdateFoodPremiseRegXmlA xmlA) throws java.rmi.RemoteException;

    /**
     * Insert/Update EPA data for a worksheet
     */
    public UpdateEPAResponseUpdateEPAResult updateEPA(UpdateEPAXmlA xmlA) throws java.rmi.RemoteException;

    /**
     * Insert/Update Licence data for a worksheet
     */
    public UpdateLicenceResponseUpdateLicenceResult updateLicence(UpdateLicenceXmlA xmlA) throws java.rmi.RemoteException;

    /**
     * Insert/Update/Delete bulky waste details
     */
    public UpdateBulkyResponseUpdateBulkyResult updateBulky(UpdateBulkyXmlA xmlA) throws java.rmi.RemoteException;

    /**
     * Get bulky waste details
     */
    public GetBulkyDetailsResponseGetBulkyDetailsResult getBulkyDetails(java.lang.String strWorkId) throws java.rmi.RemoteException;

    /**
     * Insert/Update/Delete abandoned vehicle details
     */
    public UpdateAVResponseUpdateAVResult updateAV(UpdateAVXmlA xmlA) throws java.rmi.RemoteException;

    /**
     * Get abandoned vehicle details
     */
    public GetAVDetailsResponseGetAVDetailsResult getAVDetails(java.lang.String strWorkId) throws java.rmi.RemoteException;

    /**
     * Insert/Update/Delete additional works details
     */
    public UpdateAdWorksOrderResponseUpdateAdWorksOrderResult updateAdWorksOrder(UpdateAdWorksOrderXmlA xmlA) throws java.rmi.RemoteException;

    /**
     * Get additional works details
     */
    public GetAdWorksOrderDetsResponseGetAdWorksOrderDetsResult getAdWorksOrderDets(java.lang.String strWorkId) throws java.rmi.RemoteException;

    /**
     * Get available slots
     */
    public GetAvailableSlotsResponseGetAvailableSlotsResult getAvailableSlots(java.lang.String strOfficer, java.lang.String strWorkId, java.lang.String strDaysAhead) throws java.rmi.RemoteException;

    /**
     * Book an appointment
     */
    public BookAppointmentResponseBookAppointmentResult bookAppointment(BookAppointmentXmlA xmlA) throws java.rmi.RemoteException;

    /**
     * Get available bulky item slots
     */
    public GetBulkyItemSlotsResponseGetBulkyItemSlotsResult getBulkyItemSlots(java.lang.String strWorkId) throws java.rmi.RemoteException;

    /**
     * Book a bulky item appointment
     */
    public BookBulkyItemAppointmentResponseBookBulkyItemAppointmentResult bookBulkyItemAppointment(BookBulkyItemAppointmentXmlA xmlA) throws java.rmi.RemoteException;

    /**
     * Get details of a Worksheet
     */
    public GetWorksheetAppointmentsResponseGetWorksheetAppointmentsResult getWorksheetAppointments(java.lang.String strWorkId) throws java.rmi.RemoteException;

    /**
     * Get round details
     */
    public GetRoundDetailsResponseGetRoundDetailsResult getRoundDetails(java.lang.String strUKEY, java.lang.String strSITEKEY) throws java.rmi.RemoteException;

    /**
     * Insert Out Of Area Address
     */
    public OOAPropertyResponseOOAPropertyResult OOAProperty(OOAPropertyXmlA xmlA) throws java.rmi.RemoteException;

    /**
     * Get Linked Worksheets based on a Worksheet Number
     */
    public GetLinkedWorksheetsResponseGetLinkedWorksheetsResult getLinkedWorksheets(java.lang.String strWorkId) throws java.rmi.RemoteException;

    /**
     * Insert/Update/Delete Monitor Notice details
     */
    public UpdateMonitorNoticesResponseUpdateMonitorNoticesResult updateMonitorNotices(UpdateMonitorNoticesXmlMN xmlMN) throws java.rmi.RemoteException;

    /**
     * Insert/Update/Delete Multiple Source details
     */
    public UpdateSourcesResponseUpdateSourcesResult updateSources(UpdateSourcesXmlS xmlS) throws java.rmi.RemoteException;

    /**
     * Get details of Assets
     */
    public GetAssetsResponseGetAssetsResult getAssets(java.lang.String strSiteKey, java.lang.String strUPRN, java.lang.String strAssetKey) throws java.rmi.RemoteException;

    /**
     * Insert/Update Asset
     */
    public UpdateAssetResponseUpdateAssetResult updateAsset(UpdateAssetXmlAsset xmlAsset) throws java.rmi.RemoteException;

    /**
     * Delete Asset
     */
    public DeleteAssetResponseDeleteAssetResult deleteAsset(java.lang.String strAssetKey) throws java.rmi.RemoteException;

    /**
     * Get details of Asset Inspections
     */
    public GetAssetInspectionsResponseGetAssetInspectionsResult getAssetInspections(java.lang.String strAssetKey) throws java.rmi.RemoteException;

    /**
     * Insert Asset Inspection
     */
    public UpdateAssetInspectionResponseUpdateAssetInspectionResult updateAssetInspection(UpdateAssetInspectionXmlAssetInsp xmlAssetInsp) throws java.rmi.RemoteException;

    /**
     * Insert/Update Property Contact
     */
    public UpdatePropertyContactResponseUpdatePropertyContactResult updatePropertyContact(UpdatePropertyContactXmlPC xmlPC) throws java.rmi.RemoteException;

    /**
     * Insert/Update HMO Details
     */
    public UpdateHMODetailsResponseUpdateHMODetailsResult updateHMODetails(UpdateHMODetailsXmlHMO xmlHMO) throws java.rmi.RemoteException;

    /**
     * Issue Licence worksheet
     */
    public IssueLicenceResponseIssueLicenceResult issueLicence(IssueLicenceXmlLicence xmlLicence) throws java.rmi.RemoteException;
}
