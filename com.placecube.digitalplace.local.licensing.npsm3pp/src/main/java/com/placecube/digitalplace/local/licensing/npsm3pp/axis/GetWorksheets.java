/**
 * GetWorksheets.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class GetWorksheets  implements java.io.Serializable {
    private java.lang.String strUPRN;

    private java.lang.String strRepUPRN;

    private java.lang.String strDateFrom;

    private java.lang.String strDateTo;

    private java.lang.String strApplication;

    private java.lang.String strTask;

    private java.lang.String strOfficer;

    private java.lang.String strOS;

    private java.lang.String strSortColumn;

    private java.lang.String strSortOrder;

    public GetWorksheets() {
    }

    public GetWorksheets(
           java.lang.String strUPRN,
           java.lang.String strRepUPRN,
           java.lang.String strDateFrom,
           java.lang.String strDateTo,
           java.lang.String strApplication,
           java.lang.String strTask,
           java.lang.String strOfficer,
           java.lang.String strOS,
           java.lang.String strSortColumn,
           java.lang.String strSortOrder) {
           this.strUPRN = strUPRN;
           this.strRepUPRN = strRepUPRN;
           this.strDateFrom = strDateFrom;
           this.strDateTo = strDateTo;
           this.strApplication = strApplication;
           this.strTask = strTask;
           this.strOfficer = strOfficer;
           this.strOS = strOS;
           this.strSortColumn = strSortColumn;
           this.strSortOrder = strSortOrder;
    }


    /**
     * Gets the strUPRN value for this GetWorksheets.
     * 
     * @return strUPRN
     */
    public java.lang.String getStrUPRN() {
        return strUPRN;
    }


    /**
     * Sets the strUPRN value for this GetWorksheets.
     * 
     * @param strUPRN
     */
    public void setStrUPRN(java.lang.String strUPRN) {
        this.strUPRN = strUPRN;
    }


    /**
     * Gets the strRepUPRN value for this GetWorksheets.
     * 
     * @return strRepUPRN
     */
    public java.lang.String getStrRepUPRN() {
        return strRepUPRN;
    }


    /**
     * Sets the strRepUPRN value for this GetWorksheets.
     * 
     * @param strRepUPRN
     */
    public void setStrRepUPRN(java.lang.String strRepUPRN) {
        this.strRepUPRN = strRepUPRN;
    }


    /**
     * Gets the strDateFrom value for this GetWorksheets.
     * 
     * @return strDateFrom
     */
    public java.lang.String getStrDateFrom() {
        return strDateFrom;
    }


    /**
     * Sets the strDateFrom value for this GetWorksheets.
     * 
     * @param strDateFrom
     */
    public void setStrDateFrom(java.lang.String strDateFrom) {
        this.strDateFrom = strDateFrom;
    }


    /**
     * Gets the strDateTo value for this GetWorksheets.
     * 
     * @return strDateTo
     */
    public java.lang.String getStrDateTo() {
        return strDateTo;
    }


    /**
     * Sets the strDateTo value for this GetWorksheets.
     * 
     * @param strDateTo
     */
    public void setStrDateTo(java.lang.String strDateTo) {
        this.strDateTo = strDateTo;
    }


    /**
     * Gets the strApplication value for this GetWorksheets.
     * 
     * @return strApplication
     */
    public java.lang.String getStrApplication() {
        return strApplication;
    }


    /**
     * Sets the strApplication value for this GetWorksheets.
     * 
     * @param strApplication
     */
    public void setStrApplication(java.lang.String strApplication) {
        this.strApplication = strApplication;
    }


    /**
     * Gets the strTask value for this GetWorksheets.
     * 
     * @return strTask
     */
    public java.lang.String getStrTask() {
        return strTask;
    }


    /**
     * Sets the strTask value for this GetWorksheets.
     * 
     * @param strTask
     */
    public void setStrTask(java.lang.String strTask) {
        this.strTask = strTask;
    }


    /**
     * Gets the strOfficer value for this GetWorksheets.
     * 
     * @return strOfficer
     */
    public java.lang.String getStrOfficer() {
        return strOfficer;
    }


    /**
     * Sets the strOfficer value for this GetWorksheets.
     * 
     * @param strOfficer
     */
    public void setStrOfficer(java.lang.String strOfficer) {
        this.strOfficer = strOfficer;
    }


    /**
     * Gets the strOS value for this GetWorksheets.
     * 
     * @return strOS
     */
    public java.lang.String getStrOS() {
        return strOS;
    }


    /**
     * Sets the strOS value for this GetWorksheets.
     * 
     * @param strOS
     */
    public void setStrOS(java.lang.String strOS) {
        this.strOS = strOS;
    }


    /**
     * Gets the strSortColumn value for this GetWorksheets.
     * 
     * @return strSortColumn
     */
    public java.lang.String getStrSortColumn() {
        return strSortColumn;
    }


    /**
     * Sets the strSortColumn value for this GetWorksheets.
     * 
     * @param strSortColumn
     */
    public void setStrSortColumn(java.lang.String strSortColumn) {
        this.strSortColumn = strSortColumn;
    }


    /**
     * Gets the strSortOrder value for this GetWorksheets.
     * 
     * @return strSortOrder
     */
    public java.lang.String getStrSortOrder() {
        return strSortOrder;
    }


    /**
     * Sets the strSortOrder value for this GetWorksheets.
     * 
     * @param strSortOrder
     */
    public void setStrSortOrder(java.lang.String strSortOrder) {
        this.strSortOrder = strSortOrder;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetWorksheets)) return false;
        GetWorksheets other = (GetWorksheets) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.strUPRN==null && other.getStrUPRN()==null) || 
             (this.strUPRN!=null &&
              this.strUPRN.equals(other.getStrUPRN()))) &&
            ((this.strRepUPRN==null && other.getStrRepUPRN()==null) || 
             (this.strRepUPRN!=null &&
              this.strRepUPRN.equals(other.getStrRepUPRN()))) &&
            ((this.strDateFrom==null && other.getStrDateFrom()==null) || 
             (this.strDateFrom!=null &&
              this.strDateFrom.equals(other.getStrDateFrom()))) &&
            ((this.strDateTo==null && other.getStrDateTo()==null) || 
             (this.strDateTo!=null &&
              this.strDateTo.equals(other.getStrDateTo()))) &&
            ((this.strApplication==null && other.getStrApplication()==null) || 
             (this.strApplication!=null &&
              this.strApplication.equals(other.getStrApplication()))) &&
            ((this.strTask==null && other.getStrTask()==null) || 
             (this.strTask!=null &&
              this.strTask.equals(other.getStrTask()))) &&
            ((this.strOfficer==null && other.getStrOfficer()==null) || 
             (this.strOfficer!=null &&
              this.strOfficer.equals(other.getStrOfficer()))) &&
            ((this.strOS==null && other.getStrOS()==null) || 
             (this.strOS!=null &&
              this.strOS.equals(other.getStrOS()))) &&
            ((this.strSortColumn==null && other.getStrSortColumn()==null) || 
             (this.strSortColumn!=null &&
              this.strSortColumn.equals(other.getStrSortColumn()))) &&
            ((this.strSortOrder==null && other.getStrSortOrder()==null) || 
             (this.strSortOrder!=null &&
              this.strSortOrder.equals(other.getStrSortOrder())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStrUPRN() != null) {
            _hashCode += getStrUPRN().hashCode();
        }
        if (getStrRepUPRN() != null) {
            _hashCode += getStrRepUPRN().hashCode();
        }
        if (getStrDateFrom() != null) {
            _hashCode += getStrDateFrom().hashCode();
        }
        if (getStrDateTo() != null) {
            _hashCode += getStrDateTo().hashCode();
        }
        if (getStrApplication() != null) {
            _hashCode += getStrApplication().hashCode();
        }
        if (getStrTask() != null) {
            _hashCode += getStrTask().hashCode();
        }
        if (getStrOfficer() != null) {
            _hashCode += getStrOfficer().hashCode();
        }
        if (getStrOS() != null) {
            _hashCode += getStrOS().hashCode();
        }
        if (getStrSortColumn() != null) {
            _hashCode += getStrSortColumn().hashCode();
        }
        if (getStrSortOrder() != null) {
            _hashCode += getStrSortOrder().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetWorksheets.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetWorksheets"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strUPRN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strUPRN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strRepUPRN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strRepUPRN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strDateFrom");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strDateFrom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strDateTo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strDateTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strApplication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strApplication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strTask");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strTask"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strOfficer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strOfficer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strOS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strOS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strSortColumn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strSortColumn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strSortOrder");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strSortOrder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
