/**
 * GetLinkedWorksheetsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class GetLinkedWorksheetsResponse  implements java.io.Serializable {
    private GetLinkedWorksheetsResponseGetLinkedWorksheetsResult getLinkedWorksheetsResult;

    public GetLinkedWorksheetsResponse() {
    }

    public GetLinkedWorksheetsResponse(
           GetLinkedWorksheetsResponseGetLinkedWorksheetsResult getLinkedWorksheetsResult) {
           this.getLinkedWorksheetsResult = getLinkedWorksheetsResult;
    }


    /**
     * Gets the getLinkedWorksheetsResult value for this GetLinkedWorksheetsResponse.
     * 
     * @return getLinkedWorksheetsResult
     */
    public GetLinkedWorksheetsResponseGetLinkedWorksheetsResult getGetLinkedWorksheetsResult() {
        return getLinkedWorksheetsResult;
    }


    /**
     * Sets the getLinkedWorksheetsResult value for this GetLinkedWorksheetsResponse.
     * 
     * @param getLinkedWorksheetsResult
     */
    public void setGetLinkedWorksheetsResult(GetLinkedWorksheetsResponseGetLinkedWorksheetsResult getLinkedWorksheetsResult) {
        this.getLinkedWorksheetsResult = getLinkedWorksheetsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetLinkedWorksheetsResponse)) return false;
        GetLinkedWorksheetsResponse other = (GetLinkedWorksheetsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getLinkedWorksheetsResult==null && other.getGetLinkedWorksheetsResult()==null) || 
             (this.getLinkedWorksheetsResult!=null &&
              this.getLinkedWorksheetsResult.equals(other.getGetLinkedWorksheetsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetLinkedWorksheetsResult() != null) {
            _hashCode += getGetLinkedWorksheetsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetLinkedWorksheetsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetLinkedWorksheetsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getLinkedWorksheetsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "GetLinkedWorksheetsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">>GetLinkedWorksheetsResponse>GetLinkedWorksheetsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
