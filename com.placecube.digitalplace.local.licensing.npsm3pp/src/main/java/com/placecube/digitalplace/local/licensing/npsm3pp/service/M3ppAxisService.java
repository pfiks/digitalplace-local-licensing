package com.placecube.digitalplace.local.licensing.npsm3pp.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.placecube.digitalplace.local.licensing.npsm3pp.axis.M3PPServiceLocator;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.M3PPServiceSoap;
import com.placecube.digitalplace.local.licensing.npsm3pp.configuration.M3ppCompanyConfiguration;

@Component(immediate = true, service = M3ppAxisService.class)
public class M3ppAxisService {

	@Reference
	private M3ppConfigurationService m3ppConfigurationService;

	public M3PPServiceSoap getM3PPAxisService(long companyId) throws Exception {
		M3ppCompanyConfiguration configuration = m3ppConfigurationService.getConfiguration(companyId);

		M3PPServiceLocator serviceLocation = getServiceLocator();
		serviceLocation.setM3PPServiceSoapEndpointAddress(m3ppConfigurationService.getM3ppEndpointUrl(configuration));

		return serviceLocation.getM3PPServiceSoap();
	}

	public M3PPServiceLocator getServiceLocator() {
		return new M3PPServiceLocator();
	}

}