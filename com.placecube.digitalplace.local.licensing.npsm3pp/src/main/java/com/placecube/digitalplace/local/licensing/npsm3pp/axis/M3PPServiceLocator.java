/**
 * M3PPServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class M3PPServiceLocator extends org.apache.axis.client.Service implements M3PPService {

/**
 * SIG 2.9.3.7
 */

    public M3PPServiceLocator() {
    }


    public M3PPServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public M3PPServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for M3PPServiceSoap
    private java.lang.String M3PPServiceSoap_address = "http://assure-web/Northgate/SIGWebServices/M3PPServ/M3PPServ.asmx";

    public java.lang.String getM3PPServiceSoapAddress() {
        return M3PPServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String M3PPServiceSoapWSDDServiceName = "M3PPServiceSoap";

    public java.lang.String getM3PPServiceSoapWSDDServiceName() {
        return M3PPServiceSoapWSDDServiceName;
    }

    public void setM3PPServiceSoapWSDDServiceName(java.lang.String name) {
        M3PPServiceSoapWSDDServiceName = name;
    }

    public M3PPServiceSoap getM3PPServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(M3PPServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getM3PPServiceSoap(endpoint);
    }

    public M3PPServiceSoap getM3PPServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            M3PPServiceSoapStub _stub = new M3PPServiceSoapStub(portAddress, this);
            _stub.setPortName(getM3PPServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setM3PPServiceSoapEndpointAddress(java.lang.String address) {
        M3PPServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (M3PPServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                M3PPServiceSoapStub _stub = new M3PPServiceSoapStub(new java.net.URL(M3PPServiceSoap_address), this);
                _stub.setPortName(getM3PPServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("M3PPServiceSoap".equals(inputPortName)) {
            return getM3PPServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "M3PPService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "M3PPServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("M3PPServiceSoap".equals(portName)) {
            setM3PPServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
