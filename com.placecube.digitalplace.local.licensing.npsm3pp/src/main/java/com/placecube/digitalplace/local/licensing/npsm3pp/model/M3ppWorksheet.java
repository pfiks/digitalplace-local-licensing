package com.placecube.digitalplace.local.licensing.npsm3pp.model;

public class M3ppWorksheet {

	private String application;
	private String area;
	private String department;
	private String details;
	private String dtRecd;
	private String message;
	private String method;
	private String officer;
	private String recDby;
	private String remarks;
	private String repeMail;
	private String repFamilyName;
	private String repFirstName;
	private String repTelHome;
	private String repTelMob;
	private String repTitle;
	private String repUkey;
	private String repUprn;
	private String source;
	private String subUkey;
	private String subUprn;
	private String task;
	private String taskGroup;
	private String tmRecd;
	private String workid;

	public M3ppWorksheet() {
	}

	public String getApplication() {
		return application;
	}

	public String getArea() {
		return area;
	}

	public String getDepartment() {
		return department;
	}

	public String getDetails() {
		return details;
	}

	public String getDtRecd() {
		return dtRecd;
	}

	public String getMessage() {
		return message;
	}

	public String getMethod() {
		return method;
	}

	public String getOfficer() {
		return officer;
	}

	public String getRecDby() {
		return recDby;
	}

	public String getRemarks() {
		return remarks;
	}

	public String getRepeMail() {
		return repeMail;
	}

	public String getRepFamilyName() {
		return repFamilyName;
	}

	public String getRepFirstName() {
		return repFirstName;
	}

	public String getRepTelHome() {
		return repTelHome;
	}

	public String getRepTelMob() {
		return repTelMob;
	}

	public String getRepTitle() {
		return repTitle;
	}

	public String getRepUkey() {
		return repUkey;
	}

	public String getRepUprn() {
		return repUprn;
	}

	public String getSource() {
		return source;
	}

	public String getSubUkey() {
		return subUkey;
	}

	public String getSubUprn() {
		return subUprn;
	}

	public String getTask() {
		return task;
	}

	public String getTaskGroup() {
		return taskGroup;
	}

	public String getTmRecd() {
		return tmRecd;
	}

	public String getWorkid() {
		return workid;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public void setDtRecd(String dtRecd) {
		this.dtRecd = dtRecd;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public void setOfficer(String officer) {
		this.officer = officer;
	}

	public void setRecDby(String recDby) {
		this.recDby = recDby;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setRepeMail(String repeMail) {
		this.repeMail = repeMail;
	}

	public void setRepFamilyName(String repFamilyName) {
		this.repFamilyName = repFamilyName;
	}

	public void setRepFirstName(String repFirstName) {
		this.repFirstName = repFirstName;
	}

	public void setRepTelHome(String repTelHome) {
		this.repTelHome = repTelHome;
	}

	public void setRepTelMob(String repTelMob) {
		this.repTelMob = repTelMob;
	}

	public void setRepTitle(String repTitle) {
		this.repTitle = repTitle;
	}

	public void setRepUkey(String repUkey) {
		this.repUkey = repUkey;
	}

	public void setRepUprn(String repUprn) {
		this.repUprn = repUprn;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setSubUkey(String subUkey) {
		this.subUkey = subUkey;
	}

	public void setSubUprn(String subUprn) {
		this.subUprn = subUprn;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public void setTaskGroup(String taskGroup) {
		this.taskGroup = taskGroup;
	}

	public void setTmRecd(String tmRecd) {
		this.tmRecd = tmRecd;
	}

	public void setWorkid(String workid) {
		this.workid = workid;
	}
}
