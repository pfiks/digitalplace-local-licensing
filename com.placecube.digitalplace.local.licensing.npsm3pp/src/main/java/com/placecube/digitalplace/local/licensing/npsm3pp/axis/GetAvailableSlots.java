/**
 * GetAvailableSlots.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class GetAvailableSlots  implements java.io.Serializable {
    private java.lang.String strOfficer;

    private java.lang.String strWorkId;

    private java.lang.String strDaysAhead;

    public GetAvailableSlots() {
    }

    public GetAvailableSlots(
           java.lang.String strOfficer,
           java.lang.String strWorkId,
           java.lang.String strDaysAhead) {
           this.strOfficer = strOfficer;
           this.strWorkId = strWorkId;
           this.strDaysAhead = strDaysAhead;
    }


    /**
     * Gets the strOfficer value for this GetAvailableSlots.
     * 
     * @return strOfficer
     */
    public java.lang.String getStrOfficer() {
        return strOfficer;
    }


    /**
     * Sets the strOfficer value for this GetAvailableSlots.
     * 
     * @param strOfficer
     */
    public void setStrOfficer(java.lang.String strOfficer) {
        this.strOfficer = strOfficer;
    }


    /**
     * Gets the strWorkId value for this GetAvailableSlots.
     * 
     * @return strWorkId
     */
    public java.lang.String getStrWorkId() {
        return strWorkId;
    }


    /**
     * Sets the strWorkId value for this GetAvailableSlots.
     * 
     * @param strWorkId
     */
    public void setStrWorkId(java.lang.String strWorkId) {
        this.strWorkId = strWorkId;
    }


    /**
     * Gets the strDaysAhead value for this GetAvailableSlots.
     * 
     * @return strDaysAhead
     */
    public java.lang.String getStrDaysAhead() {
        return strDaysAhead;
    }


    /**
     * Sets the strDaysAhead value for this GetAvailableSlots.
     * 
     * @param strDaysAhead
     */
    public void setStrDaysAhead(java.lang.String strDaysAhead) {
        this.strDaysAhead = strDaysAhead;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAvailableSlots)) return false;
        GetAvailableSlots other = (GetAvailableSlots) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.strOfficer==null && other.getStrOfficer()==null) || 
             (this.strOfficer!=null &&
              this.strOfficer.equals(other.getStrOfficer()))) &&
            ((this.strWorkId==null && other.getStrWorkId()==null) || 
             (this.strWorkId!=null &&
              this.strWorkId.equals(other.getStrWorkId()))) &&
            ((this.strDaysAhead==null && other.getStrDaysAhead()==null) || 
             (this.strDaysAhead!=null &&
              this.strDaysAhead.equals(other.getStrDaysAhead())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStrOfficer() != null) {
            _hashCode += getStrOfficer().hashCode();
        }
        if (getStrWorkId() != null) {
            _hashCode += getStrWorkId().hashCode();
        }
        if (getStrDaysAhead() != null) {
            _hashCode += getStrDaysAhead().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAvailableSlots.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetAvailableSlots"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strOfficer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strOfficer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strWorkId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strDaysAhead");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strDaysAhead"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
