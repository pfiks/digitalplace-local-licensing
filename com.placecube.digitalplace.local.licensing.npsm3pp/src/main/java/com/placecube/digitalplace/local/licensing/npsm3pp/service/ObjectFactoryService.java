package com.placecube.digitalplace.local.licensing.npsm3pp.service;

import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.local.licensing.npsm3pp.axis.OOAPropertyXmlA;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.UpdateLicenceXmlA;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.UpdateWorksheetXmlWork;

@Component(immediate = true, service = ObjectFactoryService.class)
public class ObjectFactoryService {

	public OOAPropertyXmlA getOOAPropertyXmlA() {
		return new OOAPropertyXmlA();
	}

	public UpdateLicenceXmlA getUpdateLicenceXmlA() {
		return new UpdateLicenceXmlA();
	}

	public UpdateWorksheetXmlWork getUpdateWorksheetXmlWork() {
		return new UpdateWorksheetXmlWork();
	}

}
