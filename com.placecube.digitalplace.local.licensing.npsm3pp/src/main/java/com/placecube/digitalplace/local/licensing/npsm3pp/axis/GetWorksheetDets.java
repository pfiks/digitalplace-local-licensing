/**
 * GetWorksheetDets.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.npsm3pp.axis;

public class GetWorksheetDets  implements java.io.Serializable {
    private java.lang.String strWorkId;

    private java.lang.String strWorkSearch;

    private java.lang.String strSearchFld;

    public GetWorksheetDets() {
    }

    public GetWorksheetDets(
           java.lang.String strWorkId,
           java.lang.String strWorkSearch,
           java.lang.String strSearchFld) {
           this.strWorkId = strWorkId;
           this.strWorkSearch = strWorkSearch;
           this.strSearchFld = strSearchFld;
    }


    /**
     * Gets the strWorkId value for this GetWorksheetDets.
     * 
     * @return strWorkId
     */
    public java.lang.String getStrWorkId() {
        return strWorkId;
    }


    /**
     * Sets the strWorkId value for this GetWorksheetDets.
     * 
     * @param strWorkId
     */
    public void setStrWorkId(java.lang.String strWorkId) {
        this.strWorkId = strWorkId;
    }


    /**
     * Gets the strWorkSearch value for this GetWorksheetDets.
     * 
     * @return strWorkSearch
     */
    public java.lang.String getStrWorkSearch() {
        return strWorkSearch;
    }


    /**
     * Sets the strWorkSearch value for this GetWorksheetDets.
     * 
     * @param strWorkSearch
     */
    public void setStrWorkSearch(java.lang.String strWorkSearch) {
        this.strWorkSearch = strWorkSearch;
    }


    /**
     * Gets the strSearchFld value for this GetWorksheetDets.
     * 
     * @return strSearchFld
     */
    public java.lang.String getStrSearchFld() {
        return strSearchFld;
    }


    /**
     * Sets the strSearchFld value for this GetWorksheetDets.
     * 
     * @param strSearchFld
     */
    public void setStrSearchFld(java.lang.String strSearchFld) {
        this.strSearchFld = strSearchFld;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetWorksheetDets)) return false;
        GetWorksheetDets other = (GetWorksheetDets) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.strWorkId==null && other.getStrWorkId()==null) || 
             (this.strWorkId!=null &&
              this.strWorkId.equals(other.getStrWorkId()))) &&
            ((this.strWorkSearch==null && other.getStrWorkSearch()==null) || 
             (this.strWorkSearch!=null &&
              this.strWorkSearch.equals(other.getStrWorkSearch()))) &&
            ((this.strSearchFld==null && other.getStrSearchFld()==null) || 
             (this.strSearchFld!=null &&
              this.strSearchFld.equals(other.getStrSearchFld())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStrWorkId() != null) {
            _hashCode += getStrWorkId().hashCode();
        }
        if (getStrWorkSearch() != null) {
            _hashCode += getStrWorkSearch().hashCode();
        }
        if (getStrSearchFld() != null) {
            _hashCode += getStrSearchFld().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetWorksheetDets.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", ">GetWorksheetDets"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strWorkId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strWorkSearch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strWorkSearch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strSearchFld");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.mvm.co.uk/webservices/M3PP", "strSearchFld"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
