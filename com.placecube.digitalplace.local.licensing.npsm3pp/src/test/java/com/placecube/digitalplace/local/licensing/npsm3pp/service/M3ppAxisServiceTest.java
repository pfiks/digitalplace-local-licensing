package com.placecube.digitalplace.local.licensing.npsm3pp.service;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.local.licensing.npsm3pp.axis.M3PPServiceLocator;
import com.placecube.digitalplace.local.licensing.npsm3pp.axis.M3PPServiceSoap;
import com.placecube.digitalplace.local.licensing.npsm3pp.configuration.M3ppCompanyConfiguration;

public class M3ppAxisServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final String M3PP_ENDPOINT_URL = "http://assure-web/Northgate/SIGWebServices/M3PPServ/M3PPServ.asmx";

	@InjectMocks
	private M3ppAxisService m3ppAxisService;

	@Mock
	private M3ppConfigurationService m3ppConfigurationService;

	@Mock
	private M3ppCompanyConfiguration mockM3ppCompanyConfiguration;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getServiceLocator_WhenNoErrors_ThenReturnsServiceLocator() {
		M3PPServiceLocator result = m3ppAxisService.getServiceLocator();

		assertThat(result, instanceOf(M3PPServiceLocator.class));
	}

	@Test
	public void getM3PPAxisService_WhenNoErrors_ThenReturnsM3PPServiceSoap() throws Exception {

		when(m3ppConfigurationService.getConfiguration(COMPANY_ID)).thenReturn(mockM3ppCompanyConfiguration);

		when(mockM3ppCompanyConfiguration.m3ppEndpointUrl()).thenReturn(M3PP_ENDPOINT_URL);

		when(m3ppConfigurationService.getM3ppEndpointUrl(mockM3ppCompanyConfiguration)).thenReturn(M3PP_ENDPOINT_URL);

		M3PPServiceSoap m3PPServiceSoap = m3ppAxisService.getM3PPAxisService(COMPANY_ID);

		assertThat(m3PPServiceSoap, instanceOf(M3PPServiceSoap.class));
	}

}