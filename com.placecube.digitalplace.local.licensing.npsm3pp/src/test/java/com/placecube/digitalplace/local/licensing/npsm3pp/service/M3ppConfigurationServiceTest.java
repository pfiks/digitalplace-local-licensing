package com.placecube.digitalplace.local.licensing.npsm3pp.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.licensing.npsm3pp.configuration.M3ppCompanyConfiguration;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class M3ppConfigurationServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final String M3PP_ENDPOINT_URL = "http://assure-web/Northgate/SIGWebServices/M3PPServ/M3PPServ.asmx";

	@InjectMocks
	private M3ppConfigurationService m3ppConfigurationService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private M3ppCompanyConfiguration mockM3ppCompanyConfiguration;

	@Test(expected = ConfigurationException.class)
	public void getConfiguration_WhenErrorGettingConfiguration_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(M3ppCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		m3ppConfigurationService.getConfiguration(COMPANY_ID);
	}

	@Test
	public void getConfiguration_WhenNoError_ThenReturnsConfiguration() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(M3ppCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockM3ppCompanyConfiguration);

		m3ppConfigurationService.getConfiguration(COMPANY_ID);
	}


	@Test(expected = ConfigurationException.class)
	public void getM3ppEndpointUrl_WhenM3ppEndpointUrlIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(mockM3ppCompanyConfiguration.m3ppEndpointUrl()).thenReturn(StringPool.BLANK);

		m3ppConfigurationService.getM3ppEndpointUrl(mockM3ppCompanyConfiguration);
	}

	@Test
	public void getM3ppEndpointUrl_WhenM3ppEndpointUrlIsNotBlank_ThenReturnsM3ppEndpointUrl() throws Exception {
		when(mockM3ppCompanyConfiguration.m3ppEndpointUrl()).thenReturn(M3PP_ENDPOINT_URL);

		String result = m3ppConfigurationService.getM3ppEndpointUrl(mockM3ppCompanyConfiguration);

		assertEquals(M3PP_ENDPOINT_URL, result);
	}


}
