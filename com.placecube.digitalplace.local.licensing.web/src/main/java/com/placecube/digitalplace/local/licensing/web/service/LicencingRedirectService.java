package com.placecube.digitalplace.local.licensing.web.service;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingViewKeys;

@Component(immediate = true, service = LicencingRedirectService.class)
public class LicencingRedirectService {

	public String getVehicleViewPageBasedOnStatus(RenderRequest renderRequest, VehicleLicenceDetails vehicleLicenceDetails) {
		switch (vehicleLicenceDetails.getStatus()) {

		case ERROR:
			return VehicleLicensingViewKeys.VEHICLE_RENEWAL_ERROR_VIEW;

		case EXPIRED:
			return VehicleLicensingViewKeys.VEHICLE_YOUR_LICENSE_HAS_EXPIRED_VIEW;

		case RENEWAL_TOO_EARLY:
			return VehicleLicensingViewKeys.VEHICLE_YOUR_RENEWAL_IS_TOO_SOON_VIEW;

		case RENEWAL_TOO_LATE:
			return getRenewalTooLateRedirect(renderRequest, vehicleLicenceDetails);

		default:
			return VehicleLicensingViewKeys.VEHICLE_CONFIRM_VEHICLE_DETAILS_VIEW;
		}

	}

	private String getRenewalTooLateRedirect(RenderRequest renderRequest, VehicleLicenceDetails vehicleLicenceDetails) {
		if (Validator.isNull(renderRequest.getAttribute("visitedRenewalIsTooLate"))) {
			return VehicleLicensingViewKeys.VEHICLE_CONFIRM_VEHICLE_DETAILS_VIEW;
		} else {
			renderRequest.setAttribute("renewalDateTime", vehicleLicenceDetails.getRenewalDate().toString());
			return VehicleLicensingViewKeys.VEHICLE_RENEWAL_IS_TOO_LATE_VIEW;
		}
	}
}
