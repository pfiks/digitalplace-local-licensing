package com.placecube.digitalplace.local.licensing.web.portlet.vehicle;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingMVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.portlet.licensing.SimpleFormMVCRenderCommand;

@Component(immediate = true, property = { "javax.portlet.name=" + VehicleLicensingPortletKeys.VEHICLE,
		"mvc.command.name=" + VehicleLicensingMVCCommandKeys.RENDER_YOUR_DOCUMENTS }, service = MVCRenderCommand.class)
public class YourDocumentsMVCRenderCommand extends SimpleFormMVCRenderCommand<VehicleLicenceDetails> {

	private String redirectPage = VehicleLicensingViewKeys.VEHICLE_YOUR_DOCUMENTS_VIEW;

	@Override
	public String getSuccessRedirectPage() {
		return redirectPage;
	}

	@Override
	public void handleForm(RenderRequest renderRequest, VehicleLicenceDetails licenceDetails) {
		String logbookChanges = ParamUtil.getString(renderRequest, RequestParamKeys.LOGBOOK_KEY);
		String meterChanges = ParamUtil.getString(renderRequest, RequestParamKeys.VEHICLE_METER_KEY);

		if (Validator.isNull(logbookChanges) || Validator.isNull(meterChanges)) {
			renderRequest.setAttribute("error", true);
			redirectPage = VehicleLicensingViewKeys.VEHICLE_CONFIRM_LOGBOOK_METER;
		} else {
			licenceDetails.setLogBookV5(logbookChanges.equals("yes"));
			licenceDetails.setMeter(meterChanges.equals("yes"));
		}
	}

}