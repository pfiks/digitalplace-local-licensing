package com.placecube.digitalplace.local.licensing.web.constants;

public enum LicenceLength {

	ONE_YEAR(),

	THREE_YEARS(),

	TWO_YEARS();

	private LicenceLength() {
	}

}