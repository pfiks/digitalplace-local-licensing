package com.placecube.digitalplace.local.licensing.web.portlet.licensing;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.model.LicenceStatus;
import com.placecube.digitalplace.local.licensing.service.LicensingService;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;
import com.placecube.digitalplace.local.licensing.web.service.LicenceUpdateService;
import com.placecube.digitalplace.local.licensing.web.service.LicensingSessionUtils;

@Component(immediate = true, property = { "javax.portlet.name=" + LicensingPortletKeys.LICENSING, "mvc.command.name=" + MVCCommandKeys.CONFIRM_DETAILS }, service = MVCRenderCommand.class)
public class ConfirmDetailsMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(ConfirmDetailsMVCRenderCommand.class);

	@Reference
	private LicenceUpdateService licenceUpdateService;

	@Reference
	private LicensingService licensingService;

	@Reference
	private LicensingSessionUtils licensingSessionUtils;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		LicenceDetails licenceDetails = getLicenceDetails(renderRequest);

		if (Validator.isNull(licenceDetails)) {
			renderRequest.setAttribute("error", true);
			return LicensingViewKeys.LICENCE_RENEWAL_LOOKUP;

		} else {
			String redirectPage = LicensingViewKeys.CONFIRM_YOUR_DETAILS;

			LicenceStatus licenceStatus = licenceDetails.getStatus();

			if (LicenceStatus.RENEWAL_TOO_EARLY.equals(licenceStatus) || LicenceStatus.EXPIRED.equals(licenceStatus)) {
				renderRequest.setAttribute(RequestParamKeys.LICENCE_STATUS, licenceStatus);
				return LicensingViewKeys.ERROR;
			} else if (LicenceStatus.CHECKS_DUE.equals(licenceStatus)) {
				redirectPage = LicensingViewKeys.CHECKS_DUE;
			}

			renderRequest.setAttribute(RequestParamKeys.LICENCE_DETAILS, licenceDetails);
			licensingSessionUtils.updateLicenceDetails(renderRequest, licenceDetails);
			return redirectPage;
		}
	}

	private LicenceDetails getLicenceDetails(RenderRequest renderRequest) {
		String referenceLicenceNumber = ParamUtil.getString(renderRequest, RequestParamKeys.REFERENCE_LICENCE_NUMBER);
		long companyId = PortalUtil.getCompanyId(renderRequest);
		LicenceDetails licenceDetails = licensingSessionUtils.getLicenceDetailsFromSession(renderRequest);

		if (invalidReferenceNumbers(referenceLicenceNumber, ParamUtil.getString(renderRequest, RequestParamKeys.DRIVING_LICENCE_NUMBER))) {
			licenceUpdateService.updateAddressOnLicence(companyId, ParamUtil.getString(renderRequest, "uprn"), licenceDetails);
			return licenceDetails;
		} else {
			return getLicenceDetailsFromReferenceNumber(companyId, referenceLicenceNumber);
		}
	}

	private LicenceDetails getLicenceDetailsFromReferenceNumber(long companyId, String referenceLicenceNumber) {
		try {
			return licensingService.getLicenceDetailsByReference(companyId, referenceLicenceNumber);
		} catch (LicenceRenewalException e) {
			LOG.error(e);
		}
		return null;
	}

	private boolean invalidReferenceNumbers(String referenceLicenceNumber, String drivingLicenceNumber) {
		return Validator.isNull(referenceLicenceNumber) || Validator.isNull(drivingLicenceNumber);
	}
}