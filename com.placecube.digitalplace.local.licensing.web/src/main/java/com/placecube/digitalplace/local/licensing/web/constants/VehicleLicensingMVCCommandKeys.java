package com.placecube.digitalplace.local.licensing.web.constants;

public final class VehicleLicensingMVCCommandKeys {

	public static final String ACTION_FILE_UPLOAD = "/action/file-upload";

	public static final String ACTION_VEHICLE_LICENCE_PAYMENT = "/action/vehicle-licence-payment";

	public static final String RENDER_CONFIRM_LOGBOOK_METER = "/render/logbook-meter-confirmation";

	public static final String RENDER_CONFIRM_VEHICLE_DETAILS = "/render/confirm-vehicle-details";

	public static final String RENDER_PAYMENT_ERROR = "/render/payment-error";

	public static final String RENDER_UPLOAD_DOCUMENTS_FAILED = "/render/upload-document-failed";

	public static final String RENDER_VEHICLE_CONFIRM_AND_PAY = "/render/confirm-and-pay";

	public static final String RENDER_VEHICLE_CONFIRMATION = "/render/vehicle-confirmation";

	public static final String RENDER_VEHICLE_DECLARATION = "/render/vehicle-declaration";

	public static final String RENDER_YOUR_DOCUMENTS = "/render/your-documents";

	public static final String RENDER_YOUR_RENEWAL_IS_TOO_LATE = "/render/your-renewal-is-late";

	public static final String RENDER_YOUR_VEHICLE_DETAILS = "/render/your-vehicle-details";

	private VehicleLicensingMVCCommandKeys() {

	}

}