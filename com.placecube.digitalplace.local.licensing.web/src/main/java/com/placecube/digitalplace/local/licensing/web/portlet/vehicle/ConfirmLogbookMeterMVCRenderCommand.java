package com.placecube.digitalplace.local.licensing.web.portlet.vehicle;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingMVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.portlet.licensing.SimpleFormMVCRenderCommand;

@Component(immediate = true, property = { "javax.portlet.name=" + VehicleLicensingPortletKeys.VEHICLE,
		"mvc.command.name=" + VehicleLicensingMVCCommandKeys.RENDER_CONFIRM_LOGBOOK_METER }, service = MVCRenderCommand.class)
public class ConfirmLogbookMeterMVCRenderCommand extends SimpleFormMVCRenderCommand<VehicleLicenceDetails> {

	@Override
	public String getSuccessRedirectPage() {
		return VehicleLicensingViewKeys.VEHICLE_CONFIRM_LOGBOOK_METER;
	}

	@Override
	public void handleForm(RenderRequest renderRequest, VehicleLicenceDetails licenceDetails) {
		String email = ParamUtil.getString(renderRequest, RequestParamKeys.EMAIL);
		if (Validator.isNotNull(email)) {
			licenceDetails.setEmail(email);
		}
	}
}