package com.placecube.digitalplace.local.licensing.web.lifecyclelistener;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.service.ServiceContext;

@Component(immediate = true, service = ServiceContextFactory.class)
public class ServiceContextFactory {

	public ServiceContext createServiceContext(long scopeGroupId, long userId) {
		ServiceContext serviceContext = new ServiceContext();

		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);

		serviceContext.setScopeGroupId(scopeGroupId);
		serviceContext.setUserId(userId);

		return serviceContext;
	}
}