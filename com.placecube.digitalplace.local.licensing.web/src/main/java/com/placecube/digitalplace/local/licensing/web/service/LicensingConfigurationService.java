package com.placecube.digitalplace.local.licensing.web.service;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.local.licensing.web.configuration.LicensingPortletInstanceConfiguration;
import com.placecube.digitalplace.local.licensing.web.configuration.vehicle.VehicleLicensingPortletInstanceConfiguration;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = LicensingConfigurationService.class)
public class LicensingConfigurationService {

	public LicensingPortletInstanceConfiguration getLicensingPortletInstanceConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		return themeDisplay.getPortletDisplay().getPortletInstanceConfiguration(LicensingPortletInstanceConfiguration.class);
	}

	public VehicleLicensingPortletInstanceConfiguration getVehicleLicensingPortletInstanceConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		return themeDisplay.getPortletDisplay().getPortletInstanceConfiguration(VehicleLicensingPortletInstanceConfiguration.class);
	}
}
