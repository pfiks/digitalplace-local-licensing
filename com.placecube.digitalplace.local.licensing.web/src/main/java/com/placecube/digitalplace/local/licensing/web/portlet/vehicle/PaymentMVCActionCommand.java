package com.placecube.digitalplace.local.licensing.web.portlet.vehicle;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingMVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.service.LicensingSessionUtils;
import com.placecube.digitalplace.local.licensing.web.service.PaymentHelperService;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;
import com.placecube.digitalplace.payment.service.PaymentService;

@Component(property = { "javax.portlet.name=" + VehicleLicensingPortletKeys.VEHICLE,
		"mvc.command.name=" + VehicleLicensingMVCCommandKeys.ACTION_VEHICLE_LICENCE_PAYMENT }, service = MVCActionCommand.class)
public class PaymentMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(PaymentMVCActionCommand.class);

	@Reference
	private LicensingSessionUtils licensingSessionUtils;

	@Reference
	private PaymentHelperService paymentHelperService;

	@Reference
	private PaymentService paymentService;

	@Override
	public void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {

		try {
			VehicleLicenceDetails licenceDetails = licensingSessionUtils.getVehicleLicenceDetails(actionRequest);
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DDMFormInstanceRecord.class.getName(), actionRequest);

			PaymentContext paymentContext = paymentHelperService.createPaymentContext(licenceDetails, serviceContext, actionRequest);
			PaymentResponse paymentResponse = paymentService.preparePayment(paymentContext, serviceContext);

			if (PaymentStatus.success() == paymentResponse.getStatus()) {
				hideDefaultSuccessMessage(actionRequest);
				actionRequest.setAttribute(WebKeys.REDIRECT, paymentResponse.getRedirectURL());
			} else {
				actionResponse.getRenderParameters().setValue("mvcPath", VehicleLicensingViewKeys.VEHICLE_PAYMENT_ERROR);
			}

			licensingSessionUtils.updatePaymentReference(actionRequest, paymentContext.getSalesReference());
			licensingSessionUtils.updateLicenceDetails(actionRequest, licenceDetails);

			quietlySendRedirect(actionRequest, actionResponse);

		} catch (Exception e) {
			LOG.error(e);
			throw new PortletException(e);
		}
	}

	private void quietlySendRedirect(ActionRequest actionRequest, ActionResponse actionResponse) {
		try {
			sendRedirect(actionRequest, actionResponse);
		} catch (Exception e) {
			LOG.error(e);
		}
	}
}