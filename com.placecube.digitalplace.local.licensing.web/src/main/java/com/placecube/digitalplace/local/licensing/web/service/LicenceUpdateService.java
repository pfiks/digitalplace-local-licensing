package com.placecube.digitalplace.local.licensing.web.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.service.LicensingService;

@Component(immediate = true, service = LicenceUpdateService.class)
public class LicenceUpdateService {

	private static final Log LOG = LogFactoryUtil.getLog(LicenceUpdateService.class);

	@Reference
	private AddressLookupService addressLookupService;

	@Reference
	private LicensingService licensingService;

	public void changeLicenseStatus(long companyId, VehicleLicenceDetails vehicleLicenceDetails) {
		try {
			licensingService.changeLicenceStatus(companyId, vehicleLicenceDetails.getLicenceId(), false);
		} catch (LicenceRenewalException e) {
			LOG.error("Couldn't change/update licence status", e);
		}
	}

	public void updateAddressOnLicence(long companyId, String uprn, LicenceDetails licenceDetails) {
		if (Validator.isNotNull(licenceDetails) && Validator.isNotNull(uprn)) {
			try {
				Optional<AddressContext> newAddress = addressLookupService.getByUprn(companyId, uprn);
				if (newAddress.isPresent()) {
					licenceDetails.setAddress(newAddress.get());
					licensingService.changeAddress(companyId, licenceDetails.getLicenceId(), licenceDetails.getAddress());
				} else {
					LOG.error("No Address found for UPRN: " + uprn);
				}
			} catch (PortalException e) {
				LOG.error(e);
			}
		}
	}
}
