package com.placecube.digitalplace.local.licensing.web.portlet.vehicle;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingMVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.portlet.licensing.SimpleFormMVCRenderCommand;

@Component(immediate = true, property = { "javax.portlet.name=" + VehicleLicensingPortletKeys.VEHICLE,
		"mvc.command.name=" + VehicleLicensingMVCCommandKeys.RENDER_YOUR_RENEWAL_IS_TOO_LATE }, service = MVCRenderCommand.class)
public class YourRenewalIsTooLateMVCRenderCommand extends SimpleFormMVCRenderCommand<VehicleLicenceDetails> {

	private String redirectPage = VehicleLicensingViewKeys.VEHICLE_RENEWAL_IS_TOO_LATE_VIEW;

	@Override
	public String getSuccessRedirectPage() {
		return redirectPage;
	}

	@Override
	public void handleForm(RenderRequest renderRequest, VehicleLicenceDetails licenceDetails) {

		if (licenceDetails.getFirstRegistered() == null) {
			redirectPage = VehicleLicensingViewKeys.VEHICLE_RENEWAL_ERROR_VIEW;

		} else {
			renderRequest.setAttribute("visitedRenewalIsTooLate", true);

			redirectPage = VehicleLicensingViewKeys.VEHICLE_CONFIRM_VEHICLE_DETAILS_VIEW;
		}
	}

}
