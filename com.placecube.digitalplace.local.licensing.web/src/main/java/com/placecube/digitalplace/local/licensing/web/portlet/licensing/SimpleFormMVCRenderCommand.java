package com.placecube.digitalplace.local.licensing.web.portlet.licensing;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;

public abstract class SimpleFormMVCRenderCommand<T extends LicenceDetails> implements MVCRenderCommand {

	public String getRedirectPage() {
		return getSuccessRedirectPage();
	}

	public abstract String getSuccessRedirectPage();

	public void addConfiguration(RenderRequest renderRequest) {
	}

	public void handleForm(RenderRequest renderRequest, T licenceDetails) {
	}

	@Override
	@SuppressWarnings("unchecked")
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		boolean submitted = ParamUtil.get(renderRequest, "formSubmitted", false);
		T licenceDetails = (T) renderRequest.getPortletSession().getAttribute(RequestParamKeys.LICENCE_DETAILS);

		if (licenceDetails == null) {
			return LicensingViewKeys.VIEW;
		}

		if (submitted) {
			handleForm(renderRequest, licenceDetails);
		}

		addConfiguration(renderRequest);

		renderRequest.getPortletSession().setAttribute(RequestParamKeys.LICENCE_DETAILS, licenceDetails);
		renderRequest.setAttribute(RequestParamKeys.LICENCE_DETAILS, licenceDetails);
		return getRedirectPage();
	}

}