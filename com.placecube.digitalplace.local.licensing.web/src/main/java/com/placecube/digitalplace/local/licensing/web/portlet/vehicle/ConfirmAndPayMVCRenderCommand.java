package com.placecube.digitalplace.local.licensing.web.portlet.vehicle;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.PortalUtil;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.service.LicensingService;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingMVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.service.LicensingSessionUtils;

@Component(immediate = true, property = { "javax.portlet.name=" + VehicleLicensingPortletKeys.VEHICLE,
		"mvc.command.name=" + VehicleLicensingMVCCommandKeys.RENDER_VEHICLE_CONFIRM_AND_PAY }, service = MVCRenderCommand.class)
public class ConfirmAndPayMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(FormFileUploadMVCActionCommand.class);

	@Reference
	private LicensingService licensingService;

	@Reference
	private LicensingSessionUtils licensingSessionUtils;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			long companyId = PortalUtil.getCompanyId(renderRequest);
			VehicleLicenceDetails vehicleLicenceDetails = licensingSessionUtils.getVehicleLicenceDetails(renderRequest);

			String licenceId = vehicleLicenceDetails.getLicenceId();

			BigDecimal renewalPayment = licensingService.getVehicleRenewalPrice(companyId, licenceId);
			renewalPayment = renewalPayment.setScale(2, RoundingMode.CEILING);

			renderRequest.setAttribute("renewalPrice", renewalPayment);
			renderRequest.setAttribute("renewalDateTime", licensingService.getVehicleRenewalDateTime(companyId, licenceId));

		} catch (LicenceRenewalException e) {
			LOG.error(e);
			return LicensingViewKeys.ERROR;
		}

		return VehicleLicensingViewKeys.VEHICLE_CONFIRM_AND_PAY_VIEW;
	}

}