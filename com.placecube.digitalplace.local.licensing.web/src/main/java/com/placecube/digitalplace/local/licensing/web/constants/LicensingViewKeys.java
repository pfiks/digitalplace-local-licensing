package com.placecube.digitalplace.local.licensing.web.constants;

public final class LicensingViewKeys {

	public static final String CHECKS_DUE = "/checksDue.jsp";

	public static final String CONFIRM_YOUR_DETAILS = "/confirmYourDetails.jsp";

	public static final String CONFIRMATION = "/confirmation.jsp";

	public static final String DECLARATION = "/declaration.jsp";

	public static final String ERROR = "/error.jsp";

	public static final String ERROR_PAYMENT = "/error_payment.jsp";

	public static final String LICENCE_RENEWAL_LENGTH = "/licenceRenewalLength.jsp";

	public static final String LICENCE_RENEWAL_LOOKUP = "/licenceRenewalLookup.jsp";

	public static final String VIEW = "/view.jsp";

	private LicensingViewKeys() {
	}
}
