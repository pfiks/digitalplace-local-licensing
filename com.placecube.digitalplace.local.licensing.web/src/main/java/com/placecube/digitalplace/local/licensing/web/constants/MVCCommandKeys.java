package com.placecube.digitalplace.local.licensing.web.constants;

public final class MVCCommandKeys {

	public static final String ACTION_LICENCE_PAYMENT = "/action/licence-payment";

	public static final String CONFIRM_DETAILS = "/render/confirm-details";

	public static final String VIEW_CONFIRMATION = "/render/confirmation";

	public static final String VIEW_DECLERATION = "/render/declaration-confirm";

	public static final String ERROR = "/render/error";

	public static final String LICENCE_LENGTH = "/render/licence-renewal-length";

	private MVCCommandKeys() {
	}

}