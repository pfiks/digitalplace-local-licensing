package com.placecube.digitalplace.local.licensing.web.configuration;

public final class LicensingConfigurationConstants {

	public static final String NOTIFICATION_EMAIL_REPLY_TO_ID = "notificationEmailReplyToId";

	public static final String NOTIFICATION_TEMPLATE_ID = "notificationTemplateId";

	public static final String PAYMENT_ACCOUNT_ID = "paymentAccountId";

	public static final String PAYMENT_ONE_YEAR_AMOUNT = "paymentOneYearAmount";

	public static final String PAYMENT_TWO_YEAR_AMOUNT = "paymentTwoYearAmount";

	public static final String PAYMENT_THREE_YEAR_AMOUNT = "paymentThreeYearAmount";

	private LicensingConfigurationConstants() {
	}
}
