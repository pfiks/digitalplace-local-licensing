package com.placecube.digitalplace.local.licensing.web.service;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.service.LicensingService;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;
import com.placecube.digitalplace.local.licensing.web.portlet.vehicle.ConfirmVehicleDetailsMVCRenderCommand;

@Component(immediate = true, service = LicencingRetrievalService.class)
public class LicencingRetrievalService {

	private static final Log LOG = LogFactoryUtil.getLog(ConfirmVehicleDetailsMVCRenderCommand.class);

	@Reference
	private LicensingService licensingService;

	@Reference
	private LicensingSessionUtils licensingSessionUtils;

	public VehicleLicenceDetails getVehichleDetails(PortletRequest portletRequest, String referenceLicenceNumber, long companyId) {
		VehicleLicenceDetails vehicleLicenceDetails = (VehicleLicenceDetails) portletRequest.getPortletSession().getAttribute(RequestParamKeys.LICENCE_DETAILS);
		if (vehicleLicenceDetails == null) {
			try {
				vehicleLicenceDetails = (VehicleLicenceDetails) licensingService.getLicenceDetailsByReference(companyId, referenceLicenceNumber);
			} catch (LicenceRenewalException e) {
				LOG.error(e);
			}
		}
		return vehicleLicenceDetails;
	}
}
