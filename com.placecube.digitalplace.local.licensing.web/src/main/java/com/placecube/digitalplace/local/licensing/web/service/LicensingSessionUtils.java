package com.placecube.digitalplace.local.licensing.web.service;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;

@Component(immediate = true, service = LicensingSessionUtils.class)
public class LicensingSessionUtils {

	public LicenceDetails getLicenceDetailsFromSession(PortletRequest portletRequest) {
		return (LicenceDetails) portletRequest.getPortletSession().getAttribute(RequestParamKeys.LICENCE_DETAILS);
	}

	public String getPaymentReference(PortletRequest portletRequest) {
		return GetterUtil.getString(portletRequest.getPortletSession().getAttribute(RequestParamKeys.PAYMENT_REFERENCE));
	}

	public String getRenewalPeriod(PortletRequest portletRequest) {
		return GetterUtil.getString(portletRequest.getPortletSession().getAttribute(RequestParamKeys.RENEWAL_PERIOD));
	}

	public VehicleLicenceDetails getVehicleLicenceDetails(PortletRequest portletRequest) {
		return (VehicleLicenceDetails) portletRequest.getPortletSession().getAttribute(RequestParamKeys.LICENCE_DETAILS);
	}

	public void updateLicenceDetails(PortletRequest portletRequest, LicenceDetails licenceDetails) {
		portletRequest.getPortletSession().setAttribute(RequestParamKeys.LICENCE_DETAILS, licenceDetails);
	}

	public void updatePaymentReference(PortletRequest portletRequest, String salesReference) {
		portletRequest.getPortletSession().setAttribute(RequestParamKeys.PAYMENT_REFERENCE, salesReference);
	}

	public void updateRenewalPerionInSession(PortletRequest portletRequest, String renewalPeriod) {
		portletRequest.getPortletSession().setAttribute(RequestParamKeys.RENEWAL_PERIOD, renewalPeriod);
	}
}
