package com.placecube.digitalplace.local.licensing.web.portlet.licensing;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.service.LicensingService;
import com.placecube.digitalplace.local.licensing.web.configuration.LicensingPortletInstanceConfiguration;
import com.placecube.digitalplace.local.licensing.web.constants.LicenceLength;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;
import com.placecube.digitalplace.local.licensing.web.service.LicenceNotificationService;
import com.placecube.digitalplace.local.licensing.web.service.LicensingConfigurationService;
import com.placecube.digitalplace.local.licensing.web.service.LicensingSessionUtils;
import com.placecube.digitalplace.local.licensing.web.service.PaymentHelperService;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.service.PaymentService;

@Component(immediate = true, property = { "javax.portlet.name=" + LicensingPortletKeys.LICENSING, "mvc.command.name=" + MVCCommandKeys.VIEW_CONFIRMATION }, service = MVCRenderCommand.class)
public class ConfirmationMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(ConfirmationMVCRenderCommand.class);

	@Reference
	private LicenceNotificationService licenceNotificationService;

	@Reference
	private LicensingConfigurationService licensingConfigurationService;

	@Reference
	private LicensingService licensingService;

	@Reference
	private LicensingSessionUtils licensingSessionUtils;

	@Reference
	private PaymentHelperService paymentHelperService;

	@Reference
	private PaymentService paymentService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			LicenceDetails licenceDetails = licensingSessionUtils.getLicenceDetailsFromSession(renderRequest);
			String renewalPeriodString = licensingSessionUtils.getRenewalPeriod(renderRequest);
			String paymentReference = licensingSessionUtils.getPaymentReference(renderRequest);

			if (Validator.isNotNull(licenceDetails) && Validator.isNotNull(renewalPeriodString) && Validator.isNotNull(paymentReference)) {

				LicenceLength renewalPeriod = LicenceLength.valueOf(renewalPeriodString);
				ServiceContext serviceContext = ServiceContextFactory.getInstance(DDMFormInstanceRecord.class.getName(), renderRequest);
				PaymentStatus status = paymentService.getPaymentStatus(paymentReference, serviceContext);

				if (PaymentStatus.success().equals(status)) {
					long companyId = PortalUtil.getCompanyId(renderRequest);
					licensingService.renewLicence(companyId, licenceDetails.getLicenceId(), renewalPeriod.ordinal() + 1, paymentReference);

					LicensingPortletInstanceConfiguration configuration = licensingConfigurationService.getLicensingPortletInstanceConfiguration(serviceContext.getThemeDisplay());

					sendConfirmationEmail(licenceDetails, renewalPeriod, configuration, serviceContext);

					renderRequest.setAttribute(RequestParamKeys.LICENCE_DETAILS, licenceDetails);
					renderRequest.setAttribute(RequestParamKeys.RENEWAL_PAYMENT, paymentHelperService.getPaymentAmount(configuration, renewalPeriod));

					return LicensingViewKeys.CONFIRMATION;
				} else {
					return LicensingViewKeys.ERROR_PAYMENT;
				}
			}
		} catch (Exception e) {
			LOG.error(e);
		}

		return LicensingViewKeys.ERROR;
	}

	private void sendConfirmationEmail(LicenceDetails licenceDetails, LicenceLength renewalPeriod, LicensingPortletInstanceConfiguration configuration, ServiceContext serviceContext) {
		try {
			licenceNotificationService.sendConfirmationEmail(licenceDetails, paymentHelperService.getPaymentAmount(configuration, renewalPeriod), configuration.notificationTemplateId(),
					configuration.notificationEmailReplyToId(), serviceContext);
		} catch (com.placecube.digitalplace.notifications.exception.NotificationsException | ConfigurationException ne) {
			LOG.error(ne, ne);
		}
	}

}