package com.placecube.digitalplace.local.licensing.web.configuration.vehicle;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.local.licensing.web.configuration.vehicle.VehicleLicensingPortletInstanceConfiguration")
public interface VehicleLicensingPortletInstanceConfiguration {

	@Meta.AD(required = false, deflt = "DRIVINGLICENCEEMAIL")
	String notificationTemplateId();

	@Meta.AD(required = false, deflt = "noreply@placecube.com")
	String notificationEmailReplyToId();

	@Meta.AD(required = false, deflt = "")
	String paymentAccountId();

	@Meta.AD(required = false, deflt = "/licensing")
	String fileUploadPath();
}
