package com.placecube.digitalplace.local.licensing.web.configuration.vehicle;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.service.LicensingConfigurationService;

@Component(immediate = true, property = "javax.portlet.name=" + VehicleLicensingPortletKeys.VEHICLE, service = ConfigurationAction.class)
public class VehicleLicensingPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private LicensingConfigurationService licencingConfigurationService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		httpServletRequest.setAttribute("configuration", licencingConfigurationService.getVehicleLicensingPortletInstanceConfiguration(themeDisplay));

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		String fileUploadPath = ParamUtil.getString(actionRequest, VehicleLicensingConfigurationConstants.FILE_UPLOAD_PATH, StringPool.BLANK);
		setPreference(actionRequest, VehicleLicensingConfigurationConstants.FILE_UPLOAD_PATH, fileUploadPath);

		String notificationEmailReplyToId = ParamUtil.getString(actionRequest, VehicleLicensingConfigurationConstants.NOTIFICATION_EMAIL_REPLY_TO_ID, StringPool.BLANK);
		setPreference(actionRequest, VehicleLicensingConfigurationConstants.NOTIFICATION_EMAIL_REPLY_TO_ID, String.valueOf(notificationEmailReplyToId));

		String notificationTemplateId = ParamUtil.getString(actionRequest, VehicleLicensingConfigurationConstants.NOTIFICATION_TEMPLATE_ID, StringPool.BLANK);
		setPreference(actionRequest, VehicleLicensingConfigurationConstants.NOTIFICATION_TEMPLATE_ID, String.valueOf(notificationTemplateId));

		String paymentAccountId = ParamUtil.getString(actionRequest, VehicleLicensingConfigurationConstants.PAYMENT_ACCOUNT_ID, StringPool.BLANK);
		setPreference(actionRequest, VehicleLicensingConfigurationConstants.PAYMENT_ACCOUNT_ID, paymentAccountId);

		super.processAction(portletConfig, actionRequest, actionResponse);
	}
}
