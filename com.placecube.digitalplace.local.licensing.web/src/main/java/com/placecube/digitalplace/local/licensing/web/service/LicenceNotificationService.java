package com.placecube.digitalplace.local.licensing.web.service;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendMailContext;
import com.placecube.digitalplace.notifications.model.SendMailContext.SendMailContextBuilder;
import com.placecube.digitalplace.notifications.service.NotificationsService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = LicenceNotificationService.class)
public class LicenceNotificationService {

	@Reference
	private NotificationsService notificationsService;

	public void sendConfirmationEmail(LicenceDetails licenceDetails, BigDecimal renewalprice, String templateId, String replyEmailTemplateId, ServiceContext serviceContext)
			throws NotificationsException, ConfigurationException {
		Map<String, Object> personalisation = getEmailPersonalisation(licenceDetails, renewalprice, serviceContext.getLocale());

		SendMailContextBuilder sendMailContextBuilder = notificationsService.createSendMailContextBuilder(templateId, licenceDetails.getEmail(), personalisation, licenceDetails.getTaxiLicenceNumber(),
				replyEmailTemplateId);
		SendMailContext sendMailContext = sendMailContextBuilder.build();

		notificationsService.sendMail(sendMailContext, serviceContext);
	}

	private Map<String, Object> getEmailPersonalisation(LicenceDetails licenceDetails, BigDecimal renewalPrice, Locale locale) {
		DateTimeFormatter displayFormat = DateTimeFormatter.ofPattern("dd MMM yyyy");
		String languageKey = getLicenceType(licenceDetails.getTaxiLicenceNumber());

		Map<String, Object> personalisation = new HashMap<>();

		personalisation.put("ADDRESS", licenceDetails.getAddress().getFullAddress());
		personalisation.put("NEWLICENCENUMBER", licenceDetails.getDrivingLicenceNumber());
		personalisation.put("LICENCETYPE", LanguageUtil.get(locale, languageKey));
		personalisation.put("NEWEXPIRYDATE", licenceDetails.getRenewalDate().format(displayFormat));
		personalisation.put("AMOUNTPAID", renewalPrice);
		personalisation.put("ORDERDATE", LocalDate.now().format(displayFormat));

		return personalisation;
	}

	private String getLicenceType(String lincenceNumber) {
		if (lincenceNumber.endsWith("TXPHD")) {
			return "licence.type.txphd";
		} else if (lincenceNumber.endsWith("TXHCD")) {
			return "licence.type.txhcd";
		}
		return StringPool.BLANK;
	}

}