package com.placecube.digitalplace.local.licensing.web.configuration;

import java.math.BigDecimal;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.local.licensing.web.configuration.LicensingPortletInstanceConfiguration")
public interface LicensingPortletInstanceConfiguration {

	@Meta.AD(required = false, deflt = "DRIVINGLICENCEEMAIL")
	String notificationTemplateId();

	@Meta.AD(required = false, deflt = "noreply@placecube.com")
	String notificationEmailReplyToId();

	@Meta.AD(required = false, deflt = "56.40")
	BigDecimal paymentOneYearAmount();

	@Meta.AD(required = false, deflt = "90.68")
	BigDecimal paymentTwoYearAmount();

	@Meta.AD(required = false, deflt = "153.72")
	BigDecimal paymentThreeYearAmount();

	@Meta.AD(required = false, deflt = "")
	String paymentAccountId();
}
