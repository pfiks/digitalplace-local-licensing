package com.placecube.digitalplace.local.licensing.web.portlet.vehicle;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingMVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.service.LicenceUpdateService;
import com.placecube.digitalplace.local.licensing.web.service.LicencingRedirectService;
import com.placecube.digitalplace.local.licensing.web.service.LicencingRetrievalService;
import com.placecube.digitalplace.local.licensing.web.service.LicensingSessionUtils;

@Component(immediate = true, property = { "javax.portlet.name=" + VehicleLicensingPortletKeys.VEHICLE,
		"mvc.command.name=" + VehicleLicensingMVCCommandKeys.RENDER_CONFIRM_VEHICLE_DETAILS }, service = MVCRenderCommand.class)
public class ConfirmVehicleDetailsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private LicenceUpdateService licenceUpdateService;

	@Reference
	private LicencingRedirectService licencingRedirectService;

	@Reference
	private LicencingRetrievalService licencingRetrievalService;

	@Reference
	private LicensingSessionUtils licensingSessionUtils;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		String referenceLicenceNumber = ParamUtil.getString(renderRequest, RequestParamKeys.REFERENCE_LICENCE_NUMBER);
		long companyId = PortalUtil.getCompanyId(renderRequest);

		VehicleLicenceDetails vehicleLicenceDetails = licencingRetrievalService.getVehichleDetails(renderRequest, referenceLicenceNumber, companyId);

		renderRequest.setAttribute(RequestParamKeys.LICENCE_DETAILS, vehicleLicenceDetails);
		licensingSessionUtils.updateLicenceDetails(renderRequest, vehicleLicenceDetails);

		return getView(renderRequest, referenceLicenceNumber, companyId, vehicleLicenceDetails);
	}

	private String getView(RenderRequest renderRequest, String referenceLicenceNumber, long companyId, VehicleLicenceDetails vehicleLicenceDetails) {
		if (Validator.isNotNull(vehicleLicenceDetails)) {
			licenceUpdateService.changeLicenseStatus(companyId, vehicleLicenceDetails);

			return licencingRedirectService.getVehicleViewPageBasedOnStatus(renderRequest, vehicleLicenceDetails);

		} else if (isLicenseInvalid(referenceLicenceNumber, vehicleLicenceDetails, renderRequest)) {

			renderRequest.setAttribute("error", true);
			return VehicleLicensingViewKeys.VEHICLE_RENEWAL_LOOKUP_VIEW;

		} else {
			return VehicleLicensingViewKeys.VEHICLE_CONFIRM_VEHICLE_DETAILS_VIEW;
		}
	}

	private boolean isLicenseInvalid(String referenceLicenceNumber, VehicleLicenceDetails vehicleLicenceDetails, RenderRequest renderRequest) {
		return vehicleLicenceDetails == null || Validator.isNull(referenceLicenceNumber) || Validator.isNull(ParamUtil.getString(renderRequest, RequestParamKeys.VEHICLE_REGISTRATION_NUMBER));
	}

}