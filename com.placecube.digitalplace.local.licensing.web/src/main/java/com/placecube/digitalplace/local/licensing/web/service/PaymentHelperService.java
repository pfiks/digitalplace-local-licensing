package com.placecube.digitalplace.local.licensing.web.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.portlet.ActionRequest;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.service.LicensingService;
import com.placecube.digitalplace.local.licensing.web.configuration.LicensingPortletInstanceConfiguration;
import com.placecube.digitalplace.local.licensing.web.configuration.vehicle.VehicleLicensingPortletInstanceConfiguration;
import com.placecube.digitalplace.local.licensing.web.constants.LicenceLength;
import com.placecube.digitalplace.local.licensing.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingMVCCommandKeys;
import com.placecube.digitalplace.notifications.service.NotificationsService;
import com.placecube.digitalplace.payment.model.PaymentContext;

@Component(immediate = true, service = PaymentHelperService.class)
public class PaymentHelperService {

	private static final String RENDER_URL_CONST_PART = "&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&_";

	@Reference
	private LicensingService licencingService;

	@Reference
	private LicensingSessionUtils licensingSessionUtils;

	@Reference
	private NotificationsService notificationsService;

	@Reference
	private LicensingConfigurationService licensingConfigurationService;

	public PaymentContext createPaymentContext(LicenceDetails licenceDetails, LicenceLength selectedExtension, BigDecimal amount, ServiceContext serviceContext) throws PortalException {
		String salesDescription = getSalesDescription(selectedExtension.toString(), "Driver");

		String returnURL = generateRenderURL(serviceContext, MVCCommandKeys.VIEW_CONFIRMATION);

		LicensingPortletInstanceConfiguration configuration = licensingConfigurationService.getLicensingPortletInstanceConfiguration(serviceContext.getThemeDisplay());

		return getPaymentContext(licenceDetails, serviceContext, salesDescription, amount, returnURL, configuration.paymentAccountId());
	}

	public PaymentContext createPaymentContext(VehicleLicenceDetails licenceDetails, ServiceContext serviceContext, ActionRequest actionRequest) throws PortalException {
		long companyId = PortalUtil.getCompanyId(actionRequest);

		String salesDescription = getSalesDescription(licencingService.getVehicleRenewalLicenseLength(companyId, licenceDetails.getLicenceId()), "Vehicle");
		BigDecimal amount = licencingService.getVehicleRenewalPrice(companyId, licenceDetails.getLicenceId());
		String returnURL = generateRenderURL(serviceContext, VehicleLicensingMVCCommandKeys.RENDER_VEHICLE_CONFIRMATION);

		VehicleLicensingPortletInstanceConfiguration configuration = licensingConfigurationService.getVehicleLicensingPortletInstanceConfiguration(serviceContext.getThemeDisplay());

		return getPaymentContext(licenceDetails, serviceContext, salesDescription, amount, returnURL, configuration.paymentAccountId());
	}

	public String generateRenderURL(ServiceContext serviceContext, String renderCommandName) {
		return serviceContext.getPortalURL() + serviceContext.getCurrentURL().split("\\?")[0] + "?p_p_id=" + serviceContext.getPortletId() + RENDER_URL_CONST_PART + serviceContext.getPortletId()
				+ "_mvcRenderCommandName=" + renderCommandName;
	}

	public LicenceLength getExtensionPeriod(PortletRequest portletRequest) {
		String extensionPeriodString = ParamUtil.getString(portletRequest, RequestParamKeys.RENEWAL_PERIOD);
		String sessionParam = licensingSessionUtils.getRenewalPeriod(portletRequest);

		if (Validator.isNull(extensionPeriodString) && Validator.isNotNull(sessionParam)) {
			return LicenceLength.valueOf(sessionParam);
		}

		LicenceLength result = LicenceLength.valueOf(extensionPeriodString);
		licensingSessionUtils.updateRenewalPerionInSession(portletRequest, extensionPeriodString);
		return result;
	}

	public BigDecimal getPaymentAmount(LicensingPortletInstanceConfiguration configuration, LicenceLength selectedExtension) {

		BigDecimal paymentAmount = configuration.paymentOneYearAmount();

		if (selectedExtension == LicenceLength.ONE_YEAR) {
			paymentAmount = configuration.paymentOneYearAmount();
		} else if (selectedExtension == LicenceLength.TWO_YEARS) {
			paymentAmount = configuration.paymentTwoYearAmount();
		} else if (selectedExtension == LicenceLength.THREE_YEARS) {
			paymentAmount = configuration.paymentThreeYearAmount();
		}

		return paymentAmount.setScale(2, RoundingMode.CEILING);
	}

	private PaymentContext getPaymentContext(LicenceDetails licenceDetails, ServiceContext serviceContext, String salesDescription, BigDecimal amount, String returnURL, String accountId) {
		String backURL = GetterUtil.getString(serviceContext.getAttribute("redirect"));

		return new PaymentContext(accountId, salesDescription, licenceDetails.getTaxiLicenceNumber(), licenceDetails.getTaxiLicenceNumber(), amount, licenceDetails.getFirstName(),
				licenceDetails.getLastName(), licenceDetails.getEmail(), licenceDetails.getAddress(), returnURL, backURL);
	}

	private String getSalesDescription(String selected, String type) {
		return "Council Taxi " + type + " Renewal Order for " + selected.toLowerCase().replace('_', ' ');
	}

}