package com.placecube.digitalplace.local.licensing.web.portlet.licensing;

import java.math.RoundingMode;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.web.configuration.LicensingPortletInstanceConfiguration;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;
import com.placecube.digitalplace.local.licensing.web.service.LicensingConfigurationService;

@Component(immediate = true, property = { "javax.portlet.name=" + LicensingPortletKeys.LICENSING, "mvc.command.name=" + MVCCommandKeys.LICENCE_LENGTH }, service = MVCRenderCommand.class)
public class LicenceLengthMVCRenderCommand extends SimpleFormMVCRenderCommand<LicenceDetails> {

	private static final Log LOG = LogFactoryUtil.getLog(ConfirmationMVCRenderCommand.class);

	private String redirectPage = LicensingViewKeys.LICENCE_RENEWAL_LENGTH;

	@Reference
	private LicensingConfigurationService licensingConfigurationService;

	@Override
	public void addConfiguration(RenderRequest renderRequest) {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			LicensingPortletInstanceConfiguration configuration = licensingConfigurationService.getLicensingPortletInstanceConfiguration(themeDisplay);
			renderRequest.setAttribute(RequestParamKeys.PAYMENT_ONE_YEAR_AMOUNT, configuration.paymentOneYearAmount().setScale(2, RoundingMode.CEILING));
			renderRequest.setAttribute(RequestParamKeys.PAYMENT_TWO_YEAR_AMOUNT, configuration.paymentTwoYearAmount().setScale(2, RoundingMode.CEILING));
			renderRequest.setAttribute(RequestParamKeys.PAYMENT_THREE_YEAR_AMOUNT, configuration.paymentThreeYearAmount().setScale(2, RoundingMode.CEILING));
		} catch (ConfigurationException e) {
			LOG.error("Error getting configuration: " + e.getMessage(),e);
		}
	}

	@Override
	public String getSuccessRedirectPage() {
		return redirectPage;
	}

	@Override
	public void handleForm(RenderRequest renderRequest, LicenceDetails licenceDetails) {
		boolean noCriminalConvictionsConfirmed = ParamUtil.get(renderRequest, RequestParamKeys.NO_CRIMINAL_CONVICTION, false);
		boolean noInvestigationConfirmed = ParamUtil.get(renderRequest, RequestParamKeys.NO_INVESTIGATION, false);
		boolean noMedicalConditionsConfirmed = ParamUtil.get(renderRequest, RequestParamKeys.NO_MEDICAL_CONDITION, false);

		if (!noCriminalConvictionsConfirmed || !noInvestigationConfirmed || !noMedicalConditionsConfirmed) {
			redirectPage = LicensingViewKeys.DECLARATION;
		}
	}
}