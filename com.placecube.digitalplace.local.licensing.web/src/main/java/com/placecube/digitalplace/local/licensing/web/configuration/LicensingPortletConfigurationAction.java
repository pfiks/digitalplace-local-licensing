package com.placecube.digitalplace.local.licensing.web.configuration;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.service.LicensingConfigurationService;

@Component(immediate = true, property = "javax.portlet.name=" + LicensingPortletKeys.LICENSING, service = ConfigurationAction.class)
public class LicensingPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private LicensingConfigurationService licencingConfigurationService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		httpServletRequest.setAttribute("configuration", licencingConfigurationService.getLicensingPortletInstanceConfiguration(themeDisplay));

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		String notificationEmailReplyToId = ParamUtil.getString(actionRequest, LicensingConfigurationConstants.NOTIFICATION_EMAIL_REPLY_TO_ID, StringPool.BLANK);
		setPreference(actionRequest, LicensingConfigurationConstants.NOTIFICATION_EMAIL_REPLY_TO_ID, String.valueOf(notificationEmailReplyToId));

		String notificationTemplateId = ParamUtil.getString(actionRequest, LicensingConfigurationConstants.NOTIFICATION_TEMPLATE_ID, StringPool.BLANK);
		setPreference(actionRequest, LicensingConfigurationConstants.NOTIFICATION_TEMPLATE_ID, String.valueOf(notificationTemplateId));

		String paymentAccountId = ParamUtil.getString(actionRequest, LicensingConfigurationConstants.PAYMENT_ACCOUNT_ID, StringPool.BLANK);
		setPreference(actionRequest, LicensingConfigurationConstants.PAYMENT_ACCOUNT_ID, paymentAccountId);

		BigDecimal paymentOneYearAmount = BigDecimal.valueOf(ParamUtil.getDouble(actionRequest, LicensingConfigurationConstants.PAYMENT_ONE_YEAR_AMOUNT, 0.0d));
		paymentOneYearAmount = paymentOneYearAmount.setScale(2, RoundingMode.CEILING);
		setPreference(actionRequest, LicensingConfigurationConstants.PAYMENT_ONE_YEAR_AMOUNT, String.valueOf(paymentOneYearAmount));

		BigDecimal paymentTwoYearAmount = BigDecimal.valueOf(ParamUtil.getDouble(actionRequest, LicensingConfigurationConstants.PAYMENT_TWO_YEAR_AMOUNT, 0.0d));
		paymentTwoYearAmount = paymentTwoYearAmount.setScale(2, RoundingMode.CEILING);
		setPreference(actionRequest, LicensingConfigurationConstants.PAYMENT_TWO_YEAR_AMOUNT, String.valueOf(paymentTwoYearAmount));

		BigDecimal paymentThreeYearAmount = BigDecimal.valueOf(ParamUtil.getDouble(actionRequest, LicensingConfigurationConstants.PAYMENT_THREE_YEAR_AMOUNT, 0.0d));
		paymentThreeYearAmount = paymentThreeYearAmount.setScale(2, RoundingMode.CEILING);
		setPreference(actionRequest, LicensingConfigurationConstants.PAYMENT_THREE_YEAR_AMOUNT, String.valueOf(paymentThreeYearAmount));

		super.processAction(portletConfig, actionRequest, actionResponse);
	}
}
