package com.placecube.digitalplace.local.licensing.web.portlet.vehicle;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseFormMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.FileItem;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.fileupload.service.FileUploadService;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.web.configuration.vehicle.VehicleLicensingPortletInstanceConfiguration;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;
import com.placecube.digitalplace.local.licensing.web.constants.SessionErrorsKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingMVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.service.LicensingConfigurationService;

@Component(property = { "javax.portlet.name=" + PortletKeys.DOCUMENT_LIBRARY, "javax.portlet.name=" + PortletKeys.DOCUMENT_LIBRARY_ADMIN, "javax.portlet.name=" + VehicleLicensingPortletKeys.VEHICLE,
		"mvc.command.name=" + VehicleLicensingMVCCommandKeys.ACTION_FILE_UPLOAD }, service = MVCActionCommand.class)
public class FormFileUploadMVCActionCommand extends BaseFormMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(FormFileUploadMVCActionCommand.class);

	@Reference
	private FileUploadService fileUploadService;

	@Reference
	private Portal portal;

	@Reference
	private LicensingConfigurationService licensingConfigurationService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		List<FileItem> fileItems = new ArrayList<>();
		if (SessionErrors.isEmpty(actionRequest)) {
			final VehicleLicenceDetails licenceDetails = (VehicleLicenceDetails) actionRequest.getPortletSession().getAttribute(RequestParamKeys.LICENCE_DETAILS);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFolder.class.getName(), actionRequest);

			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

			long groupId = themeDisplay.getScopeGroupId();
			serviceContext.setCompanyId(themeDisplay.getCompanyId());
			serviceContext.setScopeGroupId(groupId);

			VehicleLicensingPortletInstanceConfiguration configuration = licensingConfigurationService.getVehicleLicensingPortletInstanceConfiguration(themeDisplay);
			String folderName = configuration.fileUploadPath() + StringPool.FORWARD_SLASH + licenceDetails.getTaxiLicenceNumber();

			UploadPortletRequest uploadPortletRequest = portal.getUploadPortletRequest(actionRequest);

			Set<Entry<String, FileItem[]>> fileItemMapEntrySet = uploadPortletRequest.getMultipartParameterMap().entrySet();
			for (Entry<String, FileItem[]> fileItemEntry : fileItemMapEntrySet) {
				List<FileItem> entryfileItems = Stream.of(fileItemEntry.getValue()).filter(fileItem -> Validator.isNotNull(fileItem.getFileName()) && !fileItem.getFileName().equals(StringPool.BLANK))
						.collect(Collectors.toList());

				String name = fileItemEntry.getKey();

				for (FileItem fileItem : entryfileItems) {
					String fileName = fileItem.getFullFileName();
					try (InputStream inputStream = uploadPortletRequest.getFileAsStream(name, true)) {
						fileUploadService.uploadFile(fileName, folderName, inputStream, Optional.empty(), serviceContext);
						fileItems.add(fileItem);
					} catch (PortalException e) {
						SessionErrors.add(actionRequest, SessionErrorsKeys.FILE_NOT_UPLOADED);
						LOG.error(e);
					}
				}
			}

			actionRequest.setAttribute(RequestParamKeys.LICENCE_DETAILS, licenceDetails);
			actionRequest.setAttribute("fileItems", fileItems);
			actionRequest.setAttribute("decimalFormat", new DecimalFormat("0.00"));

			SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE);
			actionResponse.getRenderParameters().setValue(RequestParamKeys.MVC_PATH, VehicleLicensingViewKeys.VEHICLE_DECLARATION_VIEW);
		} else {
			SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			actionResponse.getRenderParameters().setValue(RequestParamKeys.MVC_PATH, VehicleLicensingViewKeys.VEHICLE_UPLOAD_DOCUMENTS_FAILED_VIEW);

		}
	}

	@Override
	protected void doValidateForm(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		UploadPortletRequest uploadPortletRequest = portal.getUploadPortletRequest(actionRequest);

		Set<Entry<String, FileItem[]>> fileItemMapEntrySet = uploadPortletRequest.getMultipartParameterMap().entrySet();
		for (Entry<String, FileItem[]> fileItemEntry : fileItemMapEntrySet) {
			List<FileItem> entryfileItems = Stream.of(fileItemEntry.getValue()).filter(fileItem -> fileItem.getFileName() != null && !fileItem.getFileName().equals("")).collect(Collectors.toList());
			for (FileItem fileItem : entryfileItems) {

				if (fileItem.getSize() <= 0) {
					SessionErrors.add(actionRequest, SessionErrorsKeys.FILE_NOT_UPLOADED);
				}
				if (isFileGreaterThanMb(fileItem, 5)) {
					SessionErrors.add(actionRequest, SessionErrorsKeys.FILE_NOT_UPLOADED);
				}

				if (!isFileOfType(fileItem, "jpeg", "jpg", "png", "pdf")) {
					SessionErrors.add(actionRequest, SessionErrorsKeys.FILE_NOT_UPLOADED);
				}
			}
			if (!SessionErrors.isEmpty(actionRequest)) {
				actionResponse.getRenderParameters().setValue(RequestParamKeys.MVC_PATH, VehicleLicensingViewKeys.VEHICLE_UPLOAD_DOCUMENTS_FAILED_VIEW);
				break;
			}
		}

	}

	private boolean isFileGreaterThanMb(FileItem fileItem, int sizeMb) {
		return Math.round(fileItem.getSize() * Math.pow(10, -6)) > sizeMb;
	}

	private boolean isFileOfType(FileItem fileItem, String... strArr) {
		return Stream.of(strArr).parallel().anyMatch(fileItem.getFileNameExtension()::contains);
	}

}
