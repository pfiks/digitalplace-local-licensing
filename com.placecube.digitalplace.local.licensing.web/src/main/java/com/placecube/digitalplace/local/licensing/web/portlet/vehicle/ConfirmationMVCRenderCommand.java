package com.placecube.digitalplace.local.licensing.web.portlet.vehicle;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.service.LicensingService;
import com.placecube.digitalplace.local.licensing.web.configuration.vehicle.VehicleLicensingPortletInstanceConfiguration;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingMVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.service.LicenceNotificationService;
import com.placecube.digitalplace.local.licensing.web.service.LicensingConfigurationService;
import com.placecube.digitalplace.local.licensing.web.service.LicensingSessionUtils;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.service.PaymentService;

@Component(immediate = true, property = { "javax.portlet.name=" + VehicleLicensingPortletKeys.VEHICLE,
		"mvc.command.name=" + VehicleLicensingMVCCommandKeys.RENDER_VEHICLE_CONFIRMATION }, service = MVCRenderCommand.class)
public class ConfirmationMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(ConfirmationMVCRenderCommand.class);

	@Reference
	private LicenceNotificationService licenceNotificationService;

	@Reference
	private LicensingService licensingService;

	@Reference
	private LicensingSessionUtils licensingSessionUtils;

	@Reference
	private PaymentService paymentService;

	@Reference
	private LicensingConfigurationService licensingConfigurationService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {
			VehicleLicenceDetails licenceDetails = licensingSessionUtils.getVehicleLicenceDetails(renderRequest);
			String paymentReference = licensingSessionUtils.getPaymentReference(renderRequest);

			if (Validator.isNotNull(licenceDetails) && Validator.isNotNull(paymentReference)) {

				ServiceContext serviceContext = ServiceContextFactory.getInstance(DDMFormInstanceRecord.class.getName(), renderRequest);
				PaymentStatus status = paymentService.getPaymentStatus(paymentReference, serviceContext);

				if (PaymentStatus.success().equals(status)) {
					long companyId = PortalUtil.getCompanyId(renderRequest);
					int renewalPeriod = licensingService.getVehicleRenewalLicenseLengthIntMonths(companyId, licenceDetails.getLicenceId());
					BigDecimal renewalPayment = licensingService.getVehicleRenewalPrice(companyId, licenceDetails.getLicenceId());
					renewalPayment = renewalPayment.setScale(2, RoundingMode.CEILING);

					licensingService.renewLicence(companyId, licenceDetails.getLicenceId(), renewalPeriod, paymentReference);

					VehicleLicensingPortletInstanceConfiguration configuration = licensingConfigurationService.getVehicleLicensingPortletInstanceConfiguration(serviceContext.getThemeDisplay());

					sendConfirmationEmail(licenceDetails, configuration, renewalPayment, serviceContext);

					renderRequest.setAttribute(RequestParamKeys.LICENCE_DETAILS, licenceDetails);
					renderRequest.setAttribute(RequestParamKeys.RENEWAL_PAYMENT, renewalPayment);

					return VehicleLicensingViewKeys.VEHICLE_CONFIRMATION_VIEW;
				} else {
					return LicensingViewKeys.ERROR_PAYMENT;
				}
			}

		} catch (Exception e) {
			LOG.error(e);
		}
		return LicensingViewKeys.ERROR;

	}

	private void sendConfirmationEmail(VehicleLicenceDetails licenceDetails, VehicleLicensingPortletInstanceConfiguration configuration, BigDecimal renewalPayment, ServiceContext serviceContext) {
		try {
			licenceNotificationService.sendConfirmationEmail(licenceDetails, renewalPayment, configuration.notificationTemplateId(), configuration.notificationEmailReplyToId(), serviceContext);
		} catch (com.placecube.digitalplace.notifications.exception.NotificationsException | ConfigurationException ne) {
			LOG.error(ne, ne);
		}
	}

}