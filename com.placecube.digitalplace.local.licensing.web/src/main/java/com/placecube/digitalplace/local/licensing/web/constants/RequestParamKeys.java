package com.placecube.digitalplace.local.licensing.web.constants;

public final class RequestParamKeys {

	public static final String DRIVING_LICENCE_NUMBER = "drivingLicenceNumber";

	public static final String EMAIL = "email";

	public static final String LICENCE_DETAILS = "licenceDetails";

	public static final String LICENCE_STATUS = "licenceStatus";

	public static final String LOGBOOK_KEY = "logBookV5";

	public static final String MOBILE = "mobile";

	public static final String MVC_PATH = "mvcPath";

	public static final String NO_CRIMINAL_CONVICTION = "noCriminalConvictionsConfirmed";

	public static final String NO_INVESTIGATION = "noInvestigationConfirmed";

	public static final String NO_MEDICAL_CONDITION = "noMedicalConditionsConfirmed";

	public static final String PAYMENT_REFERENCE = "paymentReference";

	public static final String PAYMENT_ONE_YEAR_AMOUNT = "paymentOneYearAmount";

	public static final String PAYMENT_TWO_YEAR_AMOUNT = "paymentTwoYearAmount";

	public static final String PAYMENT_THREE_YEAR_AMOUNT = "paymentThreeYearAmount";

	public static final String REFERENCE_LICENCE_NUMBER = "referenceLicenceNumber";

	public static final String RENEWAL_PAYMENT = "renewalPayment";

	public static final String RENEWAL_PERIOD = "renewalPeriod";

	public static final String VEHICLE_METER_KEY = "meter";

	public static final String VEHICLE_REGISTRATION_NUMBER = "vehicleRegistrationNumber";

	private RequestParamKeys() {
	}

}