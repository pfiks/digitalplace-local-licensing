package com.placecube.digitalplace.local.licensing.web.constants;

public class SessionErrorsKeys {
    
    public static final String FILE_NOT_UPLOADED = "fileNotUploaded";

    private SessionErrorsKeys(){
    }
}
