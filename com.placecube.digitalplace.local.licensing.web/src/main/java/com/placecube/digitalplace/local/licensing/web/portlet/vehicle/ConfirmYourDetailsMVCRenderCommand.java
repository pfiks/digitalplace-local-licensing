package com.placecube.digitalplace.local.licensing.web.portlet.vehicle;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingMVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.portlet.licensing.SimpleFormMVCRenderCommand;

@Component(immediate = true, property = { "javax.portlet.name=" + VehicleLicensingPortletKeys.VEHICLE,
		"mvc.command.name=" + VehicleLicensingMVCCommandKeys.RENDER_YOUR_VEHICLE_DETAILS }, service = MVCRenderCommand.class)
public class ConfirmYourDetailsMVCRenderCommand extends SimpleFormMVCRenderCommand<VehicleLicenceDetails> {

	@Override
	public String getSuccessRedirectPage() {
		return VehicleLicensingViewKeys.VEHICLE_CONFIRM_YOUR_DETAILS_VIEW;
	}

}