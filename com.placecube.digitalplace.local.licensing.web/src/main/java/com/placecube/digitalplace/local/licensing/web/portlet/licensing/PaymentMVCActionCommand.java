package com.placecube.digitalplace.local.licensing.web.portlet.licensing;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.web.configuration.LicensingPortletInstanceConfiguration;
import com.placecube.digitalplace.local.licensing.web.constants.LicenceLength;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.service.LicensingConfigurationService;
import com.placecube.digitalplace.local.licensing.web.service.LicensingSessionUtils;
import com.placecube.digitalplace.local.licensing.web.service.PaymentHelperService;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;
import com.placecube.digitalplace.payment.service.PaymentService;

@Component(property = { "javax.portlet.name=" + LicensingPortletKeys.LICENSING, "mvc.command.name=" + MVCCommandKeys.ACTION_LICENCE_PAYMENT }, service = MVCActionCommand.class)
public class PaymentMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(PaymentMVCActionCommand.class);

	@Reference
	private LicensingSessionUtils licensingSessionUtils;

	@Reference
	private PaymentHelperService paymentHelperService;

	@Reference
	private PaymentService paymentService;

	@Reference
	private LicensingConfigurationService licensingConfigurationService;

	@Override
	public void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {
		try {
			LicenceDetails licenceDetails = licensingSessionUtils.getLicenceDetailsFromSession(actionRequest);
			LicenceLength selectedExtension = paymentHelperService.getExtensionPeriod(actionRequest);
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DDMFormInstanceRecord.class.getName(), actionRequest);
			LicensingPortletInstanceConfiguration configuration = licensingConfigurationService.getLicensingPortletInstanceConfiguration(serviceContext.getThemeDisplay());
			PaymentContext paymentContext = paymentHelperService.createPaymentContext(licenceDetails, selectedExtension, paymentHelperService.getPaymentAmount(configuration, selectedExtension),
					serviceContext);
			PaymentResponse paymentResponse = paymentService.preparePayment(paymentContext, serviceContext);
			String redirectURL = getRedirectUrl(paymentResponse, paymentContext, serviceContext, actionRequest);

			actionRequest.setAttribute(WebKeys.REDIRECT, redirectURL);
			sendRedirect(actionRequest, actionResponse);
		} catch (Exception e) {
			LOG.error(e);
			throw new PortletException();
		}
	}

	private String getRedirectUrl(PaymentResponse paymentResponse, PaymentContext paymentContext, ServiceContext serviceContext, ActionRequest actionRequest) {
		PaymentStatus paymentPreparationStatus = paymentResponse.getStatus();
		if (PaymentStatus.success() == paymentPreparationStatus) {
			licensingSessionUtils.updatePaymentReference(actionRequest, paymentContext.getSalesReference());
			return paymentResponse.getRedirectURL();
		}
		return paymentHelperService.generateRenderURL(serviceContext, MVCCommandKeys.ERROR);
	}
}