package com.placecube.digitalplace.local.licensing.web.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.exportimport.service.ExportImportService;
import com.placecube.digitalplace.local.webcontent.licensing.service.LicensingWebContentService;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class LicensingLifecycleListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(LicensingLifecycleListener.class);

	@Reference
	private ExportImportService exportImportService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private LicensingWebContentService licensingWebContentService;

	@Reference
	private ServiceContextFactory serviceContextFactory;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		LOG.info("Initialising Licensing web for companyId: " + company.getCompanyId());

		Group guestGroup = groupLocalService.getGroup(company.getCompanyId(), GroupConstants.GUEST);

		ServiceContext serviceContext = serviceContextFactory.createServiceContext(guestGroup.getGroupId(), company.getDefaultUser().getUserId());

		LOG.info("Creating content...");

		exportImportService.disableValidateLayoutReferences(company.getCompanyId());

		licensingWebContentService.initializeLicensingWebContent(serviceContext);

		exportImportService.enableValidateLayoutReferences(company.getCompanyId());

		LOG.info("Content creation complete");
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		return;
	}

}
