package com.placecube.digitalplace.local.licensing.web.portlet.licensing;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.constants.MVCCommandKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + LicensingPortletKeys.LICENSING, "mvc.command.name=" + MVCCommandKeys.ERROR }, service = MVCRenderCommand.class)
public class ErrorMVCRenderCommand implements MVCRenderCommand {

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		return LicensingViewKeys.ERROR;
	}
}