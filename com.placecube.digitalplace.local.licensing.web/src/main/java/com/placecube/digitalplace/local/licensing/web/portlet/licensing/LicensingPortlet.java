package com.placecube.digitalplace.local.licensing.web.portlet.licensing;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingPortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.licensing", "com.liferay.portlet.instanceable=false", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.name=" + LicensingPortletKeys.LICENSING, "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class LicensingPortlet extends MVCPortlet {
}