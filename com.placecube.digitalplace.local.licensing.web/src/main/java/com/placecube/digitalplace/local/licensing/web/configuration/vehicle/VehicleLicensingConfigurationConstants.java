package com.placecube.digitalplace.local.licensing.web.configuration.vehicle;

public final class VehicleLicensingConfigurationConstants {

	public static final String FILE_UPLOAD_PATH = "fileUploadPath";

	public static final String NOTIFICATION_EMAIL_REPLY_TO_ID = "notificationEmailReplyToId";

	public static final String NOTIFICATION_TEMPLATE_ID = "notificationTemplateId";

	public static final String PAYMENT_ACCOUNT_ID = "paymentAccountId";

	private VehicleLicensingConfigurationConstants() {
	}
}
