package com.placecube.digitalplace.local.licensing.web.portlet.licensing;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingPortletKeys;
import com.placecube.digitalplace.local.licensing.web.constants.LicensingViewKeys;
import com.placecube.digitalplace.local.licensing.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + LicensingPortletKeys.LICENSING, "mvc.command.name=" + MVCCommandKeys.VIEW_DECLERATION }, service = MVCRenderCommand.class)
public class DeclerationMVCRenderCommand extends SimpleFormMVCRenderCommand<LicenceDetails> {

	private String redirectPage = LicensingViewKeys.DECLARATION;

	@Override
	public String getSuccessRedirectPage() {
		return redirectPage;
	}

	@Override
	public void handleForm(RenderRequest renderRequest, LicenceDetails licenceDetails) {
		String email = ParamUtil.getString(renderRequest, RequestParamKeys.EMAIL);
		String mobile = ParamUtil.getString(renderRequest, RequestParamKeys.MOBILE);

		if (Validator.isNull(email) || Validator.isNull(mobile)) {
			redirectPage = LicensingViewKeys.CONFIRM_YOUR_DETAILS;
		}

		licenceDetails.setEmail(email);
		licenceDetails.setMobile(mobile);
	}
}