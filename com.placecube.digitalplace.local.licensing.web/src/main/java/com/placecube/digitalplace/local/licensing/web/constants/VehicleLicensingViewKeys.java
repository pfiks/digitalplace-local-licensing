package com.placecube.digitalplace.local.licensing.web.constants;

public class VehicleLicensingViewKeys {

	public static final String VEHICLE_CONFIRM_AND_PAY_VIEW = "/vehicle/confirmAndPay.jsp";

	public static final String VEHICLE_CONFIRM_LOGBOOK_METER = "/vehicle/confirmLogbookMeter.jsp";

	public static final String VEHICLE_CONFIRM_VEHICLE_DETAILS_VIEW = "/vehicle/confirmVehicleDetails.jsp";

	public static final String VEHICLE_CONFIRM_YOUR_DETAILS_VIEW = "/vehicle/confirmYourDetails.jsp";

	public static final String VEHICLE_CONFIRMATION_VIEW = "/vehicle/confirmation.jsp";

	public static final String VEHICLE_DECLARATION_VIEW = "/vehicle/vehicleDeclaration.jsp";

	public static final String VEHICLE_PAYMENT_ERROR = "/vehicle/paymentError.jsp";

	public static final String VEHICLE_RENEWAL_ERROR_VIEW = "/vehicle/renewalError.jsp";

	public static final String VEHICLE_RENEWAL_IS_TOO_LATE_VIEW = "/vehicle/yourRenewalIsTooLate.jsp";

	public static final String VEHICLE_RENEWAL_LOOKUP_VIEW = "/vehicle/vehicleRenewalLookup.jsp";

	public static final String VEHICLE_UPLOAD_DOCUMENTS_FAILED_VIEW = "/vehicle/uploadDocuments-failed.jsp";

	public static final String VEHICLE_VIEW = "/vehicle/view.jsp";

	public static final String VEHICLE_YOUR_DOCUMENTS_VIEW = "/vehicle/yourDocuments.jsp";

	public static final String VEHICLE_YOUR_LICENSE_HAS_EXPIRED_VIEW = "/vehicle/yourLicenseHasExpired.jsp";

	public static final String VEHICLE_YOUR_RENEWAL_IS_TOO_SOON_VIEW = "/vehicle/yourRenewalIsTooSoon.jsp";

	private VehicleLicensingViewKeys() {

	}
}
