
package com.placecube.digitalplace.local.licensing.web.portlet.vehicle;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingPortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.licensing", //
		"com.liferay.portlet.single-page-application=false", //
		"com.liferay.portlet.instanceable=false", //
		"javax.portlet.init-param.template-path=/", //
		"javax.portlet.init-param.view-template=/vehicle/view.jsp", //
		"javax.portlet.init-param.config-template=/vehicle/configuration.jsp", //
		"javax.portlet.name=" + VehicleLicensingPortletKeys.VEHICLE,//
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class VehicleLicensingPortlet extends MVCPortlet {
}