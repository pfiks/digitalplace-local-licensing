package com.placecube.digitalplace.local.licensing.web.constants;

public final class LicensingPortletKeys {

	public static final String LICENSING = "com_placecube_digitalplace_local_licensing_LicensingPortlet";

	private LicensingPortletKeys() {
	}

}