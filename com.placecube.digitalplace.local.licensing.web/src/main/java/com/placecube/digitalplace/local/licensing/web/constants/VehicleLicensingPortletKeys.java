package com.placecube.digitalplace.local.licensing.web.constants;

public final class VehicleLicensingPortletKeys {

	public static final String VEHICLE = "com_placecube_digitalplace_local_licensing_VehicleLicensingPortlet";

	private VehicleLicensingPortletKeys() {
	}

}