<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/frontend" prefix="liferay-frontend" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/journal" prefix="liferay-journal" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://placecube.com/digitalplace/tld/address" prefix="dp-address" %>
<%@ taglib uri="http://placecube.com/digitalplace/tld/frontend" prefix="dp-frontend" %>

<%@page import="com.placecube.digitalplace.local.licensing.model.LicenceStatus"%>
<%@page import="com.placecube.digitalplace.local.licensing.web.constants.LicenceLength"%>
<%@page import="com.placecube.digitalplace.local.licensing.web.constants.LicensingPortletKeys"%>
<%@page import="com.placecube.digitalplace.local.licensing.web.constants.MVCCommandKeys"%>
<%@page import="com.placecube.digitalplace.local.licensing.web.constants.RequestParamKeys"%>
<%@page import="com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingPortletKeys"%>
<%@page import="com.placecube.digitalplace.local.licensing.web.constants.VehicleLicensingMVCCommandKeys"%>
<%@page import="com.placecube.digitalplace.local.webcontent.licensing.constants.LicensingWebContent"%>

<liferay-theme:defineObjects />

<portlet:defineObjects />
<c:set var="groupId"><%= themeDisplay.getScopeGroupId() %></c:set>