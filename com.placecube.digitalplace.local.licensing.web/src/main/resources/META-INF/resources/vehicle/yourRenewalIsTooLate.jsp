<%@ include file="/init.jsp" %>

<div>
    <div>
        <span>
            <liferay-ui:message key="vehicle.steps.header.title" /></span>
        <span>
            :
            <liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\"2\", \"8\"}%>"/>
        </span>
    </div>
    <hr />
    <h1>
        <liferay-ui:message key="your.renewal.is.too.late.title" />
    </h1>
</div>
<br />

<portlet:renderURL var="yourRenewalIsTooLatesUrl">
    <portlet:param name="mvcRenderCommandName" value="<%= VehicleLicensingMVCCommandKeys.RENDER_YOUR_RENEWAL_IS_TOO_LATE %>" />
</portlet:renderURL>

<aui:form action="<%= yourRenewalIsTooLatesUrl %>" method="post" name="fm">
	<aui:input type="hidden" name="formSubmitted" value="true" />
	<div class="info" id="lateRenewal">
	    <div class="info__title">
	    <ul>
	    	<li><liferay-ui:message key="your.licence.expires.on"/> ${renewalDateTime}</li>
	    	<li><liferay-ui:message key="it.might.take"/></li>
	    </ul>
        </div>
		<div class="info__copy">
			<p>	<liferay-ui:message key="well.try.to.process.your.application"/> ${renewalDateTime} <strong><liferay-ui:message key="your.vehicle.cant.be.used"/></strong>.</p>
        </div>
	</div>
	
	<button type="submit" class="btn btn-primary">
		<liferay-ui:message key="next" />
	</button>
	
</aui:form>