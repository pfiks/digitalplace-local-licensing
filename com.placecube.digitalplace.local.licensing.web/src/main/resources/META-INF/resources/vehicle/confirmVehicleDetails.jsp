<%@ include file="/init.jsp" %>

	<div>
		<div>
			<span>
				<liferay-ui:message key="vehicle.steps.header.title" />
			</span>
			<span> : <liferay-ui:message key="step.x.of.x" arguments="<%=new String[] {\" 2\",\"8\" }%>"/>
			</span>
		</div>
		<hr />
		<h1>
			<liferay-ui:message key="confirm.vehicle.details.title" />
		</h1>
	</div>
	<br />
	<div style="padding: 15px; border-left: 10px solid lightgray;">
		<h1>
			<strong>
				<liferay-ui:message key="confirm.vehicle.details.vehicle.title" />
			</strong>
		</h1>
		<div class="mb-4">
			<liferay-ui:message key="confirm.vehicle.details.registration" />
			<br /> <strong>${licenceDetails.vehicle.regNumber}</strong>
		</div>
		<div class="mb-4">
			<liferay-ui:message key="confirm.vehicle.details.make" />
			<br /> <strong>${licenceDetails.vehicle.make}
				${licenceDetails.vehicle.model}</strong>
		</div>
		<div class="mb-4">
			<liferay-ui:message key="confirm.vehicle.details.colour" />
			<br /> <strong>${licenceDetails.vehicle.colour}</strong>
		</div>
		<div class="mb-4">
			<liferay-ui:message key="confirm.vehicle.details.seats" />
			<br /> <strong>${licenceDetails.vehicle.seats}</strong>
		</div>
		<div class="mb-4">
			<liferay-ui:message key="confirm.vehicle.details.first.registration" />
			<br />
			<fmt:parseDate value="${licenceDetails.firstRegistered}" pattern="yyyy-MM-dd" var="parsedDate"
				type="date" />
			<fmt:formatDate value="${parsedDate}" pattern="dd MMMMM yyyy" var="firstRegistered" />
			<strong>${firstRegistered}</strong>
		</div>
		<br />
		<h1>
			<strong>
				<liferay-ui:message key="confirm.vehicle.details.keeper" />
			</strong>
		</h1>
		<div class="mb-4">
			<liferay-ui:message key="full.name.title" />
			<br /> <strong>${licenceDetails.registeredKeeper.firstName}
				${licenceDetails.registeredKeeper.lastName}</strong>
		</div>
		<div class="mb-4">
			<liferay-ui:message key="home.address.title" />
			<br /> <strong>${licenceDetails.registeredKeeper.address.fullAddress}</strong>
		</div>

		<br />
		<h1>
			<strong>
				<liferay-ui:message key="confirm.vehicle.details.parties.title" />
			</strong>
		</h1>

		<liferay-journal:journal-article
			articleId="<%= LicensingWebContent.CONFIRM_VEHICLE_DETAILS_INTERESTED_PARTIES.getArticleId() %>"
			groupId="${ groupId }" />

		<c:forEach var="thirdParty" items="${licenceDetails.thirdParties}">
			<div class="mb-4">
				<liferay-ui:message key="confirm.vehicle.details.interested.parties" />
				<br /> <strong>${thirdParty.firstName}
					${thirdParty.lastName}</strong>
			</div>
			<div class="mb-4">
				<liferay-ui:message key="home.address.title" />
				<br /> <strong>${thirdParty.address.fullAddress}</strong>
			</div>
		</c:forEach>
	</div>

	<br />
	<br />

	<portlet:renderURL var="contactURL">
		<portlet:param name="mvcPath" value="/contactTheLicensingTeam.jsp" />
	</portlet:renderURL>

	<portlet:renderURL var="confirmYourDetailsURL">
		<portlet:param name="mvcRenderCommandName"
			value="<%=VehicleLicensingMVCCommandKeys.RENDER_YOUR_VEHICLE_DETAILS%>" />
	</portlet:renderURL>


	<a class="btn btn-primary mr-3" href="${confirmYourDetailsURL}">
		<liferay-ui:message key="confirm.vehicle.details.button" />
	</a>

	<a href="${ contactURL }">
		<liferay-ui:message key="confirm.vehicle.details.change.link" />
	</a>