<%@ include file="/init.jsp" %>

	<div>
		<div>
			<span>
				<liferay-ui:message key="vehicle.steps.header.title" />
			</span>
			<span>
				:
				<liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\" 4\", \"8\"}%>"/>
			</span>
		</div>
		<hr />
		<h1>
			<liferay-ui:message key="vehicle.your.logbook.meter.title" />
		</h1>
	</div>
	<br />
	<portlet:renderURL var="licenceLookupUrl">
		<portlet:param name="mvcRenderCommandName"
			value="<%= VehicleLicensingMVCCommandKeys.RENDER_YOUR_DOCUMENTS %>" />
	</portlet:renderURL>

	<c:if test="${ error }">
		<div style="padding: 15px; border: solid red 1px;">
			<liferay-ui:message key="vehicle.your.logbook.meter.question.error" />
		</div>
	</c:if>

	<aui:form action="<%= licenceLookupUrl %>" method="post" name="fm">
		<aui:input type="hidden" name="formSubmitted" value="true" />

		<aui:row>
			<liferay-ui:message key="vehicle.your.logbook.meter.question.logbook" />
		</aui:row>
		<aui:row>
			<aui:col width="50" cssClass="pl-0">
				<div class="row">
					<div class="col-sm-3">
						<aui:input name="<%= RequestParamKeys.LOGBOOK_KEY %>" type="radio" value="yes"
							label="vehicle.your.logbook.meter.option.yes">
							<aui:validator name="required" />
						</aui:input>
					</div>
					<div class="col-sm-3">
						<aui:input name="<%= RequestParamKeys.LOGBOOK_KEY %>" type="radio" value="no" required="true"
							label="vehicle.your.logbook.meter.option.no" />
					</div>
				</div>
			</aui:col>
		</aui:row>

		<br />

		<aui:row>
			<liferay-ui:message key="vehicle.your.logbook.meter.question.meter" />
		</aui:row>
		<aui:row>
			<aui:col width="50">
				<div class="row">
					<div class="col-sm-3">
						<aui:input name="<%= RequestParamKeys.VEHICLE_METER_KEY %>" type="radio" value="yes"
							label="vehicle.your.logbook.meter.option.yes">
							<aui:validator name="required" />
						</aui:input>
					</div>
					<div class="col-sm-3">
						<aui:input name="<%= RequestParamKeys.VEHICLE_METER_KEY %>" type="radio" value="no"
							label="vehicle.your.logbook.meter.option.no" required="true" />
					</div>
				</div>
			</aui:col>
		</aui:row>

		<br />

		<button type="submit" class="btn btn-primary">
			<liferay-ui:message key="next" />
		</button>

	</aui:form>