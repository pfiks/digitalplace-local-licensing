<%@ include file="/init.jsp" %>

    <div>
        <div>
            <span>
                <liferay-ui:message key="vehicle.steps.header.title" />
            </span>
            <span>
                :
                <liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\" 1\", \"8\"}%>"/>
            </span>
        </div>
        <hr />
        <h1>
            <liferay-ui:message key="vehicle.details.title" />
        </h1>
    </div>
    <br />
    <portlet:renderURL var="licenceLookupUrl">
        <portlet:param name="mvcRenderCommandName"
            value="<%= VehicleLicensingMVCCommandKeys.RENDER_CONFIRM_VEHICLE_DETAILS %>" />
    </portlet:renderURL>
    <c:if test="${ error }">
        <div style="padding: 15px; border: solid red 1px;">
            <liferay-ui:message key="taxiLicenceNumber.not.found" />
        </div>
    </c:if>

    <aui:form action="<%= licenceLookupUrl %>" method="post" name="fm">
        <aui:row>
            <aui:col width="50" cssClass="pl-0">
                <aui:input label="vehicle.registration.number.label" name="vehicleRegistrationNumber" type="text">
                    <aui:validator name="required" errorMessage="vehicle.registration.number.validation" />
                </aui:input>
            </aui:col>
        </aui:row>
        <br />
        <aui:row>
            <aui:col width="50" cssClass="pl-0">
                <aui:input label="vehicle.licence.number.label" name="referenceLicenceNumber"
                    placeholder="reference.licence.number.placeholder" type="text">
                    <aui:validator name="required" errorMessage="reference.licence.number.validation" />
                </aui:input>
                <div style="margin-top: -15px;">
                    <liferay-ui:message key="vehicle.licence.number.label.instructions" />
                </div>
            </aui:col>
        </aui:row>
        <br />

        <button type="submit" class="btn btn-primary">
            <liferay-ui:message key="next" />
        </button>
    </aui:form>