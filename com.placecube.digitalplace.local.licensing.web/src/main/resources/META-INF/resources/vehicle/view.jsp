<%@ include file="/init.jsp" %>

<h1>
    <liferay-ui:message key="start.vehicle.renewal.process.title" />
</h1>

<liferay-journal:journal-article articleId="<%= LicensingWebContent.VEHICLE_RENEWAL_LEAD_IN_TOP.getArticleId() %>" groupId="${groupId}"/>

<portlet:renderURL var="renewaLicenseURL">
    <portlet:param name="mvcPath" value="/vehicle/vehicleRenewalLookup.jsp" />
</portlet:renderURL>

<p class="pt-4 pb-4">
    <a href="${renewaLicenseURL}" class="btn btn-primary">
        <liferay-ui:message key="start.renewal.process.button" />
    </a>
</p>

<liferay-journal:journal-article articleId="<%= LicensingWebContent.VEHICLE_RENEWAL_LEAD_IN_BOTTOM.getArticleId() %>" groupId="${groupId}"/>