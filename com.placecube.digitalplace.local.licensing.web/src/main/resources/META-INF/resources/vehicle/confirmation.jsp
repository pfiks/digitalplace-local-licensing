<%@ include file="/init.jsp" %>

    <div>
        <div>
            <span>
                <liferay-ui:message key="vehicle.steps.header.title" />
            </span>
            <span>
                :
                <liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\" 8\", \"8\"}%>"/>
            </span>
        </div>
        <hr />
        <h1>
            <liferay-ui:message key="vehicle.confirmation.title" />
        </h1>
    </div>
    <br />

    <div class="info info--reverse" style="padding: 15px; border-left: 10px solid lightgray;">
        <div class="info__copy">
            <liferay-ui:message key="vehicle.confirmation.post.address.title" /><br />
            <strong><span id="postAddress">${licenceDetails.address.fullAddress}</span></strong>
        </div>
        <br />
        <div class="info__copy">
            <liferay-ui:message key="vehicle.confirmation.receive.by.title" /> <strong>
                <liferay-ui:message key="vehicle.confirmation.receive.by.text" />
            </strong>
            <liferay-ui:message key="vehicle.confirmation.receive.by.title2" />
        </div>
        <br />
        <div class="info__copy">
            <liferay-ui:message key="vehicle.confirmation.email.title" /><br />
            <strong><span id="emailAddress">${licenceDetails.email}</span></strong>
        </div>
    </div>
    <br />
    <div class="slate">
        <div class="slate__title" style="background-color: lightgray; padding:25px;">
            <liferay-ui:message key="vehicle.confirmation.order.details.title" />
        </div>
        <div class="slate__copy">
            <p>
                <liferay-ui:message key="vehicle.confirmation.licence.number.title" /><br />
                <strong><span id="taxiLicenceNumber">${licenceDetails.taxiLicenceNumber}</span></strong>
            </p>
            <p>
                <liferay-ui:message key="vehicle.confirmation.vehicle.registration.title" /><br />
                <strong><span id="regNumber">${licenceDetails.vehicle.regNumber}</span></strong>
            </p>
            <p>
                <liferay-ui:message key="vehicle.confirmation.amount.paid.title" /><br />
                <strong><span id="renewalPayment">&pound;${renewalPayment}</span></strong>
            </p>
            <p>
                <c:set var="now" value="<%= new java.util.Date()%>" />
                <liferay-ui:message key="vehicle.confirmation.order.date.title" /><br />
                <strong><span id="orderDate">
                        <fmt:formatDate value="${now}" pattern="dd MMM yyyy" />
                    </span></strong>
            </p>
        </div>
    </div>

    <liferay-journal:journal-article articleId="<%= LicensingWebContent.VEHICLE_CONFIRMATION.getArticleId() %>"
        groupId="${ groupId }" />