<%@ include file="/init.jsp" %>

<div>
    <h1 id="contact-licensing-team-header">
        <liferay-ui:message key="uniform.error.title" />
    </h1>
</div>
<br />

<portlet:renderURL var="renewalsURL">
	<portlet:param name="mvcPath" value="/vehicle/vehicleRenewalLookup.jsp" />
</portlet:renderURL>

<portlet:renderURL var="contactURL">
	<portlet:param name="mvcPath" value="/contactTheLicensingTeam.jsp" />
</portlet:renderURL>


<liferay-journal:journal-article articleId="<%= LicensingWebContent.UNIFORM_ERROR.getArticleId() %>" groupId="${ groupId }"/>
