<%@page import="com.liferay.portal.kernel.util.Constants" %>
	<%@ include file="/init.jsp" %>

		<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

		<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

		<liferay-frontend:edit-form action="${configurationActionURL}" method="post" name="vehicleLicensingForm">

			<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
			<aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />

			<liferay-frontend:edit-form-body>
				<dp-frontend:fieldset-group>

					<liferay-frontend:fieldset>
						<aui:input label="configuration.notification.template.id" name="notificationTemplateId"
							type="text" value="${ configuration.notificationTemplateId() }" />
					</liferay-frontend:fieldset>

					<liferay-frontend:fieldset>
						<aui:input label="configuration.notification.email.reply.to.id"
							name="notificationEmailReplyToId" type="text"
							value="${ configuration.notificationEmailReplyToId() }" />
					</liferay-frontend:fieldset>

					<liferay-frontend:fieldset>
						<aui:input label="configuration.payment.account.id" name="paymentAccountId" type="text"
							value="${ configuration.paymentAccountId() }" />
					</liferay-frontend:fieldset>

				</dp-frontend:fieldset-group>
			</liferay-frontend:edit-form-body>

			<liferay-frontend:edit-form-footer>
				<aui:button type="submit" />

				<aui:button type="cancel" />
			</liferay-frontend:edit-form-footer>

		</liferay-frontend:edit-form>