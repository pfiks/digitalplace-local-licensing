<%@ include file="/init.jsp"%>

<div>
	<div>
		<span> <liferay-ui:message key="vehicle.steps.header.title" />
		</span>
		<span>
			: <liferay-ui:message key="step.x.of.x"arguments="<%=new String[] {\"6\",\"8\" }%>" />
		</span>
	</div>
	<hr />
	<h1>
		<liferay-ui:message key="vehicle.declaration.title" />
	</h1>
</div>
<br />

<portlet:renderURL var="editYourDocumentsUrl">
	<portlet:param name="mvcRenderCommandName"
		value="<%=VehicleLicensingMVCCommandKeys.RENDER_YOUR_DOCUMENTS%>" />
</portlet:renderURL>

<portlet:renderURL var="licenceLookupUrl">
	<portlet:param name="mvcRenderCommandName"
		value="<%=VehicleLicensingMVCCommandKeys.RENDER_VEHICLE_CONFIRM_AND_PAY%>" />
</portlet:renderURL>

<div style="padding: 15px; border-left: 10px solid lightgray;">
	<div class="mb-4">
		<liferay-ui:message key="confirm.vehicle.details.registration" />
		<br /> <strong>${ licenceDetails.getVehicle().getRegNumber() }</strong>
	</div>

	<div class="mb-4">
		<c:choose>
			<c:when test="${ fileItems.size() > 0 }">
				<liferay-ui:message key="vehicle.your.documents.title" />
				<c:forEach items="${ fileItems }" var="document">
					<br />
					<strong>
						<c:out value="${ document.getFileName() }" />
					</strong>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<liferay-ui:message key="confirm.vehicle.details.documents.none" />
				<br />
				<br />
				<a href="${ editYourDocumentsUrl }"> <liferay-ui:message key="confirm.vehicle.details.edit.documents.none" />
				</a>
			</c:otherwise>
		</c:choose>
	</div>
</div>

<br />

<c:if
	test="${ fn:endsWith(licenceDetails.getTaxiLicenceNumber(), 'TXPHD') }">
	<liferay-journal:journal-article
		articleId="<%= LicensingWebContent.VEHICLE_DECLARATION_IAG_PRIVATE_HIRE.getArticleId() %>"
		groupId="${ groupId }"/>
</c:if>
<c:if
	test="${ fn:endsWith(licenceDetails.getTaxiLicenceNumber(), 'TXHCD') }">
	<liferay-journal:journal-article
		articleId="<%= LicensingWebContent.VEHICLE_DECLARATION_IAG_HACKNEY_CARRIAGE.getArticleId() %>"
		groupId="${ groupId }"/>
</c:if>

<br />
<aui:form action="<%=licenceLookupUrl%>" method="post" name="fm">
	<aui:input type="hidden" name="formSubmitted" value="true" />

	<button type="submit" class="btn btn-primary">
		<liferay-ui:message key="vehicle.declaration.confirm" />
	</button>
</aui:form>
