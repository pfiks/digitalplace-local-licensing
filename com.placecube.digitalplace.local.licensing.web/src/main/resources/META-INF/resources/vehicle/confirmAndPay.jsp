<%@ include file="/init.jsp" %>

<div>
    <div>
        <span>
            <liferay-ui:message key="vehicle.steps.header.title" /></span>
        <span>
            :
            <liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\"7\", \"8\"}%>"/>
        </span>
    </div>
    <hr />
    <h1>
        <liferay-ui:message key="confirm.and.pay.title" />
    </h1>
</div>
<br />

<portlet:actionURL name="<%= VehicleLicensingMVCCommandKeys.ACTION_VEHICLE_LICENCE_PAYMENT %>" var="paymentUrl" />


<form method="POST" action="<%= paymentUrl %>">
    
	<div class="info">
	    <div class="info__title">
			<liferay-ui:message key="confirm.and.pay.cost.label"/> &pound;${renewalPrice}
	    </div>
	    <div class="info__copy">
	        <p>
				<liferay-ui:message key="confirm.and.pay.your.new.licence.will.expire.on.vehicle"/> <strong>${renewalDateTime}</strong>
			</p>
		</div>
	</div>
   
   	<div class="cta-group">
		<button type="submit" class="btn btn-primary">
			<liferay-ui:message key="confirm.and.pay.pay.now" />
		</button>
		<div class="cta-group__secondly">
			<small class="light">
		           <liferay-ui:message key="confirm.and.pay.pay.now.subtext.1" /><br/>
		           <liferay-ui:message key="confirm.and.pay.pay.now.subtext.2" />
	       </small>
		</div>
	</div>

</form>