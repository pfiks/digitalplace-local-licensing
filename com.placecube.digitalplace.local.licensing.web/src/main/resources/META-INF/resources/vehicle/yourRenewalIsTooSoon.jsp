<%@ include file="/init.jsp" %>

<div>
    <div>
        <span>
            <liferay-ui:message key="vehicle.steps.header.title" /></span>
        <span>
            :
            <liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\"2\", \"8\"}%>"/>
        </span>
    </div>
    <hr />
    <h1>
        <liferay-ui:message key="your.renewal.is.too.soon.title" />
    </h1>
</div>
<br />

<liferay-journal:journal-article articleId="<%= LicensingWebContent.YOUR_VEHICLE_RENEWAL_IS_TOO_SOON.getArticleId() %>" groupId="${ groupId }"/>
