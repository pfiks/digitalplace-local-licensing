<%@ include file="/init.jsp"%>

<div>
	<div>
		<span> <liferay-ui:message key="vehicle.steps.header.title" />
		</span> <span> : <liferay-ui:message key="step.x.of.x"
				arguments="<%=new String[] {\"5\",\"8\" }%>"/>
		</span>
	</div>
	<hr />
	<h1>
		<liferay-ui:message key="vehicle.your.documents.title" />
	</h1>
</div>
<br />

<liferay-journal:journal-article
	articleId="<%= LicensingWebContent.UPLOAD_DOCUMENTS_FAILED_PANEL.getArticleId() %>"
	groupId="${ portletGroupId }"/>

<portlet:renderURL var="confirmAndPayUrl">
	<portlet:param name="mvcRenderCommandName"
		value="<%=VehicleLicensingMVCCommandKeys.RENDER_VEHICLE_CONFIRM_AND_PAY%>" />
</portlet:renderURL>

<portlet:renderURL var="yourDocumentsUrl">
    <portlet:param name="mvcRenderCommandName" value="<%= VehicleLicensingMVCCommandKeys.RENDER_YOUR_DOCUMENTS %>" />
</portlet:renderURL>

<p>
	<a class="btn btn-primary" href="<%= confirmAndPayUrl %>"><liferay-ui:message	key="next" /></a> <a href="<%= yourDocumentsUrl %>"><liferay-ui:message	key="upload files" /></a>
</p>