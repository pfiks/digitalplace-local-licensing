<%@ include file="/init.jsp"%>

<liferay-ui:error key="fileUploadFailed" message="file-not-uploaded" />
<liferay-ui:success key="fileUploaded" message="file-uploaded" />

<div>
	<div>
		<span> <liferay-ui:message key="vehicle.steps.header.title" /></span>
		<span> : <liferay-ui:message key="step.x.of.x"
				arguments="<%=new String[] {\"5\",\"8\" }%>"/>
		</span>
	</div>
	<hr />
	<h1>
		<liferay-ui:message key="vehicle.your.documents.title" />
	</h1>
</div>
<br />

<p>
	<liferay-ui:message key="vehicle.your.documents.description.title" />
</p>
<ul>
	<li id="insurance"><liferay-ui:message
			key="vehicle.your.documents.description.insurance" /></li>
	<li id="inpection_certificate"><liferay-ui:message
			key="vehicle.your.documents.description.vehicle.inspection.certificate" />
	</li>

	<c:if test="${ licenceDetails.logBookV5 }">
		<li><liferay-ui:message
				key="vehicle.your.documents.description.logbook" /></li>
	</c:if>

	<c:if test="${ licenceDetails.meter }">
		<li><liferay-ui:message
				key="vehicle.your.documents.description.meter" /></li>
	</c:if>
</ul>

<p>
	<liferay-journal:journal-article
		articleId="<%= LicensingWebContent.VEHICLE_YOUR_DOCUMENTS_YOU_CAN.getArticleId() %>"
		groupId="${ groupId }" />
</p>

<portlet:actionURL name="<%=VehicleLicensingMVCCommandKeys.ACTION_FILE_UPLOAD%>"
	var="fileUploadUrl" />

<c:if test="${ error }">
	<div style="padding: 15px; border: solid red 1px;">
		<liferay-ui:message key="file.upload.files.missing" />
	</div>
</c:if>
<br />
<h2>
	<b><liferay-ui:message key="file.upload.heading" /></b>
</h2>
<p>
	<liferay-ui:message key="file.upload.subheading" />
</p>
<aui:form action="<%=fileUploadUrl%>" method="post" name="fm">
	<aui:input type="hidden" name="formSubmitted" value="true" />

	<c:forEach var="i" begin="0" end="9">
		<aui:input label="" name="file${i}" type="file" size="50">
			<aui:validator name="acceptFiles">
					'jpeg,jpg,png,pdf'
				</aui:validator>
		</aui:input>
	</c:forEach>

	<br />

	<p>
		<liferay-journal:journal-article
			articleId="<%= LicensingWebContent.VEHICLE_YOUR_DOCUMENTS_PROBLEMS_PANEL.getArticleId() %>"
			groupId="${ groupId }" />
	</p>

	<br />

	<button type="submit" class="btn btn-primary">
		<liferay-ui:message key="next" />
	</button>

</aui:form>