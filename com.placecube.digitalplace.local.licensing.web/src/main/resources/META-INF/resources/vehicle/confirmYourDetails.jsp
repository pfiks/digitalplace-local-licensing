<%@ include file="/init.jsp" %>

	<div>
		<div>
			<span>
				<liferay-ui:message key="vehicle.steps.header.title" />
			</span>
			<span> : <liferay-ui:message key="step.x.of.x" arguments="<%=new String[] {\" 3\",\"8\" }%>"/>
			</span>
		</div>
		<hr />
		<h1>
			<liferay-ui:message key="confirm.your.details.title" />
		</h1>
	</div>
	<br />
	<div style="padding: 15px; border-left: 10px solid lightgray;">
		<div class="mb-4">
			<liferay-ui:message key="full.name.title" />
			<br /> <strong>${licenceDetails.firstName}
				${licenceDetails.lastName}</strong>
		</div>
		<div class="mb-4">
			<liferay-ui:message key="home.address.title" />
			<br /> <strong>${licenceDetails.address.fullAddress}</strong>
		</div>
		<div class="mb-4">
			<liferay-ui:message key="your.licence.expires.on" />
			<br />
			<fmt:parseDate value="${licenceDetails.renewalDate}" pattern="yyyy-MM-dd" var="parsedDate" type="date" />
			<fmt:formatDate value="${parsedDate}" pattern="dd MMM yyyy" var="renewalDateTime" />
			<strong>${renewalDateTime}</strong>
		</div>
	</div>

	<portlet:renderURL var="contactURL">
		<portlet:param name="mvcPath" value="/contactTheLicensingTeam.jsp" />
	</portlet:renderURL>


	<a href="${ contactURL }" id="incorrectDetailsLink">
		<spring:message code="these.details.are.incorrect.link" />
	</a>

	<br />

	<portlet:renderURL var="confirmLogbookMeterURL">
		<portlet:param name="mvcRenderCommandName"
			value="<%=VehicleLicensingMVCCommandKeys.RENDER_CONFIRM_LOGBOOK_METER%>" />
	</portlet:renderURL>

	<aui:form action="<%=confirmLogbookMeterURL%>">
		<aui:input type="hidden" name="formSubmitted" value="true" />
		<aui:row>
			<aui:col width="50" cssClass="pl-0">
				<aui:input label="email.address.label" name="email" value="${licenceDetails.email}" type="text">
					<aui:validator name="required" errorMessage="email.address.validation" />
					<aui:validator name="email" />
				</aui:input>
				<div style="margin-top: -15px;">
					<liferay-ui:message key="email.address.label.instructions" />
				</div>
			</aui:col>
		</aui:row>
		<br />
		<liferay-journal:journal-article
			articleId="<%= LicensingWebContent.CONFIRM_YOUR_DETAILS_BOTTOM.getArticleId() %>" groupId="${ groupId }" />
		<br />
		<button type="submit" class="btn btn-primary">
			<liferay-ui:message key="next" />
		</button>
	</aui:form>