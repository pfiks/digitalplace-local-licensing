<%@ include file="/init.jsp" %>
<div>
    <div>
        <span>
            <liferay-ui:message key="steps.header.title" /></span>
        <span>
            :
            <liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\"2\", \"5\"}%>"/>
        </span>
    </div>
    <hr />
    <h1>
        <liferay-ui:message key="change.your.address.title" />
    </h1>
</div>
<br />

<portlet:renderURL var="addressChangeURL">
    <portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.CONFIRM_DETAILS %>" />
</portlet:renderURL>
<div>
    <aui:form action="<%= addressChangeURL %>" name="<portlet:namespace />fm">
        <dp-address:postcode-lookup />
        <aui:button-row>
            <aui:button type="submit" value="change.your.address.button"></aui:button>
        </aui:button-row>
    </aui:form>
</div>