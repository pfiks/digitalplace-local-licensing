<%@ include file="/init.jsp" %>

    <!-- HEADER -->
    <div>
        <div>
            <span>
                <liferay-ui:message key="steps.header.title" />
            </span>
            <span>
                :
                <liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\" 3\", \"5\"}%>"/>
            </span>
        </div>
        <hr />
    </div>
    <div class="row">
        <div class="col-md-12 ">

            <div class="border-bottom pb-5">
                <h1>
                    <liferay-ui:message key="declaration.title" />
                </h1>
            </div>


            <portlet:renderURL var="declerationConfirmationUrl">
                <portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.LICENCE_LENGTH %>" />
            </portlet:renderURL>

            <aui:form action="<%= declerationConfirmationUrl %>">
                <aui:input type="hidden" name="formSubmitted" value="true" />

                <div class="border-bottom pt-4 pb-3">
                    <p class="lead">
                        <liferay-ui:message key="declaration.its.your.responsibility" />
                    </p>
                </div>

                <p class="lead">
                    <liferay-ui:message key="declaration.check.each.box" />
                </p>
                <br />
                <aui:row>
                    <aui:col cssClass="pl-0">
                        <aui:input label="declaration.no.criminal.convictions" name="noCriminalConvictionsConfirmed"
                            value="${licenceDetails.noCriminalConvictionsConfirmed}" cssClass="mr-2" type="checkbox">
                            <aui:validator name="required" />
                        </aui:input>
                        <div style="margin-top: -15px;">
                            <liferay-ui:message key="declaration.no.criminal.convictions.sub" />
                        </div>
                    </aui:col>
                </aui:row>
                <br />
                <aui:row>
                    <aui:col cssClass="pl-0">
                        <aui:input label="declaration.no.investigation" name="noInvestigationConfirmed"
                            value="${licenceDetails.noInvestigationConfirmed}" type="checkbox" cssClass="mr-2">
                            <aui:validator name="required" />
                        </aui:input>
                    </aui:col>
                </aui:row>
                <br />
                <aui:row>
                    <aui:col cssClass="pl-0">
                        <aui:input label="declaration.no.medical.conditions" name="noMedicalConditionsConfirmed"
                            value="${licenceDetails.noMedicalConditionsConfirmed}" type="checkbox" cssClass="mr-2">
                            <aui:validator name="required" />
                        </aui:input>
                    </aui:col>
                </aui:row>
                <br />
                <portlet:renderURL var="contactActionURL">
                    <portlet:param name="mvcPath" value="/contactTheLicensingTeam.jsp" />
                </portlet:renderURL>
                <p>
                    <liferay-ui:message key="declaration.contact.team.1" />
                    <a href="${ contactActionURL }" id="contactLicensingTeamLink">
                        <liferay-ui:message key="declaration.contact.team.2" />
                    </a>
                    <liferay-ui:message key="declaration.contact.team.3" />
                </p>
                <liferay-journal:journal-article
                    articleId="<%= LicensingWebContent.DECLARATION_WHAT_THE_LAW_SAYS.getArticleId() %>"
                    groupId="${ groupId }" />
                <br />

                <button type="submit" class="btn btn-primary">
                    <liferay-ui:message key="confirm.and.continue" />
                </button>
            </aui:form>
        </div>
    </div>