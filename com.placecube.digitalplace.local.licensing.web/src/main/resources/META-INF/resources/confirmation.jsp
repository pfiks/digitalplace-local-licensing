<%@ include file="init.jsp" %>
<div>
    <div>
        <span>
            <liferay-ui:message key="steps.header.title" /></span>
        <span>
            :
            <liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\"5\", \"5\"}%>"/>
        </span>
    </div>
    <hr />
    <h1>
        <liferay-ui:message key="confirmation.title" />
    </h1>
</div>
<br />


<div style="padding: 15px;border-left: 10px solid lightgray;">
    <div class="lead">
        <liferay-ui:message key="confirmation.post.address.title" /><br/>
        <strong>${licenceDetails.address.fullAddress}</strong>
    </div>
    <br/>
    <div class="lead">
        <liferay-ui:message key="confirmation.receive.by.title" /> <strong><liferay-ui:message key="confirmation.receive.by.text" /></strong>
    </div>
    <br/>
    <div class="lead">
        <liferay-ui:message key="confirmation.email.title" /><br/>
        <strong>${licenceDetails.email}</strong>
    </div>
</div>
<br />
<br />
<div style="background-color: lightgray; padding:25px">
    <div class="lead">
        <liferay-ui:message key="confirmation.order.details.title" />
    </div>
    <br />
    <div>
        <p>
            <liferay-ui:message key="confirmation.licence.number.title" /><br/>
            <strong><span id="taxiLicenceNumber">${licenceDetails.taxiLicenceNumber}</span></strong>
        </p>
        <p>
            <liferay-ui:message key="confirmation.licence.type.title" /><br/>
            <strong>
                <span id="licenceType">
                <c:if test="${fn:endsWith(licenceDetails.taxiLicenceNumber, 'TXPHD')}">
                   <liferay-ui:message key="licence.type.txphd" />
                </c:if>
                <c:if test="${fn:endsWith(licenceDetails.taxiLicenceNumber, 'TXHCD')}">
                   <liferay-ui:message key="licence.type.txhcd" />
                </c:if>
                </span>
            </strong>
        </p>
        <p>
            <fmt:parseDate value="${licenceDetails.renewalDate}" pattern="yyyy-MM-dd" var="parsedDate" type="date" />
            <fmt:formatDate value="${parsedDate}" pattern="dd MMM yyyy" var="expiryDate" />
            <liferay-ui:message key="confirmation.expiry.date.title" /><br/>
            <strong><span id="expiryDate">${expiryDate}</span></strong>
        </p>
        <p>
            <liferay-ui:message key="confirmation.amount.paid.title" /><br/>
            <strong><span id="renewalPayment">&pound;${renewalPayment}</span></strong>
        </p>
        <p>
            <c:set var = "now" value = "<%= new java.util.Date()%>" />
            <liferay-ui:message key="confirmation.order.date.title" /><br/>
            <strong><span id="orderDate"><fmt:formatDate value="${now}" pattern="dd MMM yyyy"/></span></strong>
        </p>
    </div>
</div>

<liferay-journal:journal-article articleId="<%= LicensingWebContent.CONFIRMATION.getArticleId() %>" groupId="${ groupId }"/>



