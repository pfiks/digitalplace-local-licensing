<%@ include file="/init.jsp" %>

<h1><liferay-ui:message key="contact.team.title" /></h1>

<liferay-journal:journal-article articleId="<%= LicensingWebContent.CONTACT_THE_LICENSING_TEAM.getArticleId() %>" groupId="${ groupId }"/>
