<%@ include file="init.jsp" %>

	<div>
		<div>
			<span>
				<liferay-ui:message key="steps.header.title" />
			</span>
			<span>
				:
				<liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\" 5\", \"5\"}%>"/>
			</span>
		</div>
		<hr />
		<h1>
			<c:choose>
				<c:when test="${licenceStatus == 'EXPIRED'}">
					<liferay-ui:message key="your.licence.has.expired.title" />
				</c:when>
				<c:when test="${licenceStatus == 'RENEWAL_TOO_EARLY'}">
					<liferay-ui:message key="your.renewal.is.too.soon.title" />
				</c:when>
				<c:otherwise>
					<liferay-ui:message key="general.error.title" />
				</c:otherwise>
			</c:choose>
		</h1>
	</div>
	<br />

	<portlet:actionURL var="actionURL">
		<portlet:param name="execution" value="${ flowExecutionKey }" />
		<portlet:param name="_eventId" value="back" />
	</portlet:actionURL>

	<portlet:renderURL var="contactActionURL">
		<portlet:param name="mvcPath" value="/contactTheLicensingTeam.jsp" />
	</portlet:renderURL>

	<c:choose>
		<c:when test="${licenceStatus == 'EXPIRED'}">
			<liferay-journal:journal-article
				articleId="<%= LicensingWebContent.YOUR_LICENCE_HAS_EXPIRED.getArticleId() %>"
				groupId="${ portletGroupId }" />
		</c:when>
		<c:when test="${licenceStatus == 'RENEWAL_TOO_EARLY'}">
			<liferay-journal:journal-article
				articleId="<%= LicensingWebContent.YOUR_RENEWAL_IS_TOO_SOON.getArticleId() %>"
				groupId="${ portletGroupId }" />
		</c:when>
		<c:otherwise>
			<div class="info">
				<div class="info__title">
					<liferay-ui:message key="general.error.code" />
				</div>
			</div>

			<p>
				<liferay-ui:message key="general.error.bottom.text" /> <a href="${ contactActionURL }" id="contactLink">
					<liferay-ui:message key="payment.error.contact.link" />
				</a>
			</p>
		</c:otherwise>
	</c:choose>