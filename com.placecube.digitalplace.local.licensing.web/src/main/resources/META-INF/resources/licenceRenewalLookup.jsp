<%@ include file="init.jsp" %>

    <div>
        <div>
            <span>
                <liferay-ui:message key="steps.header.title" />
            </span>
            <span>
                :
                <liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\" 1\", \"5\"}%>"></liferay-ui:message>
            </span>
        </div>
        <hr />
        <h1>
            <liferay-ui:message key="your.details.title" />
        </h1>
    </div>
    <br />
    <portlet:renderURL var="licenceLookupUrl">
        <portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.CONFIRM_DETAILS %>" />
    </portlet:renderURL>

    <c:if test="${ error }">
        <div style="padding: 15px; border: solid red 1px;">
            <liferay-ui:message key="taxiLicenceNumber.not.found" />
        </div>
    </c:if>

    <aui:form action="<%= licenceLookupUrl %>" method="post" name="fm">
        <aui:row>
            <aui:col width="50" cssClass="pl-0">
                <aui:input label="reference.licence.number.label" name="referenceLicenceNumber"
                    placeholder="reference.licence.number.placeholder" type="text">
                    <aui:validator name="required" errorMessage="reference.licence.number.validation" />
                </aui:input>
                <div style="margin-top: -15px;">
                    <liferay-ui:message key="reference.licence.number.label.instructions" />
                </div>
            </aui:col>
        </aui:row>
        <br />
        <aui:row>
            <aui:col width="50" cssClass="pl-0">
                <aui:input label="driving.licence.number.label" name="drivingLicenceNumber"
                    placeholder="driving.licence.number.placeholder" type="text">
                    <aui:validator name="required" errorMessage="driving.licence.number.validation" />
                </aui:input>
                <div style="margin-top: -15px;">
                    <liferay-ui:message key="driving.licence.number.label.instructions" />
                </div>
            </aui:col>
        </aui:row>
        <br />

        <button type="submit" class="btn btn-primary">
            <liferay-ui:message key="next" />
        </button>
    </aui:form>