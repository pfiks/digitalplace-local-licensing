<%@ include file="/init.jsp" %>

    <div>
        <div>
            <span>
                <liferay-ui:message key="steps.header.title" />
            </span>
            <span>
                :
                <liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\" 2\", \"5\"}%>"/>
            </span>
        </div>
        <hr />
        <h1>
            <liferay-ui:message key="confirm.your.details.title" />
        </h1>
    </div>
    <br />
    <div style="padding: 15px;border-left: 10px solid lightgray;">
        <div>
            <liferay-ui:message key="full.name.title" /><br />
            <strong>${licenceDetails.firstName} ${licenceDetails.lastName}</strong>
        </div>
        <br />
        <div>
            <liferay-ui:message key="home.address.title" /><br />
            <strong>${licenceDetails.address.fullAddress}</strong>
            <portlet:renderURL var="changeAddressRenderURL">
                <portlet:param name="mvcPath" value="/changeAddress.jsp" />
            </portlet:renderURL>
            <div>
                <a href="${ changeAddressRenderURL }" id="changeAddressLink">
                    <liferay-ui:message key="change.address.link" />
                </a>
            </div>
        </div>
        <br />
        <div>
            <liferay-ui:message key="licence.type.title" /><br />
            <strong>
                <c:if test="${fn:endsWith(licenceDetails.taxiLicenceNumber, 'TXPHD')}">
                    <liferay-ui:message key="licence.type.txphd" />
                </c:if>
                <c:if test="${fn:endsWith(licenceDetails.taxiLicenceNumber, 'TXHCD')}">
                    <liferay-ui:message key="licence.type.txhcd" />
                </c:if>
            </strong>
        </div>
        <br />
        <fmt:parseDate value="${licenceDetails.renewalDate}" pattern="yyyy-MM-dd" var="parsedDate" type="date" />
        <fmt:formatDate value="${parsedDate}" pattern="dd MMM yyyy" var="renewalDateTime" />
        <liferay-ui:message key="current.licence.expiry.date.title" /><br />
        <c:choose>
            <c:when test="${licenceDetails.status == LicenceStatus.RENEWAL_TOO_LATE}">
                <div class="info" id="lateRenewal">
                    <div class="info__title">
                        <ul>
                            <li>
                                <liferay-ui:message key="your.licence.expires.on" /> ${renewalDateTime}
                            </li>
                            <li>
                                <liferay-ui:message key="it.might.take" />
                            </li>
                        </ul>
                    </div>
                    <div class="info__copy">
                        <p>
                            <liferay-ui:message key="well.try.to.process.your.application" /> ${renewalDateTime}
                            <strong>
                                <liferay-ui:message key="you.wont.be.able.to.drive.your.vehicle" />
                            </strong>.
                        </p>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div>
                    <strong> ${renewalDateTime} </strong>
                </div>
            </c:otherwise>
        </c:choose>
        <br />

        <portlet:renderURL var="contactActionURL">
            <portlet:param name="mvcPath" value="/contactTheLicensingTeam.jsp" />
        </portlet:renderURL>
        <a href="${ contactActionURL }">
            <liferay-ui:message key="these.details.are.incorrect.link" />
        </a>
    </div>
    <br />
    <h2>
        <liferay-ui:message key="your.other.contact.details" />
    </h2>
    <br />
    <portlet:renderURL var="confirmYourDetailsURL">
        <portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.VIEW_DECLERATION %>" />
    </portlet:renderURL>

    <aui:form action="<%= confirmYourDetailsURL %>">
        <aui:input type="hidden" name="formSubmitted" value="true" />
        <aui:row>
            <aui:col width="50" cssClass="pl-0">
                <aui:input label="email.address.label" name="email" value="${licenceDetails.email}" type="text">
                    <aui:validator name="required" />
                    <aui:validator name="email" />
                </aui:input>
                <div style="margin-top: -15px;">
                    <liferay-ui:message key="email.address.label.instructions" />
                </div>
            </aui:col>
        </aui:row>
        <br />
        <aui:row>
            <aui:col width="50" cssClass="pl-0">
                <aui:input label="mobile.label" name="mobile" value="${licenceDetails.mobile}" type="text">
                    <aui:validator name="required" />
                    <aui:validator name="digits" />
                </aui:input>
            </aui:col>
        </aui:row>
        <br />
        <liferay-journal:journal-article
            articleId="<%= LicensingWebContent.CONFIRM_YOUR_DETAILS_BOTTOM.getArticleId() %>" groupId="${ groupId }" />
        <br />
        <button type="submit" class="btn btn-primary">
            <liferay-ui:message key="next" />
        </button>
    </aui:form>