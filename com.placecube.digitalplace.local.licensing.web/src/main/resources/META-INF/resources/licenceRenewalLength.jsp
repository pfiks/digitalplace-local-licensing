<%@ include file="/init.jsp" %>

    <div>
        <div>
            <span>
                <liferay-ui:message key="steps.header.title" />
            </span>
            <span>
                :
                <liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\" 4\", \"5\"}%>"/>
            </span>
        </div>
        <hr />
        <h1>
            <c:choose>
                <c:when test="${ licenceDetails.eligibleThreeYearRenewal || licenceDetails.eligibleTwoYearRenewal }">
                    <liferay-ui:message key="confirm.and.pay.title.licence.length" />
                </c:when>
                <c:otherwise>
                    <liferay-ui:message key="confirm.and.pay.title.one.year" />
                </c:otherwise>
            </c:choose>
        </h1>
    </div>
    <br />

    <portlet:actionURL name="<%= MVCCommandKeys.ACTION_LICENCE_PAYMENT %>" var="paymentUrl" />

    <form method="POST" action="<%= paymentUrl %>">
        <fmt:parseDate value="${licenceDetails.renewalDate}" pattern="yyyy-MM-dd" var="parsedDate" type="date" />
        <fmt:formatDate pattern="yyyy" value="${parsedDate}" var="expYear" />
        <fmt:formatDate value="${parsedDate}" pattern="dd MMM " var="expDate" />
        <c:if test="${ !licenceDetails.eligibleThreeYearRenewal && !licenceDetails.eligibleTwoYearRenewal }">

            <input type="hidden" name="renewalPeriod" value="ONE_YEAR" id="field-renewalPeriod" />

            <div>
                <div>
                    <liferay-ui:message key="confirm.and.pay.cost.label" />
                    &pound;${ configuration.paymentOneYearAmount().setScale(2, java.math.RoundingMode.CEILING) }
                </div>
                <div>
                    <p>
                        <liferay-ui:message key="confirm.and.pay.your.new.licence.will.expire.on" />
                        <strong>${expDate} ${expYear + 1}</strong>
                    </p>
                </div>
            </div>

            <liferay-journal:journal-article
                articleId="<%= LicensingWebContent.CONFIRM_AND_PAYONEYEAR.getArticleId() %>"
                groupId="${ portletGroupId }" />

        </c:if>

        <c:if test="${ licenceDetails.eligibleThreeYearRenewal || licenceDetails.eligibleTwoYearRenewal }">

            <span class="lead">
                <liferay-ui:message key="confirm.and.pay.licence.length.legend" />
            </span>

            <div class="row">
                <div class="col-sm-6 pl-0">
                    <input type="radio" name="<portlet:namespace />renewalPeriod" id="oneYear" value="ONE_YEAR"
                        onclick="javascript:displayRenewalDate('${ expYear + 1 }')" />
                    <label for="oneYear" class="lead ml-2">
                        <liferay-ui:message key="confirm.and.pay.licence.length.one.year" />
                        &pound;${ paymentOneYearAmount }
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 pl-0">
                    <input type="radio" name="<portlet:namespace />renewalPeriod" id="twoYear" value="TWO_YEARS"
                        onclick="javascript:displayRenewalDate('${ expYear + 2 }')" />
                    <label for="twoYear" class="lead ml-2">
                        <liferay-ui:message key="confirm.and.pay.licence.length.two.year" />
                        &pound;${ paymentTwoYearAmount }
                    </label>
                </div>
            </div>
            <c:if test="${ licenceDetails.eligibleThreeYearRenewal }">
                <div class="row">
                    <div class="col-sm-6 pl-0">
                        <input type="radio" name="<portlet:namespace />renewalPeriod" id="threeYear" value="THREE_YEARS"
                            onclick="javascript:displayRenewalDate('${ expYear + 3 }')" />
                        <label for="threeYear" class="lead ml-2">
                            <liferay-ui:message key="confirm.and.pay.licence.length.three.year" />
                            &pound;${ paymentThreeYearAmount }
                        </label>
                    </div>
                </div>
            </c:if>

            <br />

            <p id="renewalDateSection" aria-hidden="true" class="pb-4" style="display:none;">
                <liferay-ui:message key="confirm.and.pay.your.new.licence.will.expire.on" />
                <strong>${expDate} <label id="resultYear"></label>
                </strong>
            </p>

            <liferay-journal:journal-article
                articleId="<%= LicensingWebContent.CONFIRM_AND_PAY_MULTIYEAR.getArticleId() %>"
                groupId="${ portletGroupId }" />

        </c:if>
        <br />
        <div>
            <button type="submit" class="btn btn-primary">
                <liferay-ui:message key="confirm.and.pay.pay.now" />
            </button>
            <div class="text-muted pt-3">
                <small>
                    <liferay-ui:message key="confirm.and.pay.pay.now.subtext.1" /><br />
                    <liferay-ui:message key="confirm.and.pay.pay.now.subtext.2" />
                </small>
            </div>
        </div>

    </form>

    <script>
        function displayRenewalDate(renewalDate) {
            document.getElementById('renewalDateSection').style.display = 'block';
            document.getElementById('resultYear').innerHTML = renewalDate;
        }
    </script>