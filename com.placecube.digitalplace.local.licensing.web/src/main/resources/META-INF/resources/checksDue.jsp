<%@ include file="/init.jsp" %>

<div>
    <div>
        <span>
            <liferay-ui:message key="steps.header.title" /></span>
        <span>
            :
            <liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\"5\", \"5\"}%>"/>
        </span>
    </div>
    <hr />
    <h1>
        <liferay-ui:message key="checks.due.title" />
    </h1>
</div>
<br />
<portlet:actionURL var="contactActionURL">
      <portlet:param name="execution" value="${ flowExecutionKey }" />
    <portlet:param name="_eventId" value="contact" />
</portlet:actionURL>

<c:forEach var="check" items="${licenceDetails.licenceChecks}">
    <c:if test="${check.due && check.name != 'dbsRenewal'}">
        <fmt:parseDate value="${check.dueDate}" pattern="yyyy-MM-dd" var="parsedDate" type="date" />
        <fmt:formatDate value="${parsedDate}" pattern="dd MMM yyyy" var="formattedDueDate" />
        <div class="info">
            <div>
                <c:if test="${check.dueDate == null}">
                    <strong><liferay-ui:message key="${check.label}.no.date" /></strong>
                </c:if>
                <c:if test="${check.dueDate != null}">
                    <c:if test="${check.historic}">
                        <strong><liferay-ui:message key="${check.label}.historic" /> ${formattedDueDate}</strong>
                    </c:if>
                    <c:if test="${!check.historic}">
                        <strong>
                            <liferay-ui:message key="${check.label}" />
                            ${formattedDueDate}
                        </strong>
                    </c:if>
                </c:if>
            </div>
            <div class="info__copy">
                <p><liferay-ui:message key="checks.due.contact.team.1" /> <a href="${ contactActionURL }"><liferay-ui:message key="checks.due.contact.team.link" /></a> <liferay-ui:message key="checks.due.contact.team.2" /></p>
            </div>
        </div>
    </c:if>
    <c:if test="${check.due && check.name == 'dbsRenewal'}">
        <div class="info" id="dbsRenewalNotClear">
            <div class="info__title">
                <strong><liferay-ui:message key="dbs.renewal.new.info" /></strong>
            </div>
            <div class="info__copy">
                <p><liferay-ui:message key="checks.due.contact.team.1" /> <a href="${ contactActionURL }"><liferay-ui:message key="checks.due.contact.team.link" /></a> <liferay-ui:message key="checks.due.contact.team.2" /></p>
            </div>
        </div>
    </c:if>
</c:forEach>

<liferay-journal:journal-article articleId="<%= LicensingWebContent.CHECKS_DUE_FIND_OUT_MORE.getArticleId() %>" groupId="${groupId}"/>
