<%@ include file="init.jsp" %>

	<div>
		<div>
			<span>
				<liferay-ui:message key="steps.header.title" />
			</span>
			<span>
				:
				<liferay-ui:message key="step.x.of.x" arguments="<%=new String[]{\" 5\", \"5\"}%>"/>
			</span>
		</div>
		<hr />
		<h1>
			<liferay-ui:message key="payment.error.title" />
		</h1>
	</div>
	<br />

	<portlet:actionURL var="actionURL">
		<portlet:param name="execution" value="${ flowExecutionKey }" />
		<portlet:param name="_eventId" value="back" />
	</portlet:actionURL>

	<portlet:renderURL var="contactActionURL">
		<portlet:param name="mvcPath" value="/contactTheLicensingTeam.jsp" />
	</portlet:renderURL>

	<div class="info">
		<div class="info__title">
			<liferay-ui:message key="payment.error.code" />
		</div>
		<div class="info__copy">
			<a href="${ actionURL }" id="backLink">
				<liferay-ui:message key="payment.error.back.link" />
			</a>
		</div>
	</div>

	<p>
		<liferay-ui:message key="payment.error.bottom.text" /> <a href="${ contactActionURL }" id="contactLink">
			<liferay-ui:message key="payment.error.contact.link" />
		</a>
	</p>