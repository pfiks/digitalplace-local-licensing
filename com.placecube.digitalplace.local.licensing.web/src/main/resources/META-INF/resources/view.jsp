<%@ include file="/init.jsp" %>

<h1>
    <liferay-ui:message key="start.renewal.process.title" />
</h1>

<liferay-journal:journal-article articleId="<%= LicensingWebContent.RENEWAL_LEAD_IN_TOP.getArticleId() %>" groupId="${groupId}"/>
<portlet:renderURL var="renewLicenceURL">
    <portlet:param name="mvcPath" value="/licenceRenewalLookup.jsp" />
</portlet:renderURL>

<p class="pt-4 pb-4">
    <a href="${renewLicenceURL}" class="btn btn-primary">
        <liferay-ui:message key="start.renewal.process.button" />
    </a>
</p>

<liferay-journal:journal-article articleId="<%= LicensingWebContent.RENEWAL_LEAD_IN_BOTTOM.getArticleId() %>" groupId="${groupId}"/>