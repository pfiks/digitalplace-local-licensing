package com.placecube.digitalplace.local.licensing.service.impl;

import java.math.BigDecimal;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.model.LicenceApplicationContext;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.model.LicenceStatus;
import com.placecube.digitalplace.local.licensing.service.LicensingConnector;
import com.placecube.digitalplace.local.licensing.service.LicensingService;
import com.placecube.digitalplace.local.licensing.service.impl.util.LicencingConnectorRetrievalUtil;

@Component(immediate = true, service = LicensingService.class)
public class LicensingServiceImpl implements LicensingService {

	@Reference
	private LicencingConnectorRetrievalUtil licencingConnectorRetrievalUtil;

	@Override
	public String applyForNewLicence(long companyId, LicenceApplicationContext licenceApplicationContext) throws LicenceRenewalException {
		return licencingConnectorRetrievalUtil.getLicensingConnector(companyId).applyForNewLicence(companyId, licenceApplicationContext);
	}

	@Override
	public void changeAddress(long companyId, String licenceId, AddressContext address) throws LicenceRenewalException {
		licencingConnectorRetrievalUtil.getLicensingConnector(companyId).changeAddress(licenceId, address);
	}

	@Override
	public void changeLicenceStatus(long companyId, String licenceId, boolean statusExists) throws LicenceRenewalException {
		LicensingConnector licensingConnector = licencingConnectorRetrievalUtil.getLicensingConnector(companyId);

		LicenceDetails licence = licensingConnector.getLicenceDetailsByLicenceId(licenceId);

		if (statusExists || licence.getStatus() == null) {
			if (licensingConnector.hasLicenceExpired(licenceId)) {
				licence.setStatus(LicenceStatus.EXPIRED);
			} else if (licensingConnector.isRenewalTooLate(licenceId)) {
				licence.setStatus(LicenceStatus.RENEWAL_TOO_LATE);
			} else if (licensingConnector.isRenewalTooSoon(licenceId)) {
				licence.setStatus(LicenceStatus.RENEWAL_TOO_EARLY);
			} else if (Validator.isNull(licensingConnector.getFirstRegisteredDate(licenceId))) {
				licence.setStatus(LicenceStatus.ERROR);
			} else {
				licence.setStatus(LicenceStatus.VALID);
			}
		}
	}

	@Override
	public String getFirstRegisteredDate(long companyId, String licenceId) throws LicenceRenewalException {
		return licencingConnectorRetrievalUtil.getLicensingConnector(companyId).getFirstRegisteredDate(licenceId);
	}

	@Override
	public LicenceDetails getLicenceDetailsByLicenceId(long companyId, String licenceId) throws LicenceRenewalException {
		return licencingConnectorRetrievalUtil.getLicensingConnector(companyId).getLicenceDetailsByLicenceId(licenceId);
	}

	@Override
	public LicenceDetails getLicenceDetailsByReference(long companyId, String renewalReference) throws LicenceRenewalException {
		return licencingConnectorRetrievalUtil.getLicensingConnector(companyId).getLicenceDetailsByReference(renewalReference);
	}

	@Override
	public String getVehicleRenewalDateTime(long companyId, String licenceId) throws LicenceRenewalException {
		return licencingConnectorRetrievalUtil.getLicensingConnector(companyId).getVehicleRenewalDateTime(licenceId);
	}

	@Override
	public String getVehicleRenewalLicenseLength(long companyId, String licenceId) throws LicenceRenewalException {
		return licencingConnectorRetrievalUtil.getLicensingConnector(companyId).getVehicleRenewalLicenseLength(licenceId);
	}

	@Override
	public int getVehicleRenewalLicenseLengthIntMonths(long companyId, String licenceId) throws LicenceRenewalException {
		return licencingConnectorRetrievalUtil.getLicensingConnector(companyId).getVehicleRenewalLicenseLengthIntMonths(licenceId);
	}

	@Override
	public BigDecimal getVehicleRenewalPrice(long companyId, String licenceId) throws LicenceRenewalException {
		return licencingConnectorRetrievalUtil.getLicensingConnector(companyId).getVehicleRenewalPrice(licenceId);
	}

	@Override
	public boolean hasLicenceExpired(long companyId, String licenceId) throws LicenceRenewalException {
		return licencingConnectorRetrievalUtil.getLicensingConnector(companyId).hasLicenceExpired(licenceId);
	}

	@Override
	public boolean isRenewalTooLate(long companyId, String licenceId) throws LicenceRenewalException {
		return licencingConnectorRetrievalUtil.getLicensingConnector(companyId).isRenewalTooLate(licenceId);
	}

	@Override
	public boolean isRenewalTooSoon(long companyId, String licenceId) throws LicenceRenewalException {
		return licencingConnectorRetrievalUtil.getLicensingConnector(companyId).isRenewalTooSoon(licenceId);
	}

	@Override
	public void renewLicence(long companyId, String licenceId, int monthsToAdd, String paymentReference) throws LicenceRenewalException {
		licencingConnectorRetrievalUtil.getLicensingConnector(companyId).renewLicence(licenceId, monthsToAdd, paymentReference);
	}


}
