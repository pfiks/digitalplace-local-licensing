package com.placecube.digitalplace.local.licensing.service.impl.util;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.service.LicensingConnector;

@Component(immediate = true, service = LicencingConnectorRetrievalUtil.class)
public class LicencingConnectorRetrievalUtil {

	private Set<LicensingConnector> licensingConnectors = new LinkedHashSet<>();

	public LicensingConnector getLicensingConnector(long companyId) throws LicenceRenewalException {
		Optional<LicensingConnector> licensingConnector = licensingConnectors.stream().filter(entry -> entry.enabled(companyId)).findFirst();
		if (licensingConnector.isPresent()) {
			return licensingConnector.get();
		} else {
			throw new LicenceRenewalException("No licensing connector configured");
		}
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setLicensingConnector(LicensingConnector licensingConnector) {
		licensingConnectors.add(licensingConnector);
	}

	protected void unsetLicensingConnector(LicensingConnector licensingConnector) {
		licensingConnectors.remove(licensingConnector);
	}
}
