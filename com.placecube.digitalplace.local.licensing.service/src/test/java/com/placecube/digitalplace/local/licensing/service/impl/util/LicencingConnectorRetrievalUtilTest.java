package com.placecube.digitalplace.local.licensing.service.impl.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.service.LicensingConnector;

public class LicencingConnectorRetrievalUtilTest extends PowerMockito {

	private static final Long COMPANY_ID = 11l;

	@InjectMocks
	private LicencingConnectorRetrievalUtil licencingConnectorRetrievalUtil;

	@Mock
	private LicensingConnector mockLicensingConnector1;

	@Mock
	private LicensingConnector mockLicensingConnector2;

	@Mock
	private LicensingConnector mockLicensingConnector3;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getLicensingConnector_WhenMoreThanOneEnabledConnectorAvailableForTheCompany_ThenReturnTheFirstEnabledOne() throws Exception {
		addLicensingConnector(COMPANY_ID, mockLicensingConnector1, false);
		addLicensingConnector(COMPANY_ID, mockLicensingConnector2, true);
		addLicensingConnector(COMPANY_ID, mockLicensingConnector3, true);

		LicensingConnector result = licencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID);

		assertEquals(mockLicensingConnector2, result);
	}

	@Test(expected = LicenceRenewalException.class)
	public void getLicensingConnector_WhenThereAreNoEnabledConnectorsForCompany_ThenThrowException() throws Exception {
		addLicensingConnector(COMPANY_ID, mockLicensingConnector1, false);
		addLicensingConnector(1l, mockLicensingConnector2, true);

		licencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID);
	}

	private void addLicensingConnector(long companyId, LicensingConnector licensingConnector, boolean enabled) {
		when(licensingConnector.enabled(companyId)).thenReturn(enabled);
		licencingConnectorRetrievalUtil.setLicensingConnector(licensingConnector);
	}
}
