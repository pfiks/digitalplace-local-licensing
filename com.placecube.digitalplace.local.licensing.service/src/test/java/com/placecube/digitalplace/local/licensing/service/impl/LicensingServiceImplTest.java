package com.placecube.digitalplace.local.licensing.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.model.LicenceApplicationContext;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.model.LicenceStatus;
import com.placecube.digitalplace.local.licensing.service.LicensingConnector;
import com.placecube.digitalplace.local.licensing.service.impl.util.LicencingConnectorRetrievalUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LicensingServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 11;
	private static final String EXPECTED = "expectedValue";
	private static final String LICENCE_ID = "licenceIdValue";
	private static final String REFERENCE = "licenceRefValue";

	@InjectMocks
	private LicensingServiceImpl licensingServiceImpl;

	@Mock
	private AddressContext mockAddressContext;

	@Mock
	private LicenceApplicationContext mockLicenceApplicationContext;

	@Mock
	private LicenceDetails mockLicenceDetails;

	@Mock
	private LicencingConnectorRetrievalUtil mockLicencingConnectorRetrievalUtil;

	@Mock
	private LicensingConnector mockLicensingConnector;

	@Test(expected = LicenceRenewalException.class)
	public void applyForNewLicence_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.applyForNewLicence(COMPANY_ID, mockLicenceApplicationContext);
	}

	@Test
	public void applyForNewLicence_WhenNoError_ThenNewLicenceApplied() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);

		licensingServiceImpl.applyForNewLicence(COMPANY_ID, mockLicenceApplicationContext);

		verify(mockLicensingConnector, times(1)).applyForNewLicence(COMPANY_ID, mockLicenceApplicationContext);
	}

	@Test(expected = LicenceRenewalException.class)
	public void changeAddress_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.changeAddress(COMPANY_ID, LICENCE_ID, mockAddressContext);
	}

	@Test
	public void changeAddress_WhenNoError_ThenChangesTheAddress() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);

		licensingServiceImpl.changeAddress(COMPANY_ID, LICENCE_ID, mockAddressContext);

		verify(mockLicensingConnector, times(1)).changeAddress(LICENCE_ID, mockAddressContext);
	}

	@Test(expected = LicenceRenewalException.class)
	@Parameters({ "true", "false" })
	public void changeLicenceStatus_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException(boolean statusExists) throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.changeLicenceStatus(COMPANY_ID, LICENCE_ID, statusExists);
	}

	@Test
	public void changeLicenceStatus_WhenStatusDoesNotExistAndLicenceHasAstatus_ThenNoChangesAreMade() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getLicenceDetailsByLicenceId(LICENCE_ID)).thenReturn(mockLicenceDetails);
		when(mockLicenceDetails.getStatus()).thenReturn(LicenceStatus.CHECKS_DUE);

		licensingServiceImpl.changeLicenceStatus(COMPANY_ID, LICENCE_ID, false);

		verify(mockLicenceDetails, never()).setStatus(any());
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false" })
	public void changeLicenceStatus_WhenStatusExistsOrLicenceDoesNotHaveAstatusAndRenewalIsTooLate_ThenSetsTheStatusToRenewalTooLate(boolean statusExists, boolean licenceHasStatus)
			throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getLicenceDetailsByLicenceId(LICENCE_ID)).thenReturn(mockLicenceDetails);
		when(mockLicenceDetails.getStatus()).thenReturn(licenceHasStatus ? LicenceStatus.CHECKS_DUE : null);
		when(mockLicensingConnector.hasLicenceExpired(LICENCE_ID)).thenReturn(false);
		when(mockLicensingConnector.isRenewalTooLate(LICENCE_ID)).thenReturn(true);

		licensingServiceImpl.changeLicenceStatus(COMPANY_ID, LICENCE_ID, statusExists);

		verify(mockLicenceDetails, times(1)).setStatus(LicenceStatus.RENEWAL_TOO_LATE);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false" })
	public void changeLicenceStatus_WhenStatusExistsOrLicenceDoesNotHaveAstatusAndTheFirstRegisteredDateIsNull_ThenSetsTheStatusToError(boolean statusExists, boolean licenceHasStatus)
			throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getLicenceDetailsByLicenceId(LICENCE_ID)).thenReturn(mockLicenceDetails);
		when(mockLicenceDetails.getStatus()).thenReturn(licenceHasStatus ? LicenceStatus.CHECKS_DUE : null);
		when(mockLicensingConnector.hasLicenceExpired(LICENCE_ID)).thenReturn(false);
		when(mockLicensingConnector.isRenewalTooLate(LICENCE_ID)).thenReturn(false);
		when(mockLicensingConnector.isRenewalTooSoon(LICENCE_ID)).thenReturn(false);
		when(mockLicensingConnector.getFirstRegisteredDate(LICENCE_ID)).thenReturn(" ");

		licensingServiceImpl.changeLicenceStatus(COMPANY_ID, LICENCE_ID, statusExists);

		verify(mockLicenceDetails, times(1)).setStatus(LicenceStatus.ERROR);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false" })
	public void changeLicenceStatus_WhenStatusExistsOrLicenceDoesNotHaveAstatusAndTheLicenceHasExpired_ThenSetsTheStatusToExpired(boolean statusExists, boolean licenceHasStatus)
			throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getLicenceDetailsByLicenceId(LICENCE_ID)).thenReturn(mockLicenceDetails);
		when(mockLicenceDetails.getStatus()).thenReturn(licenceHasStatus ? LicenceStatus.CHECKS_DUE : null);
		when(mockLicensingConnector.hasLicenceExpired(LICENCE_ID)).thenReturn(true);

		licensingServiceImpl.changeLicenceStatus(COMPANY_ID, LICENCE_ID, statusExists);

		verify(mockLicenceDetails, times(1)).setStatus(LicenceStatus.EXPIRED);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false" })
	public void changeLicenceStatus_WhenStatusExistsOrLicenceDoesNotHaveAstatusAndTheRenewalIsTooSoon_ThenSetsTheStatusToRenewalTooEarly(boolean statusExists, boolean licenceHasStatus)
			throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getLicenceDetailsByLicenceId(LICENCE_ID)).thenReturn(mockLicenceDetails);
		when(mockLicenceDetails.getStatus()).thenReturn(licenceHasStatus ? LicenceStatus.CHECKS_DUE : null);
		when(mockLicensingConnector.hasLicenceExpired(LICENCE_ID)).thenReturn(false);
		when(mockLicensingConnector.isRenewalTooLate(LICENCE_ID)).thenReturn(false);
		when(mockLicensingConnector.isRenewalTooSoon(LICENCE_ID)).thenReturn(true);

		licensingServiceImpl.changeLicenceStatus(COMPANY_ID, LICENCE_ID, statusExists);

		verify(mockLicenceDetails, times(1)).setStatus(LicenceStatus.RENEWAL_TOO_EARLY);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false" })
	public void changeLicenceStatus_WhenStatusExistsOrLicenceIsNotExpiredAndRenewalIsNotTooLateNorTooSoonAndItHasATheFirstRegisteredDateValue_ThenSetsTheStatusToValid(boolean statusExists,
			boolean licenceHasStatus) throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getLicenceDetailsByLicenceId(LICENCE_ID)).thenReturn(mockLicenceDetails);
		when(mockLicenceDetails.getStatus()).thenReturn(licenceHasStatus ? LicenceStatus.CHECKS_DUE : null);
		when(mockLicensingConnector.hasLicenceExpired(LICENCE_ID)).thenReturn(false);
		when(mockLicensingConnector.isRenewalTooLate(LICENCE_ID)).thenReturn(false);
		when(mockLicensingConnector.isRenewalTooSoon(LICENCE_ID)).thenReturn(false);
		when(mockLicensingConnector.getFirstRegisteredDate(LICENCE_ID)).thenReturn("dateValue");

		licensingServiceImpl.changeLicenceStatus(COMPANY_ID, LICENCE_ID, statusExists);

		verify(mockLicenceDetails, times(1)).setStatus(LicenceStatus.VALID);
	}

	@Test(expected = LicenceRenewalException.class)
	public void getFirstRegisteredDate_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.getFirstRegisteredDate(COMPANY_ID, LICENCE_ID);
	}

	@Test
	public void getFirstRegisteredDate_WhenNoError_ThenReturnsTheFirstRegisteredDate() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getFirstRegisteredDate(LICENCE_ID)).thenReturn(EXPECTED);

		String result = licensingServiceImpl.getFirstRegisteredDate(COMPANY_ID, LICENCE_ID);

		assertThat(result, equalTo(EXPECTED));
	}

	@Test(expected = LicenceRenewalException.class)
	public void getLicenceDetailsByLicenceId_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.getLicenceDetailsByLicenceId(COMPANY_ID, LICENCE_ID);
	}

	@Test
	public void getLicenceDetailsByLicenceId_WhenNoError_ThenReturnsTheLicenceDetails() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getLicenceDetailsByLicenceId(LICENCE_ID)).thenReturn(mockLicenceDetails);

		LicenceDetails result = licensingServiceImpl.getLicenceDetailsByLicenceId(COMPANY_ID, LICENCE_ID);

		assertThat(result, sameInstance(mockLicenceDetails));
	}

	@Test(expected = LicenceRenewalException.class)
	public void getLicenceDetailsByReference_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.getLicenceDetailsByReference(COMPANY_ID, REFERENCE);
	}

	@Test
	public void getLicenceDetailsByReference_WhenNoError_ThenReturnsTheLicenceDetails() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getLicenceDetailsByReference(REFERENCE)).thenReturn(mockLicenceDetails);

		LicenceDetails result = licensingServiceImpl.getLicenceDetailsByReference(COMPANY_ID, REFERENCE);

		assertThat(result, sameInstance(mockLicenceDetails));
	}

	@Test(expected = LicenceRenewalException.class)
	public void getVehicleRenewalDateTime_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.getVehicleRenewalDateTime(COMPANY_ID, LICENCE_ID);
	}

	@Test
	public void getVehicleRenewalDateTime_WhenNoError_ThenReturnsTheRenewalDate() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getVehicleRenewalDateTime(LICENCE_ID)).thenReturn(EXPECTED);

		String result = licensingServiceImpl.getVehicleRenewalDateTime(COMPANY_ID, LICENCE_ID);

		assertThat(result, equalTo(EXPECTED));
	}

	@Test(expected = LicenceRenewalException.class)
	public void getVehicleRenewalLicenseLength_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.getVehicleRenewalLicenseLength(COMPANY_ID, LICENCE_ID);
	}

	@Test
	public void getVehicleRenewalLicenseLength_WhenNoError_ThenReturnsTheRenewalLength() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getVehicleRenewalLicenseLength(LICENCE_ID)).thenReturn(EXPECTED);

		String result = licensingServiceImpl.getVehicleRenewalLicenseLength(COMPANY_ID, LICENCE_ID);

		assertThat(result, equalTo(EXPECTED));
	}

	@Test(expected = LicenceRenewalException.class)
	public void getVehicleRenewalLicenseLengthIntMonths_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.getVehicleRenewalLicenseLengthIntMonths(COMPANY_ID, LICENCE_ID);
	}

	@Test
	public void getVehicleRenewalLicenseLengthIntMonths_WhenNoError_ThenReturnsTheRenewalLengthInMonths() throws LicenceRenewalException {
		int expected = 123;
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getVehicleRenewalLicenseLengthIntMonths(LICENCE_ID)).thenReturn(expected);

		int result = licensingServiceImpl.getVehicleRenewalLicenseLengthIntMonths(COMPANY_ID, LICENCE_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = LicenceRenewalException.class)
	public void getVehicleRenewalPrice_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.getVehicleRenewalPrice(COMPANY_ID, LICENCE_ID);
	}

	@Test
	public void getVehicleRenewalPrice_WhenNoError_ThenReturnsTheRenewalPrice() throws LicenceRenewalException {
		BigDecimal expected = BigDecimal.valueOf(123.56);
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.getVehicleRenewalPrice(LICENCE_ID)).thenReturn(expected);

		BigDecimal result = licensingServiceImpl.getVehicleRenewalPrice(COMPANY_ID, LICENCE_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = LicenceRenewalException.class)
	public void hasLicenceExpired_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.hasLicenceExpired(COMPANY_ID, LICENCE_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void hasLicenceExpired_WhenNoError_ThenReturnsIfTheLicenceHasExpired(boolean expected) throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.hasLicenceExpired(LICENCE_ID)).thenReturn(expected);

		boolean result = licensingServiceImpl.hasLicenceExpired(COMPANY_ID, LICENCE_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = LicenceRenewalException.class)
	public void isRenewalTooLate_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.isRenewalTooLate(COMPANY_ID, LICENCE_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isRenewalTooLate_WhenNoError_ThenReturnsIfTheRenewalIsTooLate(boolean expected) throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.isRenewalTooLate(LICENCE_ID)).thenReturn(expected);

		boolean result = licensingServiceImpl.isRenewalTooLate(COMPANY_ID, LICENCE_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = LicenceRenewalException.class)
	public void isRenewalTooSoon_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.isRenewalTooSoon(COMPANY_ID, LICENCE_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isRenewalTooSoon_WhenNoError_ThenReturnsIfTheRenewalIsTooSoon(boolean expected) throws LicenceRenewalException {
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);
		when(mockLicensingConnector.isRenewalTooSoon(LICENCE_ID)).thenReturn(expected);

		boolean result = licensingServiceImpl.isRenewalTooSoon(COMPANY_ID, LICENCE_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = LicenceRenewalException.class)
	public void renewLicence_WhenExceptionRetrievingTheConnector_ThenThrowsLicenceRenewalException() throws LicenceRenewalException {
		int months = 123;
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenThrow(new LicenceRenewalException());

		licensingServiceImpl.renewLicence(COMPANY_ID, LICENCE_ID, months, REFERENCE);
	}

	@Test
	public void renewLicence_WhenNoError_ThenRenewsTheLicence() throws LicenceRenewalException {
		int months = 123;
		when(mockLicencingConnectorRetrievalUtil.getLicensingConnector(COMPANY_ID)).thenReturn(mockLicensingConnector);

		licensingServiceImpl.renewLicence(COMPANY_ID, LICENCE_ID, months, REFERENCE);

		verify(mockLicensingConnector, times(1)).renewLicence(LICENCE_ID, months, REFERENCE);
	}
}
