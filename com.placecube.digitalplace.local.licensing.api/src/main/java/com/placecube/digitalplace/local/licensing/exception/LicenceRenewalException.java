package com.placecube.digitalplace.local.licensing.exception;

import com.liferay.portal.kernel.exception.PortalException;

public class LicenceRenewalException extends PortalException {

	private static final long serialVersionUID = 1L;

	public LicenceRenewalException() {
		super();
	}

	public LicenceRenewalException(String msg) {
		super(msg);
	}

	public LicenceRenewalException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public LicenceRenewalException(Throwable cause) {
		super(cause);
	}
}