package com.placecube.digitalplace.local.licensing.model;

public enum LicenceStatus {

	CHECKS_DUE("checks-due"), EXPIRED("expired"), RENEWAL_TOO_EARLY("renewal-too-early"), RENEWAL_TOO_LATE("renewal-too-late"), VALID("valid"), ERROR("error");

	private final String status;

	private LicenceStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
}