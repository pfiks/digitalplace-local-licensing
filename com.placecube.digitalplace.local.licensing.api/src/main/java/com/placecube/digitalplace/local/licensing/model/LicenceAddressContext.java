package com.placecube.digitalplace.local.licensing.model;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

public class LicenceAddressContext {

	private String addressLine1;

	private String addressLine2;

	private String addressLine3;

	private String city;

	private String postcode;

	private String uprn;

	public LicenceAddressContext() {

	}

	public LicenceAddressContext(String uprn, String addressLine1, String addressLine2, String addressLine3, String city, String postcode) {
		this.uprn = uprn;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.addressLine3 = addressLine3;
		this.city = city;
		this.postcode = postcode;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public String getCity() {
		return city;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getUprn() {
		return uprn;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public void setUprn(String uprn) {
		this.uprn = uprn;
	}

	public String toJSON() {

		JSONObject json = JSONFactoryUtil.createJSONObject();

		json.put("addressLine1", addressLine1);
		json.put("addressLine2", addressLine2);
		json.put("addressLine3", addressLine3);
		json.put("city", city);
		json.put("postcode", postcode);
		json.put("uprn", uprn);

		return json.toString();
	}

}
