package com.placecube.digitalplace.local.licensing.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

public class LicenceApplicationContext {

	private String details;

	private LicenceAddressContext licenceAddressContext;

	private Map<String, Serializable> parameters = new HashMap<>();

	private String registrationNumber;

	private String typeOfLicense;

	private String vehicleColour;

	private String vehicleFuel;

	private String vehicleMake;

	private String vehicleModel;

	private String vehicleNumberOfSeats;

	private String vehicleRegistrationDate;

	private String vehicleSize;

	public String getDetails() {
		return details;
	}

	public LicenceAddressContext getLicenceAddressContext() {
		return licenceAddressContext;
	}

	public Map<String, Serializable> getParameters() {
		return parameters;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public String getTypeOfLicense() {
		return typeOfLicense;
	}

	public String getVehicleColour() {
		return vehicleColour;
	}

	public String getVehicleFuel() {
		return vehicleFuel;
	}

	public String getVehicleMake() {
		return vehicleMake;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public String getVehicleNumberOfSeats() {
		return vehicleNumberOfSeats;
	}

	public String getVehicleRegistrationDate() {
		return vehicleRegistrationDate;
	}

	public String getVehicleSize() {
		return vehicleSize;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public void setLicenceAddressContext(LicenceAddressContext licenceAddressContext) {
		this.licenceAddressContext = licenceAddressContext;
	}

	public void setParameters(Map<String, Serializable> parameters) {
		this.parameters = parameters;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public void setTypeOfLicense(String typeOfLicense) {
		this.typeOfLicense = typeOfLicense;
	}

	public void setVehicleColour(String vehicleColour) {
		this.vehicleColour = vehicleColour;
	}

	public void setVehicleFuel(String vehicleFuel) {
		this.vehicleFuel = vehicleFuel;
	}

	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public void setVehicleNumberOfSeats(String vehicleNumberOfSeats) {
		this.vehicleNumberOfSeats = vehicleNumberOfSeats;
	}

	public void setVehicleRegistrationDate(String vehicleRegistrationDate) {
		this.vehicleRegistrationDate = vehicleRegistrationDate;
	}

	public void setVehicleSize(String vehicleSize) {
		this.vehicleSize = vehicleSize;
	}

	public String toJSON() {
		
		JSONObject json = JSONFactoryUtil.createJSONObject();

		json.put("details", details);
		json.put("registrationNumber", registrationNumber);
		json.put("typeOfLicense", typeOfLicense);
		json.put("vehicleColour", vehicleColour);
		json.put("vehicleFuel", vehicleFuel);
		json.put("vehicleMake", vehicleMake);
		json.put("vehicleModel", vehicleModel);
		json.put("vehicleNumberOfSeats", vehicleNumberOfSeats);
		json.put("vehicleRegistrationDate", vehicleRegistrationDate);
		json.put("vehicleSize", vehicleSize);
		json.put("parameters", parameters);
		try {
			json.put("licenceAddressContext", JSONFactoryUtil.createJSONObject(licenceAddressContext.toJSON()));
		} catch (JSONException e) {
			// Ignore parse exception
		}

		return json.toString();
	}

}
