package com.placecube.digitalplace.local.licensing.model;

import java.io.Serializable;
import java.time.LocalDate;

public class LicenceCheck implements Serializable {

	private static final long serialVersionUID = 2L;
	
	private boolean due = false;

	private LocalDate dueDate;

	private boolean historic = false;

	private String label;

	private String name;
	

	public LicenceCheck(boolean due, String name, String label, boolean historic, LocalDate dueDate) {
		this.due = due;
		this.label = label;
		this.name = name;
		this.historic = historic;
		this.dueDate = dueDate;
	}

	public boolean getDue() {
		return due;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public boolean getHistoric() {
		return historic;
	}

	public String getLabel() {
		return label;
	}

	public String getName() {
		return name;
	}
}
