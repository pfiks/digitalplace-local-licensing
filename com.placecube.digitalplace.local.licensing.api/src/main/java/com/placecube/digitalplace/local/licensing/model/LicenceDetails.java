package com.placecube.digitalplace.local.licensing.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import com.placecube.digitalplace.address.model.AddressContext;

public class LicenceDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private AddressContext address;
	private String drivingLicenceNumber;
	private boolean eligibleOneYearRenewal = true;
	private boolean eligibleThreeYearRenewal = true;
	private boolean eligibleTwoYearRenewal = true;
	private String email;
	private String firstName;
	private String lastName;

	private List<LicenceCheck> licenceChecks;

	private String licenceId;
	private String mobile;

	private boolean noCriminalConvictionsConfirmed = false;
	private boolean noInvestigationConfirmed = false;
	private boolean noMedicalConditionsConfirmed = false;

	private LocalDate renewalDate;
	private LicenceStatus status;
	private String taxiLicenceNumber;

	public AddressContext getAddress() {
		return address;
	}

	public String getDrivingLicenceNumber() {
		return drivingLicenceNumber;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public List<LicenceCheck> getLicenceChecks() {
		if (licenceChecks != null) {
			return licenceChecks;
		} else {
			return Collections.emptyList();
		}
	}

	public String getLicenceId() {
		return licenceId;
	}

	public String getMobile() {
		return mobile;
	}

	public LocalDate getRenewalDate() {
		return renewalDate;
	}

	public LicenceStatus getStatus() {
		return status;
	}

	public String getTaxiLicenceNumber() {
		return taxiLicenceNumber;
	}

	public boolean isEligibleOneYearRenewal() {
		return eligibleOneYearRenewal;
	}

	public boolean isEligibleThreeYearRenewal() {
		return eligibleThreeYearRenewal;
	}

	public boolean isEligibleTwoYearRenewal() {
		return eligibleTwoYearRenewal;
	}

	public boolean isNoCriminalConvictionsConfirmed() {
		return noCriminalConvictionsConfirmed;
	}

	public boolean isNoInvestigationConfirmed() {
		return noInvestigationConfirmed;
	}

	public boolean isNoMedicalConditionsConfirmed() {
		return noMedicalConditionsConfirmed;
	}

	public void setAddress(AddressContext address) {
		this.address = address;
	}

	public void setDrivingLicenceNumber(String drivingLicenceNumber) {
		this.drivingLicenceNumber = drivingLicenceNumber;
	}

	public void setEligibleOneYearRenewal(boolean eligibleOneYearRenewal) {
		this.eligibleOneYearRenewal = eligibleOneYearRenewal;
	}

	public void setEligibleThreeYearRenewal(boolean eligibleThreeYearRenewal) {
		this.eligibleThreeYearRenewal = eligibleThreeYearRenewal;
	}

	public void setEligibleTwoYearRenewal(boolean eligibleTwoYearRenewal) {
		this.eligibleTwoYearRenewal = eligibleTwoYearRenewal;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setLicenceChecks(List<LicenceCheck> licenceChecks) {
		this.licenceChecks = licenceChecks;
	}

	public void setLicenceId(String licenceId) {
		this.licenceId = licenceId;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setNoCriminalConvictionsConfirmed(boolean noCriminalConvictionsConfirmed) {
		this.noCriminalConvictionsConfirmed = noCriminalConvictionsConfirmed;
	}

	public void setNoInvestigationConfirmed(boolean noInvestigationConfirmed) {
		this.noInvestigationConfirmed = noInvestigationConfirmed;
	}

	public void setNoMedicalConditionsConfirmed(boolean noMedicalConditionsConfirmed) {
		this.noMedicalConditionsConfirmed = noMedicalConditionsConfirmed;
	}

	public void setRenewalDate(LocalDate renewalDate) {
		this.renewalDate = renewalDate;
	}

	public void setStatus(LicenceStatus status) {
		this.status = status;
	}

	public void setTaxiLicenceNumber(String taxiLicenceNumber) {
		this.taxiLicenceNumber = taxiLicenceNumber;
	}

}