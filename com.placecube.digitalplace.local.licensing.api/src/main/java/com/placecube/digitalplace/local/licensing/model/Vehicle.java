package com.placecube.digitalplace.local.licensing.model;

import java.io.Serializable;

public class Vehicle implements Serializable {

	private static final long serialVersionUID = 1L;

	private String colour;
	private String make;
	private String model;
	private String regNumber;
	private int seats;

	public Vehicle(String regNumber, String make, String model, String colour, int seats) {
		this.regNumber = regNumber;
		this.make = make;
		this.model = model;
		this.colour = colour;
		this.seats = seats;
	}

	public String getColour() {
		return colour;
	}

	public String getMake() {
		return make;
	}

	public String getModel() {
		return model;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public int getSeats() {
		return seats;
	}

}