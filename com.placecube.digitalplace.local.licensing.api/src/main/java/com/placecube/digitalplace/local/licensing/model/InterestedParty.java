package com.placecube.digitalplace.local.licensing.model;

import java.io.Serializable;

import com.placecube.digitalplace.address.model.AddressContext;

public class InterestedParty implements Serializable {

	private static final long serialVersionUID = 2061525755577585908L;

	private AddressContext address;
	private String firstName;
	private String lastName;

	public InterestedParty(String firstName, String lastName, AddressContext address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
	}

	public AddressContext getAddress() {
		return address;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

}