package com.placecube.digitalplace.local.licensing.service;

import java.math.BigDecimal;

import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.model.LicenceApplicationContext;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;

public interface LicensingConnector {

	String applyForNewLicence(long companyId, LicenceApplicationContext licenceApplicationContext) throws LicenceRenewalException;

	void changeAddress(String licenceId, AddressContext address);

	boolean enabled(long companyId);

	String getFirstRegisteredDate(String licenceId) throws LicenceRenewalException;

	LicenceDetails getLicenceDetailsByLicenceId(String licenceId);

	LicenceDetails getLicenceDetailsByReference(String renewalReference);

	String getVehicleRenewalDateTime(String licenceId) throws LicenceRenewalException;

	String getVehicleRenewalLicenseLength(String licenceId) throws LicenceRenewalException;

	int getVehicleRenewalLicenseLengthIntMonths(String licenceId) throws LicenceRenewalException;

	BigDecimal getVehicleRenewalPrice(String licenceId) throws LicenceRenewalException;

	boolean hasLicenceExpired(String licenceId) throws LicenceRenewalException;

	boolean isRenewalTooLate(String licenceId) throws LicenceRenewalException;

	boolean isRenewalTooSoon(String licenceId) throws LicenceRenewalException;

	void renewLicence(String licenceId, int monthsToAdd, String paymentReference) throws LicenceRenewalException;

}
