package com.placecube.digitalplace.local.licensing.service;

import java.math.BigDecimal;

import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.model.LicenceApplicationContext;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;

public interface LicensingService {

	/**
	 * Submits the required information for a new licence application.
	 *
	 * @param companyId needed to load the right service connector
	 * @param licenceApplicationContext new application data for the new licence
	 * @throws LicenceRenewalException is thrown when connector not found
	 */
	String applyForNewLicence(long companyId, LicenceApplicationContext licenceApplicationContext) throws LicenceRenewalException;

	/**
	 * Changes the address associated with taxi licence.
	 *
	 * @param companyId needed to load the right service connector
	 * @param licenceId taxi licence Id
	 * @param address new address to be added to licence
	 * @throws LicenceRenewalException is thrown when connector not found
	 */
	void changeAddress(long companyId, String licenceId, AddressContext address) throws LicenceRenewalException;

	/**
	 * Set the Licence Status
	 *
	 * @param companyId companyId needed to load the right service connector
	 * @param licenceId the licence on which to set the status
	 * @param statusExists if true will overwrite the existing status else set a
	 *            status if one hasn't yet
	 * @throws LicenceRenewalException when licence can't be found
	 */
	void changeLicenceStatus(long companyId, String licenceId, boolean statusExists) throws LicenceRenewalException;

	/**
	 * Gets the first registered date.
	 *
	 * @param companyId companyId needed to load the right service connector
	 * @param licenceId the licence on which to get the formatted registered
	 *            date
	 * @return the formatted registered date for the licence
	 * @throws LicenceRenewalException when licence can't be found
	 */
	String getFirstRegisteredDate(long companyId, String licenceId) throws LicenceRenewalException;

	/**
	 *
	 * @param companyId needed to load the right service connector
	 * @param licenceId id of the licence
	 * @return taxi licence detail for given parameters, or null of no licnece
	 *         found
	 * @throws LicenceRenewalException is thrown when connector not found
	 */
	LicenceDetails getLicenceDetailsByLicenceId(long companyId, String licenceId) throws LicenceRenewalException;

	/**
	 *
	 * @param companyId needed to load the right service connector
	 * @param renewalReference the 12-digit number on renewal notification and
	 *            on paper licence
	 * @return taxi licence detail for given parameters, or null of no licnece
	 *         found
	 * @throws LicenceRenewalException is thrown when connector not found
	 */
	LicenceDetails getLicenceDetailsByReference(long companyId, String renewalReference) throws LicenceRenewalException;

	/**
	 * Generates the renewal date time for a vehicle license
	 *
	 * @param companyId companyId needed to load the right service connector
	 * @param licenceId the licence on which to generate the renewal date time
	 * @return renewal date time for vehicle licence
	 * @throws LicenceRenewalException when licence can't be found
	 */
	String getVehicleRenewalDateTime(long companyId, String licenceId) throws LicenceRenewalException;

	/**
	 * Generates the renewal licence length for a vehicle license
	 *
	 * @param companyId companyId needed to load the right service connector
	 * @param licenceId the licence on which to generate the renewal licence
	 *            length
	 * @return renewal licence length for vehicle licence
	 * @throws LicenceRenewalException when licence can't be found
	 */
	String getVehicleRenewalLicenseLength(long companyId, String licenceId) throws LicenceRenewalException;

	/**
	 * Generates the renewal licence length for a vehicle license
	 *
	 * @param companyId companyId needed to load the right service connector
	 * @param licenceId the licence on which to generate the renewal licence
	 *            length
	 * @return renewal licence length for vehicle licence
	 * @throws LicenceRenewalException when licence can't be found
	 */
	int getVehicleRenewalLicenseLengthIntMonths(long companyId, String licenceId) throws LicenceRenewalException;

	/**
	 * Generates the renewal price for a vehicle license
	 *
	 * @param companyId companyId needed to load the right service connector
	 * @param licenceId the licence on which to generate the renewal price
	 * @return renewal price for vehicle licence
	 * @throws LicenceRenewalException when licence can't be found
	 */
	BigDecimal getVehicleRenewalPrice(long companyId, String licenceId) throws LicenceRenewalException;

	/**
	 * Check if the driver's licence has expired
	 *
	 * @param companyId needed to load the right service connector
	 * @param licenceId the licence on which to check if expired
	 * @return true if licence has expire
	 * @throws LicenceRenewalException when licence can't be found
	 */
	boolean hasLicenceExpired(long companyId, String licenceId) throws LicenceRenewalException;

	/**
	 * Check if driver is renewing too close to their renewal date
	 *
	 * @param companyId needed to load the right service connector
	 * @param licenceId the licence on which to check if renewal is too late
	 * @return true if the renewal is too late
	 * @throws LicenceRenewalException when licence can't be found
	 */
	boolean isRenewalTooLate(long companyId, String licenceId) throws LicenceRenewalException;

	/**
	 * Check if the driver is trying to renew before they are allowed
	 *
	 * @param companyId needed to load the right service connector
	 * @param licenceId the licence on which to check if renewal is too soon
	 * @return true if the renewal is too soon
	 * @throws LicenceRenewalException when licence can't be found
	 */
	boolean isRenewalTooSoon(long companyId, String licenceId) throws LicenceRenewalException;

	/**
	 * Extends given taxi licence for given number of years.
	 *
	 * @param companyId needed to load the right service connector
	 * @param licenceId licence to be renewed
	 * @param monthsToAdd number of months it will be extended by
	 * @param paymentReference proof of payment
	 * @throws LicenceRenewalException when renewal unsuccessful
	 */
	void renewLicence(long companyId, String licenceId, int monthsToAdd, String paymentReference) throws LicenceRenewalException;

}
