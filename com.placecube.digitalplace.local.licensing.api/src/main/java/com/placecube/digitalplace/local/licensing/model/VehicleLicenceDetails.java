package com.placecube.digitalplace.local.licensing.model;

import java.time.LocalDate;
import java.util.List;

public class VehicleLicenceDetails extends LicenceDetails {

	private static final long serialVersionUID = 1L;

	private LocalDate firstRegistered;

	private boolean logBookV5;
	private boolean meter;
	private InterestedParty registeredKeeper;

	private List<InterestedParty> thirdParties;
	private Vehicle vehicle;

	public LocalDate getFirstRegistered() {
		return firstRegistered;
	}

	public InterestedParty getRegisteredKeeper() {
		return registeredKeeper;
	}

	public List<InterestedParty> getThirdParties() {
		return thirdParties;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public boolean isLogBookV5() {
		return logBookV5;
	}

	public boolean isMeter() {
		return meter;
	}

	public void setFirstRegistered(LocalDate firstRegistered) {
		this.firstRegistered = firstRegistered;
	}

	public void setLogBookV5(boolean logBookV5) {
		this.logBookV5 = logBookV5;
	}

	public void setMeter(boolean meter) {
		this.meter = meter;
	}

	public void setRegisteredKeeper(InterestedParty registeredKeeper) {
		this.registeredKeeper = registeredKeeper;
	}

	public void setThirdParties(List<InterestedParty> thirdParties) {
		this.thirdParties = thirdParties;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

}