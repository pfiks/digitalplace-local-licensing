package com.placecube.digitalplace.local.licensing.civicaapp.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.licensing.civicaapp.configuration.CivicaAppCompanyConfiguration", localization = "content/Language", name = "licensing-app")
public interface CivicaAppCompanyConfiguration {

	@Meta.AD(required = false, deflt = "http://ntappgistest/appuniwebservice/WebInterop.asmx", name = "app-endpoint-url")
	String appEndpointUrl();

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "", name = "password", type = Meta.Type.Password)
	String password();

	@Meta.AD(required = false, deflt = "", name = "username")
	String username();

}
