package com.placecube.digitalplace.local.licensing.civicaapp.util;

import java.io.Serializable;
import java.util.Map;

import javax.xml.soap.SOAPException;

import org.apache.axis.message.MessageElement;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.civicaapp.constants.AppSoapKeys;
import com.placecube.digitalplace.local.licensing.model.LicenceAddressContext;
import com.placecube.digitalplace.local.licensing.model.LicenceApplicationContext;

@Component(immediate = true, service = MessageElementUtil.class)
public class MessageElementUtil {

	@Reference
	private ObjectFactoryService objectFactoryService;

	public MessageElement[] buildTypeUpdateULIXmlMessageElements(LicenceApplicationContext licenceApplicationContext) throws SOAPException {
		Map<String, Serializable> licenceParametersMap = licenceApplicationContext.getParameters();

		// activity Record
		MessageElement activityRecordElementElement = getActivityRecordElement();

		// activity
		MessageElement activityElement = getActivityElement(licenceParametersMap, licenceApplicationContext);

		activityRecordElementElement.addChild(activityElement);

		// miscActivity
		MessageElement miscActivityElement = getMiscActivityElement(licenceParametersMap);

		activityRecordElementElement.addChild(miscActivityElement);

		// ActivityText
		MessageElement activityTextElement = getActivityTextElement(licenceParametersMap);

		activityRecordElementElement.addChild(activityTextElement);

		// Action

		MessageElement actionElement = getActionElement(licenceParametersMap);

		activityRecordElementElement.addChild(actionElement);

		// NamAdd

		MessageElement namAddElement = getNamAddElement(licenceParametersMap, licenceApplicationContext);

		activityRecordElementElement.addChild(namAddElement);

		// importRecord
		MessageElement importRecordElement = getImportRecordElement();

		// importControl
		MessageElement importControlElement = getImportControlElement(licenceParametersMap);

		importRecordElement.addChild(importControlElement);

		importRecordElement.addChild(activityRecordElementElement);

		return getRootMessageElements(importRecordElement);
	}

	private MessageElement getActionElement(Map<String, Serializable> licenceParametersMap) throws SOAPException {

		String ahAction = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.AH_ACTION));

		MessageElement dataElement = objectFactoryService.getNewMessageElement();
		dataElement.setName(AppSoapKeys.DATA);
		dataElement.addChildElement(AppSoapKeys.AH_ACTION).addTextNode(ahAction);

		MessageElement headElement = objectFactoryService.getNewMessageElement();
		headElement.setName(AppSoapKeys.HEAD);

		headElement.addChild(dataElement);

		MessageElement actionElement = objectFactoryService.getNewMessageElement();
		actionElement.setName(AppSoapKeys.ACTION);

		actionElement.addChild(headElement);

		return actionElement;
	}

	private MessageElement getActivityElement(Map<String, Serializable> licenceParametersMap, LicenceApplicationContext licenceApplicationContext) throws SOAPException {

		String openDate = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.OPEN_DATE));
		String unit = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.UNIT));
		String type = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.TYPE));
		String invOfficer = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.INV_OFFICER));
		String fee = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.FEE));

		MessageElement activityElement = objectFactoryService.getNewMessageElement();
		activityElement.setName(AppSoapKeys.ACTIVITY);

		setKeyAndValuesToElement(activityElement, AppSoapKeys.OPEN_DATE, openDate);
		setKeyAndValuesToElement(activityElement, AppSoapKeys.UNIT, unit);
		setKeyAndValuesToElement(activityElement, AppSoapKeys.TYPE, type);
		setKeyAndValuesToElement(activityElement, AppSoapKeys.FEE, fee);
		setKeyAndValuesToElement(activityElement, AppSoapKeys.INV_OFFICER, invOfficer);

		activityElement.addChildElement(AppSoapKeys.RESPONSE_DATE);
		activityElement.addChildElement(AppSoapKeys.CLOSED_DATE);

		// address
		MessageElement addressElement = getAddressElement(licenceApplicationContext);

		activityElement.addChild(addressElement);

		return activityElement;
	}

	private MessageElement getActivityRecordElement() {
		MessageElement importRecordElement = objectFactoryService.getNewMessageElement();
		importRecordElement.setName(AppSoapKeys.ACTIVITY_RECORD);
		return importRecordElement;
	}

	private MessageElement getActivityTextElement(Map<String, Serializable> licenceParametersMap) throws SOAPException {

		String updateMode = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.UPDATE_MODE));
		String text = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.TEXT));

		MessageElement activityTextElement = objectFactoryService.getNewMessageElement();
		activityTextElement.setName(AppSoapKeys.ACTIVITY_TEXT);

		setKeyAndValuesToElement(activityTextElement, AppSoapKeys.UPDATE_MODE, updateMode);
		setKeyAndValuesToElement(activityTextElement, AppSoapKeys.TEXT, text);

		return activityTextElement;
	}

	private MessageElement getAddressElement(LicenceApplicationContext licenceApplicationContext) throws SOAPException {
		LicenceAddressContext licenceAddressContext = licenceApplicationContext.getLicenceAddressContext();
		Map<String, Serializable> licenceParametersMap = licenceApplicationContext.getParameters();

		MessageElement addressElement = objectFactoryService.getNewMessageElement();
		addressElement.setName(AppSoapKeys.ADDRESS);

		String name = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.NAME), "");
		if (Validator.isNull(name)) {
			name = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.FORE_NAMES)) + " " + GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.SUR_NAME));
		}
		setKeyAndValuesToElement(addressElement, AppSoapKeys.NAME, name);

		MessageElement looseElement = objectFactoryService.getNewMessageElement();
		looseElement.setName(AppSoapKeys.LOOSE_ADDRESS);

		setKeyAndValuesToElement(looseElement, AppSoapKeys.ADDR_LINE1, licenceAddressContext.getAddressLine1());
		setKeyAndValuesToElement(looseElement, AppSoapKeys.ADDR_LINE2, licenceAddressContext.getAddressLine2());
		setKeyAndValuesToElement(looseElement, AppSoapKeys.ADDR_LINE3, licenceAddressContext.getAddressLine3());
		looseElement.addChildElement(AppSoapKeys.ADDR_LINE4);
		looseElement.addChildElement(AppSoapKeys.ADDR_LINE5);
		setKeyAndValuesToElement(looseElement, AppSoapKeys.POST_CODE, licenceAddressContext.getPostcode());
		setKeyAndValuesToElement(looseElement, AppSoapKeys.UPRN, licenceAddressContext.getUprn());

		addressElement.addChild(looseElement);

		return addressElement;

	}

	private MessageElement getImportControlElement(Map<String, Serializable> licenceParametersMap) throws SOAPException {

		String updtype = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.UPD_TYPE));
		String flareDB = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.FLARE_DB));
		String number = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.NUMBER));
		String laref = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.LA_REF));

		MessageElement importControlElement = objectFactoryService.getNewMessageElement();
		importControlElement.setName(AppSoapKeys.IMPORT_CONTROL);

		setKeyAndValuesToElement(importControlElement, AppSoapKeys.UPD_TYPE, updtype);
		setKeyAndValuesToElement(importControlElement, AppSoapKeys.FLARE_DB, flareDB);
		setKeyAndValuesToElement(importControlElement, AppSoapKeys.NUMBER, number);
		setKeyAndValuesToElement(importControlElement, AppSoapKeys.LA_REF, laref);

		return importControlElement;
	}

	private MessageElement getImportRecordElement() {
		MessageElement importRecordElement = objectFactoryService.getNewMessageElement();
		importRecordElement.setName(AppSoapKeys.IMPORT_RECORD);
		return importRecordElement;
	}

	private MessageElement getMiscActivityElement(Map<String, Serializable> licenceParametersMap) throws SOAPException {

		String desc1 = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.DESC1));
		String desc2 = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.DESC2));

		MessageElement miscActivityElement = objectFactoryService.getNewMessageElement();
		miscActivityElement.setName(AppSoapKeys.MISC_ACTIVITY);

		MessageElement miscActElement = objectFactoryService.getNewMessageElement();
		miscActElement.setName(AppSoapKeys.MISC_ACT);

		setKeyAndValuesToElement(miscActElement, AppSoapKeys.DESC1, desc1);
		setKeyAndValuesToElement(miscActElement, AppSoapKeys.DESC2, desc2);

		miscActivityElement.addChild(miscActElement);

		return miscActivityElement;
	}

	private MessageElement getNamAddElement(Map<String, Serializable> licenceParametersMap, LicenceApplicationContext licenceApplicationContext) throws SOAPException {
		String laref = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.LA_REF));

		// address
		MessageElement addressElement = getAddressElement(licenceApplicationContext);

		String telephone = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.TELEPHONE));
		String mobile = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.MOBILE));
		String email = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.EMAIL));

		setKeyAndValuesToElement(addressElement, AppSoapKeys.TELEPHONE, telephone);
		setKeyAndValuesToElement(addressElement, AppSoapKeys.MOBILE, mobile);
		setKeyAndValuesToElement(addressElement, AppSoapKeys.EMAIL, email);
		setKeyAndValuesToElement(addressElement, AppSoapKeys.LA_REF, laref);

		// SplitName
		MessageElement splitNameElement = objectFactoryService.getNewMessageElement();
		splitNameElement.setName(AppSoapKeys.SPLITE_NAME);

		String title = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.TITLE));
		String forenames = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.FORE_NAMES));
		String surname = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.SUR_NAME));
		String suffix = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.SUFFIX));

		setKeyAndValuesToElement(splitNameElement, AppSoapKeys.TITLE, title);
		setKeyAndValuesToElement(splitNameElement, AppSoapKeys.FORE_NAMES, forenames);
		setKeyAndValuesToElement(splitNameElement, AppSoapKeys.SUR_NAME, surname);
		setKeyAndValuesToElement(splitNameElement, AppSoapKeys.SUFFIX, suffix);

		// findby
		MessageElement findByElement = objectFactoryService.getNewMessageElement();
		findByElement.setName(AppSoapKeys.FIND_BY);
		findByElement.addChildElement(AppSoapKeys.LA_REF).addTextNode(laref);

		// data
		MessageElement dataElement = objectFactoryService.getNewMessageElement();
		dataElement.setName(AppSoapKeys.DATA);

		String recordId = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.RECORD_ID));
		String typeCode = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.TYPE_CODE));
		String organisation = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.ORGANISATION));
		String naUpdtype = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.NA_UPD_TYPE));

		setKeyAndValuesToElement(dataElement, AppSoapKeys.RECORD_ID, recordId);
		setKeyAndValuesToElement(dataElement, AppSoapKeys.TYPE_CODE, typeCode);
		setKeyAndValuesToElement(dataElement, AppSoapKeys.ORGANISATION, organisation);
		setKeyAndValuesToElement(dataElement, AppSoapKeys.NA_UPD_TYPE, naUpdtype);

		dataElement.addChild(findByElement);
		dataElement.addChild(addressElement);
		dataElement.addChild(splitNameElement);

		String preferredTelephone = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.PREFERRED_TELEPHONE));
		String preferredDaytime = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.PREFERRED_DATE_TIME));
		String telephone2 = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.TELEPHONE2));
		String telephone3 = GetterUtil.getString(licenceParametersMap.get(AppSoapKeys.TELEPHONE3));

		setKeyAndValuesToElement(dataElement, AppSoapKeys.PREFERRED_TELEPHONE, preferredTelephone);
		setKeyAndValuesToElement(dataElement, AppSoapKeys.PREFERRED_DATE_TIME, preferredDaytime);
		setKeyAndValuesToElement(dataElement, AppSoapKeys.TELEPHONE2, telephone2);
		setKeyAndValuesToElement(dataElement, AppSoapKeys.TELEPHONE3, telephone3);

		MessageElement namAddElement = objectFactoryService.getNewMessageElement();
		namAddElement.setName(AppSoapKeys.NAM_ADD);

		namAddElement.addChild(dataElement);

		return namAddElement;
	}

	private MessageElement[] getRootMessageElements(MessageElement importRecordElement) throws SOAPException {

		MessageElement importFileElement = objectFactoryService.getNewMessageElement();
		importFileElement.setName(AppSoapKeys.IMPORT_FILE);
		importFileElement.setAttributeNS("", "xmlns", AppSoapKeys.XMLNS_URL);
		importFileElement.setAttributeNS("", "xmlns:BS7666", AppSoapKeys.XMLNS_BS7666_URL);
		importFileElement.setAttributeNS("", "xmlns:xsi", AppSoapKeys.XMLNS_xsi_URL);

		importFileElement.addChild(importRecordElement);

		MessageElement[] messageElements = new MessageElement[1];
		messageElements[0] = importFileElement;
		return messageElements;
	}

	private static void setKeyAndValuesToElement(MessageElement messageElement, String key, String value) throws SOAPException {
		if (Validator.isNotNull(value)) {

			messageElement.addChildElement(key).addTextNode(value);
		} else {
			messageElement.addChildElement(key);
		}
	}
}
