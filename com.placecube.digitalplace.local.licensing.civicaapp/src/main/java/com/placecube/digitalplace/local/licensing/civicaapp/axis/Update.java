/**
 * Update.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.civicaapp.axis;

public class Update  implements java.io.Serializable {
    private UpdateULIxml ULIxml;

    public Update() {
    }

    public Update(
           UpdateULIxml ULIxml) {
           this.ULIxml = ULIxml;
    }


    /**
     * Gets the ULIxml value for this Update.
     * 
     * @return ULIxml
     */
    public UpdateULIxml getULIxml() {
        return ULIxml;
    }


    /**
     * Sets the ULIxml value for this Update.
     * 
     * @param ULIxml
     */
    public void setULIxml(UpdateULIxml ULIxml) {
        this.ULIxml = ULIxml;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Update)) return false;
        Update other = (Update) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ULIxml==null && other.getULIxml()==null) || 
             (this.ULIxml!=null &&
              this.ULIxml.equals(other.getULIxml())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getULIxml() != null) {
            _hashCode += getULIxml().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Update.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", ">Update"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ULIxml");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "ULIxml"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", ">>Update>ULIxml"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
