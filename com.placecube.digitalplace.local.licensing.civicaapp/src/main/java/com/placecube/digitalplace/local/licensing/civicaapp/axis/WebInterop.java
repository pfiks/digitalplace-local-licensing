/**
 * WebInterop.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.civicaapp.axis;

public interface WebInterop extends javax.xml.rpc.Service {

/**
 * Flare for Windows Interoperability Web Service
 */
    public java.lang.String getWebInteropSoapAddress();

    public WebInteropSoap getWebInteropSoap() throws javax.xml.rpc.ServiceException;

    public WebInteropSoap getWebInteropSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
