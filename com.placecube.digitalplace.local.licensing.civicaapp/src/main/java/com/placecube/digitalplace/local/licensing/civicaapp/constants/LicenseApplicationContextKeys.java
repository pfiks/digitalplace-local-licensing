package com.placecube.digitalplace.local.licensing.civicaapp.constants;

public final class LicenseApplicationContextKeys {

	public static final String AH_ACTION_DRIVER_LICENCE_AND_HACKNEY_CARRIAGE_VEHICLE_CODE = "LG1";

	public static final String AH_ACTION_PRIVATE_HIRE_VEHICLE_LICENCE_CODE = "LP2";

	public static final String AH_ACTION_TAXI_OPERATOR_LICENCE_CODE = "L01";

	public static final String FLARE_DB_CODE = "M";

	public static final String IMPORT_TYPE_CODE = "E7";

	public static final String LDU_CODE = "LDU";

	public static final String LICENSE_TYPE_TAXI_DRIVER_LICENCE = "DRIVER";

	public static final String LICENSE_TYPE_TAXI_OPERATOR_LICENCE = "OPERATOR";

	public static final String LICENSE_TYPE_TAXI_VEHICLE_LICENCE = "VEHICLE";

	public static final String TAXI_DRIVER_LICENCE_CODE = "LCD";

	public static final String TAXI_OPERATOR_LICENCE_CODE = "LO1";

	public static final String TAXI_VEHICLE_LICENCE_HACKNEY_CARRIAGE_CODE = "LT2";

	public static final String TAXI_VEHICLE_LICENCE_PRIVATE_HIRE_CODE = "LP2";

	public static final String UNIT_CODE = "F";

	public static final String UPDATE_MODE_CODE = "A";

	public static final String VEHICLE_TYPE_HACKNEY_CARRIAGE = "HackneyCarriage";

	public static final String VEHICLE_TYPE_PRIVATE_HIRE = "PrivateHire";

	private LicenseApplicationContextKeys() {
	}

}