package com.placecube.digitalplace.local.licensing.civicaapp.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.placecube.digitalplace.local.licensing.civicaapp.axis.WebInteropLocator;
import com.placecube.digitalplace.local.licensing.civicaapp.axis.WebInteropSoap;
import com.placecube.digitalplace.local.licensing.civicaapp.configuration.CivicaAppCompanyConfiguration;
import com.placecube.digitalplace.local.licensing.civicaapp.util.ObjectFactoryService;

@Component(immediate = true, service = CivicaAppAxisService.class)
public class CivicaAppAxisService {

	@Reference
	private CivicaAppConfigurationService civicaAppConfigurationService;
	
	@Reference
	private ObjectFactoryService objectFactoryService;

	public WebInteropSoap getCivicaAppAxisService(long companyId) throws Exception {
		CivicaAppCompanyConfiguration configuration = civicaAppConfigurationService.getConfiguration(companyId);

		WebInteropLocator serviceLocation = objectFactoryService.getServiceLocator();
		serviceLocation.setWebInteropSoapEndpointAddress(civicaAppConfigurationService.getAppEndpointUrl(configuration));

		return serviceLocation.getWebInteropSoap();
	}



}