package com.placecube.digitalplace.local.licensing.civicaapp.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.civicaapp.configuration.CivicaAppCompanyConfiguration;

@Component(immediate = true, service = CivicaAppConfigurationService.class)
public class CivicaAppConfigurationService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public String getAppEndpointUrl(CivicaAppCompanyConfiguration configuration) throws ConfigurationException {
		String appEndpointUrl = configuration.appEndpointUrl();

		if (Validator.isNull(appEndpointUrl)) {
			throw new ConfigurationException("App End point Url configuration cannot be empty");
		}

		return appEndpointUrl;
	}

	public CivicaAppCompanyConfiguration getConfiguration(Long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(CivicaAppCompanyConfiguration.class, companyId);
	}

	public String getPassword(CivicaAppCompanyConfiguration configuration) throws ConfigurationException {
		String password = configuration.password();

		if (Validator.isNull(password)) {
			throw new ConfigurationException("Password configuration cannot be empty");
		}

		return password;
	}

	public String getUsername(CivicaAppCompanyConfiguration configuration) throws ConfigurationException {
		String username = configuration.username();

		if (Validator.isNull(username)) {
			throw new ConfigurationException("Username configuration cannot be empty");
		}

		return username;
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		CivicaAppCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(CivicaAppCompanyConfiguration.class, companyId);
		return configuration.enabled();
	}

}
