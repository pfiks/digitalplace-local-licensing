package com.placecube.digitalplace.local.licensing.civicaapp.util;

import java.time.LocalDate;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.civicaapp.constants.AppSoapKeys;
import com.placecube.digitalplace.local.licensing.civicaapp.constants.JsonInputKeys;
import com.placecube.digitalplace.local.licensing.civicaapp.constants.LicenseApplicationContextKeys;
import com.placecube.digitalplace.local.licensing.model.LicenceApplicationContext;

public class AppLicenceApplicationContextUtil {

	public static LicenceApplicationContext getLicenceApplicationContext(String jsonRawInput) throws JSONException {
		JSONObject jsonInput = JSONFactoryUtil.createJSONObject(jsonRawInput);
		LicenceApplicationContext licenceApplicationContext = JSONFactoryUtil.looseDeserialize(jsonRawInput, LicenceApplicationContext.class);

		// fixed values
		licenceApplicationContext.getParameters().put(AppSoapKeys.FLARE_DB, LicenseApplicationContextKeys.FLARE_DB_CODE);
		licenceApplicationContext.getParameters().put(AppSoapKeys.UNIT, LicenseApplicationContextKeys.UNIT_CODE);
		licenceApplicationContext.getParameters().put(AppSoapKeys.UPDATE_MODE, LicenseApplicationContextKeys.UPDATE_MODE_CODE);
		licenceApplicationContext.getParameters().put(AppSoapKeys.TYPE_CODE, LicenseApplicationContextKeys.LDU_CODE);

		// ImportControl
		licenceApplicationContext.getParameters().put(AppSoapKeys.UPD_TYPE, jsonInput.getString(JsonInputKeys.UPD_TYPE));
		licenceApplicationContext.getParameters().put(AppSoapKeys.NUMBER, jsonInput.getString(JsonInputKeys.NUMBER));
		licenceApplicationContext.getParameters().put(AppSoapKeys.LA_REF, jsonInput.getString(JsonInputKeys.LA_REF));

		// activity

		licenceApplicationContext.getParameters().put(AppSoapKeys.OPEN_DATE, LocalDate.now().toString());
		licenceApplicationContext.getParameters().put(AppSoapKeys.RESPONSE_DATE, jsonInput.getString(JsonInputKeys.RESPONSE_DATE));
		licenceApplicationContext.getParameters().put(AppSoapKeys.CLOSED_DATE, jsonInput.getString(JsonInputKeys.CLOSED_DATE));

		licenceApplicationContext.getParameters().put(AppSoapKeys.INV_OFFICER, jsonInput.getString(JsonInputKeys.INV_OFFICER));
		licenceApplicationContext.getParameters().put(AppSoapKeys.FEE, jsonInput.getString(JsonInputKeys.FEE_PAID));
		licenceApplicationContext.getParameters().put(AppSoapKeys.NAME, jsonInput.getString(JsonInputKeys.NAME));

		String vehicleType = jsonInput.getString(JsonInputKeys.VEHICLE_TYPE);

		if (Validator.isNotNull(licenceApplicationContext.getTypeOfLicense())) {

			if (licenceApplicationContext.getTypeOfLicense().equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_TAXI_DRIVER_LICENCE)) {

				licenceApplicationContext.getParameters().put(AppSoapKeys.TYPE, LicenseApplicationContextKeys.TAXI_DRIVER_LICENCE_CODE);

				licenceApplicationContext.getParameters().put(AppSoapKeys.AH_ACTION, LicenseApplicationContextKeys.AH_ACTION_DRIVER_LICENCE_AND_HACKNEY_CARRIAGE_VEHICLE_CODE);

			} else if (licenceApplicationContext.getTypeOfLicense().equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_TAXI_OPERATOR_LICENCE)) {

				licenceApplicationContext.getParameters().put(AppSoapKeys.TYPE, LicenseApplicationContextKeys.TAXI_OPERATOR_LICENCE_CODE);

				licenceApplicationContext.getParameters().put(AppSoapKeys.AH_ACTION, LicenseApplicationContextKeys.AH_ACTION_TAXI_OPERATOR_LICENCE_CODE);

			} else if (licenceApplicationContext.getTypeOfLicense().equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_TAXI_VEHICLE_LICENCE)
					&& vehicleType.equalsIgnoreCase(LicenseApplicationContextKeys.VEHICLE_TYPE_HACKNEY_CARRIAGE)) {

				licenceApplicationContext.getParameters().put(AppSoapKeys.TYPE, LicenseApplicationContextKeys.TAXI_VEHICLE_LICENCE_HACKNEY_CARRIAGE_CODE);

				licenceApplicationContext.getParameters().put(AppSoapKeys.AH_ACTION, LicenseApplicationContextKeys.AH_ACTION_DRIVER_LICENCE_AND_HACKNEY_CARRIAGE_VEHICLE_CODE);

			} else if (licenceApplicationContext.getTypeOfLicense().equalsIgnoreCase(LicenseApplicationContextKeys.LICENSE_TYPE_TAXI_VEHICLE_LICENCE)
					&& vehicleType.equalsIgnoreCase(LicenseApplicationContextKeys.VEHICLE_TYPE_PRIVATE_HIRE)) {

				licenceApplicationContext.getParameters().put(AppSoapKeys.TYPE, LicenseApplicationContextKeys.TAXI_VEHICLE_LICENCE_PRIVATE_HIRE_CODE);

				licenceApplicationContext.getParameters().put(AppSoapKeys.AH_ACTION, LicenseApplicationContextKeys.AH_ACTION_PRIVATE_HIRE_VEHICLE_LICENCE_CODE);

			}
		}

		// MiscActivity
		licenceApplicationContext.getParameters().put(AppSoapKeys.DESC1, jsonInput.getString(JsonInputKeys.DESCRIPTION_1));
		licenceApplicationContext.getParameters().put(AppSoapKeys.DESC2, jsonInput.getString(JsonInputKeys.DESCRIPTION_2));

		// ActivityText

		licenceApplicationContext.getParameters().put(AppSoapKeys.TEXT, jsonInput.getString(JsonInputKeys.TEXT));

		//// NamAdd

		// address
		licenceApplicationContext.getParameters().put(AppSoapKeys.TELEPHONE, jsonInput.getString(JsonInputKeys.TELEPHONE));
		licenceApplicationContext.getParameters().put(AppSoapKeys.MOBILE, jsonInput.getString(JsonInputKeys.MOBILE));
		licenceApplicationContext.getParameters().put(AppSoapKeys.EMAIL, jsonInput.getString(JsonInputKeys.EMAIL));

		// SplitName
		licenceApplicationContext.getParameters().put(AppSoapKeys.TITLE, jsonInput.getString(JsonInputKeys.TITLE));
		licenceApplicationContext.getParameters().put(AppSoapKeys.FORE_NAMES, jsonInput.getString(JsonInputKeys.FIRST_NAME));
		licenceApplicationContext.getParameters().put(AppSoapKeys.SUR_NAME, jsonInput.getString(JsonInputKeys.FAMILY_NAME));
		licenceApplicationContext.getParameters().put(AppSoapKeys.SUFFIX, jsonInput.getString(JsonInputKeys.SUFFIX));

		// data
		licenceApplicationContext.getParameters().put(AppSoapKeys.RECORD_ID, jsonInput.getString(JsonInputKeys.RECORD_ID));
		licenceApplicationContext.getParameters().put(AppSoapKeys.ORGANISATION, jsonInput.getString(JsonInputKeys.ORGANISATION));
		licenceApplicationContext.getParameters().put(AppSoapKeys.NA_UPD_TYPE, jsonInput.getString(JsonInputKeys.NA_UPD_TYPE));
		licenceApplicationContext.getParameters().put(AppSoapKeys.PREFERRED_TELEPHONE, jsonInput.getString(JsonInputKeys.PREFERRED_TELEPHONE));
		licenceApplicationContext.getParameters().put(AppSoapKeys.PREFERRED_DATE_TIME, jsonInput.getString(JsonInputKeys.PREFERRED_DAY_TIME));
		licenceApplicationContext.getParameters().put(AppSoapKeys.TELEPHONE2, jsonInput.getString(JsonInputKeys.TELEPHONE2));
		licenceApplicationContext.getParameters().put(AppSoapKeys.TELEPHONE3, jsonInput.getString(JsonInputKeys.TELEPHONE3));

		return licenceApplicationContext;
	}

}
