package com.placecube.digitalplace.local.licensing.civicaapp.service;

import java.math.BigDecimal;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.model.LicenceApplicationContext;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.service.LicensingConnector;

@Component(immediate = true, service = LicensingConnector.class)
public class CivicaAppConnector implements LicensingConnector {

	private static final Log LOG = LogFactoryUtil.getLog(CivicaAppConnector.class);

	@Reference
	private CivicaAppConfigurationService civicaAppConfigurationService;

	@Reference
	private CivicaAppRequestService civicaAppRequestService;

	@Override
	public String applyForNewLicence(long companyId, LicenceApplicationContext licenceApplicationContext) throws LicenceRenewalException {

		try {
			return civicaAppRequestService.typeUpdate(companyId, licenceApplicationContext);
		} catch (LicenceRenewalException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("CivicaAppConnector:Exception while applyForNewLicence:TypeUpdate.", e);
			throw new LicenceRenewalException("CivicaAppConnector:Exception while applyForNewLicence:TypeUpdate.", e);
		}
	}

	@Override
	public void changeAddress(String licenceId, AddressContext address) {
	}

	@Override
	public boolean enabled(long companyId) {
		try {
			return civicaAppConfigurationService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public String getFirstRegisteredDate(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return null;
	}

	@Override
	public LicenceDetails getLicenceDetailsByLicenceId(String licenceId) {
		// Not implemented
		return null;
	}

	@Override
	public LicenceDetails getLicenceDetailsByReference(String renewalReference) {
		// Not implemented
		return null;
	}

	@Override
	public String getVehicleRenewalDateTime(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return null;
	}

	@Override
	public String getVehicleRenewalLicenseLength(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return null;
	}

	@Override
	public int getVehicleRenewalLicenseLengthIntMonths(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return 0;
	}

	@Override
	public BigDecimal getVehicleRenewalPrice(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return null;
	}

	@Override
	public boolean hasLicenceExpired(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return false;
	}

	@Override
	public boolean isRenewalTooLate(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return false;
	}

	@Override
	public boolean isRenewalTooSoon(String licenceId) throws LicenceRenewalException {
		// Not implemented
		return false;
	}

	@Override
	public void renewLicence(String licenceId, int monthsToAdd, String paymentReference) throws LicenceRenewalException {
		// Not implemented
	}

}
