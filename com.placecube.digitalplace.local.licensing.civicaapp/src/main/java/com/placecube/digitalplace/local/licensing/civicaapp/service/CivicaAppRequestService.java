package com.placecube.digitalplace.local.licensing.civicaapp.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.civicaapp.axis.TypeUpdateULIxml;
import com.placecube.digitalplace.local.licensing.civicaapp.axis.WebInteropSoap;
import com.placecube.digitalplace.local.licensing.civicaapp.constants.LicenseApplicationContextKeys;
import com.placecube.digitalplace.local.licensing.civicaapp.util.MessageElementUtil;
import com.placecube.digitalplace.local.licensing.civicaapp.util.ObjectFactoryService;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.model.LicenceApplicationContext;
import com.placecube.digitalplace.local.licensing.util.ObjectUtil;

@Component(immediate = true, service = CivicaAppRequestService.class)
public class CivicaAppRequestService {

	private static final Log LOG = LogFactoryUtil.getLog(CivicaAppRequestService.class);

	@Reference
	private CivicaAppAxisService civicaAppAxisService;

	@Reference
	private MessageElementUtil messageElementUtil;
	
	@Reference
	private ObjectFactoryService objectFactoryService;

	public String applyForNewLicence(long companyId, LicenceApplicationContext licenceApplicationContext) throws LicenceRenewalException {

		try {
			return typeUpdate(companyId, licenceApplicationContext);
		} catch (LicenceRenewalException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("CivicaAppRequestService:Exception while applyForNewLicence:TypeUpdate.", e);
			throw new LicenceRenewalException("CivicaAppRequestService:Exception while applyForNewLicence:TypeUpdate.", e);
		}
	}

	public String typeUpdate(long companyId, LicenceApplicationContext licenceApplicationContext) throws Exception {

		TypeUpdateULIxml input = objectFactoryService.getTypeUpdateULIxml();

		if (Validator.isNull(licenceApplicationContext.getLicenceAddressContext())) {
			licenceApplicationContext.setLicenceAddressContext(ObjectUtil.getNewLicenceAddressContext());
		}

		input.set_any(messageElementUtil.buildTypeUpdateULIXmlMessageElements(licenceApplicationContext));

		return getAppService(companyId).typeUpdate(input, LicenseApplicationContextKeys.IMPORT_TYPE_CODE);
	}

	private WebInteropSoap getAppService(long companyId) throws Exception {
		return civicaAppAxisService.getCivicaAppAxisService(companyId);
	}

}
