package com.placecube.digitalplace.local.licensing.civicaapp.constants;

public final class JsonInputKeys {

	public static final String CLOSED_DATE = "closedDate";

	public static final String DESCRIPTION_1 = "description1";

	public static final String DESCRIPTION_2 = "description2";

	public static final String EMAIL = "email";

	public static final String FAMILY_NAME = "familyName";

	public static final String FEE_PAID = "feePaid";

	public static final String FIRST_NAME = "firstName";

	public static final String INV_OFFICER = "invOfficer";

	public static final String LA_REF = "laref";

	public static final String MOBILE = "mobile";

	public static final String NA_UPD_TYPE = "naUpdtype";

	public static final String NAME = "name";

	public static final String NUMBER = "number";

	public static final String OPEN_DATE = "opendate";

	public static final String ORGANISATION = "organisation";

	public static final String PREFERRED_DAY_TIME = "preferredDaytime";

	public static final String PREFERRED_TELEPHONE = "preferredTelephone";

	public static final String RECORD_ID = "recordId";

	public static final String RESPONSE_DATE = "responseDate";

	public static final String SUFFIX = "suffix";

	public static final String TELEPHONE = "telephone";

	public static final String TELEPHONE2 = "telephone2";

	public static final String TELEPHONE3 = "telephone3";

	public static final String TEXT = "text";

	public static final String TITLE = "title";

	public static final String UPD_TYPE = "updtype";

	public static final String VEHICLE_TYPE = "vehicleType";

	private JsonInputKeys() {
	}

}