/**
 * TypeUpdate.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.civicaapp.axis;

public class TypeUpdate  implements java.io.Serializable {
    private TypeUpdateULIxml ULIxml;

    private java.lang.String importType;

    public TypeUpdate() {
    }

    public TypeUpdate(
           TypeUpdateULIxml ULIxml,
           java.lang.String importType) {
           this.ULIxml = ULIxml;
           this.importType = importType;
    }


    /**
     * Gets the ULIxml value for this TypeUpdate.
     * 
     * @return ULIxml
     */
    public TypeUpdateULIxml getULIxml() {
        return ULIxml;
    }


    /**
     * Sets the ULIxml value for this TypeUpdate.
     * 
     * @param ULIxml
     */
    public void setULIxml(TypeUpdateULIxml ULIxml) {
        this.ULIxml = ULIxml;
    }


    /**
     * Gets the importType value for this TypeUpdate.
     * 
     * @return importType
     */
    public java.lang.String getImportType() {
        return importType;
    }


    /**
     * Sets the importType value for this TypeUpdate.
     * 
     * @param importType
     */
    public void setImportType(java.lang.String importType) {
        this.importType = importType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TypeUpdate)) return false;
        TypeUpdate other = (TypeUpdate) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ULIxml==null && other.getULIxml()==null) || 
             (this.ULIxml!=null &&
              this.ULIxml.equals(other.getULIxml()))) &&
            ((this.importType==null && other.getImportType()==null) || 
             (this.importType!=null &&
              this.importType.equals(other.getImportType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getULIxml() != null) {
            _hashCode += getULIxml().hashCode();
        }
        if (getImportType() != null) {
            _hashCode += getImportType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TypeUpdate.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", ">TypeUpdate"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ULIxml");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "ULIxml"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", ">>TypeUpdate>ULIxml"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "ImportType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
