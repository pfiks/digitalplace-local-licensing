/**
 * WebInteropSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.civicaapp.axis;

public interface WebInteropSoap extends java.rmi.Remote {

    /**
     * Returns XML in Flare ULI Schema format
     */
    public SearchResponseSearchResult search(SearchParameters searchParameters) throws java.rmi.RemoteException;

    /**
     * Accepts XML in the Flare ULI Schema Format.  Returns ID of
     * first inserted or updated record
     */
    public java.lang.String update(UpdateULIxml ULIxml) throws java.rmi.RemoteException;

    /**
     * Accepts XML in the Flare ULI Schema Format.  Returns ID of
     * first inserted or updated record
     */
    public java.lang.String typeUpdate(TypeUpdateULIxml ULIxml, java.lang.String importType) throws java.rmi.RemoteException;
}
