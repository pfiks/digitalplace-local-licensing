/**
 * SearchParameters.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.civicaapp.axis;

public class SearchParameters  implements java.io.Serializable {
    private java.lang.String db;

    private java.lang.String flareRef;

    private java.lang.String laRef;

    private java.lang.String qformat;

    private java.lang.String unit;

    private java.lang.String officer;

    private java.lang.String activitytype;

    private java.lang.String ward;

    private java.lang.String minodate;

    private java.lang.String maxodate;

    private java.lang.String mincdate;

    private java.lang.String maxcdate;

    private java.lang.String name;

    private java.lang.String address;

    private java.lang.String category;

    private java.lang.String group;

    private java.lang.String code;

    private java.lang.String authCode;

    private java.lang.String authCodeDataPath;

    private boolean isRecordAuthRequired;

    private java.lang.String rspfilename;

    private java.lang.String rspfilepath;

    public SearchParameters() {
    }

    public SearchParameters(
           java.lang.String db,
           java.lang.String flareRef,
           java.lang.String laRef,
           java.lang.String qformat,
           java.lang.String unit,
           java.lang.String officer,
           java.lang.String activitytype,
           java.lang.String ward,
           java.lang.String minodate,
           java.lang.String maxodate,
           java.lang.String mincdate,
           java.lang.String maxcdate,
           java.lang.String name,
           java.lang.String address,
           java.lang.String category,
           java.lang.String group,
           java.lang.String code,
           java.lang.String authCode,
           java.lang.String authCodeDataPath,
           boolean isRecordAuthRequired,
           java.lang.String rspfilename,
           java.lang.String rspfilepath) {
           this.db = db;
           this.flareRef = flareRef;
           this.laRef = laRef;
           this.qformat = qformat;
           this.unit = unit;
           this.officer = officer;
           this.activitytype = activitytype;
           this.ward = ward;
           this.minodate = minodate;
           this.maxodate = maxodate;
           this.mincdate = mincdate;
           this.maxcdate = maxcdate;
           this.name = name;
           this.address = address;
           this.category = category;
           this.group = group;
           this.code = code;
           this.authCode = authCode;
           this.authCodeDataPath = authCodeDataPath;
           this.isRecordAuthRequired = isRecordAuthRequired;
           this.rspfilename = rspfilename;
           this.rspfilepath = rspfilepath;
    }


    /**
     * Gets the db value for this SearchParameters.
     * 
     * @return db
     */
    public java.lang.String getDb() {
        return db;
    }


    /**
     * Sets the db value for this SearchParameters.
     * 
     * @param db
     */
    public void setDb(java.lang.String db) {
        this.db = db;
    }


    /**
     * Gets the flareRef value for this SearchParameters.
     * 
     * @return flareRef
     */
    public java.lang.String getFlareRef() {
        return flareRef;
    }


    /**
     * Sets the flareRef value for this SearchParameters.
     * 
     * @param flareRef
     */
    public void setFlareRef(java.lang.String flareRef) {
        this.flareRef = flareRef;
    }


    /**
     * Gets the laRef value for this SearchParameters.
     * 
     * @return laRef
     */
    public java.lang.String getLaRef() {
        return laRef;
    }


    /**
     * Sets the laRef value for this SearchParameters.
     * 
     * @param laRef
     */
    public void setLaRef(java.lang.String laRef) {
        this.laRef = laRef;
    }


    /**
     * Gets the qformat value for this SearchParameters.
     * 
     * @return qformat
     */
    public java.lang.String getQformat() {
        return qformat;
    }


    /**
     * Sets the qformat value for this SearchParameters.
     * 
     * @param qformat
     */
    public void setQformat(java.lang.String qformat) {
        this.qformat = qformat;
    }


    /**
     * Gets the unit value for this SearchParameters.
     * 
     * @return unit
     */
    public java.lang.String getUnit() {
        return unit;
    }


    /**
     * Sets the unit value for this SearchParameters.
     * 
     * @param unit
     */
    public void setUnit(java.lang.String unit) {
        this.unit = unit;
    }


    /**
     * Gets the officer value for this SearchParameters.
     * 
     * @return officer
     */
    public java.lang.String getOfficer() {
        return officer;
    }


    /**
     * Sets the officer value for this SearchParameters.
     * 
     * @param officer
     */
    public void setOfficer(java.lang.String officer) {
        this.officer = officer;
    }


    /**
     * Gets the activitytype value for this SearchParameters.
     * 
     * @return activitytype
     */
    public java.lang.String getActivitytype() {
        return activitytype;
    }


    /**
     * Sets the activitytype value for this SearchParameters.
     * 
     * @param activitytype
     */
    public void setActivitytype(java.lang.String activitytype) {
        this.activitytype = activitytype;
    }


    /**
     * Gets the ward value for this SearchParameters.
     * 
     * @return ward
     */
    public java.lang.String getWard() {
        return ward;
    }


    /**
     * Sets the ward value for this SearchParameters.
     * 
     * @param ward
     */
    public void setWard(java.lang.String ward) {
        this.ward = ward;
    }


    /**
     * Gets the minodate value for this SearchParameters.
     * 
     * @return minodate
     */
    public java.lang.String getMinodate() {
        return minodate;
    }


    /**
     * Sets the minodate value for this SearchParameters.
     * 
     * @param minodate
     */
    public void setMinodate(java.lang.String minodate) {
        this.minodate = minodate;
    }


    /**
     * Gets the maxodate value for this SearchParameters.
     * 
     * @return maxodate
     */
    public java.lang.String getMaxodate() {
        return maxodate;
    }


    /**
     * Sets the maxodate value for this SearchParameters.
     * 
     * @param maxodate
     */
    public void setMaxodate(java.lang.String maxodate) {
        this.maxodate = maxodate;
    }


    /**
     * Gets the mincdate value for this SearchParameters.
     * 
     * @return mincdate
     */
    public java.lang.String getMincdate() {
        return mincdate;
    }


    /**
     * Sets the mincdate value for this SearchParameters.
     * 
     * @param mincdate
     */
    public void setMincdate(java.lang.String mincdate) {
        this.mincdate = mincdate;
    }


    /**
     * Gets the maxcdate value for this SearchParameters.
     * 
     * @return maxcdate
     */
    public java.lang.String getMaxcdate() {
        return maxcdate;
    }


    /**
     * Sets the maxcdate value for this SearchParameters.
     * 
     * @param maxcdate
     */
    public void setMaxcdate(java.lang.String maxcdate) {
        this.maxcdate = maxcdate;
    }


    /**
     * Gets the name value for this SearchParameters.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this SearchParameters.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the address value for this SearchParameters.
     * 
     * @return address
     */
    public java.lang.String getAddress() {
        return address;
    }


    /**
     * Sets the address value for this SearchParameters.
     * 
     * @param address
     */
    public void setAddress(java.lang.String address) {
        this.address = address;
    }


    /**
     * Gets the category value for this SearchParameters.
     * 
     * @return category
     */
    public java.lang.String getCategory() {
        return category;
    }


    /**
     * Sets the category value for this SearchParameters.
     * 
     * @param category
     */
    public void setCategory(java.lang.String category) {
        this.category = category;
    }


    /**
     * Gets the group value for this SearchParameters.
     * 
     * @return group
     */
    public java.lang.String getGroup() {
        return group;
    }


    /**
     * Sets the group value for this SearchParameters.
     * 
     * @param group
     */
    public void setGroup(java.lang.String group) {
        this.group = group;
    }


    /**
     * Gets the code value for this SearchParameters.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this SearchParameters.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the authCode value for this SearchParameters.
     * 
     * @return authCode
     */
    public java.lang.String getAuthCode() {
        return authCode;
    }


    /**
     * Sets the authCode value for this SearchParameters.
     * 
     * @param authCode
     */
    public void setAuthCode(java.lang.String authCode) {
        this.authCode = authCode;
    }


    /**
     * Gets the authCodeDataPath value for this SearchParameters.
     * 
     * @return authCodeDataPath
     */
    public java.lang.String getAuthCodeDataPath() {
        return authCodeDataPath;
    }


    /**
     * Sets the authCodeDataPath value for this SearchParameters.
     * 
     * @param authCodeDataPath
     */
    public void setAuthCodeDataPath(java.lang.String authCodeDataPath) {
        this.authCodeDataPath = authCodeDataPath;
    }


    /**
     * Gets the isRecordAuthRequired value for this SearchParameters.
     * 
     * @return isRecordAuthRequired
     */
    public boolean isIsRecordAuthRequired() {
        return isRecordAuthRequired;
    }


    /**
     * Sets the isRecordAuthRequired value for this SearchParameters.
     * 
     * @param isRecordAuthRequired
     */
    public void setIsRecordAuthRequired(boolean isRecordAuthRequired) {
        this.isRecordAuthRequired = isRecordAuthRequired;
    }


    /**
     * Gets the rspfilename value for this SearchParameters.
     * 
     * @return rspfilename
     */
    public java.lang.String getRspfilename() {
        return rspfilename;
    }


    /**
     * Sets the rspfilename value for this SearchParameters.
     * 
     * @param rspfilename
     */
    public void setRspfilename(java.lang.String rspfilename) {
        this.rspfilename = rspfilename;
    }


    /**
     * Gets the rspfilepath value for this SearchParameters.
     * 
     * @return rspfilepath
     */
    public java.lang.String getRspfilepath() {
        return rspfilepath;
    }


    /**
     * Sets the rspfilepath value for this SearchParameters.
     * 
     * @param rspfilepath
     */
    public void setRspfilepath(java.lang.String rspfilepath) {
        this.rspfilepath = rspfilepath;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SearchParameters)) return false;
        SearchParameters other = (SearchParameters) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.db==null && other.getDb()==null) || 
             (this.db!=null &&
              this.db.equals(other.getDb()))) &&
            ((this.flareRef==null && other.getFlareRef()==null) || 
             (this.flareRef!=null &&
              this.flareRef.equals(other.getFlareRef()))) &&
            ((this.laRef==null && other.getLaRef()==null) || 
             (this.laRef!=null &&
              this.laRef.equals(other.getLaRef()))) &&
            ((this.qformat==null && other.getQformat()==null) || 
             (this.qformat!=null &&
              this.qformat.equals(other.getQformat()))) &&
            ((this.unit==null && other.getUnit()==null) || 
             (this.unit!=null &&
              this.unit.equals(other.getUnit()))) &&
            ((this.officer==null && other.getOfficer()==null) || 
             (this.officer!=null &&
              this.officer.equals(other.getOfficer()))) &&
            ((this.activitytype==null && other.getActivitytype()==null) || 
             (this.activitytype!=null &&
              this.activitytype.equals(other.getActivitytype()))) &&
            ((this.ward==null && other.getWard()==null) || 
             (this.ward!=null &&
              this.ward.equals(other.getWard()))) &&
            ((this.minodate==null && other.getMinodate()==null) || 
             (this.minodate!=null &&
              this.minodate.equals(other.getMinodate()))) &&
            ((this.maxodate==null && other.getMaxodate()==null) || 
             (this.maxodate!=null &&
              this.maxodate.equals(other.getMaxodate()))) &&
            ((this.mincdate==null && other.getMincdate()==null) || 
             (this.mincdate!=null &&
              this.mincdate.equals(other.getMincdate()))) &&
            ((this.maxcdate==null && other.getMaxcdate()==null) || 
             (this.maxcdate!=null &&
              this.maxcdate.equals(other.getMaxcdate()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.address==null && other.getAddress()==null) || 
             (this.address!=null &&
              this.address.equals(other.getAddress()))) &&
            ((this.category==null && other.getCategory()==null) || 
             (this.category!=null &&
              this.category.equals(other.getCategory()))) &&
            ((this.group==null && other.getGroup()==null) || 
             (this.group!=null &&
              this.group.equals(other.getGroup()))) &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.authCode==null && other.getAuthCode()==null) || 
             (this.authCode!=null &&
              this.authCode.equals(other.getAuthCode()))) &&
            ((this.authCodeDataPath==null && other.getAuthCodeDataPath()==null) || 
             (this.authCodeDataPath!=null &&
              this.authCodeDataPath.equals(other.getAuthCodeDataPath()))) &&
            this.isRecordAuthRequired == other.isIsRecordAuthRequired() &&
            ((this.rspfilename==null && other.getRspfilename()==null) || 
             (this.rspfilename!=null &&
              this.rspfilename.equals(other.getRspfilename()))) &&
            ((this.rspfilepath==null && other.getRspfilepath()==null) || 
             (this.rspfilepath!=null &&
              this.rspfilepath.equals(other.getRspfilepath())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDb() != null) {
            _hashCode += getDb().hashCode();
        }
        if (getFlareRef() != null) {
            _hashCode += getFlareRef().hashCode();
        }
        if (getLaRef() != null) {
            _hashCode += getLaRef().hashCode();
        }
        if (getQformat() != null) {
            _hashCode += getQformat().hashCode();
        }
        if (getUnit() != null) {
            _hashCode += getUnit().hashCode();
        }
        if (getOfficer() != null) {
            _hashCode += getOfficer().hashCode();
        }
        if (getActivitytype() != null) {
            _hashCode += getActivitytype().hashCode();
        }
        if (getWard() != null) {
            _hashCode += getWard().hashCode();
        }
        if (getMinodate() != null) {
            _hashCode += getMinodate().hashCode();
        }
        if (getMaxodate() != null) {
            _hashCode += getMaxodate().hashCode();
        }
        if (getMincdate() != null) {
            _hashCode += getMincdate().hashCode();
        }
        if (getMaxcdate() != null) {
            _hashCode += getMaxcdate().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getAddress() != null) {
            _hashCode += getAddress().hashCode();
        }
        if (getCategory() != null) {
            _hashCode += getCategory().hashCode();
        }
        if (getGroup() != null) {
            _hashCode += getGroup().hashCode();
        }
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getAuthCode() != null) {
            _hashCode += getAuthCode().hashCode();
        }
        if (getAuthCodeDataPath() != null) {
            _hashCode += getAuthCodeDataPath().hashCode();
        }
        _hashCode += (isIsRecordAuthRequired() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getRspfilename() != null) {
            _hashCode += getRspfilename().hashCode();
        }
        if (getRspfilepath() != null) {
            _hashCode += getRspfilepath().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SearchParameters.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "SearchParameters"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("db");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Db"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flareRef");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "FlareRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("laRef");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "LaRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qformat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Qformat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Unit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("officer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Officer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activitytype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Activitytype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ward");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Ward"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("minodate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Minodate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxodate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Maxodate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mincdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Mincdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxcdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Maxcdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Category"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("group");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Group"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "AuthCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authCodeDataPath");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "AuthCodeDataPath"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isRecordAuthRequired");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "IsRecordAuthRequired"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspfilename");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Rspfilename"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspfilepath");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "Rspfilepath"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
