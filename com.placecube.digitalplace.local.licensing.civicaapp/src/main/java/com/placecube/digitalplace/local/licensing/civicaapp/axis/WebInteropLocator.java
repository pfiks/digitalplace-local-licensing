/**
 * WebInteropLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.licensing.civicaapp.axis;

public class WebInteropLocator extends org.apache.axis.client.Service implements WebInterop {

/**
 * Flare for Windows Interoperability Web Service
 */

    public WebInteropLocator() {
    }


    public WebInteropLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WebInteropLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WebInteropSoap
    private java.lang.String WebInteropSoap_address = "http://ntappgistest/appuniwebservice/WebInterop.asmx";

    public java.lang.String getWebInteropSoapAddress() {
        return WebInteropSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WebInteropSoapWSDDServiceName = "WebInteropSoap";

    public java.lang.String getWebInteropSoapWSDDServiceName() {
        return WebInteropSoapWSDDServiceName;
    }

    public void setWebInteropSoapWSDDServiceName(java.lang.String name) {
        WebInteropSoapWSDDServiceName = name;
    }

    public WebInteropSoap getWebInteropSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WebInteropSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWebInteropSoap(endpoint);
    }

    public WebInteropSoap getWebInteropSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            WebInteropSoapStub _stub = new WebInteropSoapStub(portAddress, this);
            _stub.setPortName(getWebInteropSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWebInteropSoapEndpointAddress(java.lang.String address) {
        WebInteropSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (WebInteropSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                WebInteropSoapStub _stub = new WebInteropSoapStub(new java.net.URL(WebInteropSoap_address), this);
                _stub.setPortName(getWebInteropSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WebInteropSoap".equals(inputPortName)) {
            return getWebInteropSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "WebInterop");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.flare.co.uk/webservices/FFW/", "WebInteropSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WebInteropSoap".equals(portName)) {
            setWebInteropSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
