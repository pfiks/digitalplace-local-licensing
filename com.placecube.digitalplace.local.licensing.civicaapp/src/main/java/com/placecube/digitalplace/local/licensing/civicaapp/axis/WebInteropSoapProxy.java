package com.placecube.digitalplace.local.licensing.civicaapp.axis;

public class WebInteropSoapProxy implements WebInteropSoap {
  private String _endpoint = null;
  private WebInteropSoap webInteropSoap = null;
  
  public WebInteropSoapProxy() {
    _initWebInteropSoapProxy();
  }
  
  public WebInteropSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initWebInteropSoapProxy();
  }
  
  private void _initWebInteropSoapProxy() {
    try {
      webInteropSoap = (new WebInteropLocator()).getWebInteropSoap();
      if (webInteropSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)webInteropSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)webInteropSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (webInteropSoap != null)
      ((javax.xml.rpc.Stub)webInteropSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public WebInteropSoap getWebInteropSoap() {
    if (webInteropSoap == null)
      _initWebInteropSoapProxy();
    return webInteropSoap;
  }
  
  public SearchResponseSearchResult search(SearchParameters searchParameters) throws java.rmi.RemoteException{
    if (webInteropSoap == null)
      _initWebInteropSoapProxy();
    return webInteropSoap.search(searchParameters);
  }
  
  public java.lang.String update(UpdateULIxml ULIxml) throws java.rmi.RemoteException{
    if (webInteropSoap == null)
      _initWebInteropSoapProxy();
    return webInteropSoap.update(ULIxml);
  }
  
  public java.lang.String typeUpdate(TypeUpdateULIxml ULIxml, java.lang.String importType) throws java.rmi.RemoteException{
    if (webInteropSoap == null)
      _initWebInteropSoapProxy();
    return webInteropSoap.typeUpdate(ULIxml, importType);
  }
  
  
}