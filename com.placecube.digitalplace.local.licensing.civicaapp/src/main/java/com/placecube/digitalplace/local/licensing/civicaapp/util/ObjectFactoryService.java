package com.placecube.digitalplace.local.licensing.civicaapp.util;

import org.apache.axis.message.MessageElement;
import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.local.licensing.civicaapp.axis.TypeUpdateULIxml;
import com.placecube.digitalplace.local.licensing.civicaapp.axis.WebInteropLocator;

@Component(immediate = true, service = ObjectFactoryService.class)
public class ObjectFactoryService {

	public TypeUpdateULIxml getTypeUpdateULIxml() {
		return new TypeUpdateULIxml();
	}

	public MessageElement getNewMessageElement() {
		return new MessageElement();
	}
	
	public WebInteropLocator getServiceLocator() {
		return new WebInteropLocator();
	}

}
