package com.placecube.digitalplace.local.licensing.civicaapp.application;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.placecube.digitalplace.local.licensing.civicaapp.service.CivicaAppRequestService;
import com.placecube.digitalplace.local.licensing.civicaapp.util.AppLicenceApplicationContextUtil;

@Component(immediate = true, property = { JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/licensing/submission/app", JaxrsWhiteboardConstants.JAX_RS_NAME + "=LicensingAppSubmission.Rest",
		"oauth2.scopechecker.type=none", "auth.verifier.guest.allowed=false", "liferay.access.control.disable=true" }, service = Application.class)
public class CivicaAppRESTApplication extends Application {

	@Reference
	private CivicaAppRequestService civicaAppRequestService;

	@POST
	@Path("/new-license-application")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response applyForNewLicence(InputStream inputStream, @Context HttpServletRequest request) throws Exception {

		String jsonRawInput = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines().collect(Collectors.joining("\n"));

		civicaAppRequestService.applyForNewLicence(getCompanyId(request), AppLicenceApplicationContextUtil.getLicenceApplicationContext(jsonRawInput));

		return Response.ok().build();
	}

	private long getCompanyId(HttpServletRequest request) {
		return (Long) request.getAttribute("COMPANY_ID");
	}

	@Override
	public Set<Object> getSingletons() {
		return Collections.<Object>singleton(this);
	}

}