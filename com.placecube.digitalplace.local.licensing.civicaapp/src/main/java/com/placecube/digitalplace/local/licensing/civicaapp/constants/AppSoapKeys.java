package com.placecube.digitalplace.local.licensing.civicaapp.constants;

public final class AppSoapKeys {

	public static final String ACTION = "Action";

	public static final String ACTIVITY = "Activity";

	public static final String ACTIVITY_RECORD = "ActivityRecord";

	public static final String ACTIVITY_TEXT = "ActivityText";

	public static final String ADDR_LINE1 = "AddrLine1";

	public static final String ADDR_LINE2 = "AddrLine2";

	public static final String ADDR_LINE3 = "AddrLine3";

	public static final String ADDR_LINE4 = "AddrLine4";

	public static final String ADDR_LINE5 = "AddrLine5";

	public static final String ADDRESS = "Address";

	public static final String AH_ACTION = "AhAction";

	public static final String CLOSED_DATE = "ClosedDate";

	public static final String DATA = "Data";

	public static final String DESC1 = "Desc1";

	public static final String DESC2 = "Desc2";

	public static final String EMAIL = "Email";

	public static final String FEE = "Fee";

	public static final String FIND_BY = "FindBy";

	public static final String FLARE_DB = "FlareDB";

	public static final String FORE_NAMES = "Forenames";

	public static final String HEAD = "Head";

	public static final String IMPORT_CONTROL = "ImportControl";

	public static final String IMPORT_FILE = "ImportFile";

	public static final String IMPORT_RECORD = "ImportRecord";

	public static final String IMPORT_TYPE = "ImportType";

	public static final String INV_OFFICER = "InvOfficer";

	public static final String LA_REF = "LAref";

	public static final String LOOSE_ADDRESS = "LooseAddress";

	public static final String MISC_ACT = "Miscact";

	public static final String MISC_ACTIVITY = "MiscActivity";

	public static final String MOBILE = "Mobile";

	public static final String NA_UPD_TYPE = "NAUpdtype";

	public static final String NAM_ADD = "NamAdd";

	public static final String NAME = "Name";

	public static final String NUMBER = "Number";

	public static final String OPEN_DATE = "OpenDate";

	public static final String ORGANISATION = "Organisation";

	public static final String POST_CODE = "PostCode";

	public static final String PREFERRED_DATE_TIME = "PreferredDaytime";

	public static final String PREFERRED_TELEPHONE = "PreferredTelephone";

	public static final String RECORD_ID = "RecordID ";

	public static final String RESPONSE_DATE = "ResponseDate";

	public static final String SPLITE_NAME = "SplitName";

	public static final String SUFFIX = "Suffix ";

	public static final String SUR_NAME = "Surname";

	public static final String TELEPHONE = "Telephone";

	public static final String TELEPHONE2 = "Telephone2";

	public static final String TELEPHONE3 = "Telephone3";

	public static final String TEXT = "Text";

	public static final String TITLE = "Title";

	public static final String TYPE = "Type";

	public static final String TYPE_CODE = "TypeCode";

	public static final String UNIT = "Unit";

	public static final String UPD_TYPE = "Updtype";

	public static final String UPDATE_MODE = "UpdateMode";

	public static final String UPRN = "UPRN";

	public static final String XMLNS_BS7666_URL = "http://www.govtalk.gov.uk/people/bs7666";

	public static final String XMLNS_URL = "http://www.flare.co.uk";

	public static final String XMLNS_xsi_URL = "http://www.w3.org/2001/XMLSchema-instance";

	private AppSoapKeys() {
	}

}