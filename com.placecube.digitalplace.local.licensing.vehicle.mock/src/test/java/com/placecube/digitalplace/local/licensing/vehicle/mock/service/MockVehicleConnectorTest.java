package com.placecube.digitalplace.local.licensing.vehicle.mock.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.local.licensing.vehicle.api.model.VehicleDetails;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ JSONFactoryUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class MockVehicleConnectorTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private MockVehicleConfigurationService mockVehicleConfigurationService;

	@InjectMocks
	private MockVehicleConnector mockVehicleConnector;

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockVehicleConfigurationService.isEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = mockVehicleConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockVehicleConfigurationService.isEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = mockVehicleConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getVehicleDetails_WhenNoErrors_ThenReturnsVehicleDetails() {

		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJSONObject);

		Optional<VehicleDetails> vehicleDetails = mockVehicleConnector.getVehicleDetails(COMPANY_ID, "FV11 BWU");

		assertTrue(vehicleDetails.isPresent());
	}

	@Test
	public void getVehicleDetails_WhenRegistrationIsEmpty_ThenReturnsEmptyOptional() {

		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJSONObject);

		Optional<VehicleDetails> vehicleDetails = mockVehicleConnector.getVehicleDetails(COMPANY_ID, "");

		assertFalse(vehicleDetails.isPresent());
	}

	@Test
	public void getVehicleDetails_WhenRegistrationStartesWithE_ThenReturnsEmptyOptional() {

		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJSONObject);

		Optional<VehicleDetails> vehicleDetails = mockVehicleConnector.getVehicleDetails(COMPANY_ID, "E123 7AH");

		assertFalse(vehicleDetails.isPresent());
	}

	@Before
	public void setUp() {
		mockStatic(JSONFactoryUtil.class);
	}
}
