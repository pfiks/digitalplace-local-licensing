package com.placecube.digitalplace.local.licensing.vehicle.mock.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.licensing.vehicle.mock.configuration.MockVehicleConfiguration;

@Component(immediate = true, service = MockVehicleConfigurationService.class)
public class MockVehicleConfigurationService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public boolean isEnabled(long companyId) throws ConfigurationException {
		MockVehicleConfiguration configuration = configurationProvider.getCompanyConfiguration(MockVehicleConfiguration.class, companyId);
		return configuration.enabled();
	}

}
