package com.placecube.digitalplace.local.licensing.vehicle.mock.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.licensing.vehicle.mock.configuration.MockVehicleConfiguration", localization = "content/Language", name = "vehicle-mock")
public interface MockVehicleConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

}
