package com.placecube.digitalplace.local.licensing.vehicle.mock.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.licensing.vehicle.api.model.VehicleDetails;
import com.placecube.digitalplace.local.licensing.vehicle.api.service.VehicleConnector;

@Component(immediate = true, service = VehicleConnector.class)
public class MockVehicleConnector implements VehicleConnector {

	private static final Log LOG = LogFactoryUtil.getLog(MockVehicleConnector.class);

	@Reference
	private MockVehicleConfigurationService mockVehicleConfigurationService;

	@Override
	public boolean enabled(long companyId) {
		try {
			return mockVehicleConfigurationService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public Optional<VehicleDetails> getVehicleDetails(long companyId, String registrationNumber) {

		if (Validator.isNull(registrationNumber) || registrationNumber.startsWith("E")) {

			return Optional.empty();
		}

		VehicleDetails vehicleDetails = new VehicleDetails();
		vehicleDetails.setRegistrationNumber(registrationNumber);
		vehicleDetails.setCo2Emissions(105);
		vehicleDetails.setEngineCapacity(1598);
		vehicleDetails.setMarkedForExport(false);
		vehicleDetails.setFuelType("DIESEL");
		vehicleDetails.setMotStatus("Valid");
		vehicleDetails.setRevenueWeight(1590);
		vehicleDetails.setColour("BLUE");
		vehicleDetails.setMake("SKODA");
		vehicleDetails.setTypeApproval("M1");
		vehicleDetails.setYearOfManufacture(2012);
		vehicleDetails.setTaxDueDate("2022-07-09");
		vehicleDetails.setTaxStatus("Untaxed");
		vehicleDetails.setDateOfLastV5CIssued("2021-12-09");
		vehicleDetails.setMotExpiryDate("2022-12-09");
		vehicleDetails.setWheelplan("2 AXLE RIGID BODY");
		vehicleDetails.setMonthOfFirstRegistration("2012-05");

		return Optional.of(vehicleDetails);

	}

}
