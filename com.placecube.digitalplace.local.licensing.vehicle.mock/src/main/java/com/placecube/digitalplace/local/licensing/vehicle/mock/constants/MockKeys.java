package com.placecube.digitalplace.local.licensing.vehicle.mock.constants;

public final class MockKeys {

	public static final String CO2EMISSIONS = "co2Emissions";

	public static final String CODE = "code";

	public static final String COLOUR = "colour";

	public static final String DATE_OF_LAST_V5C_ISSUED = "dateOfLastV5CIssued";

	public static final String DETAILS = "details";

	public static final String ENGINE_CAPACITY = "engineCapacity";

	public static final String FUEL_TYPE = "fuelType";

	public static final String MAKE = "make";

	public static final String MARKED_FOR_EXPORT = "markedForExport";

	public static final String MONTH_OF_FIRST_REGISTRATION = "monthOfFirstRegistration";

	public static final String MOT_EXPIRY_DATE = "motExpiryDate";

	public static final String MOT_STATUS = "motStatus";

	public static final String REGISTRATION_NUMBER = "registrationNumber";

	public static final String REVENUE_WEIGHT = "revenueWeight";

	public static final String STATUS = "status";

	public static final String TAX_DUE_DATE = "taxDueDate";

	public static final String TAX_STATUS = "taxStatus";

	public static final String TITLE = "title";

	public static final String TYPE_APPROVAL = "typeApproval";

	public static final String WHEEL_PLAN = "wheelplan";

	public static final String YEAR_OF_MANUFACTURE = "yearOfManufacture";

	private MockKeys() {
	}

}