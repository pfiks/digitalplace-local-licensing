package com.placecube.digitalplace.local.licensing.mock.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.mock.configuration.MockCompanyConfiguration;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.model.LicenceStatus;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class MockLicensingConnectorTest {

	private static final long COMPANY_ID = 10;
	private static final String MOCK_LICENCE_ENDING = "-license-TXPHD";
	private static final String MOCK_PHONE = "555444333";

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private MockCompanyConfiguration mockMockCompanyConfiguration;

	@InjectMocks
	private MockLicensingConnector mockLicensingConnector;

	@Before
	public void setup() {
		initMocks(this);
	}

	@Test
	public void enabled_WhenExceptionRetrievingConfiguration_ThenReturnFalse() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(MockCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = mockLicensingConnector.enabled(COMPANY_ID);
		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabledOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(MockCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockMockCompanyConfiguration);
		when(mockMockCompanyConfiguration.enabled()).thenReturn(expected);

		boolean result = mockLicensingConnector.enabled(COMPANY_ID);

		assertEquals(expected, result);
	}

	@Test
	public void getLicenceDetailsByReference_validSearch_returnsObject() throws Exception {
		String licenceString = LicenceStatus.VALID.getStatus();
		LicenceDetails licenceDetails = mockLicensingConnector.getLicenceDetailsByReference(licenceString);
		assertEquals(licenceString + MOCK_LICENCE_ENDING, licenceDetails.getTaxiLicenceNumber());
		assertEquals(MOCK_PHONE, licenceDetails.getMobile());
	}

	@Test
	public void getLicenceDetailsByReference_vehicleSearch_returnsObject() throws Exception {
		String licenceString = LicenceStatus.VALID.getStatus();
		VehicleLicenceDetails licenceDetails = (VehicleLicenceDetails) mockLicensingConnector.getLicenceDetailsByReference(licenceString + "-vehicle");
		assertEquals("Red", licenceDetails.getVehicle().getColour());
		assertEquals(MOCK_PHONE, licenceDetails.getMobile());
	}

	@Test
	public void getLicenceDetailsByReference_invalidSearch_returnsNull() throws Exception {
		LicenceDetails licenceDetails = mockLicensingConnector.getLicenceDetailsByReference("invalidLicense");
		assertNull(licenceDetails);
	}

	@Test
	public void changeAddress_validAddress_updateSuccess() throws Exception {

		AddressContext newAddress = new AddressContext("", "4 Mock Flat", "19 Mock Ave", "", "MockFord", "MK4 5CK");
		String licenceString = LicenceStatus.VALID.getStatus();
		String targetLicenceId = mockLicensingConnector.getLicenceDetailsByReference(licenceString).getLicenceId();

		mockLicensingConnector.changeAddress(targetLicenceId, newAddress);

		AddressContext updatedaddress = mockLicensingConnector.getLicenceDetailsByReference(licenceString).getAddress();
		assertEquals(newAddress, updatedaddress);
	}

	@Test
	public void changeAddress_nulladdress_noUpdate() throws Exception {
		String licenceString = LicenceStatus.VALID.getStatus();
		String targetLicenceId = mockLicensingConnector.getLicenceDetailsByReference(licenceString).getLicenceId();

		mockLicensingConnector.changeAddress(targetLicenceId, null);

		AddressContext updatedaddress = mockLicensingConnector.getLicenceDetailsByReference(licenceString).getAddress();
		assertNotNull(updatedaddress);
	}

	@Test(expected = LicenceRenewalException.class)
	public void renewLicence_invalidtargetLicenceId_throwException() throws Exception {
		mockLicensingConnector.renewLicence("SomeId", 1, "ref");
	}
}
