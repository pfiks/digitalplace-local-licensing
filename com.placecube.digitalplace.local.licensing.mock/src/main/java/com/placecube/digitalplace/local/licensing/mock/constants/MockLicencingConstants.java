package com.placecube.digitalplace.local.licensing.mock.constants;

public final class MockLicencingConstants {

	public static final long RENEWAL_THRESHOLD_IN_DAYS = (long) 2 * (3 * 365 + 364);

	public static final int RENEWAL_TOO_LATE_DAYS = 11;

	public static final int RENEWAL_TOO_SOON_DAYS = 42;

	public static final int VEHICLE_RENEWAL_TOO_LATE_DAYS = 14;

	public static final int VEHICLE_RENEWAL_TOO_SOON_DAYS = 28;

	private MockLicencingConstants() {
	}

}
