package com.placecube.digitalplace.local.licensing.mock.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.licensing.mock.configuration.MockCompanyConfiguration", localization = "content/Language", name = "licensing-mock")
public interface MockCompanyConfiguration {

    @Meta.AD(required = false, deflt = "false", name = "enabled")
    boolean enabled();
}
