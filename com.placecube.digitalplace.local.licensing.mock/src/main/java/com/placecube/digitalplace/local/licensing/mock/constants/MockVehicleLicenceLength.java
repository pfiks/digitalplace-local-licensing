package com.placecube.digitalplace.local.licensing.mock.constants;

import java.math.BigDecimal;

public enum MockVehicleLicenceLength {

	ONE_YEAR(BigDecimal.valueOf(187.00), 12),

	SIX_MONTH(BigDecimal.valueOf(93.50), 6);

	private final int months;
	private final BigDecimal price;

	private MockVehicleLicenceLength(BigDecimal price, int months) {
		this.price = price;
		this.months = months;
	}

	public int getMonths() {
		return months;
	}

	public BigDecimal getPrice() {
		return price;
	}

}
