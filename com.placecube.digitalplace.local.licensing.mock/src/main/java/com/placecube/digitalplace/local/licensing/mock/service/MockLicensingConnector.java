package com.placecube.digitalplace.local.licensing.mock.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.local.licensing.exception.LicenceRenewalException;
import com.placecube.digitalplace.local.licensing.mock.configuration.MockCompanyConfiguration;
import com.placecube.digitalplace.local.licensing.mock.constants.MockLicencingConstants;
import com.placecube.digitalplace.local.licensing.mock.constants.MockVehicleLicenceLength;
import com.placecube.digitalplace.local.licensing.model.InterestedParty;
import com.placecube.digitalplace.local.licensing.model.LicenceApplicationContext;
import com.placecube.digitalplace.local.licensing.model.LicenceDetails;
import com.placecube.digitalplace.local.licensing.model.LicenceStatus;
import com.placecube.digitalplace.local.licensing.model.Vehicle;
import com.placecube.digitalplace.local.licensing.model.VehicleLicenceDetails;
import com.placecube.digitalplace.local.licensing.service.LicensingConnector;

@Component(immediate = true, service = LicensingConnector.class)
public class MockLicensingConnector implements LicensingConnector {

	private static final AddressContext ADDRESS = new AddressContext("", "3 Mock Flat", "18 Mock Ave", "", "MockFord", "MK1 2CK");

	private static final Log LOG = LogFactoryUtil.getLog(MockLicensingConnector.class);

	private static final String RENEWAL_DATETIME_FORMAT = "dd MMMMM YYYY";

	@Reference
	private ConfigurationProvider configurationProvider;

	private List<LicenceDetails> licences;

	public MockLicensingConnector() {
		licences = new ArrayList<>();
		licences.add(createLicence("001", LicenceStatus.VALID));
		licences.add(createLicence("002", LicenceStatus.EXPIRED));
		licences.add(createLicence("003", LicenceStatus.RENEWAL_TOO_EARLY));
		licences.add(createLicence("004", LicenceStatus.RENEWAL_TOO_LATE));
		licences.add(createLicence("005", LicenceStatus.CHECKS_DUE));
		licences.add(createVehicleLicence("005", LicenceStatus.VALID));
		licences.add(createVehicleLicence("006", LicenceStatus.EXPIRED));
		licences.add(createVehicleLicence("007", LicenceStatus.RENEWAL_TOO_EARLY));
		licences.add(createVehicleLicence("008", LicenceStatus.RENEWAL_TOO_LATE));
		licences.add(createVehicleLicence("009", LicenceStatus.ERROR));
	}

	@Override
	public String applyForNewLicence(long companyId, LicenceApplicationContext licenceApplicationContext) throws LicenceRenewalException {
		boolean success = true;
		if (Validator.isNotNull(licenceApplicationContext.getRegistrationNumber())) {
			if (licenceApplicationContext.getRegistrationNumber().equalsIgnoreCase("error")) {
				success = false;
			}

			if (!success) {
				throw new LicenceRenewalException("Invalid license application details.");
			}
		}

		return String.valueOf(success);
	}

	@Override
	public void changeAddress(String licenceId, AddressContext address) {
		if (address == null) {
			return;
		}
		licences.stream().forEach(c -> {
			if (c.getLicenceId().equals(licenceId)) {
				c.setAddress(address);
			}
		});
	}

	@Override
	public boolean enabled(long companyId) {
		try {
			MockCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(MockCompanyConfiguration.class, companyId);
			return configuration.enabled();
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public String getFirstRegisteredDate(String licenceId) throws LicenceRenewalException {

		VehicleLicenceDetails licenceDetails = (VehicleLicenceDetails) licences.stream().filter(licence -> licence.getLicenceId().equals(licenceId) && licence instanceof VehicleLicenceDetails)
				.findFirst().orElseThrow(LicenceRenewalException::new);

		String firstRegisteredDateTime = null;

		Calendar firstRegisteredDate = GregorianCalendar.from(licenceDetails.getFirstRegistered().atStartOfDay(ZoneId.systemDefault()));
		LOG.debug("[getFirstRegisteredDate] firstRegisteredDate: " + firstRegisteredDate);

		if (firstRegisteredDate != null) {
			SimpleDateFormat sdf = new SimpleDateFormat(RENEWAL_DATETIME_FORMAT);
			firstRegisteredDateTime = sdf.format(firstRegisteredDate.getTime());
		} else {
			LOG.error("[getFirstRegisteredDate] First registered date is null");
		}

		return firstRegisteredDateTime;
	}

	@Override
	public LicenceDetails getLicenceDetailsByLicenceId(String licenceId) {
		Optional<LicenceDetails> licenceOption = licences.stream().filter(licence -> licence.getLicenceId().equals(licenceId)).findFirst();
		if (!licenceOption.isPresent()) {
			return null;
		}
		return licenceOption.get();
	}

	@Override
	public LicenceDetails getLicenceDetailsByReference(String renewalReference) {
		Optional<LicenceDetails> licenceOption = licences.stream().filter(c -> c.getTaxiLicenceNumber().startsWith(renewalReference)).findFirst();
		if (!licenceOption.isPresent()) {
			return null;
		}
		return licenceOption.get();
	}

	@Override
	public String getVehicleRenewalDateTime(String licenceId) throws LicenceRenewalException {
		String renewalDateTime = "";

		VehicleLicenceDetails licenceDetails = (VehicleLicenceDetails) licences.stream().filter(licence -> licence.getLicenceId().equals(licenceId) && licence instanceof VehicleLicenceDetails)
				.findFirst().orElseThrow(LicenceRenewalException::new);

		if (isOverRenewalThreshold(licenceDetails)) {
			renewalDateTime = getRenewalDateTime(licenceDetails, 1, 0);
		} else {
			renewalDateTime = getRenewalDateTime(licenceDetails, 0, 6);
		}

		return renewalDateTime;
	}

	@Override
	public String getVehicleRenewalLicenseLength(String licenceId) throws LicenceRenewalException {
		String renewalLicenseLength = MockVehicleLicenceLength.SIX_MONTH.name();
		VehicleLicenceDetails licenceDetails = (VehicleLicenceDetails) licences.stream().filter(licence -> licence.getLicenceId().equals(licenceId) && licence instanceof VehicleLicenceDetails)
				.findFirst().orElseThrow(LicenceRenewalException::new);

		if (isOverRenewalThreshold(licenceDetails)) {
			renewalLicenseLength = MockVehicleLicenceLength.ONE_YEAR.name();
		}

		return renewalLicenseLength;
	}

	@Override
	public int getVehicleRenewalLicenseLengthIntMonths(String licenceId) throws LicenceRenewalException {

		int renewalLicenseLength = MockVehicleLicenceLength.SIX_MONTH.getMonths();
		VehicleLicenceDetails licenceDetails = (VehicleLicenceDetails) licences.stream().filter(licence -> licence.getLicenceId().equals(licenceId) && licence instanceof VehicleLicenceDetails)
				.findFirst().orElseThrow(LicenceRenewalException::new);

		if (isOverRenewalThreshold(licenceDetails)) {
			renewalLicenseLength = MockVehicleLicenceLength.ONE_YEAR.getMonths();
		}

		return renewalLicenseLength;
	}

	@Override
	public BigDecimal getVehicleRenewalPrice(String licenceId) throws LicenceRenewalException {
		BigDecimal renewalPrice = null;
		VehicleLicenceDetails licenceDetails = (VehicleLicenceDetails) licences.stream().filter(licence -> licence.getLicenceId().equals(licenceId) && licence instanceof VehicleLicenceDetails)
				.findFirst().orElseThrow(LicenceRenewalException::new);

		if (isOverRenewalThreshold(licenceDetails)) {
			renewalPrice = MockVehicleLicenceLength.ONE_YEAR.getPrice();
		} else {
			renewalPrice = MockVehicleLicenceLength.SIX_MONTH.getPrice();
		}

		return renewalPrice;

	}

	@Override
	public boolean hasLicenceExpired(String licenceId) throws LicenceRenewalException {
		LicenceDetails licenceDetails = licences.stream().filter(licence -> licence.getLicenceId().equals(licenceId)).findFirst().orElseThrow(LicenceRenewalException::new);

		boolean licenceExpired = false;

		Calendar now = Calendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);

		Calendar renewalDate = GregorianCalendar.from(licenceDetails.getRenewalDate().atStartOfDay(ZoneId.systemDefault()));

		if (now.after(renewalDate)) {
			licenceExpired = true;
		}

		return licenceExpired;
	}

	@Override
	public boolean isRenewalTooLate(String licenceId) throws LicenceRenewalException {

		LicenceDetails licenceDetails = licences.stream().filter(licence -> licence.getLicenceId().equals(licenceId)).findFirst().orElseThrow(LicenceRenewalException::new);

		boolean isRenewalTooLate = false;

		Calendar now = Calendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);

		Date renewalDate = Date.from(licenceDetails.getRenewalDate().atStartOfDay(ZoneId.systemDefault()).toInstant());

		long daysBetween = DateUtil.getDaysBetween(now.getTime(), renewalDate);

		int tooLate;

		if (licenceDetails instanceof VehicleLicenceDetails) {
			tooLate = MockLicencingConstants.VEHICLE_RENEWAL_TOO_LATE_DAYS;
		} else {
			tooLate = MockLicencingConstants.RENEWAL_TOO_LATE_DAYS;
		}

		if (daysBetween < tooLate) {
			isRenewalTooLate = true;
		}

		return isRenewalTooLate;
	}

	@Override
	public boolean isRenewalTooSoon(String licenceId) throws LicenceRenewalException {

		LicenceDetails licenceDetails = licences.stream().filter(licence -> licence.getLicenceId().equals(licenceId)).findFirst().orElseThrow(LicenceRenewalException::new);

		boolean isRenewalTooSoon = false;

		Calendar now = Calendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);

		Date renewalDate = Date.from(licenceDetails.getRenewalDate().atStartOfDay(ZoneId.systemDefault()).toInstant());

		long daysBetween = DateUtil.getDaysBetween(now.getTime(), renewalDate);

		int tooSoon = 0;

		if (licenceDetails instanceof VehicleLicenceDetails) {
			tooSoon = MockLicencingConstants.VEHICLE_RENEWAL_TOO_SOON_DAYS;
		} else {
			tooSoon = MockLicencingConstants.RENEWAL_TOO_SOON_DAYS;
		}

		if (daysBetween > tooSoon) {
			isRenewalTooSoon = true;
		}

		return isRenewalTooSoon;
	}

	@Override
	public void renewLicence(String licenceId, int monthsToAdd, String paymentReference) throws LicenceRenewalException {
		boolean updateHappened = false;
		for (LicenceDetails licence : licences) {
			if (licence.getLicenceId().equals(licenceId)) {
				licence.setRenewalDate(licence.getRenewalDate().plusMonths(monthsToAdd));
				updateHappened = true;
			}
		}

		if (!updateHappened) {
			throw new LicenceRenewalException("Invalid Licence Id");
		}
	}

	private LicenceDetails createLicence(String id, LicenceStatus licenceStatus) {
		LicenceDetails licenceDetails = new LicenceDetails();
		licenceDetails.setLicenceId(id);
		licenceDetails.setStatus(licenceStatus);
		licenceDetails.setRenewalDate(LocalDate.of(LocalDate.now().getYear(), 12, 25));
		licenceDetails.setAddress(ADDRESS);
		licenceDetails.setFirstName("Mock_" + id);
		licenceDetails.setLastName("Guy_" + id);
		licenceDetails.setEmail("mock_" + id + "@email.com");
		licenceDetails.setMobile("555444333");
		licenceDetails.setTaxiLicenceNumber(licenceStatus.getStatus() + "-license-TXPHD");
		licenceDetails.setDrivingLicenceNumber("12-34567-ABCDE");
		return licenceDetails;
	}

	private LicenceDetails createVehicleLicence(String id, LicenceStatus licenceStatus) {

		VehicleLicenceDetails vehicleLicence = new VehicleLicenceDetails();
		vehicleLicence.setLicenceId(id);
		vehicleLicence.setStatus(licenceStatus);
		vehicleLicence.setRenewalDate(LocalDate.of(2020, 8, 6));
		vehicleLicence.setAddress(ADDRESS);
		vehicleLicence.setFirstName("Mock_" + id);
		vehicleLicence.setLastName("Guy_" + id);
		vehicleLicence.setEmail("mock_" + id + "@email.com");
		vehicleLicence.setMobile("555444333");
		vehicleLicence.setTaxiLicenceNumber(licenceStatus.getStatus() + "-vehicle-TXPHD");
		vehicleLicence.setFirstRegistered(LocalDate.of(1978, 11, 13));
		vehicleLicence.setVehicle(new Vehicle("AB12 CDE", "Tesla", "Model 3", "Red", 5));

		vehicleLicence.setRegisteredKeeper(new InterestedParty("Keeper", "Guy", new AddressContext("", "6 Mock Flat", "4 Mock Ave", "", "MockChester", "MK1 8CK")));

		List<InterestedParty> parties = new ArrayList<>();
		parties.add(new InterestedParty("Jhon", "Doe", new AddressContext("", "7 Mock Flat", "14 Mock Ave", "", "MockFord", "MK3 4CK")));

		vehicleLicence.setThirdParties(parties);

		return vehicleLicence;
	}

	private String getRenewalDateTime(VehicleLicenceDetails licenceDetails, int years, int months) {

		LOG.debug("[getRenewalDateTime] Getting renewal date for: " + years + " years, " + months + " months");

		Calendar renewalDate = GregorianCalendar.from(licenceDetails.getRenewalDate().atStartOfDay(ZoneId.systemDefault()));
		LOG.debug("[getRenewalDateTime] renewalDate: " + renewalDate);
		renewalDate.add(Calendar.YEAR, years);
		renewalDate.add(Calendar.MONTH, months);

		SimpleDateFormat sdf = new SimpleDateFormat(RENEWAL_DATETIME_FORMAT);

		String appointmentDateTime = sdf.format(renewalDate.getTime());
		LOG.debug("[getRenewalDateTime] appointmentDateTime: " + appointmentDateTime);

		return appointmentDateTime;
	}

	private boolean isOverRenewalThreshold(VehicleLicenceDetails licenceDetails) {
		boolean isOverRenewalThreshold = false;

		Date now = Calendar.getInstance().getTime();
		Date firstRegistered = Date.from(licenceDetails.getFirstRegistered().atStartOfDay(ZoneId.systemDefault()).toInstant());
		LOG.debug("[isOverRenewalThreshold] firstRegistered: " + firstRegistered);

		if (firstRegistered != null) {
			isOverRenewalThreshold = DateUtil.getDaysBetween(firstRegistered, now) > MockLicencingConstants.RENEWAL_THRESHOLD_IN_DAYS;
			LOG.debug("[isOverRenewalThreshold] isOverRenewalThreshold: " + isOverRenewalThreshold);
		}

		return isOverRenewalThreshold;
	}

}
