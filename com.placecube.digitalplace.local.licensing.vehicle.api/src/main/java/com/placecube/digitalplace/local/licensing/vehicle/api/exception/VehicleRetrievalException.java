package com.placecube.digitalplace.local.licensing.vehicle.api.exception;

import com.liferay.portal.kernel.exception.PortalException;

public class VehicleRetrievalException extends PortalException {

	private static final long serialVersionUID = 1L;

	public VehicleRetrievalException() {
		super();
	}

	public VehicleRetrievalException(String msg) {
		super(msg);
	}

	public VehicleRetrievalException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public VehicleRetrievalException(Throwable cause) {
		super(cause);
	}
}