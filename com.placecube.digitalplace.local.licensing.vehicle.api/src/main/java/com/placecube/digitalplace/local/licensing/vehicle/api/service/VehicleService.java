package com.placecube.digitalplace.local.licensing.vehicle.api.service;

import java.util.Optional;

import com.placecube.digitalplace.local.licensing.vehicle.api.model.VehicleDetails;

public interface VehicleService {

	/**
	 *
	 * @param companyId needed to load the right configuration from connector
	 * @param registrationNumber vehicle registration number to get details
	 * @return VehicleDetails vehicle details.
	 * @throws Exception
	 */
	Optional<VehicleDetails> getVehicleDetails(long companyId, String registrationNumber) throws Exception;

}
