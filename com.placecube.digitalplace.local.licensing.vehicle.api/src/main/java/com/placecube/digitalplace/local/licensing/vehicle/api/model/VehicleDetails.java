package com.placecube.digitalplace.local.licensing.vehicle.api.model;

public class VehicleDetails {

	public int co2Emissions;

	public String colour;

	public String dateOfLastV5CIssued;

	public int engineCapacity;

	public String fuelType;

	public String make;

	public boolean markedForExport;

	public String monthOfFirstRegistration;

	public String motExpiryDate;

	public String motStatus;

	public String registrationNumber;

	public int revenueWeight;

	public String taxDueDate;

	public String taxStatus;

	public String typeApproval;

	public String wheelplan;

	public int yearOfManufacture;

	public int getCo2Emissions() {
		return co2Emissions;
	}

	public String getColour() {
		return colour;
	}

	public String getDateOfLastV5CIssued() {
		return dateOfLastV5CIssued;
	}

	public int getEngineCapacity() {
		return engineCapacity;
	}

	public String getFuelType() {
		return fuelType;
	}

	public String getMake() {
		return make;
	}

	public boolean getMarkedForExport() {
		return markedForExport;
	}

	public String getMonthOfFirstRegistration() {
		return monthOfFirstRegistration;
	}

	public String getMotExpiryDate() {
		return motExpiryDate;
	}

	public String getMotStatus() {
		return motStatus;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public int getRevenueWeight() {
		return revenueWeight;
	}

	public String getTaxDueDate() {
		return taxDueDate;
	}

	public String getTaxStatus() {
		return taxStatus;
	}

	public String getTypeApproval() {
		return typeApproval;
	}

	public String getWheelplan() {
		return wheelplan;
	}

	public int getYearOfManufacture() {
		return yearOfManufacture;
	}

	public void setCo2Emissions(int co2Emissions) {
		this.co2Emissions = co2Emissions;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public void setDateOfLastV5CIssued(String dateOfLastV5CIssued) {
		this.dateOfLastV5CIssued = dateOfLastV5CIssued;
	}

	public void setEngineCapacity(int engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public void setMarkedForExport(boolean markedForExport) {
		this.markedForExport = markedForExport;
	}

	public void setMonthOfFirstRegistration(String monthOfFirstRegistration) {
		this.monthOfFirstRegistration = monthOfFirstRegistration;
	}

	public void setMotExpiryDate(String motExpiryDate) {
		this.motExpiryDate = motExpiryDate;
	}

	public void setMotStatus(String motStatus) {
		this.motStatus = motStatus;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public void setRevenueWeight(int revenueWeight) {
		this.revenueWeight = revenueWeight;
	}

	public void setTaxDueDate(String taxDueDate) {
		this.taxDueDate = taxDueDate;
	}

	public void setTaxStatus(String taxStatus) {
		this.taxStatus = taxStatus;
	}

	public void setTypeApproval(String typeApproval) {
		this.typeApproval = typeApproval;
	}

	public void setWheelplan(String wheelplan) {
		this.wheelplan = wheelplan;
	}

	public void setYearOfManufacture(int yearOfManufacture) {
		this.yearOfManufacture = yearOfManufacture;
	}

}
