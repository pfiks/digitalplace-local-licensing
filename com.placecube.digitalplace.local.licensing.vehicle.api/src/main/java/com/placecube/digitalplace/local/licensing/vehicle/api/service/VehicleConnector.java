package com.placecube.digitalplace.local.licensing.vehicle.api.service;

import java.util.Optional;

import com.placecube.digitalplace.local.licensing.vehicle.api.model.VehicleDetails;

public interface VehicleConnector {

	boolean enabled(long companyId);

	Optional<VehicleDetails> getVehicleDetails(long companyId, String registrationNumber) throws Exception;
}
